import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { useUserStore } from '/@/store/modules/user';
import { getProjectValidDeptTree } from '/@/api/project/dept';
//获取当前登录用户项目编码
const userStore = useUserStore();
const currentProjectCode = userStore.getUserCurrentProjectCode;

/**
 * 定义部门表头
 */
export const deptTableColumns: BasicColumn[] = [
  {
    title: '',
    dataIndex: '',
    align: 'center',
    width: 20,
  },
  {
    title: '部门名称',
    dataIndex: 'name',
    width: 100,
    align: 'left',
    slots: { customRender: 'name' },
  },
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    // fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    slots: { customRender: 'statusFlag' },
    width: 50,
  },
  {
    title: '排序号',
    dataIndex: 'order',
    width: 50,
  },
  {
    title: '备注',
    dataIndex: 'description',
    align: 'left',
    width: 100,
  },
];

/**
 * 定义新增/修改部门表单
 */
export const deptFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'pcode',
    label: '项目编码',
    component: 'Input',
    defaultValue: currentProjectCode,
    required: true,
    show: false,
  },
  {
    field: 'name',
    label: '部门名称',
    component: 'Input',
    required: true,
  },
  // {
  //   field: 'parentid',
  //   label: '上级部门',
  //   component: 'Select',
  //   componentProps: {},
  //   required: false,
  // },
  {
    field: 'parentid',
    label: '上级部门',
    component: 'TreeSelect',
    componentProps: {}
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
  },
  {
    field: 'order',
    label: '排序号',
    component: 'Input',
    required: false,
  },
  {
    field: 'description',
    label: '备注',
    component: 'Input',
    required: false,
  },
];
