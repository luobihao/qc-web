import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import Icon from '/@/components/Icon/src/Icon.vue';
import { useUserStore } from '/@/store/modules/user';
const userStore = useUserStore();
const currentProjectCode = userStore.getUserCurrentProjectCode;

/**
 * 定义菜单表头
 */
export const menuTableColumns: BasicColumn[] = [
  // 单独使用一列用于显示菜单层级的展开向下箭头
  {
    title: '',
    dataIndex: '',
    align: 'center',
    width: 25,
  },
  {
    title: '图标',
    dataIndex: 'icon',
    width: 50,
    customRender: ({ record }) => {
      return h(Icon, { icon: record.icon });
    },
  },
  {
    title: '菜单标题文字',
    dataIndex: 'title',
    width: 150,
    align: 'left',
    slots: { customRender: 'title' },
  },
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    // fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '路由URL',
    dataIndex: 'path',
    align: 'left',
    width: 150,
  },
  {
    title: '组件名称',
    dataIndex: 'name',
    width: 100,
    align: 'left',
  },
  {
    title: '页面路径',
    dataIndex: 'content',
    align: 'left',
    width: 150,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    slots: { customRender: 'statusFlag' },
    width: 80,
  },
  {
    title: '是否显示在菜单中',
    dataIndex: 'visiable',
    width: 80,
    slots: { customRender: 'visiableFlag' },
  },
  {
    title: '排序号',
    dataIndex: 'order',
    width: 80,
  },
];

/**
 * 定义查询条件表单
 */
export const queryConditionFormSchema: FormSchema[] = [
  {
    field: 'project',
    label: '项目编码',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: currentProjectCode,
    show: false,
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: '',
  },
];

/**
 * 定义新增/修改表单
 */
export const menuFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'pcode',
    label: '项目编码',
    component: 'Input',
    defaultValue: currentProjectCode,
    show: false,
    required: true,
  },
  {
    field: 'title',
    label: '菜单标题文字',
    component: 'Input',
    required: true,
    helpMessage: '显示在导航菜单中的文字。',
  },
  {
    field: 'path',
    label: '路由URL',
    component: 'Input',
    required: true,
    helpMessage: '跳转的路由url地址，后台自动添加上斜线/和上级菜单路由URL为前缀。',
  },
  {
    field: 'parentid',
    label: '上级菜单',
    component: 'Select',
    componentProps: {},
    required: false,
  },
  {
    field: 'icon',
    label: '图标',
    component: 'IconPicker',
    required: false,
  },
  {
    field: 'name',
    label: '组件名称',
    component: 'Input',
    required: false,
    helpMessage: ['需要当前菜单tab页面缓存时必须设置。',
      '设置时与vue中defineComponent的name完全相同。',
      '路由URL最后带有可变参数:id时不要设置，设置会导致界面中显示的不是预期数据。',
      '多个菜单中重复使用同一个组件名称会导致除最后一个外其他的菜单访问时均为404。'],
  },
  {
    field: 'content',
    label: '页面路径',
    component: 'Input',
    required: false,
    helpMessage: '为父级菜单时设置为LAYOUT使用统一布局模板，为末级功能菜单时设置为vue页面的完整路径（不包含.vue后缀名）。',
  },
  {
    field: 'flag',
    label: '状态',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '禁用', value: '5' },
      ],
    },
    defaultValue: '0',
  },
  {
    field: 'visiable',
    label: '是否显示在菜单中',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: true,
  },
  {
    field: 'order',
    label: '排序号',
    component: 'Input',
    required: false,
  },
];
