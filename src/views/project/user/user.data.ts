import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { useUserStore } from '/@/store/modules/user';
//获取当前登录用户项目编码
const userStore = useUserStore();
const currentProjectCode = userStore.getUserCurrentProjectCode;

/**
 * 定义用户表头
 */
export const userTableColumns: BasicColumn[] = [
  {
    title: '用户ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: true, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 80,
  },
  {
    title: '工号',
    dataIndex: 'code',
    width: 100,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 100,
  },
  {
    title: '职务',
    dataIndex: 'duty',
    width: 100,
  },
  {
    title: '短号',
    dataIndex: 'cornet',
    width: 100,
  },
  {
    title: '办公室/座机号码',
    dataIndex: 'tel',
    width: 100,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    slots: { customRender: 'statusFlag' },
    width: 80,
  },
  {
    title: '允许登录起始时间',
    dataIndex: 'allowtime',
    ifShow: false,
    width: 100,
  },
  {
    title: '过期时间',
    dataIndex: 'expiretime',
    ifShow: false,
    width: 100,
  },
  {
    title: '微信ID',
    dataIndex: 'weixinid',
    ifShow: false,
    width: 100,
  },
  {
    title: '排序号',
    dataIndex: 'order',
    width: 50,
  },
];

/**
 * 定义平台用户查询条件表单
 */
export const platformUserQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
  },
];

/**
 * 定义项目用户查询条件表单
 */
export const projectUserQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'project',
    label: '项目编码',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: currentProjectCode, //默认为当前登录用户项目编码
    show: false,
  },
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    helpMessage: '模糊匹配用户登录名、手机号、微信ID，精确匹配用户ID',
  },
];

/**
 * 定义新增/修改平台用户表单
 */
export const addPlatformUserFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '姓名',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'phone',
    label: '手机号',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'code',
    label: '登录账号',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'cornet',
    label: '短号',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'duty',
    label: '职务',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'tel',
    label: '办公室/座机号码',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'expiretime',
    label: '过期时间',
    component: 'DatePicker',
    colProps: { span: 11 },
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
    colProps: { span: 11 },
  },
  {
    field: 'allowtime',
    label: '允许登录起始时间',
    component: 'DatePicker',
    show: false,
  },

  {
    field: 'weixinid',
    label: '微信ID',
    component: 'Input',
    show: false,
  },
  {
    field: 'order',
    label: '排序号',
    component: 'Input',
    colProps: { span: 11 },
  },
];

/**
 * 定义新增/修改项目用户表单
 */
export const addProjectUserFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '姓名',
    component: 'Input',
    required: true,
  },
  {
    field: 'code',
    label: '登录账号',
    component: 'Input',
  },
  {
    field: 'depts',
    label: '所属部门',
    component: 'Select',
    componentProps: {
      mode: 'multiple',
    },
  },
  {
    field: 'duty',
    label: '职务',
    component: 'Input',
  },
  {
    field: 'phone',
    label: '手机号',
    component: 'Input',
  },
  {
    field: 'cornet',
    label: '短号',
    component: 'Input',
  },
  {
    field: 'tel',
    label: '办公室/座机号码',
    component: 'Input',
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
  },
  {
    field: 'allowtime',
    label: '允许登录起始时间',
    component: 'DatePicker',
    show: false,
  },
  {
    field: 'expiretime',
    label: '过期时间',
    component: 'DatePicker',
  },
  {
    field: 'weixinid',
    label: '微信ID',
    component: 'Input',
    show: false,
  },
  {
    field: 'order',
    label: '排序号',
    component: 'Input',
  },
  {
    field: 'project',
    label: '项目编码',
    component: 'Input',
    defaultValue: currentProjectCode,
    show: false,
  },
];

/**
 * 项目表头
 */
export const projectsTableColumns: BasicColumn[] = [
  {
    title: '项目编码', //显示表头名称
    dataIndex: 'code', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    width: 100,
  },
  {
    title: '项目名称',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '标记',
    dataIndex: 'flag',
    width: 100,
    slots: { customRender: 'statusFlag' },
  },
];

/**
 * 部门表头
 */
export const deptsTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    width: 100,
  },
  {
    title: '部门名称',
    dataIndex: 'name',
    width: 100,
  },
];
