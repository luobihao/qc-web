import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义部门表头
 */
export const projectTableColumns: BasicColumn[] = [
  {
    title: '项目编码', //显示表头名称
    dataIndex: 'code', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    // ifShow: false, //设置在表格界面中是否显示
    width: 80,
  },
  {
    title: '项目名称',
    dataIndex: 'name',
    // ifShow: false,
    width: 150,
  },
  {
    title: '完整标题',
    dataIndex: 'fulltitle',
    width: 100,
  },
  {
    title: '简要标题',
    dataIndex: 'title',
    width: 100,
  },
  {
    title: 'LOGO',
    dataIndex: 'logo',
    width: 100,
  },
  {
    title: '版权',
    dataIndex: 'copyright',
    width: 100,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    slots: { customRender: 'statusFlag' },
    width: 80,
  },
];

/**
 * 定义查询条件表单
 */
export const queryConditionFormSchema: FormSchema[] = [
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: '',
  },
];

/**
 * 定义新增/修改项目表单
 */
export const projectFormSchema: FormSchema[] = [
  {
    field: 'code',
    label: '项目编码',
    component: 'Input',
    required: true,
  },
  {
    field: 'name',
    label: '项目名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'fulltitle',
    label: '完整标题',
    component: 'Input',
    required: true,
  },
  {
    field: 'title',
    label: '简要标题',
    component: 'Input',
    required: true,
  },
  {
    field: 'logo',
    label: 'LOGO',
    component: 'Input',
    required: false,
  },
  {
    field: 'copyright',
    label: '版权',
    component: 'Input',
    required: false,
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '禁用', value: '5' },
      ],
    },
    defaultValue: '0',
  },
];

/**
 * 设置项目参数表单
 */
export const setProjectParamSchema: FormSchema[] = [
  {
    field: 'code',
    label: '项目编码',
    component: 'Input',
    required: true,
    componentProps: {
      disabled: true,
    }
  },
  {
    field: 'name',
    label: '项目名称',
    component: 'Input',
    required: true,
    componentProps: {
      disabled: true,
    }
  },
  {
    field: 'params',
    label: '参数',
    component: 'CodeMirror',
    componentProps: {
      mode: 'application/json'
    },
    // required: true,
  }
]

/**
 * 项目人员表头
 */
export const userTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 100,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 100,
  },
];
