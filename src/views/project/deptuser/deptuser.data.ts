import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { useUserStore } from '/@/store/modules/user';
import { getProjectValidDeptTree } from '/@/api/project/dept';
//获取当前登录用户项目编码
const userStore = useUserStore();
const currentProjectCode = userStore.getUserCurrentProjectCode;

/**
 * 定义用户表头
 */
export const userTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 80,
  },
  {
    title: '登录用户名',
    dataIndex: 'code',
    width: 100,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 100,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    slots: { customRender: 'statusFlag' },
    width: 80,
  },
  {
    title: '排序号',
    dataIndex: 'order',
    width: 50,
  },
];

/**
 * 定义项目用户查询条件表单
 */
export const projectUserQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'project',
    label: '项目编码',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: currentProjectCode, //默认为当前登录用户项目编码
    show: false,
  },
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
  },
];

/**
 * 定义新增/修改项目用户表单
 */
export const projectUserFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '姓名',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'phone',
    label: '手机号',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'code',
    label: '登录账号',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'cornet',
    label: '短号',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'duty',
    label: '职务',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'tel',
    label: '办公室/座机号码',
    component: 'Input',
    colProps: { span: 11 },
  },
  // {
  //   field: 'depts',
  //   label: '所属部门',
  //   component: 'Select',
  //   componentProps: {
  //     mode: 'multiple',
  //   },
  //   colProps: { span: 11 },
  // },
  {
    field: 'depts',
    label: '所属部门',
    component: 'ApiTreeSelect',
    componentProps: {
      api: getProjectValidDeptTree, //数据接口
      params: { id: currentProjectCode },
      immediate: true, //是否立即请求接口，否则将在第一次点击时候触发请求
      fieldNames: { label: 'title', value: 'key', children: 'children' },
      treeDefaultExpandAll: true, //默认展开
      listHeight: 200,
      multiple: true,
    },
    colProps: { span: 11 },
  },

  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
    colProps: { span: 11 },
  },
  {
    field: 'expiretime',
    label: '过期时间',
    component: 'DatePicker',
    colProps: { span: 11 },
  },
  {
    field: 'order',
    label: '排序号',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'project',
    label: '项目编码',
    component: 'Input',
    defaultValue: currentProjectCode,
    show: false,
  },
];
