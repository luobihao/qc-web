import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { useUserStore } from '/@/store/modules/user';
//获取当前登录用户项目编码
const userStore = useUserStore();
const currentProjectCode = userStore.getUserCurrentProjectCode;

/**
 * 定义角色表头
 */
export const roleTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 100,
  },
  {
    title: '项目编码',
    dataIndex: 'pcode',
    ifShow: false,
    width: 100,
  },
  {
    title: '角色名称',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    slots: { customRender: 'statusFlag' },
    width: 80,
  },
  {
    title: '描述',
    dataIndex: 'description',
    align: 'left',
    width: 200,
  },
];

/**
 * 定义查询条件表单
 */
export const queryConditionFormSchema: FormSchema[] = [
  {
    field: 'project',
    label: '项目编码',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: currentProjectCode, //默认为当前登录用户项目编码
    show: false,
  },
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
  },
];

/**
 * 定义新增/修改角色表单
 */
export const roleFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'pcode',
    label: '项目编码',
    component: 'Input',
    defaultValue: currentProjectCode,
    required: true,
    show: false,
  },
  {
    field: 'name',
    label: '角色名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '禁用', value: '5' },
      ],
    },
    defaultValue: '0',
  },
  {
    field: 'description',
    label: '描述',
    component: 'Input',
  },
];

/**
 * 人员表头
 */
export const roleUsersTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 100,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 100,
  },
];

/**
 * 菜单表头
 */
export const roleMenusTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '菜单名称',
    dataIndex: 'name',
    width: 100,
  },
];

/**
 * 部门表头
 */
export const roleDeptsTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '部门名称',
    dataIndex: 'name',
    width: 100,
  },
];
