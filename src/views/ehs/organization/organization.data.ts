import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { queryUnitCategory } from '/@/api/ehs/unitCategory';
import { useGlobSetting } from '/@/hooks/setting';
const { globalProjectCode } = useGlobSetting();
import { queryTreeTable } from '/@/api/project/dept';
import { queryEnumOptions } from '/@/api/platform/enum';
/**
 * 组织表配置
 */
export const organizationTableColumns: BasicColumn[] = [
  {
    title: '组织代码',
    dataIndex: 'orgcode',
    width: 100,
  },
  {
    title: '组织名称',
    dataIndex: 'orgname',
    width: 100,
  },
  {
    title: '部门',
    dataIndex: 'deptid',
    width: 100,
    slots: { customRender: 'deptid' }
  },
  {
    title: '组织类型',
    dataIndex: 'orgtype',
    slots: { customRender: 'orgtype' },
    width: 100,
  },
  {
    title: '运行导则库',
    dataIndex: 'runtpl',
    width: 150,
    slots: { customRender: 'runtpl' }
  },
  {
    title: '在建项目导则库',
    dataIndex: 'contpl',
    width: 150,
    slots: { customRender: 'contpl' }
  },
  {
    title: '安全生产配置参数',
    dataIndex: 'params',
    width: 150,
  },
  {
    title: '行政主管组织代码',
    dataIndex: 'admincode',
    width: 150,
  },
]


/**
 * 新增或编辑组织表单配置
 */
export const addOrUpdateOrganizationSchema: FormSchema[] = [
  {
    field: 'orgcode',
    label: '组织编码',
    component: 'Input',
    colProps: { span: 11 },
    required: true
  },
  {
    field: 'orgname',
    label: '组织名称',
    component: 'Input',
    componentProps: {},
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'deptid',
    label: '部门',
    component: 'ApiTreeSelect',
    required: true,
    componentProps: {
      api: queryTreeTable,
      params: {
        pcode: globalProjectCode
      },
      treeDefaultExpandAll: true, //默认展开
      getPopupContainer: (triggerNode) => document.body,
      listHeight: 200,
      labelField: 'name',
      valueField: 'id',
      immediate: true,
      showSearch: true,
    },
    colProps: { span: 11 },
  },
  {
    field: 'orgtype',
    label: '组织类型',
    component: 'Select',
    componentProps: {
      listHeight: 200,
    },
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'runtpl',
    label: '运行导则库',
    component: 'Select',
    componentProps: {
      listHeight: 200,
    },
    colProps: { span: 11 },
  },
  {
    field: 'contpl',
    label: '在建项目导则库',
    component: 'Select',
    colProps: { span: 11 },
    componentProps: {
      listHeight: 200,
    }
  },
  {
    field: 'params',
    label: '安全生产配置参数',
    component: 'CodeMirror',
    componentProps: {
      mode: 'application/json'
    },
    colProps: {
      span: 22,
    },
  },
  {
    field: 'admincode',
    label: '行政主管组织代码',
    component: 'InputTextArea',
    colProps: { span: 22 },
    show: ({ values }) => {
      if (values.orgtype == 2) {
        return true;
      } else {
        return false;
      }
    }
  },
];

