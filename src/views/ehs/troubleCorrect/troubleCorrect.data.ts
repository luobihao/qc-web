import { h, reactive } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';
import { getDeptUsers } from '/@/api/public/public';

export const queryTroubleRecordSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '上报/发现时间',
    component: 'RangePicker',
    setType: 'string',
    labelWidth: 120,
    colProps: { span: 7 },
    componentProps: {
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false
    },
    required: true,
    defaultValue: [moment().startOf('month').format('YYYY-MM-DD'), moment().endOf('month').format('YYYY-MM-DD')],
  },
  {
    field: 'unitid',
    label: '单元',
    component: 'TreeSelect',
    componentProps: {

    },
    colProps: { span: 5 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    colProps: { span: 5 },
    componentProps: {
      options: [
        {
          label: '全部',
          value: '0',
        },
        {
          label: '已验收通过',
          value: '1',
        },
        {
          label: '已整改完成',
          value: '2',
        },
        {
          label: '已派单',
          value: '3',
        },
        {
          label: '未派单',
          value: '4',
        }
      ]
    },
    defaultValue: '0'
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    labelWidth: 70,
    colProps: { span: 5 },
  },
]

export const troubleCorrectRecordColumns: BasicColumn[] = [
  {
    title: 'id',
    dataIndex: 'correctid',
    ifShow: false,
  },
  {
    title: '上报/发现时间',
    dataIndex: 'tm',
    width: 120,
    fixed: 'left',
    customRender: ({ record }) => {
      return record.tm ? moment(record.tm).format('YYYY-MM-DD') : '-'
    },
    sorter: {
      compare: (a, b) => {
        return moment(a.tm) - moment(b.tm)
      },
    },
  },
  {
    title: '隐患名称',
    dataIndex: 'title',
    fixed: 'left',
    width: 300,
    slots: { customRender: 'title' }
  },
  {
    title: '单元',
    dataIndex: 'unit',
    width: 120,
    slots: { customRender: 'unit' },
    sorter: {
      compare: (a, b) => {
        return a.unit.localeCompare(b.unit);
      },
    },
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 80,
    slots: { customRender: 'ismajor' },
    sorter: {
      compare: (a, b) => {
        return a.ismajor.localeCompare(b.ismajor);
      },
    },
  },
  {
    title: '风险等级',
    dataIndex: 'risklevel',
    width: 100,
    slots: { customRender: 'risklevel' }
  },
  {
    title: '是否上报',
    dataIndex: 'isreport',
    width: 80,
    slots: { customRender: 'boolSlot' }
  },
  {
    title: '整改治理状态',
    dataIndex: 'status',
    width: 120,
    slots: { customRender: 'status' },
    sorter: {
      compare: (a, b) => {
        return a.status.localeCompare(b.status);
      },
    },
  },
  {
    title: '整改时限',
    dataIndex: 'reqendtm',
    width: 120,
    customRender: ({ record }) => {
      return record.reqendtm ? moment(record.reqendtm).format('YYYY-MM-DD') : '-'
    },
    sorter: {
      compare: (a, b) => {
        return moment(a.reqendtm) - moment(b.reqendtm)
      },
    },
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.resdeptname.localeCompare(b.resdeptname);
      },
    },
  },
  {
    title: '责任人',
    dataIndex: 'resusername',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.resusername.localeCompare(b.resusername);
      },
    },
  },
  {
    title: '手机号',
    dataIndex: 'resuserphone',
    width: 120,
  },
  {
    title: '整改完成时间',
    dataIndex: 'correctendtm',
    width: 120,
    customRender: ({ record }) => {
      return record.correctendtm ? moment(record.correctendtm).format('YYYY-MM-DD') : '-'
    }
  }
  // {
  //   title: '是否已派单整改',
  //   dataIndex: 'hasdispatch',
  //   width: 80,
  //   slots: { customRender: 'boolSlot' }
  // },
  // {
  //   title: '是否整改完成',
  //   dataIndex: 'hascorrect',
  //   width: 80,
  //   slots: { customRender: 'boolSlot' }
  // },
  // {
  //   title: '是否验收通过',
  //   dataIndex: 'hascheck',
  //   width: 80,
  //   slots: { customRender: 'boolSlot' }
  // }
]


// 隐患治理派发
export const troubleCorrectDispatchSchema: FormSchema[] = [
  {
    field: 'endtm',
    label: '整改时限',
    component: 'DatePicker',
    colProps: { span: 22 },
    required: true,
    componentProps: ({ formModel }) => {
      return {
        style: 'width:100%',
        picker: 'date',
        valueFormat: 'YYYY-MM-DD',
        allowClear: false,
        getPopupContainer: triggerNode => document.body
      }
    },
    // defaultValue: moment().add(1, 'days').format('YYYY-MM-DD')
  },
  {
    field: 'title',
    label: '任务标题',
    required: true,
    show: true,
    component: 'Input',
    colProps: { span: 22 },
  },
  {
    field: 'deptid',
    label: '整改责任部门',
    component: 'TreeSelect',
    required: true,
    colProps: { span: 22 },
    componentProps: {
    }
  },
  {
    field: 'userid',
    label: '负责人',
    component: 'ApiSelect',
    required: true,
    ifShow: false,
    colProps: { span: 22 },
    componentProps: ({ formModel }) => {
      return {
        onChange: (value) => {
          getDeptUsers(formModel.deptid).then(res => {
            formModel.userphone = res.find(item => item.id === value)?.phone
          })
        },
        api: getDeptUsers,
        params: formModel.deptid,
        immediate: true, //是否立即请求接口，否则将在第一次点击时候触发请求
        listHeight: 220,
        getPopupContainer: (triggerNode) => document.body,
        fieldNames: { label: 'name', value: 'id' },
      };
    },
  },
  {
    field: 'userphone',
    label: '负责人手机号',
    ifShow: false,
    required: true,
    component: 'Input',
    colProps: { span: 22 },
  },
  {
    field: 'description',
    label: '整改要求',
    component: 'InputTextArea',
    colProps: { span: 22 },
    componentProps: {
      autoSize: { minRows: 5, maxRows: 10 },
    },
  }
]

// 验收表单
export const troubleCheckFormSchema: FormSchema[] = [
  {
    field: 'endtm',
    label: '验收时间',
    component: 'DatePicker',
    colProps: { span: 11 },
    required: true,
    componentProps: {
      style: 'width:100%',
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false,
      getPopupContainer: triggerNode => document.body
    },
  },
  {
    field: 'username',
    label: '验收人员',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'hascheck',
    label: '是否通过验收',
    component: 'RadioGroup',
    required: true,
    colProps: { span: 22 },
    componentProps: {
      options: [
        {
          label: '是',
          value: true,
        },
        {
          label: '否',
          value: false,
        }
      ]
    },
    defaultValue: true
  },
  {
    field: 'description',
    label: '验收情况',
    component: 'InputTextArea',
    colProps: { span: 22 },
    componentProps: {
      autoSize: { minRows: 5, maxRows: 10 },
    }
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
  {
    field: 'sign',
    label: '验收负责人签字',
    component: 'Input',
    colProps: { span: 22 },
    slot: 'sign'
  }

]

// 整改表单
export const troubleCorrectFormSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '整改时间',
    component: 'Input',
    colProps: { span: 11 },
    required: true,
  },
  {
    field: 'username',
    label: '整改人员',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'hascorrect',
    label: '是否整改完成',
    component: 'RadioGroup',
    required: true,
    colProps: { span: 11 },
    componentProps: {
      options: [
        {
          label: '是',
          value: true,
        },
        {
          label: '否',
          value: false,
        }
      ]
    },
    defaultValue: true
  },
  {
    field: 'endtm',
    label: '整改完成时间',
    ifShow: ({ values }) => values.hascorrect,
    component: 'DatePicker',
    colProps: { span: 11 },
    componentProps: {
      style: 'width:100%',
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false,
      getPopupContainer: triggerNode => document.body
    },
    defaultValue: moment().format('YYYY-MM-DD'),
    required: true,
  },
  {
    field: 'description',
    label: '整改情况',
    component: 'InputTextArea',
    colProps: { span: 22 },
    componentProps: {
      autoSize: { minRows: 5, maxRows: 10 },
    }
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
  {
    field: 'sign',
    label: '整改负责人签字',
    component: 'Input',
    colProps: { span: 22 },
    slot: 'sign'
  }
]

// 上传报告
export const recordOperateModalSchemas: FormSchema[] = [
  {
    field: 'attachment',
    label: '上传报告',
    component: 'Upload',
    required: false,
    colProps: { span: 12 },
    componentProps: {
      maxSize: 2048,
      accept: ['doc,', 'docx', 'pdf'],
      multiple: false,
      maxNumber: 1,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
  {
    field: 'upload',
    label: '',
    component: 'Input',
    slot: 'upload',
    colProps: { span: 12 },
  }
]
