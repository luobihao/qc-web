import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { useGlobSetting } from '/@/hooks/setting';
const { globalProjectCode } = useGlobSetting();
import { queryUnitCategory } from '/@/api/ehs/unitCategory';
import { queryEnumOptions } from '/@/api/platform/enum';
import { Tag } from 'ant-design-vue';
import { queryEnumOption } from '/@/api/qms/exchangeData';
/**
 * 单元表配置
 */
export const unitDivisionTableColumns: BasicColumn[] = [
  // {
  //   title: '单元代码',
  //   dataIndex: 'unitid',
  //   width: 150,
  // },
  {
    title: '单元名称',
    dataIndex: 'unitname',
    width: 200,
    align: 'left',
    slots: { customRender: 'unitname' }
  },
  {
    title: '单元分类',
    dataIndex: 'uccd',
    width: 150,
    slots: { customRender: 'uccd' }
  },
  {
    title: '经度',
    dataIndex: 'longitude',
    width: 120,
  }, {
    title: '纬度',
    dataIndex: 'latitude',
    width: 120,
  }, {
    title: '具体位置',
    dataIndex: 'location',
    width: 200,
  }, {
    title: '介绍信息',
    dataIndex: 'introduction',
    width: 150,
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 150,
  },
]


/**
 * 新增或编辑表单配置
 */
export const addOrUpdateUnitDivisionSchema: FormSchema[] = [
  {
    field: 'unitid',
    label: '单元编码',
    component: 'Input',
    colProps: { span: 11 },
    show: false,
  },
  {
    field: 'unitname',
    label: '单元名称',
    component: 'Input',
    componentProps: {},
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'parentid',
    label: '上级单元',
    component: 'TreeSelect',
    componentProps: {

    },
    colProps: { span: 22 },
  },
  {
    field: 'uccd',
    label: '所属分类',
    component: 'ApiTreeSelect',
    componentProps: {
      api: queryUnitCategory,
      immediate: true,
      fieldNames: { label: 'ucnm', value: 'uccd', children: 'children' },
      treeDefaultExpandAll: true, //默认展开
    },
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'longitude',
    label: '经度',
    component: 'InputNumber',
    componentProps: {
      stringMode: true,
      style: "width: 100%"
    },
    colProps: { span: 11 },
  },
  {
    field: 'latitude',
    label: '纬度',
    component: 'InputNumber',
    componentProps: {
      stringMode: true,
      style: "width: 100%"
    },
    colProps: { span: 11 },
  },
  {
    field: 'location',
    label: '具体位置',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    field: 'introduction',
    label: '介绍信息',
    component: 'InputTextArea',
    colProps: { span: 22 },
    componentProps: {
      autoSize: { minRows: 4, maxRows: 6 }
    }
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    componentProps: {
      style: "width: 100%"
    },
    colProps: { span: 22 },
  },
  {
    field: 'bgimg',
    label: '空间分布图',
    colProps: { span: 22 },
    component: 'Input',
    slot: 'bgimg',
  },
];

/**
 * 定义单查询条件表单
 */
export const queryUnitConditionFormSchema: FormSchema[] = [
  // {
  //   field: 'uccd',
  //   label: '所属单元分类',
  //   component: 'TreeSelect',
  //   colProps: { span: 5 },
  //   componentProps: {
  //     listHeight: 200,
  //   }
  // },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 5 },
    defaultValue: '',
  },
];

/**
 * 单元静态信息维护表格列
 */
export const unitStaticInfoTableColumns: BasicColumn[] = [
  {
    title: '信息标题',
    dataIndex: 'title',
    width: 200,
    align: 'left',
  },
  {
    title: '信息类型',
    dataIndex: 'infotype',
    width: 100,
    slots: { customRender: 'infotype' }
  },
  {
    title: '信息内容',
    dataIndex: 'content',
    slots: { customRender: 'content' },
    width: 300,
  }
]
export const addOrUpdateUnitStaticInfoSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'Id',
    show: false,
    component: 'Input',
  },
  {
    field: 'title',
    label: '信息标题',
    component: 'Input',
    colProps: { span: 22 },
    required: true,
    defaultValue: '',
  },
  {
    field: 'infotype',
    label: '信息类型',
    component: 'Select',
    defaultValue: 1,
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'content',
    label: '信息内容',
    component: 'Input',
    rulesMessageJoinLabel: false,
    slot: 'content',
    rules: [
      {
        required: true,
        message: "信息内容不能为空,请输入文字或上传图片",
      },
    ],
    colProps: { span: 22 },
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    componentProps: {
      max: 9,
      style: {
        width: '450px'
      }
    },
  },
]
