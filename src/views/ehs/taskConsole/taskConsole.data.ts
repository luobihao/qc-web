import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';

export const queryTaskConsoleConditionSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '查询时间',
    component: 'Input',
    settype: 'string',
    required: true,
    colProps: { span: 7 },
    labelWidth: 90,
    slot: 'tm',
    defaultValue: [moment().startOf('month').format('YYYY-MM-DD'),
    moment().endOf('month').format('YYYY-MM-DD')]
  },
  {
    field: 'tasktype',
    label: '任务分类',
    component: 'Select',
    labelWidth: 80,
    colProps: { span: 5 },
  },
  {
    field: 'status',
    label: '任务状态',
    labelWidth: 90,
    component: 'Select',
    colProps: { span: 4 },
    componentProps: {
    }
  },
  {
    field: 'deptid',
    label: '部门',
    component: 'TreeSelect',
    labelWidth: 50,
    colProps: { span: 4 },
  },
  {
    field: 'keywords',
    label: '关键词',
    component: 'Input',
    colProps: { span: 4 },
  }
]

/**
 * 控制台首页table列
 */
export const taskConsoleTableColumns: BasicColumn[] = [
  {
    title: '任务名称',
    dataIndex: 'tasktitle',
    slots: { customRender: 'tasktitle' },
  },
  {
    title: '任务分类',
    dataIndex: 'catname',
    width: 180,
    slots: { customRender: 'catname' },
  },
  // {
  //   title: '任务大类',
  //   dataIndex: 'tasktype',
  //   slots: { customRender: 'tasktype' },
  // },
  // {
  //   title: '任务目标对象',
  //   dataIndex: 'objtype',
  //   slots: { customRender: 'objtype' },
  // },
  {
    title: '计划执行时间',
    dataIndex: 'planbegintm',
    width: 200,
    customRender: ({ record }) => {
      return moment(record.planbegintm).format('YYYY-MM-DD') + '~' + moment(record.planendtm).format('YYYY-MM-DD')
    }
  },
  {
    title: '频次要求',
    dataIndex: 'planfrequencytype',
    slots: { customRender: 'planfrequencytype' },
    width: 90,
  },
  {
    title: '任务状态',
    dataIndex: 'status',
    slots: { customRender: 'status' },
    width: 90,
  },
  // {
  //   title: '是否派单',
  //   dataIndex: 'hasdispatch',
  //   slots: { customRender: 'boolSlot' },
  // },
  // {
  //   title: '是否执行完成',
  //   dataIndex: 'hasfinish',
  //   slots: { customRender: 'boolSlot' },
  // },
  // {
  //   title: '是否终止取消',
  //   dataIndex: 'hasend',
  //   slots: { customRender: 'boolSlot' },
  // },
  {
    title: '创建人',
    dataIndex: 'inusername',
    width: 100,
  },
  {
    title: '创建时间',
    dataIndex: 'intm',
    width: 150,
  }
]

/**
 * 指定任务的任务分组任务项显示
 */
export const taskGroupAndItemsColumns: BasicColumn[] = [
  {
    title: '名称',
    dataIndex: 'tasktitle',
    align: 'left',
    fixed: 'left',
    width: 250,
    slots: { customRender: 'tasktitle' },
  },
  {
    title: '单元',
    width: 160,
    fixed: 'left',
    dataIndex: 'unitname',
  },
  // {
  //   title: '任务分类',
  //   dataIndex: 'catname',
  // },
  // {
  //   title: '任务大类',
  //   dataIndex: 'tasktype',
  // },
  {
    title: '计划执行时间',
    dataIndex: 'tm',
    width: 220,
    customRender: ({ record }) => {
      if (record.planbegintm && record.planendtm) {
        return moment(record.planbegintm).format('YYYY-MM-DD') + '~' + moment(record.planendtm).format('YYYY-MM-DD')
      } else {
        return '-'
      }
    }
  },
  // {
  //   title: '周期类型',
  //   dataIndex: 'planfrequencytype',
  // },
  {
    title: '实际/计划执行次数',
    dataIndex: 'planfrequencycount',
    width: 150,
    customRender: ({ record }) => {
      return (record.execcount ?? '-') + '/' + (record.planfrequencycount ?? '-')
    }
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    slots: { customRender: 'status' },
  },
  {
    title: '完成时间',
    width: 150,
    dataIndex: 'execendtm',
    customRender: ({ record }) => {
      return record.execendtm ? moment(record.execendtm).format('YYYY-MM-DD') : '-'
    },
    customCell: (record) => {
      return {
        style: {
          backgroundColor: record.execendtm && moment(record.planendtm).isBefore(
            moment(record.execendtm).format('YYYY-MM-DD HH:mm:ss'),
          ) ? '#f9ff4f' : ''
        },
      };
    },
  },
  {
    title: '责任部门/责任人',
    dataIndex: 'transInfo',
    slots: { customRender: 'transInfo' },
    width: 220,
  },
  // {
  //   title: '是否执行完成',
  //   dataIndex: 'hasfinish',
  //   slots: { customRender: 'hasfinish' },
  // },
  // {
  //   title: '是否终止取消',
  //   dataIndex: 'hasend',
  //   slots: { customRender: 'hasend' },
  // },
]

/**
 * 任务执行记录
 */
export const taskExecutesColumns: BasicColumn[] = [
  {
    title: '任务名称',
    dataIndex: 'itemname',
    slots: { customRender: 'itemname' },
  },
  {
    title: '单元',
    dataIndex: 'unitname',
    slots: { customRender: 'unitname' },
    width: 200,
  },
  {
    title: '执行人员',
    dataIndex: 'executor',
    width: 110,
  }, {
    title: '执行时间',
    dataIndex: 'executetmstr',
    width: 140,
  },
  {
    title: '状态',
    dataIndex: 'status',
    slots: { customRender: 'status' },
    width: 120,
  },
  {
    title: '执行情况',
    dataIndex: 'execdescription',
    width: 280,
  }
]

/**
 * 辨识记录
 */
export const identifyRecordColumns: BasicColumn[] = [
  {
    title: '标题',
    dataIndex: 'title',
  },
  {
    title: '单元',
    dataIndex: 'unitname',
    width: 150,
  },
  {
    title: '类别',
    dataIndex: 'categoryname',
    width: 120,
  },
  {
    title: '项目',
    dataIndex: 'projectname',
    width: 120,
  },
  {
    title: '名称',
    dataIndex: 'itemname',
    width: 120,
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 80,
    slots: { customRender: 'ismajor' },
  },
  {
    title: '辨识时间',
    dataIndex: 'tm',
    width: 150,
    customRender: ({ record }) => {
      return record.tm ? moment(record.tm).format('YYYY-MM-DD') : ''
    }
  },
  {
    title: '辨识人',
    dataIndex: 'username',
    width: 120
  },
  {
    title: '辨识结果',
    dataIndex: 'identificationresult',
    width: 120,
    slots: { customRender: 'identificationresult' },
  },
]

export const confirmExecuteRecordSchema: FormSchema[] = [

  {
    field: 'confirmdescription',
    label: '确认内容描述',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    field: 'confirmattachment',
    label: '确认附件',
    component: 'Upload',
    required: false,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
  {
    field: 'confirmsign',
    label: '确认人签字',
    component: 'Input',
    colProps: { span: 22 },
    slot: 'sign'
  }

]


export const addOrUpdateTaskFormSchema: FormSchema[] = [
  {
    field: 'taskid',
    label: '任务ID',
    component: 'Input',
    show: false,
  },
  {
    field: 'catname',
    label: '任务分类',
    component: 'Select',
    componentProps: {
    },
    colProps: { span: 11 },
    required: true,
  },
  {
    field: 'type',
    label: ' ',
    labelWidth: 20,
    component: 'RadioButtonGroup',
    colProps: { span: 12 },
    componentProps: {},
    defaultValue: 1,
  },
  {
    field: 'planbegintm',
    label: '计划执行时间',
    component: 'RangePicker',
    required: true,
    setType: 'string',
    slot: 'planbegintm',
    componentProps: ({ formActionType, formModel }) => {
      return {
        onChange: (e) => {
          formModel.planfrequencytype = 0;
        },
        picker: 'date',
        valueFormat: "YYYY-MM-DD",
        getPopupContainer: triggerNode => document.body
      }

    },
    defaultValue: [moment().startOf('year').format('YYYY-MM-DD'), moment().endOf('year').format('YYYY-MM-DD')],
    colProps: { span: 22 },
  },
  {
    field: 'planfrequencytype',
    label: '每',
    component: 'Select',
    componentProps: ({ formActionType, formModel }) => {
      return {
        getPopupContainer: (triggerNode) => document.body,
        allowClear: false,
        options: [
          {
            label: '自定义',
            value: 0
          },
          {
            label: '天',
            value: 3,
            // disabled: (formModel.planbegintm && formModel.planbegintm[0] && formModel.planbegintm[1]) ? (Math.abs(moment(formModel.planbegintm[1]).diff(moment(formModel.planbegintm[0]), 'day')) < 1 ? true : false) : false
          },
          {
            label: '周',
            value: 4,
            // disabled: (formModel.planbegintm && formModel.planbegintm[0] && formModel.planbegintm[1]) ? (Math.abs(moment(formModel.planbegintm[1]).diff(moment(formModel.planbegintm[0]), 'day')) < 7 ? true : false) : false
          },
          {
            label: '旬',
            value: 5,
            // disabled: (formModel.planbegintm && formModel.planbegintm[0] && formModel.planbegintm[1]) ? (Math.abs(moment(formModel.planbegintm[1]).diff(moment(formModel.planbegintm[0]), 'day')) < 10 ? true : false) : false
          },
          {
            label: '月',
            value: 6,
            // disabled: (formModel.planbegintm && formModel.planbegintm[0] && formModel.planbegintm[1]) ? (Math.abs(moment(formModel.planbegintm[1]).diff(moment(formModel.planbegintm[0]), 'month')) < 1 ? true : false) : false
          },
          {
            label: '季度',
            value: 7,
            // disabled: (formModel.planbegintm && formModel.planbegintm[0] && formModel.planbegintm[1]) ? (Math.abs(moment(formModel.planbegintm[1]).diff(moment(formModel.planbegintm[0]), 'month')) < 3 ? true : false) : false
          },
          {
            label: '年',
            value: 8,
            // disabled: (formModel.planbegintm && formModel.planbegintm[0] && formModel.planbegintm[1]) ? (Math.abs(moment(formModel.planbegintm[1]).diff(moment(formModel.planbegintm[0]), 'year')) < 1 ? true : false) : false
          }
        ]
      }

    },
    ifShow: ({ values }) => values.type == 1,
    colProps: { span: 8 },
    defaultValue: 6,
  },
  {
    field: 'planfrequencycount',
    label: '',
    component: 'Input',
    ifShow: ({ values }) => values.type == 1,
    slot: 'planfrequencycount',
    componentProps: {
      min: 1
    },
    colProps: { span: 8 },
    defaultValue: 1,
  },
  {
    field: 'plantotalcount',
    label: '',
    colProps: { span: 7 },
    slot: 'plantotalcount',
    ifShow: ({ values }) => values.type == 1,
    component: 'InputNumber',
    componentProps: {
      min: 1
    },
    defaultValue: 1,
  },
  {
    field: 'tasktitle',
    label: '任务名称',
    component: 'Input',
    required: true,
    colProps: { span: 23 },
  },
  {
    field: 'taskcontent',
    label: '任务内容及要求',
    component: 'InputTextArea',
    colProps: { span: 23 },
    required: true,
    componentProps: {
      autoSize: { minRows: 3 }
    }
  },
  {
    field: 'tasktype',
    label: '任务大类',
    show: false,
    ifShow: ({ values }) => values.catname,
    colProps: { span: 11 },
    component: 'Select',
    required: true,
    componentProps: {

    }
  },
  {
    field: 'objtype',
    label: '关联对象类型',
    show: false,
    ifShow: ({ values }) => values.catname,
    colProps: { span: 12 },
    component: 'Select',
    required: true,
    componentProps: {

    },
  },
  {
    field: 'notnull',
    label: '是否必须关联对象',
    show: false,
    labelWidth: 160,
    ifShow: ({ values }) => values.catname,
    component: 'RadioGroup',
    required: true,
    colProps: { span: 11 },
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: true
  },
  {
    field: 'needsignin',
    label: '是否必须现场签到',
    ifShow: ({ values }) => values.catname,
    labelWidth: 160,
    colProps: { span: 8 },
    component: 'Checkbox',
    required: true,
    componentProps: {

    },
    defaultValue: true
  },
  // {
  //   field: 'needchecktable',
  //   label: '是否需要填写检查表',
  //   labelWidth: 160,
  //   colProps: { span: 8 },
  //   ifShow: ({ values }) => values.catname,
  //   component: 'Checkbox',
  //   required: true,
  //   componentProps: {

  //   },
  //   defaultValue: true
  // },
  {
    field: 'needconfirm',
    label: '执行结果是否需要确认',
    labelWidth: 160,
    colProps: { span: 8 },
    ifShow: ({ values }) => values.catname,
    component: 'Checkbox',
    required: true,
    componentProps: {
    },
    defaultValue: true
  },
  {
    field: 'taskattachment',
    label: '任务附件',
    helpMessage: '上传任务执行的照片、方案等。',
    component: 'Upload',
    required: false,
    colProps: { span: 7 },
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
]

/**
 * 单元表配置
 */
export const unitDivisionTableColumns: BasicColumn[] = [
  // {
  //   title: '单元代码',
  //   dataIndex: 'unitid',
  //   width: 150,
  // },
  {
    title: '单元名称',
    dataIndex: 'unitname',
    align: 'left',
  },
  // {
  //   title: '单元分类',
  //   dataIndex: 'uccd',
  //   width: 100,
  //   slots: { customRender: 'uccd' }
  // },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 120,
  },
  {
    title: '责任人',
    dataIndex: 'resusername',
    width: 90,
  },
  {
    title: '安全员',
    dataIndex: 'safeusername',
    width: 90,
  },
]

/**
 * 预览表配置
 */
export const previewTableColumns: BasicColumn[] = [
  {
    title: '单元',
    dataIndex: 'unit',
  },
  {
    title: '检查表',
    dataIndex: 'checkForm',
  }
]

/**
 * 自定义任务分组表列配置
 */
export const customizeGroupTableColumns: BasicColumn[] = [
  {
    title: '分组名称',
    dataIndex: 'groupname',
    width: 120,
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 120,
  },
  {
    title: '参与组员1',
    dataIndex: 'resusername1',
    width: 120,
  },
  {
    title: '参与组员2',
    dataIndex: 'resusername2',
    width: 120,
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 80,
  }
]

/**
 * 自定义任务分组表单配置
 */
export const addOrUpdatecustomizeGroupFormSchema: FormSchema[] = [
  {
    field: 'groupname',
    label: '分组名称',
    component: 'Input',
    required: true,
    colProps: { span: 22 },
    componentProps: {

    }
  },
  {
    field: 'resdeptid',
    label: '责任部门',
    component: 'Select',
    colProps: { span: 22 },
    componentProps: {
    }
  },
  {
    field: 'resuserid1',
    label: '参与组员1',
    component: 'Select',
    colProps: { span: 22 },
    componentProps: {
    }
  },
  {
    field: 'resuserid2',
    label: '参与组员2',
    component: 'Select',
    colProps: { span: 22 },
    componentProps: {
    }
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    colProps: { span: 22 },
    defaultValue: 9
  }
]

/**
 *  任务派单表单
 */
export const dispatchTaskSchema: FormSchema[] = [
  {
    field: 'tasktitle',
    label: '任务标题',
    component: 'Input',
    colProps: { span: 22 },
    componentProps: {
      disabled: true
    }
  },
  {
    field: 'plantm',
    label: '计划执行时间',
    component: 'RangePicker',
    required: true,
    setType: 'string',
    colProps: { span: 22 },
    componentProps: ({ formActionType, formModel }) => {
      return {
        style: 'width:100%',
        picker: 'date',
        valueFormat: "YYYY-MM-DD",
        getPopupContainer: triggerNode => document.body
      }
    },
    colProps: { span: 22 },
  },
]
