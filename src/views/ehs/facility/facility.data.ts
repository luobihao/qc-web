import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

export const facilityTableColumns: BasicColumn[] = [
  {
    title: '单元名称',
    dataIndex: 'unitid',
    width: 120,
    slots: { customRender: 'unitid' },
  },
  {
    title: '设备编号',
    dataIndex: 'code',
    width: 150,
    align: 'left',
  },
  {
    title: '设备名称',
    dataIndex: 'name',
    slots: { customRender: 'name' },
    align: 'left',
  },
  {
    title: '大类',
    dataIndex: 'cat1',
    width: 100,
  },
  {
    title: '分类',
    dataIndex: 'cat2',
    width: 100,
  },
  // {
  //   title: '起始日期',
  //   dataIndex: 'begintm',
  //   width: 100,
  // },
  // {
  //   title: '截止日期',
  //   dataIndex: 'endtm',
  //   width: 100,
  // },
  {
    title: '使用情况',
    dataIndex: 'status',
    width: 100,
    slots: { customRender: 'status' },
  }
]


export const facilityConditionSchema: FormSchema[] = [
  {
    field: 'status',
    label: '使用情况',
    component: 'Select',
    colProps: { span: 4 },
    componentProps: {
      options: [
        {
          label: '在用',
          value: 1
        },
        {
          label: '停用',
          value: 0
        }
      ]
    },
    defaultValue: 1,
    required: true
  },
  {
    field: 'unitid',
    label: '所属单元',
    colProps: { span: 5 },
    component: 'TreeSelect',
    componentProps: {

    },
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 5 },
  }
]


export const facilityImportSchema: FormSchema[] = [
  {
    field: 'unitid',
    label: '所属单元',
    colProps: { span: 22 },
    component: 'TreeSelect',
    componentProps: {

    },
    required: true
  },
  {
    field: 'startRow',
    label: '数据起始行',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      min: 1,
      style: {
        width: '100%'
      }
    },
    defaultValue: 2,
    required: true
  },
  {
    field: 'code',
    label: '代码所在列',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      min: 1,
      style: {
        width: '100%'
      }
    },
    required: true
  },
  {
    field: 'name',
    label: '名称所在列',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      min: 1,
      style: {
        width: '100%'
      }
    },
    required: true
  },
  {
    field: 'cat1',
    label: '大类所在列',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      min: 1,
      style: {
        width: '100%'
      }
    },
    required: true
  },
  {
    field: 'cat2',
    label: '分类所在列',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      min: 1,
      style: {
        width: '100%'
      }
    },
    required: true
  },
  {
    field: 'status',
    label: '使用状况所在列',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      min: 1,
      style: {
        width: '100%'
      }
    },
    required: true
  },
]


export const addOrUpdateFacilitySchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    show: false
  },
  {
    field: 'unitid',
    label: '所属单元',
    colProps: { span: 22 },
    component: 'TreeSelect',
    componentProps: {
    },
    required: true
  },
  {
    field: 'code',
    label: '设备编号',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'name',
    label: '设备名称',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'cat1',
    label: '设备大类',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'cat2',
    label: '设备分类',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'status',
    label: '使用状况',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '在用', value: 1 },
        { label: '停用', value: 0 },
      ]
    },
    colProps: { span: 22 },
    defaultValue: 1,
    required: true
  },
]
