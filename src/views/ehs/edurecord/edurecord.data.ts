import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { Tag } from 'ant-design-vue';
import { queryEnumOption } from '/@/api/qms/exchangeData';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';
export const queryEduRecordSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '培训时间',
    component: 'Input',
    settype: 'string',
    required: true,
    colProps: { span: 9 },
    labelWidth: 90,
    slot: 'tm',
    defaultValue: [moment().startOf('year').format('YYYY-MM-DD'),
    moment().endOf('year').format('YYYY-MM-DD')]
  },
  {
    field: 'edutype',
    label: '培训类型',
    component: 'AutoComplete',
    colProps: { span: 5 },
    componentProps: {
      options: [
        { value: '入职安全培训' },
        { value: '定期安全培训' },
        { value: '改变工艺或设备安全培训' },
        { value: '专项安全培训' },
        { value: '应急预案培训' },
        { value: '安全管理培训' },
        { value: '职业健康培训' },
      ],
      listHeight: 150,
      filterOption: (input, option) => {
        return option.value.toUpperCase().indexOf(input.toUpperCase()) >= 0;
      }
    },
  },
  {
    field: 'edumode',
    label: '开展形式',
    component: 'AutoComplete',
    colProps: { span: 5 },
    componentProps: {
      options: [
        { value: '集中授课' },
        { value: '在线培训' },
        { value: '混合式培训' },
        { value: '案例分析' },
        { value: '模拟演练' },
        { value: '角色扮演' },
        { value: '安全知识竞赛' },
        { value: '安全文化活动' },
        { value: '外部培训' },
        { value: '师带徒' },
        { value: '安全例会' },
        { value: '安全检查' },
      ],
      listHeight: 150,
      filterOption: (input, option) => {
        return option.value.toUpperCase().indexOf(input.toUpperCase()) >= 0;
      }
    },
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 5 },
  }
]


export const eduRecordColumns: BasicColumn[] = [
  {
    title: '培训时间',
    dataIndex: 'tm',
    width: 150,
    customRender: ({ record }) => {
      return record.begintm + '~' + record.endtm
    }
  },
  {
    title: '培训主题',
    dataIndex: 'title',
    width: 120,
  },
  {
    title: '培训内容',
    dataIndex: 'content',
    width: 100,
  },
  {
    title: '类型',
    dataIndex: 'edutype',
    width: 100,
  },
  {
    title: '开展形式',
    dataIndex: 'edumode',
    width: 100,
  },
  {
    title: '地点',
    dataIndex: 'location',
    width: 100,
  },
  {
    title: '时长',
    dataIndex: 'hour',
    width: 80,
    customRender: ({ record }) => {
      return record.hour + '小时'
    }
  },
  {
    title: '参加人数',
    dataIndex: 'personcount',
    width: 60,
  }
]


export const eduRecordFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    show: false,
  },
  {
    field: 'tm',
    label: '培训时间',
    component: 'RangePicker',
    setType: 'string',
    colProps: { span: 22 },
    componentProps: ({ formModel }) => {
      return {
        picker: 'date',
        valueFormat: 'YYYY-MM-DD HH:mm:ss',
        allowClear: false,
        showTime: true,
        required: true,
        onChange: (value) => {
          var duration = moment.duration(moment(value[1]).diff(moment(value[0])));
          var hours = duration.asHours();
          formModel.hour = Math.round(hours * 10) / 10;
        },
      }
    },
    defaultValue: [moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss')],
  },
  {
    field: 'title',
    label: '培训主题',
    component: 'Input',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'content',
    label: '培训内容',
    component: 'InputTextArea',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'edutype',
    label: '培训类型',
    component: 'AutoComplete',
    required: true,
    colProps: { span: 11 },
    componentProps: {
      options: [
        { value: '入职安全培训' },
        { value: '定期安全培训' },
        { value: '改变工艺或设备安全培训' },
        { value: '专项安全培训' },
        { value: '应急预案培训' },
        { value: '安全管理培训' },
        { value: '职业健康培训' },
      ],
      listHeight: 150,
      filterOption: (input, option) => {
        return option.value.toUpperCase().indexOf(input.toUpperCase()) >= 0;
      }
    },
  },
  {
    field: 'edumode',
    label: '开展形式',
    component: 'AutoComplete',
    required: true,
    colProps: { span: 11 },
    componentProps: {
      options: [
        { value: '集中授课' },
        { value: '在线培训' },
        { value: '混合式培训' },
        { value: '案例分析' },
        { value: '模拟演练' },
        { value: '角色扮演' },
        { value: '安全知识竞赛' },
        { value: '安全文化活动' },
        { value: '外部培训' },
        { value: '师带徒' },
        { value: '安全例会' },
        { value: '安全检查' },
      ],
      listHeight: 150,
      filterOption: (input, option) => {
        return option.value.toUpperCase().indexOf(input.toUpperCase()) >= 0;
      }
    },
  },
  {
    field: 'location',
    label: '地点',
    component: 'Input',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'hour',
    label: '时长',
    component: 'InputNumber',
    required: true,
    componentProps: {
      style: 'width:100%',
      addonAfter: "小时",
      min: 0
    },
    colProps: { span: 11 },
    defaultValue: 1,
  },
  {
    field: 'personcount',
    label: '参加人数',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      style: 'width:100%',
      min: 1
    },
    required: true,
    defaultValue: 1,
  },
  {
    label: '培训人员',
    field: 'participants',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    field: 'doccount',
    label: '发放资料数量',
    component: 'InputNumber',
    componentProps: {
      style: 'width:100%',
      min: 0
    },
    required: true,
    colProps: { span: 11 },
    defaultValue: 0,
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    colProps: { span: 11 },
    helpMessage: "上传签到表、培训照片、培训记录等文件；",
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },

]
