import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

export const warehouseTableColumns: BasicColumn[] = [
  {
    title: '仓库名称',
    dataIndex: 'name',
    width: 200,
  },
  {
    title: '所属部门',
    dataIndex: 'deptid',
    width: 200,
    slots: { customRender: 'deptid' },
  },
  {
    title: '仓库位置',
    dataIndex: 'location',
    width: 200,
  },
  {
    title: '说明文字',
    dataIndex: 'description',
  }
]

export const addOrUpdateWareHouseSchema: FormSchema[] = [
  {
    field: 'id',
    label: '仓库ID',
    component: 'Input',
    show: false,
  },
  {
    field: 'name',
    label: '仓库名称',
    component: 'Input',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'deptid',
    label: '所属部门',
    component: 'TreeSelect',
    required: true,
    colProps: { span: 22 },
    componentProps: {
    },
  },
  {
    field: 'location',
    label: '仓库位置',
    colProps: { span: 22 },
    component: 'Input',
    required: true,
  },
  {
    field: 'description',
    label: '说明文字',
    colProps: { span: 22 },
    component: 'InputTextArea',
    componentProps: {
      minRows: 3,
    }
  },
]
