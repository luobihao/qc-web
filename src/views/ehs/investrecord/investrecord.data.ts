import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';
export const invesrRecordcolumns: BasicColumn[] = [
  {
    title: '记录填报时间',
    dataIndex: 'intm',
    width: 100,
  },
  {
    title: '记录填报人',
    dataIndex: 'username',
    width: 100,
  },
  {
    title: '资金投入类型',
    dataIndex: 'investtype',
    width: 100,
  },
  {
    title: '资金投入项目名称',
    dataIndex: 'title',
    width: 100,
  },
  {
    title: '资金投入数值',
    dataIndex: 'invest',
    width: 100,
    customRender: ({ record }) => {
      return h('span', {}, record.invest + '元')
    }
  },
  {
    title: '实际投入时间',
    dataIndex: 'investtm',
    width: 100,
    customRender: ({ record }) => {
      return h('span', {}, moment(record.begintm).format('YYYY-MM-DD') + '至' + moment(record.endtm).format('YYYY-MM-DD'))
    }
  },
  {
    title: '资金投入说明',
    dataIndex: 'description',
    width: 100,
  }
]

export const queryInvestRecordSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '实际投入时间',
    component: 'Input',
    settype: 'string',
    required: true,
    colProps: { span: 9 },
    labelWidth: 120,
    slot: 'tm',
    defaultValue: [moment().startOf('year').format('YYYY-MM-DD'),
    moment().endOf('year').format('YYYY-MM-DD')]
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    labelWidth: 70,
    colProps: { span: 5 },
  },
]

export const addInvestRecordSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    show: false,
    component: 'Input',
  },
  {
    field: 'title',
    label: '投入项目名称',
    component: 'Input',
    colProps: { span: 22 },
    required: true,
  },
  {
    field: 'investtype',
    label: '资金投入类型',
    component: 'Input',
    colProps: { span: 22 },
    required: true,
  },
  {
    field: 'description',
    label: '资金投入说明',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    field: 'invest',
    label: '资金投入数值',
    component: 'InputNumber',
    componentProps: {
      style: "width: 100%",
      suffix: "元"
    },
    colProps: { span: 22 },
    required: true,
  },
  {
    field: 'tm',
    label: '实际投入时间',
    component: 'RangePicker',
    setType: 'string',
    colProps: { span: 22 },
    componentProps: {
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false
    },
    required: true,
    defaultValue: [moment().startOf('month').format('YYYY-MM-DD'), moment().endOf('month').format('YYYY-MM-DD')],
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    colProps: { span: 22 },
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
]
