import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { queryEnumOptions } from '/@/api/platform/enum';


export const taskCategoryTableColumns: BasicColumn[] = [
  {
    title: '分类代码',
    dataIndex: 'catcode',
    width: 120,
  },
  {
    title: '分类名称',
    dataIndex: 'catname',
    width: 150,
  },
  {
    title: '任务大类',
    dataIndex: 'tasktype',
    slots: { customRender: 'tasktype' },
    width: 150,
  },
  {
    title: '图标',
    dataIndex: 'icon',
    slots: { customRender: 'icon' },
    width: 80,
  },
  {
    title: '关联对象类型',
    dataIndex: 'objecttype',
    width: 120,
    slots: { customRender: 'objecttype' },
  },
  {
    title: '是否必须关联对象',
    dataIndex: 'notnull',
    width: 80,
    customRender: ({ record }) => {
      return record.notnull ? '是' : '否'
    }
  },
  {
    title: '是否必须现场签到',
    dataIndex: 'needsignin',
    width: 80,
    customRender: ({ record }) => {
      return record.needsignin ? '是' : '否'
    }
  },
  {
    title: '是否需要填写检查表',
    dataIndex: 'needchecktable',
    width: 80,
    customRender: ({ record }) => {
      return record.needchecktable ? '是' : '否'
    }
  },
  {
    title: '执行结果是否需要确认',
    dataIndex: 'needconfirm',
    width: 80,
    customRender: ({ record }) => {
      return record.needconfirm ? '是' : '否'
    }
  }
]


export const addOrUpdateTaskCategorySchema: FormSchema[] = [
  {
    field: 'catcode',
    label: '分类代码',
    component: 'Input',
    colProps: { span: 11 },
    required: true,
    disabled: false,
  },
  {
    field: 'catname',
    label: '分类名称',
    colProps: { span: 11 },
    component: 'Input',
    required: true,
  },
  {
    field: 'tasktype',
    label: '任务大类',
    colProps: { span: 11 },
    component: 'Select',
    required: true,
    componentProps: {

    }
  },
  {
    field: 'objecttype',
    label: '关联对象类型',
    colProps: { span: 11 },
    component: 'Select',
    required: true,
    componentProps: {

    },
  },
  {
    field: 'icon',
    label: '图标',
    component: 'IconPicker',
    colProps: { span: 22 },
    required: true,
  },
  {
    field: 'notnull',
    label: '是否必须关联对象',
    labelWidth: 160,
    component: 'RadioGroup',
    required: true,
    colProps: { span: 11 },
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: true
  },
  {
    field: 'needsignin',
    label: '是否必须现场签到',
    labelWidth: 160,
    colProps: { span: 11 },
    component: 'RadioGroup',
    required: true,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: true
  },
  {
    field: 'needchecktable',
    label: '是否需要填写检查表',
    labelWidth: 160,
    colProps: { span: 11 },
    component: 'RadioGroup',
    required: true,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: true
  },
  {
    field: 'needconfirm',
    label: '执行结果是否需要确认',
    labelWidth: 160,
    colProps: { span: 11 },
    component: 'RadioGroup',
    required: true,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ]
    },
    defaultValue: true
  }
]
