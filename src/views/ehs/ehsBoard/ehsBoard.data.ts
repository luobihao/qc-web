import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { Tag } from 'ant-design-vue';
import { queryEnumOption } from '/@/api/qms/exchangeData';
import moment from 'moment';


export const deptBoardTableColumns: BasicColumn[] = [
  {
    title: '部门名称',
    dataIndex: 'name',
    slots: { customRender: 'name' }
  },
  {
    title: '危险源',
    dataIndex: 'hazard',
    children: [
      {
        title: '重大',
        dataIndex: 'major',
        width: 100,
        customRender: ({ record }) => {
          return h(Tag, { color: 'red' }, () => record.hazard.majorcount);
        }
      },
      {
        title: '一般',
        width: 100,
        dataIndex: 'general',
        customRender: ({ record }) => {
          return h(Tag, { color: 'blue' }, () => record.hazard.count - record.hazard.majorcount);
        }
      }
    ]
    // slots: { customRender: 'hazard' },
  },
  {
    title: '隐患',
    dataIndex: 'trouble',
    children: [
      {
        title: '重大',
        width: 100,
        dataIndex: 'major',
        customRender: ({ record }) => {
          return h(Tag, { color: 'red' }, () => record.trouble.majorcount);
        }
      },
      {
        title: '一般',
        width: 100,
        dataIndex: 'general',
        customRender: ({ record }) => {
          return h(Tag, { color: 'blue' }, () => record.trouble.count - record.trouble.majorcount);
        }
      }
    ]
  },
  {
    title: '任务',
    dataIndex: 'task',
    children: [
      {
        title: '已完成',
        width: 100,
        dataIndex: 'finish',
        customRender: ({ record }) => {
          return h(Tag, { color: 'green' }, () => record.task.finishcount);
        }
      },
      {
        title: '超时完成',
        width: 100,
        dataIndex: 'timeoutfinish',
        customRender: ({ record }) => {
          return h(Tag, { color: 'orange' }, () => record.task.timeoutfinishcount);
        }
      },
      {
        title: '未完成',
        width: 100,
        dataIndex: 'unfinish',
        customRender: ({ record }) => {
          return h(Tag, { color: 'red' }, () => record.task.count - record.task.finishcount - record.task.timeoutfinishcount);
        }
      }
    ]
  }
]
