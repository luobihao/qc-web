import { options } from '/@/components/QuCheng/Charts/echartsSettings';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { queryUnitCategory } from '/@/api/ehs/unitCategory';
export const checkFormColumns: BasicColumn[] = [
  {
    title: '标题',
    dataIndex: 'title',
    slots: { customRender: 'title' },
    sorter: {
      compare: (a, b) => {
        return a.title.localeCompare(b.title);
      },
    },
  },
  {
    title: '任务分类',
    dataIndex: 'catcode',
    slots: { customRender: 'catcode' },
    width: 120,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    slots: { customRender: 'status' },
  },
  {
    title: '单元分类',
    dataIndex: 'ucnm',
    width: 120,
  },
  {
    title: '单元',
    dataIndex: 'unit',
    width: 120,
    customRender: ({ record }) => {
      return record.unitnames?.join('，')
    }
  }
];

export const checkFormTableCondition: FormSchema[] = [
  {
    field: 'catcode',
    label: '分类代码',
    component: 'Select',
    show: false,
    colProps: {
      span: 6,
    },
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '正常',
          value: 0,
        },
        {
          label: '禁用',
          value: 5,
        }
      ]
    },
    defaultValue: 0,
    colProps: {
      span: 6,
    }
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: {
      span: 6,
    },
  },
]

// 新增或编辑检查表
export const addOrUpdateCheckFormSchema: FormSchema[] = [
  {
    field: 'tableid',
    label: 'ID',
    show: false,
    component: 'Input',
  },
  {
    field: 'title',
    label: '标题',
    component: 'Input',
    required: true,
    colProps: {
      span: 23,
    }
  },
  {
    field: 'catcode',
    label: '任务分类',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: {
      span: 23,
    }
  },
  {
    field: 'uccd',
    label: '单元分类',
    component: 'ApiTreeSelect',
    componentProps: ({ formModel, formActionType }) => {
      return {
        api: queryUnitCategory,
        immediate: true,
        fieldNames: { label: 'ucnm', value: 'uccd', children: 'children' },
        onChange: (e) => {
          formModel.unitids = []
        },
        treeDefaultExpandAll: true, //默认展开
        getPopupContainer: (triggerNode) => document.body,
      }
    },
    required: true,
    colProps: { span: 23 },
  },
  {
    field: 'unitids',
    label: '单元集合',
    colProps: { span: 23 },
    component: 'Input',
    ifShow: ({ values }) => values.uccd,
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '正常',
          value: 0,
        },
        {
          label: '禁用',
          value: 5,
        }
      ]
    },
    defaultValue: 0,
    colProps: {
      span: 23,
    }
  },
  {
    field: 'description',
    label: '备注',
    component: 'InputTextArea',
    colProps: {
      span: 23,
    },
    componentProps: {
      autoSize: { minRows: 4 }
    },
  }
]

export const checkFormGroupColumns: BasicColumn[] = [
  {
    title: '标题',
    dataIndex: 'title',
    width: 150,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 70,
    slots: { customRender: 'status' },
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 60,
  }
]
// 新增检查表分组表单
export const addOrUpdateCheckFormGroupSchema: FormSchema[] = [
  {
    field: 'groupid',
    label: 'ID',
    component: 'Input',
    show: false
  },
  {
    field: 'title',
    label: '标题',
    component: 'Input',
    required: true,
    colProps: {
      span: 22,
    }
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '正常',
          value: 0,
        },
        {
          label: '禁用',
          value: 5,
        }
      ]
    },
    defaultValue: 0,
    colProps: {
      span: 22,
    }
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    componentProps: {
      min: 1,
      max: 99
    },
    defaultValue: 1
  }
]


export const checkFormItemColumns: BasicColumn[] = [
  {
    title: '标题',
    dataIndex: 'title',
    width: 200,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    slots: { customRender: 'status' },
  },
  {
    title: '是否必填',
    dataIndex: 'notnull',
    width: 80,
    customRender: ({ record }) => {
      return record.notnull === true ? '是' : '否'
    }
  },
  {
    title: '待选项配置',
    dataIndex: 'options',
    width: 120,
    slots: { customRender: 'options' },

  },
  {
    title: '填写文本类型',
    dataIndex: 'contenttype',
    width: 100,
    customRender: ({ record }) => {
      return record.contenttype == 1 ? '始终必填' : record.contenttype == 2 ? '始终不填' : '为异常时填写'
    }
  },
  {
    title: '最少图片数量',
    dataIndex: 'minimgcount',
    width: 100
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 80,
  }
]
// 新增检查表分组项目表单
export const addOrUpdateCheckFormItemSchema: FormSchema[] = [
  {
    field: 'itemid',
    label: 'ID',
    show: false,
    component: 'Input',
  },
  {
    field: 'title',
    label: '标题',
    component: 'InputTextArea',
    componentProps: {
      minRows: 1,
    },
    required: true,
    colProps: {
      span: 22,
    }
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '正常',
          value: 0,
        },
        {
          label: '禁用',
          value: 5,
        }
      ]
    },
    defaultValue: 0,
    colProps: {
      span: 11,
    }
  },
  {
    field: 'notnull',
    label: '是否必填',
    component: 'RadioGroup',
    required: true,
    componentProps: {
      options: [
        {
          label: '是',
          value: true,
        },
        {
          label: '否',
          value: false,
        }
      ]
    },
    defaultValue: true,
    colProps: {
      span: 11,
    }
  },
  {
    field: 'options',
    label: '待选项配置',
    component: 'InputTextArea',
    ifShow: true,
    required: true,
    slot: 'options',
    defaultValue: '是,否',
    componentProps: {
      placeholder: '请输入待选项配置，多个选项用逗号分隔'
    },
    colProps: {
      span: 22,
    }
  },
  {
    field: 'contenttype',
    label: '填写文本类型',
    required: true,
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '始终必填',
          value: 1,
        },
        {
          label: '始终不填',
          value: 2,
        },
        {
          label: '为异常时填写',
          value: 3,
        }
      ]
    },
    defaultValue: 1,
    helpMessage: '默认选项的最后一项为异常情况',
    colProps: {
      span: 22,
    }
  },
  {
    field: 'description',
    label: '说明提示信息',
    component: 'InputTextArea',
    colProps: {
      span: 22,
    }
  },
  {
    field: 'minimgcount',
    label: '最少图片数量',
    component: 'InputNumber',
    colProps: {
      span: 11,
    },
    componentProps: {
      min: 0
    },
    defaultValue: 0
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    componentProps: {
      min: 1,
      max: 99
    },
    colProps: {
      span: 11,
    },
    defaultValue: 1
  }
]


export const chooseFormColumns: BasicColumn[] = [
  {
    title: '检查表名称',
    dataIndex: 'title',
    width: 100,
  },
  {
    title: '单元分类',
    dataIndex: 'ucnm',
    width: 100,
  },
  {
    title: '单元',
    dataIndex: 'unit',
    width: 120,
    customRender: ({ record }) => {
      return record.unitnames?.join(',')
    }
  }
];

export const chooseFormTableCondition: FormSchema[] = [
  {
    field: 'catcode',
    label: '分类代码',
    component: 'Select',
    colProps: {
      span: 6,
    },
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '正常',
          value: 0,
        },
        {
          label: '禁用',
          value: 5,
        }
      ]
    },
    defaultValue: 0,
    show: false,
    colProps: {
      span: 6,
    }
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: {
      span: 6,
    },
  },
]
