import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';

export const accidentRecordcolumns: BasicColumn[] = [
  {
    title: '发生时间',
    dataIndex: 'tm',
    width: 100,
    customRender: ({ record }) => {
      return h('div', moment(record.tm).format('YYYY-MM-DD'))
    }
  },
  {
    title: '事故标题',
    dataIndex: 'title',
    width: 100,
  },
  {
    title: '单元',
    dataIndex: 'unitname',
    width: 100,
  },
  {
    title: '事故经过',
    dataIndex: 'content',
    width: 100,
  },
  {
    title: '事故波及范围',
    dataIndex: 'incidence',
    width: 100
  },
  {
    title: '事故原因类型',
    dataIndex: 'causetype',
    width: 100,
  },
  {
    title: '事故等级',
    dataIndex: 'lvl',
    slots: { customRender: 'lvl' },
    width: 100,
  },
  {
    title: '是否为责任事故',
    dataIndex: 'isliability',
    customRender: ({ record }) => {
      return h('div', record.isliability ? '是' : '否')
    },
    width: 100,
  }
]

export const queryAccidentRecordSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '发生时间',
    component: 'Input',
    settype: 'string',
    required: true,
    colProps: { span: 9 },
    labelWidth: 90,
    slot: 'tm',
    defaultValue: [moment().startOf('year').format('YYYY-MM-DD'),
    moment().endOf('year').format('YYYY-MM-DD')]
  },
  {
    field: 'unitid',
    label: '单元',
    component: 'TreeSelect',
    labelWidth: 60,
    colProps: { span: 5 },
    componentProps: {
    },
  },
  {
    field: 'causetype',
    label: '事故原因类型',
    component: 'AutoComplete',
    labelWidth: 110,
    colProps: { span: 5 },
    componentProps: {
      options: [
        { value: '物体打击事故' },
        { value: '车辆伤害事故' },
        { value: '机械伤害事故' },
        { value: '起重伤害事故' },
        { value: '触电事故' },
        { value: '火灾事故' },
        { value: '灼烫事故' },
        { value: '淹溺事故' },
        { value: '坍塌事故' },
        { value: '高处坠落事故' },
        { value: '冒顶片帮事故' },
        { value: '透水事故' },
        { value: '放炮事故' },
        { value: '火药爆炸事故' },
        { value: '瓦斯爆炸事故' },
        { value: '锅炉爆炸事故' },
        { value: '容器爆炸事故' },
        { value: '其他爆炸事故' },
        { value: '中毒和窒息事故' },
        { value: '其他伤害事故' },
      ],
      filterOption: false
    },
  },
  {
    field: 'lvl',
    label: '事故等级',
    component: 'Select',
    colProps: { span: 5 },
    componentProps: {
    },
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    labelWidth: 70,
    colProps: { span: 5 },
  },
]

export const addAccidentRecordSchema1: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    show: false,
    component: 'Input',
  },
  {
    field: 'title',
    label: '事故标题',
    component: 'Input',
    colProps: { span: 23 },
    required: true,
  },
  {
    field: 'unitid',
    label: '单元',
    component: 'TreeSelect',
    colProps: { span: 23 },
    componentProps: {}
  },
  {
    field: 'tm',
    label: '发生时间',
    component: 'DatePicker',
    colProps: { span: 23 },
    componentProps: {
      style: 'width:100%',
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false,
      getPopupContainer: triggerNode => document.body
    },
    required: true,
  },
  {
    field: 'content',
    label: '事故经过',
    component: 'InputTextArea',
    colProps: { span: 23 },
  },
  {
    field: 'incidence',
    label: '事故波及范围',
    component: 'Input',
    colProps: { span: 23 },
  },
  {
    field: 'lvl',
    label: '事故等级',
    component: 'Select',
    componentProps: {

    },
    colProps: { span: 12 },
    required: true,
  },
  {
    field: 'injurednum',
    label: '重伤人数',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      style: 'width:100%',
      min: 0
    },
    required: true,
  },
  {
    field: 'deathnum',
    label: '死亡人数',
    componentProps: {
      style: 'width:100%',
      min: 0
    },
    component: 'InputNumber',
    colProps: { span: 12 },
    required: true,
  },
  {
    field: 'ecoloss',
    label: '直接经济损失',
    component: 'InputNumber',
    componentProps: {
      style: 'width:100%',
      addonAfter: "万元",
      min: 0
    },
    colProps: { span: 11 },
    required: true,
  },
  {
    field: 'isliability',
    label: '是否责任事故',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    colProps: { span: 11 },
    required: true,
    defaultValue: true,
  },
  {
    field: 'hasreport',
    label: '是否上报',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    colProps: { span: 12 },
    required: true,
    defaultValue: true,
  },
  {
    field: 'reporttype',
    label: '上报方式',
    component: 'Input',
    colProps: { span: 12 },
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    colProps: { span: 11 },
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
]


export const addAccidentRecordSchema2: FormSchema[] = [

  {
    field: 'directcause',
    label: '事故直接原因',
    required: true,
    component: 'Input',
    colProps: { span: 23 },
  },
  {
    field: 'indirectcause',
    label: '事故间接原因',
    required: true,
    component: 'Input',
    colProps: { span: 23 },
  },
  {
    field: 'causetype',
    label: '事故原因类型',
    component: 'AutoComplete',
    colProps: { span: 23 },
    componentProps: {
      options: [
        { value: '物体打击事故' },
        { value: '车辆伤害事故' },
        { value: '机械伤害事故' },
        { value: '起重伤害事故' },
        { value: '触电事故' },
        { value: '火灾事故' },
        { value: '灼烫事故' },
        { value: '淹溺事故' },
        { value: '坍塌事故' },
        { value: '高处坠落事故' },
        { value: '冒顶片帮事故' },
        { value: '透水事故' },
        { value: '放炮事故' },
        { value: '火药爆炸事故' },
        { value: '瓦斯爆炸事故' },
        { value: '锅炉爆炸事故' },
        { value: '容器爆炸事故' },
        { value: '其他爆炸事故' },
        { value: '中毒和窒息事故' },
        { value: '其他伤害事故' },
      ],
      listHeight: 150,
      filterOption: false
    },
    required: true,
  },
  {
    field: 'responsibility',
    label: '事故责任',
    // component: 'Select',
    component: 'Input',
    colProps: { span: 23 },
  },
  {
    field: 'lesson',
    label: '事故教训',
    component: 'InputTextArea',
    colProps: { span: 23 },
  },
  {
    field: 'measure',
    label: '事故整改措施',
    component: 'InputTextArea',
    colProps: { span: 23 },
  },
  {
    field: 'suggestion',
    label: '事故处理建议',
    component: 'InputTextArea',
    colProps: { span: 23 },
  },
]
