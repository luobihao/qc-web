import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { queryEnumOptions } from '/@/api/platform/enum';
/**
 * 单元表配置
 */
export const unitCategoryTableColumns: BasicColumn[] = [
  {
    title: '单元分类代码',
    dataIndex: 'uccd',
    width: 150,
    align: 'left'
  },
  {
    title: '单元分类名称',
    dataIndex: 'ucnm',
    width: 150,
  },
  {
    title: '单元类型标记',
    dataIndex: 'uctype',
    slots: { customRender: 'uctype' },
    width: 120,
  },
  {
    title: '使用的导则库',
    dataIndex: 'hdbcd',
    width: 150,
    slots: { customRender: 'hdbcd' }
  },
  {
    title: '是否为在建工程',
    dataIndex: 'isconstruction',
    width: 100,
    slots: { customRender: 'isconstruction' }
  },
  // {
  //   title: '是否为特种设备',
  //   dataIndex: 'isspecial',
  //   width: 100,
  //   slots: { customRender: 'isspecial' }
  // },
  // {
  //   title: '是否为专用设备',
  //   dataIndex: 'issingle',
  //   slots: { customRender: 'issingle' },
  //   width: 200,
  // }
]


/**
 * 新增或编辑表单配置
 */
export const addOrUpdateUnitCategorySchema: FormSchema[] = [
  {
    field: 'uccd',
    label: '单元分类代码',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'ucnm',
    label: '单元分类名称',
    component: 'Input',
    componentProps: {},
    required: true,
    colProps: { span: 22 },
  },
  // {
  //   field: 'isspecial',
  //   label: '是否为特种设备',
  //   component: 'RadioGroup',
  //   componentProps: {
  //     options: [
  //       {
  //         label: '是',
  //         value: true
  //       },
  //       {
  //         label: '否',
  //         value: false
  //       }
  //     ]
  //   },
  //   required: true,
  //   defaultValue: true,
  //   colProps: { span: 22 },
  // },
  // {
  //   field: 'issingle',
  //   label: '是否为专用设备',
  //   component: 'RadioGroup',
  //   componentProps: {
  //     options: [,
  //       {
  //         label: '是',
  //         value: true
  //       },
  //       {
  //         label: '否',
  //         value: false
  //       }
  //     ]
  //   },
  //   colProps: { span: 22 },
  //   required: true,
  //   defaultValue: true,
  // },
  {
    field: 'isconstruction',
    label: '是否为在建工程',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '是',
          value: true,
        },
        {
          label: '否',
          value: false,
        },
      ],
    },
    required: true,
    defaultValue: false,
    colProps: { span: 22 },
  },
  {
    field: 'uctype',
    label: '单元类型标记',
    component: 'RadioGroup',
    componentProps: {
      listHeight: 200,
    },
    defaultValue: '1',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'puccd',
    label: '上级单元分类',
    component: 'TreeSelect',
    componentProps: {
      listHeight: 200,
      treeDefaultExpandAll: true,
    },

    colProps: { span: 22 },
  },
  {
    field: 'hdbcd',
    label: '使用的导则库',
    component: 'Select',
    required: true,
    componentProps: {
      listHeight: 200,
    },
    colProps: { span: 22 },
  },
];
