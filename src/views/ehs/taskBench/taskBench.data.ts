import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';

// 查询任务条件
export const userTodoTaskListConditionSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '查询时间',
    component: 'Input',
    settype: 'string',
    required: true,
    colProps: { span: 11 },
    labelWidth: 90,
    slot: 'tm',
    defaultValue: [moment().startOf('month').format('YYYY-MM-DD'),
    moment().endOf('month').format('YYYY-MM-DD')]
  },
  {
    field: 'flag',
    label: '状态',
    component: 'Select',
    required: true,
    colProps: { span: 4 },
    labelWidth: 50,
    ifShow: false,
    componentProps: {
      allowClear: false,
      options: [
        {
          label: '全部',
          value: 2,
        },
        {
          label: '待办',
          value: 0,
        },
        {
          label: '已办',
          value: 1,
        },
      ],
    },
    defaultValue: 2,
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    labelWidth: 80,
  }
]

/**
 * 任务list
 */
export const todoTaskListColumns: BasicColumn[] = [
  {
    title: '任务名称',
    dataIndex: 'itemname',
    fixed: 'left',
    slots: { customRender: 'itemname' },
    width: 250,
  },
  {
    title: '任务分类',
    dataIndex: 'catname',
    slots: { customRender: 'catname' },
    width: 120,
    defaultHidden: true,
  },
  {
    title: "计划执行时间",
    dataIndex: 'plantm',
    slots: { customRender: 'plantm' },
    width: 250,
  },
  {
    title: '单元',
    dataIndex: 'unitname',
    width: 120,
  },
  {
    title: '次数\n执行/计划',
    key: 'planfrequencycount',
    dataIndex: 'planfrequencycount',
    width: 90,
    customHeaderCell: () => {
      return {
        style: {
          whiteSpace: 'pre-wrap',
        }
      }
    },
    customRender: ({ record }) => {
      return (record.execcount ?? '-') + '/' + (record.planfrequencycount ?? '-')
    }
  },
  {
    title: '任务状态',
    dataIndex: 'status',
    width: 90,
    slots: { customRender: 'status' },
  },
  {
    title: '完成时间',
    dataIndex: 'execendtm',
    width: 120,
    customRender: ({ record }) => {
      return record.execendtm ? moment(record.execendtm).format('YYYY-MM-DD') : '-'
    }
  },
  {
    title: '责任部门/责任人',
    dataIndex: 'transInfo',
    slots: { customRender: 'transInfo' },
    width: 200,
  },
  // {
  //   title: '责任部门',
  //   dataIndex: 'transdeptname',
  //   slots: { customRender: 'transdeptname' },
  //   width: 120,
  // },
  // {
  //   title: '负责人',
  //   dataIndex: 'transusername',
  //   slots: { customRender: 'transusername' },
  //   width: 100,
  // }
]

/**
 * 执行记录list
 */
export const executeRecordListColumns: BasicColumn[] = [
  {
    title: '任务名称',
    dataIndex: 'itemname',
    slots: { customRender: 'itemname' },
  },
  {
    title: '单元',
    dataIndex: 'unitname',
    slots: { customRender: 'unitname' },
    width: 200,
  },
  {
    title: '执行人员',
    dataIndex: 'executor',
    width: 110,
  }, {
    title: '执行时间',
    dataIndex: 'executetmstr',
    width: 140,
  },
  {
    title: '状态',
    dataIndex: 'status',
    slots: { customRender: 'status' },
    width: 120,
  },
  {
    title: '执行情况',
    dataIndex: 'execdescription',
    width: 280,
  }
]

export const transmitFormSchema: FormSchema[] = [
  {
    field: 'transdeptid',
    label: '转交部门',
    component: 'Select',
    colProps: { span: 22 },

  },
  {
    field: 'transuserid',
    label: '转交人员',
    component: 'Select',
    colProps: { span: 22 },
  }
]


export const addOrUpdateExecuteRecordSchema: FormSchema[] = [
  {
    field: 'itemname',
    label: '任务项名称',
    component: 'Input',
    colProps: { span: 23 },
    componentProps: {
      disabled: true,
    }
  },
  {
    field: 'unitid',
    label: '关联的单元',
    colProps: { span: 23 },
    ifShow: false,
    helpMessage: '如任务对指定的1个单元执行请选定一个单元。如任务需要对多个单元执行请勿选择，可以在内容及要求中进行说明。',
    component: 'ApiTreeSelect',
    itemProps: { validateTrigger: 'blur' },
    componentProps: ({ formActionType, formModel }) => {

    },
  },
  {
    field: 'hazardid',
    component: 'ApiSelect',
    colProps: { span: 23 },
    ifShow: false,
    helpMessage: '如任务对指定的1个危险源执行请选定一个危险源。如任务需要对多个危险源执行请勿选择，可以在内容及要求中进行说明。',
    label: '单元中的危险源',
    itemProps: { validateTrigger: 'blur' },
  },
  {
    field: 'facilityid',
    component: 'ApiSelect',
    colProps: { span: 23 },
    helpMessage: '如任务对指定的1个设备设施执行请选定一个设备设施。如任务需要对多个设备设施执行请勿选择，可以在内容及要求中进行说明。',
    ifShow: false,
    label: '单元中的设备设施',
    itemProps: { validateTrigger: 'blur' },

  },
  {
    field: 'exectm',
    label: '执行起止时间',
    component: 'RangePicker',
    setType: 'string',
    colProps: { span: 13 },
    componentProps: {
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      getPopupContainer: (triggerNode) => document.body,
    },
    required: true,
    defaultValue: [moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')],
  },
  {
    field: 'executetmstr',
    label: '执行时间',
    labelWidth: 80,
    component: 'Input',
    colProps: { span: 10 },
    required: true,
    defaultValue: moment().format('YYYY年MM月DD日'),
  },
  {
    field: 'executor',
    label: '执行人员',
    component: 'Input',
    required: true,
    colProps: { span: 23 },
  },
  {
    field: 'hasexec',
    label: '是否执行完成',
    component: 'RadioButtonGroup',
    helpMessage: '选择“任务执行完成”将自动完成本次任务；选择“任务未全部完成”可以暂时保存执行记录，在任务执行完成后再进行修改。',
    required: true,
    slot: 'hasexec',
    defaultValue: true,
    colProps: {
      span: 12
    }
  },
  {
    field: 'isnormal',
    label: '是否执行正常',
    required: true,
    component: 'RadioButtonGroup',
    helpMessage: '任务执行中未发现任何异常选择“均正常”，如执行过程中发现有异常或隐患选择“发现异常/隐患”并在“上报隐患”模块中进行填报。',
    slot: 'isnormal',
    defaultValue: true,
    colProps: {
      span: 11
    }
  },
  {
    field: 'execdescription',
    label: '执行内容描述',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 5, maxRows: 5 },
      placeHolder: '请填写任务的执行过程、结果、结论等信息。'
    },
    colProps: { span: 23 },
  },
  {
    field: 'execattachment',
    label: '执行照片/记录单',
    required: true,
    helpMessage: '上传任务执行的照片、方案等。',
    component: 'Upload',
    colProps: { span: 10 },
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
  // {
  //   field: 'checkrecordid',
  //   label: '检查表',
  //   component: 'Input',
  //   labelWidth: 70,
  //   slot: 'checkForm',
  //   colProps: { span: 6 },
  // },
  {
    field: 'execsign',
    label: '执行负责人签字',
    labelWidth: 120,
    component: 'Input',
    colProps: { span: 10 },
    slot: 'sign'
  }
]


// 查看检查表表格配置

export const checkFormColumns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
  },
  {
    title: '检查项目',
    dataIndex: 'parentGroupName',
    width: 100,
    fixed: 'left',
    align: 'center',
    customCell: (record) => {
      return {
        rowSpan: record?.rowSpan ?? 1,
      };
    },
  },
  {
    title: '检查内容',
    dataIndex: 'field',
    width: 150,
    align: 'center',
    fixed: 'left',
  },
  {
    title: '检查结果',
    dataIndex: 'result',
    width: 100,
    slots: { customRender: 'result' },
    align: 'center',
  },
  {
    title: '备注',
    dataIndex: 'description',
    width: 150,
    slots: { customRender: 'description' },
  },
  {
    title: '附件',
    slots: { customRender: 'images' },
    dataIndex: 'images',
    width: 100,
  }

]
