import { h, reactive } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';
import { getDeptUsers } from '/@/api/public/public';

export const ehsRecordConditionFormSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '时间',
    component: 'Input',
    settype: 'string',
    required: true,
    colProps: { span: 7 },
    slot: 'tm',
    defaultValue: [moment().startOf('year').format('YYYY-MM-DD'),
    moment().endOf('year').format('YYYY-MM-DD')]
  },
  {
    field: 'category',
    label: '任务大类',
    component: 'Select',
    colProps: { span: 5 },
  },
  {
    field: 'unitid',
    label: '单元',
    component: 'TreeSelect',
    colProps: { span: 5 },
  },
  {
    field: 'title',
    label: '标题',
    colProps: { span: 6 },
    component: 'Input',
  },
  {
    field: 'deptid',
    label: '责任部门',
    component: 'TreeSelect',
    colProps: { span: 5 },
  },
  {
    field: 'userid',
    label: '负责人',
    component: 'ApiSelect',
    colProps: { span: 5 },
    ifShow: ({ values }) => values.deptid,
    componentProps: ({ formModel, formActionType }) => {
      return {
        api: getDeptUsers,
        params: formModel.deptid,
        immediate: false, //是否立即请求接口，否则将在第一次点击时候触发请求
        listHeight: 220,
        getPopupContainer: (triggerNode) => document.body,
        fieldNames: { label: 'name', value: 'id', phone: 'phone' },
      };
    },
  },
]



/**
 * 任务list
 */
export const todoTaskListColumns: BasicColumn[] = [
  {
    title: '任务名称',
    dataIndex: 'itemname',
    fixed: 'left',
    width: 250,
  },
  {
    title: '任务分类',
    dataIndex: 'catname',
    slots: { customRender: 'catname' },
    width: 120,
    defaultHidden: true,
  },
  {
    title: "计划执行时间",
    dataIndex: 'plantm',
    slots: { customRender: 'plantm' },
    width: 250,
  },
  {
    title: '单元',
    dataIndex: 'unitname',
    width: 120,
  },
  {
    title: '次数\n执行/计划',
    key: 'planfrequencycount',
    dataIndex: 'planfrequencycount',
    width: 90,
    customHeaderCell: () => {
      return {
        style: {
          whiteSpace: 'pre-wrap',
        }
      }
    },
    customRender: ({ record }) => {
      return (record.execcount ?? '-') + '/' + (record.planfrequencycount ?? '-')
    }
  },
  {
    title: '任务状态',
    dataIndex: 'status',
    width: 90,
    slots: { customRender: 'status' },
  },
  {
    title: '完成时间',
    dataIndex: 'execendtm',
    width: 120,
    customRender: ({ record }) => {
      return record.execendtm ? moment(record.execendtm).format('YYYY-MM-DD') : '-'
    }
  },
  {
    title: '责任部门/责任人',
    dataIndex: 'transInfo',
    slots: { customRender: 'transInfo' },
    width: 200,
  },
]

/**
 * 隐患治理List
 */

export const troubleCorrectListColumns: BasicColumn[] = [
  {
    title: '上报/发现时间',
    dataIndex: 'tm',
    width: 140,
    fixed: 'left',
    customRender: ({ record }) => {
      return record.tm ? moment(record.tm).format('YYYY-MM-DD') : '-'
    },
    sorter: {
      compare: (a, b) => {
        return moment(a.tm) - moment(b.tm)
      },
    },
  },
  {
    title: '隐患名称',
    dataIndex: 'title',
    fixed: 'left',
    width: 300,
  },
  {
    title: '单元',
    dataIndex: 'unit',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.unit.localeCompare(b.unit);
      },
    },
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 90,
    slots: { customRender: 'ismajor' },
    sorter: {
      compare: (a, b) => {
        return a.ismajor.localeCompare(b.ismajor);
      },
    },
  },
  {
    title: '风险等级',
    dataIndex: 'risklevel',
    width: 100,
    slots: { customRender: 'risklevel' }
  },
  {
    title: '是否上报',
    dataIndex: 'isreport',
    width: 80,
    slots: { customRender: 'boolSlot' }
  },
  {
    title: '整改治理状态',
    dataIndex: 'status',
    width: 120,
    slots: { customRender: 'troublestatus' },
    sorter: {
      compare: (a, b) => {
        return a.status.localeCompare(b.status);
      },
    },
  },
  {
    title: '整改时限',
    dataIndex: 'reqendtm',
    width: 120,
    customRender: ({ record }) => {
      return record.reqendtm ? moment(record.reqendtm).format('YYYY-MM-DD') : '-'
    },
    sorter: {
      compare: (a, b) => {
        return moment(a.reqendtm) - moment(b.reqendtm)
      },
    },
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.resdeptname.localeCompare(b.resdeptname);
      },
    },
  },
  {
    title: '责任人',
    dataIndex: 'resusername',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.resusername.localeCompare(b.resusername);
      },
    },
  },
  {
    title: '手机号',
    dataIndex: 'resuserphone',
    width: 120,
  },
  {
    title: '整改完成时间',
    dataIndex: 'correctendtm',
    width: 120,
    customRender: ({ record }) => {
      return record.correctendtm ? moment(record.correctendtm).format('YYYY-MM-DD') : '-'
    }
  }
]
