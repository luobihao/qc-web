import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { queryUnitCategory } from '/@/api/ehs/unitCategory';
import { Tag } from 'ant-design-vue';
import { getDeptUsers } from '/@/api/public/public';
import { queryEnumOption } from '/@/api/qms/exchangeData';
/**
 * 单元表配置
 */
export const unitDivisionTableColumns: BasicColumn[] = [
  // {
  //   title: '单元代码',
  //   dataIndex: 'unitid',
  //   width: 150,
  // },
  {
    title: '单元名称',
    dataIndex: 'unitname',
    width: 200,
    align: 'left',
    slots: { customRender: 'unitname' }
  },
  {
    title: '单元分类',
    dataIndex: 'uccd',
    width: 150,
    defaultHidden: true,
    slots: { customRender: 'uccd' }
  },
  {
    title: '经度',
    dataIndex: 'longitude',
    defaultHidden: true,
    width: 120,
  }, {
    title: '纬度',
    dataIndex: 'latitude',
    width: 120,
    defaultHidden: true,
  }, {
    title: '具体位置',
    dataIndex: 'location',
    width: 200,
    defaultHidden: true,
  }, {
    title: '介绍信息',
    dataIndex: 'introduction',
    width: 150,
    defaultHidden: true,
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 100,
  },
  {
    title: '责任人',
    dataIndex: 'resusername',
    width: 100,
  },
  {
    title: '责任人手机号',
    dataIndex: 'resuserphone',
    width: 150,
  },
  {
    title: '安全员',
    dataIndex: 'safeusername',
    width: 100,
  },
  {
    title: '安全员手机号',
    dataIndex: 'safeuserphone',
    width: 150,
  },
]

/**
 * 定义单查询条件表单
 */
export const queryUnitConditionFormSchema: FormSchema[] = [
  // {
  //   field: 'uccd',
  //   label: '所属单元分类',
  //   component: 'TreeSelect',
  //   colProps: { span: 5 },
  //   componentProps: {
  //     listHeight: 200,
  //   }
  // },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 5 },
    defaultValue: '',
  },
];

export const setDutyModalSchemas: FormSchema[] = [
  {
    field: 'unitids',
    label: '单元',
    component: 'TreeSelect',
    colProps: { span: 23 },
    componentProps: {},
    required: true
  },
  {
    field: 'resdeptid',
    label: '责任部门',
    component: 'TreeSelect',
    required: true,
    colProps: { span: 23 },
  },
  {
    field: 'resuserid',
    label: '责任人',
    component: 'Select',
    colProps: { span: 23 },
    ifShow: ({ values }) => values.resdeptid,

  },
  {
    field: 'resuserphone',
    label: '责任人手机号',
    colProps: { span: 23 },
    component: 'Input',
    ifShow: ({ values }) => values.resdeptid,
  },
  {
    field: 'safeuserid',
    label: '安全员',
    component: 'Select',
    colProps: { span: 23 },
    ifShow: ({ values }) => values.resdeptid,

  },
  {
    field: 'safeuserphone',
    label: '安全员手机号',
    component: 'Input',
    colProps: { span: 23 },
    ifShow: ({ values }) => values.resdeptid,
  }
]
