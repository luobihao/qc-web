import { h, reactive } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';
import { Tag } from 'ant-design-vue';

/**
 * 单元统计表格列配置
 */
export const unitStatisticsColumn: BasicColumn[] = [
  {
    title: '单元编码',
    dataIndex: 'unitid',
    ifShow: false,
  },
  {
    title: '标题',
    dataIndex: 'title',
    width: 200,
    align: 'left',
    fixed: 'left',
    slots: { customRender: 'title' },
  },
  {
    title: '类别',
    dataIndex: 'categoryname',
    width: 120,
  },
  {
    title: '项目',
    dataIndex: 'projectname',
    width: 120,
  },
  {
    title: '名称',
    dataIndex: 'itemname',
    width: 120,
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 80,
    slots: { customRender: 'ismajor' },
  },
  {
    title: '辨识时间',
    dataIndex: 'tm',
    width: 150,
    customRender: ({ record }) => {
      return record.tm ? moment(record.tm).format('YYYY-MM-DD') : '';
    }
  },
  {
    title: '辨识人',
    dataIndex: 'username',
    width: 100
  },
  {
    title: '辨识结果',
    dataIndex: 'identificationresult',
    width: 120,
    slots: { customRender: 'identificationresult' },
  },
  {
    title: '确认结果',
    dataIndex: 'confirmresult',
    width: 120,
    slots: { customRender: 'confirmresult' },
  },
  {
    title: '确认人',
    dataIndex: 'confirmusername',
    width: 100
  },
  {
    title: '具体部位',
    dataIndex: 'position',
    width: 120
  },
  {
    title: '事故诱因',
    dataIndex: 'couse',
    width: 120
  },
  {
    title: '可能导致的危害',
    dataIndex: 'maycouseharm',
    width: 150
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 200
  }
]

/**
 * 按单元查找指定危险源表格
 */
export const unitViewHazardColumn: BasicColumn[] = reactive([
  {
    title: '标题',
    dataIndex: 'title',
    width: 200,
    align: 'left',
    fixed: 'left',
  },
  {
    title: '类别',
    dataIndex: 'categoryname',
    width: 150,
    customFilterDropdown: true,
    onFilter: (value, record) => record.categoryname.toString().toLowerCase().includes(value.toLowerCase()),
    sorter: {
      compare: (a, b) => {
        return a.categoryname.localeCompare(b.categoryname);
      },
    },
  },
  {
    title: '项目',
    dataIndex: 'projectname',
    width: 150,
    customFilterDropdown: true,
    onFilter: (value, record) => record.projectname.toString().toLowerCase().includes(value.toLowerCase()),
    sorter: {
      compare: (a, b) => {
        return a.projectname.localeCompare(b.projectname);
      },
    },
  },
  {
    title: '名称',
    dataIndex: 'hazardname',
    width: 200,
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 150,
    sorter: {
      compare: (a, b) => {
        return a.ismajor - b.ismajor;
      },
    },
    slots: { customRender: 'ismajor' }
  },
  {
    title: '辨识结果',
    dataIndex: 'identificationresult',
    width: 250,
    onFilter: (value: string, record: TableDataType) => record.identificationresult === value,
    slots: { customRender: 'identificationresult' },
  },
  {
    title: '风险等级',
    dataIndex: 'risklevel',
    width: 200,
    slots: { customRender: 'risklevel' },
  },
  {
    title: '最新辨识时间',
    dataIndex: 'recentidentificationtm',
    width: 180,
    customRender: ({ record }) => {
      return record.recentidentificationtm ? moment(record.recentidentificationtm).format('YYYY-MM-DD') : ''
    }
  },
  {
    title: '具体部位',
    dataIndex: 'position',
    width: 120,
  },
  {
    title: '事故诱因',
    dataIndex: 'couse',
    width: 200
  },
  {
    title: '可能导致的危害',
    dataIndex: 'maycouseharm',
    width: 200,
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 200
  }
])

/**
 * 获取指定单元的危险源辨识记录表格-getOneUnit
 */
export const getUnitIdentifyRecordColumn: BasicColumn[] = [
  {
    title: 'id',
    dataIndex: 'identifyid',
    ifShow: false,
  },
  {
    title: '标题',
    dataIndex: 'title',
    width: 200,
    align: 'left',
    fixed: 'left',
    slots: { customRender: 'title' },
    sorter: {
      compare: (a, b) => {
        return a.title.localeCompare(b.title);
      },
    },
  },
  {
    title: '类别',
    dataIndex: 'categoryname',
    width: 120,
  },
  {
    title: '项目',
    dataIndex: 'projectname',
    width: 120,
  },
  {
    title: '名称',
    dataIndex: 'itemname',
    width: 120,
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 80,
    slots: { customRender: 'ismajor' },
  },
  {
    title: '辨识时间',
    dataIndex: 'tm',
    width: 150,
    customRender: ({ record }) => {
      return record.tm ? moment(record.tm).format('YYYY-MM-DD') : '';
    }
  },
  {
    title: '辨识人',
    dataIndex: 'username',
    width: 100
  },
  {
    title: '辨识结果',
    dataIndex: 'identificationresult',
    width: 120,
    slots: { customRender: 'identificationresult' },
  },
  {
    title: '确认结果',
    dataIndex: 'confirmresult',
    width: 120,
    slots: { customRender: 'confirmresult' },
  },
  {
    title: '确认人',
    dataIndex: 'confirmusername',
    width: 100
  },
  {
    title: '具体部位',
    dataIndex: 'position',
    width: 120
  },
  {
    title: '事故诱因',
    dataIndex: 'couse',
    width: 120
  },
  {
    title: '可能导致的危害',
    dataIndex: 'maycouseharm',
    width: 150
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 200
  }
]


/**
 * 危险源辨识
 */
export const identifyFormSchema: FormSchema[] = [
  {
    field: 'maycouseharm',
    label: '可能导致的后果',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    field: 'identificationresult',
    label: '辨识结果',
    component: 'RadioGroup',
    defaultValue: 0,
    required: true,
    colProps: { span: 11 },
    componentProps: {
      options: [{
        label: '不存在',
        value: 0
      },
      {
        label: '存在',
        value: 1
      }]
    },
  },
  {
    field: 'undercontrol',
    label: '防控措施',
    required: false,
    component: 'RadioGroup',
    colProps: { span: 11 },
    defaultValue: true,
    componentProps: ({ formModel, formActionType }) => {
      return {
        options: [{
          label: '防控措施有效',
          value: true
        },
        {
          label: '防控措施失效或破坏',
          value: false
        }],
      }
    },
    ifShow: ({ values }) => {
      if (values.identificationresult !== 0) {
        return true;
      } else {
        return false;
      }
    }
  },
  {
    field: 'unsafefactor',
    label: '不安全因素',
    component: 'CheckboxGroup',
    required: true,
    colProps: { span: 22 },
    componentProps: {
      options: [{
        label: '人的不安全行为',
        value: 1
      },
      {
        label: '物的不安全状态',
        value: 2
      },
      {
        label: '管理缺陷',
        value: 4
      }]
    },
    ifShow: ({ values }) => {
      if (values.identificationresult !== 0 && !values.undercontrol) {
        return true;
      } else {
        return false;
      }
    }
  },
  {
    field: 'title',
    label: '危险源/隐患标题',
    component: 'Input',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'description',
    label: '危险源/隐患描述',
    component: 'InputTextArea',
    colProps: { span: 22 },
    ifShow: ({ values }) => {
      if (values.identificationresult !== 0) {
        return true;
      } else {
        return false;
      }
    }
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
    ifShow: ({ values }) => {
      if (values.identificationresult !== 0) {
        return true;
      } else {
        return false;
      }
    }
  },
];


/**
 * 风险评价表单
 */
export const riskEvaluationFormSchema: FormSchema[] = [
  {
    field: 'method',
    label: '评价方法',
    component: 'RadioGroup',
    slot: 'method',
    required: true,
    defaultValue: 1,
    colProps: { span: 22 },
    componentProps: {}
  },
  {
    field: 'riskValue',
    colProps: { span: 22 },
    component: 'Input',
    slot: 'riskValue',
    ifShow: ({ values }) => {
      if (values.method == 1) {
        return false;
      } else return true
    },
  },
  {
    field: 'risklevel',
    label: '风险等级',
    component: 'RadioGroup',
    required: true,
    componentProps: {
      disabled: false
    },
    colProps: { span: 20 },
    slot: 'risklevel',
  },
  {
    field: 'tm',
    label: '评价时间',
    component: 'DatePicker',
    colProps: { span: 11 },
    required: true,
    componentProps: ({ formModel }) => {
      return {
        picker: 'date',
        valueFormat: 'YYYY-MM-DD',
        allowClear: false,
        getPopupContainer: triggerNode => document.body
      }
    },
    defaultValue: moment().format('YYYY-MM-DD')
  },
  {
    field: 'byuser',
    label: '评价人',
    required: true,
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    }
  },
]

/**
 * 危险源清单视图台账查询条件
 */
export const listViewConditionFormSchema: FormSchema[] = [
  {
    field: 'ishidden',
    label: '类别',
    component: 'Select',
    componentProps: {
      options: [
        { label: '一类', value: '0' },
        { label: '二类', value: '1' },
      ]
    },
    labelWidth: 50,
    colProps: { span: 3 },
  },
  {
    field: 'ismajor',
    label: '级别',
    component: 'Select',
    componentProps: {
      options: [
        { label: '一般', value: '0' },
        { label: '重大', value: '1' },
      ]
    },
    labelWidth: 50,
    colProps: { span: 3 },
  },
  {
    field: 'risklevel',
    label: '风险等级',
    component: 'Select',
    labelWidth: 70,
    colProps: { span: 3 },
  },
  {
    field: 'unitid',
    label: '单元',
    component: 'TreeSelect',
    labelWidth: 50,
    colProps: { span: 4 },
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    labelWidth: 60,
    colProps: { span: 3 },
  },
  {
    field: 'includechilds',
    label: '包含子级单元',
    component: 'Checkbox',
    labelWidth: 100,
    defaultValue: true,
    colProps: { span: 3 },
  },
  {
    field: 'isconstruction',
    label: '只显示在建工程',
    labelWidth: 110,
    component: 'Checkbox',
    defaultValue: false,
    colProps: { span: 3 },
  },
];

/**
 * 按单元和周期查找指定危险源表格
 */
export const listViewHazardColumnbyUnit: BasicColumn[] = [
  {
    title: '单元',
    dataIndex: 'unit',
    ifShow: false
  },
  {
    title: '标题',
    dataIndex: 'name',
    slots: { customRender: 'name' },
    width: 250,
    align: 'left',
    fixed: 'left'
  },
  {
    title: '类别',
    dataIndex: 'categoryname',
    slots: { customRender: 'categoryname' },
    width: 180,
    sorter: {
      compare: (a, b) => {
        return a.categoryname && b.categoryname && a.categoryname.localeCompare(b.categoryname);
      },
    },
  },
  {
    title: '项目',
    dataIndex: 'projectname',
    slots: { customRender: 'projectname' },
    width: 200,
    sorter: {
      compare: (a, b) => {
        return a.projectname && b.projectname && a.projectname.localeCompare(b.projectname);
      },
    },
  },
  {
    title: '名称',
    dataIndex: 'hazardname',
    width: 200,
    customRender: ({ record }) => {
      return record.hazardname ? record.hazardname : '-'
    }
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 150,
    sorter: {
      compare: (a, b) => {
        return a.ismajor - b.ismajor
      },
    },
    slots: { customRender: 'ismajor' }
  },
  {
    title: '危险源类别',
    ifShow: false,
    dataIndex: 'identificationresult',
    width: 180,
    slots: { customRender: 'identificationresult' },
    sorter: {
      compare: (a, b) => {
        return a.identificationresult && b.identificationresult && a.identificationresult - b.identificationresult;
      },
    },
  },
  {
    title: '风险等级',
    dataIndex: 'risklevel',
    width: 150,
    slots: { customRender: 'risklevel' },
    sorter: {
      compare: (a, b) => {
        return a.risklevel && b.risklevel && a.risklevel - b.risklevel;
      },
    },
  },
  {
    title: '最新辨识时间',
    dataIndex: 'recentidentificationtm',
    width: 150,
    slots: { customRender: 'recentidentificationtm' },
    customRender: ({ record }) => {
      return record.recentidentificationtm ? moment(record.recentidentificationtm).format('YYYY-MM-DD') : '-'
    }
  },
  {
    title: '是否上报',
    dataIndex: 'isreport',
    width: 90,
    customRender: ({ record }) => {
      if (record.isreport !== null || record.isreport !== undefined) {
        return h(Tag, { color: record.isreport === true ? 'red' : 'green' }, record.isreport === true ? '是' : '否')
      } else {
        return '-'
      }

    }
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 120,
    customRender: ({ record }) => {
      return record.resdeptname ? record.resdeptname : '-'
    }
  },
  {
    title: '责任人',
    dataIndex: 'resusername',
    width: 120,
    customRender: ({ record }) => {
      return record.resusername ? record.resusername : '-'
    }
  },
  {
    title: '电话',
    dataIndex: 'resuserphone',
    width: 120,
    customRender: ({ record }) => {
      return record.resuserphone ? record.resuserphone : '-'
    }
  },
  {
    title: '具体部位',
    dataIndex: 'position',
    width: 150,
    customRender: ({ record }) => {
      return record.position ? record.position : '-'
    }
  },
  {
    title: '事故诱因',
    dataIndex: 'couse',
    width: 150,
    customRender: ({ record }) => {
      return record.couse ? record.couse : '-'
    }
  },
  {
    title: '可能导致的危害',
    dataIndex: 'maycouseharm',
    width: 150,
    customRender: ({ record }) => {
      return record.maycouseharm ? record.maycouseharm : '-'
    }
  },
  {
    title: '描述',
    dataIndex: 'description',
    customRender: ({ record }) => {
      return record.description ?? '-'
    },
    width: 150,
  }
]

export const listViewHazardColumnbyHazard: BasicColumn[] = [
  {
    title: '类别',
    dataIndex: 'categoryname',
    ifShow: false
  },
  {
    title: '项目',
    dataIndex: 'projectname',
    ifShow: false
  },
  {
    title: '名称',
    dataIndex: 'name',
    slots: { customRender: 'name' },
    width: 250,
    align: 'left',
    fixed: 'left'
  },

  {
    title: '单元',
    dataIndex: 'unitname',
    slots: { customRender: 'unitname' },
    sorter: {
      compare: (a, b) => {
        return a.unitname && b.unitname && a.unitname.localeCompare(b.unitname);
      },
    },
    width: 150,
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 150,
    sorter: {
      compare: (a, b) => {
        return a.ismajor - b.ismajor
      },
    },
    slots: { customRender: 'ismajor' }
  },
  {
    title: '危险源类别',
    dataIndex: 'identificationresult',
    width: 250,
    ifShow: false,
    slots: { customRender: 'identificationresult' },
    sorter: {
      compare: (a, b) => {
        return a.identificationresult && b.identificationresult && a.identificationresult - b.identificationresult;
      },
    },

  },
  {
    title: '风险等级',
    dataIndex: 'risklevel',
    width: 200,
    slots: { customRender: 'risklevel' },
    sorter: {
      compare: (a, b) => {
        return a.risklevel && b.risklevel && a.risklevel - b.risklevel;
      },
    },
  },
  {
    title: '最新辨识时间',
    dataIndex: 'recentidentificationtm',
    width: 150,
    slots: { customRender: 'recentidentificationtm' },
    customRender: ({ record }) => {
      return record.recentidentificationtm ? moment(record.recentidentificationtm).format('YYYY-MM-DD') : '-'
    }
  },
  {
    title: '是否上报',
    dataIndex: 'isreport',
    width: 90,
    customRender: ({ record }) => {
      if (record.isreport !== null || record.isreport !== undefined) {
        return h(Tag, { color: record.isreport === true ? 'red' : 'green' }, record.isreport === true ? '是' : '否')
      } else {
        return '-'
      }

    }
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname',
    width: 120,
    customRender: ({ record }) => {
      return record.resdeptname ?? '-'
    }
  },
  {
    title: '责任人',
    dataIndex: 'resusername',
    width: 120,
    customRender: ({ record }) => {
      return record.resusername ?? '-'
    }
  },
  {
    title: '电话',
    dataIndex: 'resuserphone',
    width: 120,
    customRender: ({ record }) => {
      return record.resuserphone ?? '-'
    }
  },
  {
    title: '具体部位',
    dataIndex: 'position',
    width: 150,
    customRender: ({ record }) => {
      return record.position ?? '-'
    }
  },
  {
    title: '事故诱因',
    dataIndex: 'couse',
    width: 150,
    customRender: ({ record }) => {
      return record.couse ?? '-'
    }
  },
  {
    title: '可能导致的危害',
    dataIndex: 'maycouseharm',
    width: 150,
    customRender: ({ record }) => {
      return record.maycouseharm ?? '-'
    }
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 150,
    customRender: ({ record }) => {
      return record.description ?? '-'
    }
  }
]

export const chooseHazardTableColumns: BasicColumn[] = [
  {
    title: '导则库代码',
    dataIndex: 'hdbcd',
    width: 50,
    ifShow: false
  },
  {
    title: '导则库名称',
    dataIndex: 'hdbnm',
    width: 80,
  },
  {
    title: '是否在建',
    dataIndex: 'isconstruction',
    width: 30,
    slots: { customRender: 'isconstruction' },
  }
]

/**
 * 确认辨识记录查询表单
 */
export const queryConfirmRecordSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '辨识时间',
    component: 'RangePicker',
    setType: 'string',
    labelWidth: 100,
    colProps: { span: 6 },
    componentProps: {
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false
    },
    required: true,
    defaultValue: [moment().startOf('month').format('YYYY-MM-DD'), moment().endOf('month').format('YYYY-MM-DD')],
  },
  {
    field: 'confirmstatus',
    label: '确认状态',
    component: 'Select',
    labelWidth: 80,
    componentProps: {
      allowClear: false,
      options: [
        { label: '全部', value: 0 },
        { label: '未确认', value: 1 },
        { label: '已确认', value: 2 },
      ]
    },
    defaultValue: 0,
    colProps: { span: 4 },
  },
  {
    field: 'unitid',
    label: '单元',
    component: 'TreeSelect',
    componentProps: {

    },
    colProps: { span: 4 },
  },
  {
    field: 'ishidden',
    label: '类别',
    component: 'Select',
    componentProps: {
      options: [
        { label: '一类', value: '0' },
        { label: '二类', value: '1' },
      ]
    },
    colProps: { span: 3 },
  },
  {
    field: 'ismajor',
    label: '级别',
    component: 'Select',
    componentProps: {
      options: [
        { label: '一般', value: '0' },
        { label: '重大', value: '1' },
      ]
    },
    colProps: { span: 3 },
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    labelWidth: 70,
    colProps: { span: 4 },
  },
]
/**
 * 危险源确认表格列
 */
export const confirmRecordColumns: BasicColumn[] = [
  {
    title: 'identifyid',
    dataIndex: 'identifyid',
    ifShow: false,
  },
  {
    title: '标题',
    dataIndex: 'title',
    width: 200,
    align: 'left',
    fixed: 'left',
    slots: { customRender: 'title' },
    sorter: {
      compare: (a, b) => {
        return a.title.localeCompare(b.title);
      },
    },
  },
  {
    title: '单元',
    dataIndex: 'unitname',
    fixed: 'left',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.unitname.localeCompare(b.unitname);
      },
    },
  },
  {
    title: '类别',
    dataIndex: 'categoryname',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.categoryname.localeCompare(b.categoryname);
      },
    },
  },
  {
    title: '项目',
    dataIndex: 'projectname',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.projectname.localeCompare(b.projectname);
      },
    },
  },
  {
    title: '名称',
    dataIndex: 'itemname',
    width: 120,
    sorter: {
      compare: (a, b) => {
        return a.itemname.localeCompare(b.itemname);
      },
    },
  },
  {
    title: '一般/重大',
    dataIndex: 'ismajor',
    width: 80,
    slots: { customRender: 'ismajor' },
  },
  {
    title: '辨识时间',
    dataIndex: 'tm',
    width: 150,
    customRender: ({ record }) => {
      return record.tm ? moment(record.tm).format('YYYY-MM-DD') : ''
    },
    sorter: {
      compare: (a, b) => {
        return moment(a.tm) - moment(b.tm)
      },
    },
  },
  {
    title: '辨识人',
    dataIndex: 'username',
    width: 100
  },
  {
    title: '辨识结果',
    dataIndex: 'identificationresult',
    width: 120,
    slots: { customRender: 'identificationresult' },
  },
  {
    title: '确认结果',
    dataIndex: 'confirmresult',
    width: 120,
    slots: { customRender: 'confirmresult' },
  },
  {
    title: '确认人',
    dataIndex: 'confirmusername',
    width: 100
  },
  {
    title: '具体部位',
    dataIndex: 'position',
    width: 120
  },
  {
    title: '事故诱因',
    dataIndex: 'couse',
    width: 120
  },
  {
    title: '可能导致的危害',
    dataIndex: 'maycouseharm',
    width: 150
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 200
  }
]

/**
 * 确认辨识表单
 */
export const confirmIdentifyFormSchema: FormSchema[] = [
  {
    field: 'confirmresult',
    label: '确认的辨识结果',
    component: 'RadioGroup',
    colProps: { span: 23 },
    componentProps: {
      options: [
        { label: '不存在', value: 0 },
        { label: '存在(第一类)', value: 1 },
        { label: '存在(第二类)', value: 2 },
      ],
    },
    defaultValue: 0,
    required: true,
  },
  {
    field: 'type',
    label: ' ',
    component: 'RadioButtonGroup',
    colProps: { span: 23 },
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '新危险源', value: 0 },
        { label: '已存在的危险源', value: 1 },
      ],
    }
  },
  {
    field: "hazardid",
    label: "危险源",
    component: "Select",
    colProps: { span: 23 },
    componentProps: {

    }
  },
  {
    field: 'confirmdescription',
    label: '确认结果描述',
    colProps: { span: 23 },
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 3 }
    }
  }
]

export const updateIdentifyRecordSchema: FormSchema[] = [
  {
    field: 'identifyid',
    label: '辨识ID',
    ifShow: false,
  },
  {
    field: 'title',
    label: '标题',
    component: 'Input',
    colProps: { span: 22 },
  },
  {
    field: 'tm',
    label: '辨识时间',
    component: 'DatePicker',
    colProps: { span: 22 },
    componentProps: {
      style: 'width:100%',
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false,
      getPopupContainer: triggerNode => document.body
    },
    required: true,
  },
  {
    field: 'username',
    label: '辨识人员',
    component: 'Input',
    colProps: { span: 22 },
  },
  {
    field: 'identificationresult',
    label: '辨识结果',
    component: 'RadioGroup',
    defaultValue: 0,
    required: true,
    colProps: { span: 11 },
    componentProps: {
      options: [{
        label: '不存在',
        value: 0
      },
      {
        label: '存在',
        value: 1
      }]
    },
  },
  {
    field: 'undercontrol',
    label: '防控措施',
    required: false,
    component: 'RadioGroup',
    colProps: { span: 22 },
    defaultValue: true,
    componentProps: ({ formModel, formActionType }) => {
      return {
        options: [{
          label: '防控措施有效',
          value: true
        },
        {
          label: '防控措施失效或破坏',
          value: false
        }],
      }
    },
    ifShow: ({ values }) => {
      if (values.identificationresult !== 0) {
        return true;
      } else {
        return false;
      }
    }
  },
  {
    field: 'unsafefactor',
    label: '不安全因素',
    component: 'CheckboxGroup',
    required: true,
    colProps: { span: 22 },
    componentProps: {
      options: [{
        label: '人的不安全行为',
        value: 1
      },
      {
        label: '物的不安全状态',
        value: 2
      },
      {
        label: '管理缺陷',
        value: 4
      }]
    },
    ifShow: ({ values }) => {
      if (values.identificationresult !== 0 && !values.undercontrol) {
        return true;
      } else {
        return false;
      }
    }
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
    ifShow: ({ values }) => {
      if (values.identificationresult !== 0) {
        return true;
      } else {
        return false;
      }
    }
  },
]

// 日志相关表格列配置
// 辨识日志表
export const identifyLogColumns: BasicColumn[] = [
  {
    title: '辨识时间',
    dataIndex: 'tm',
    slots: { customRender: 'tm' }
  },
  {
    title: '辨识人',
    dataIndex: 'username'
  },
  {
    title: '辨识结果',
    dataIndex: 'identificationresult',
    slots: { customRender: 'identificationresult' }
  },
]
// 评价日志表
export const evaluateLogColumns: BasicColumn[] = [
  {
    title: '评价时间',
    dataIndex: 'tm',
    slots: { customRender: 'tm' }
  },
  {
    title: '评价人',
    dataIndex: 'byuser'
  },
  {
    title: '风险等级',
    dataIndex: 'risklevel',
    slots: { customRender: 'risklevel' }
  }
]
// 整改日志表
export const correctLogColumns: BasicColumn[] = [
  {
    title: '发现/上报时间',
    dataIndex: 'tm',
    slots: { customRender: 'tm' }
  },
  {
    title: '状态',
    dataIndex: 'status',
    slots: { customRender: 'status' }
  },
  {
    title: '责任部门',
    dataIndex: 'resdeptname'
  },
  {
    title: '负责人',
    dataIndex: 'resusername'
  },
  {
    title: '手机号',
    dataIndex: 'resuserphone'
  }
  // {
  //   title: '是否验收通过',
  //   dataIndex: 'hascheck',
  //   cusTomRender: ({ record }) => {
  //     return record.hascheck == 0 ? '否' : '是'
  //   }
  // },
  // {
  //   title: '验收人',
  //   dataIndex: 'checkusername'
  // },
  // {
  //   title: '验收时间',
  //   dataIndex: 'checktm'
  // }
]
