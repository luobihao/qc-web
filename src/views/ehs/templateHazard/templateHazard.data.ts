import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { queryEnumOptions } from '/@/api/platform/enum';
import { uploadApi } from '/@/api/platform/upload';

/**
 * 危险源导则库表配置
 */
export const templateHazardTableColumns: BasicColumn[] = [
  {
    title: '导则库代码',
    dataIndex: 'hdbcd',
    width: 100,
  },
  {
    title: '导则库名称',
    dataIndex: 'hdbnm',
    slots: { customRender: 'hdbnm' },
    width: 100,
  },
  {
    title: '是否为在建阶段',
    dataIndex: 'isconstruction',
    width: 80,
    slots: { customRender: 'isconstruction' },
  }
]

/**
 * 定义导则库查询条件表单
 */
export const queryHazardDbConditionFormSchema: FormSchema[] = [
  {
    field: 'period',
    label: '阶段标记',
    component: 'Select',
    colProps: { span: 4 },
    labelWidth: 70,
    componentProps: {
      options: [
        {
          label: '运行阶段',
          value: 0
        },
        {
          label: '在建阶段',
          value: 1
        }
      ]
    },
  },
  {
    field: 'hdbcd',
    label: '导则库代码',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    field: 'keytype',
    label: '关键字类型',
    component: 'Select',
    required: true,
    componentProps: {

    },
    defaultValue: 0,
    colProps: { span: 5 },
  },
  {
    field: 'keywords',
    label: '关键字',
    labelWidth: 70,
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    //场所/单元、设备设施、作业、管理、环境
    field: 'tags',
    label: '标签',
    labelWidth: 60,
    component: 'Select',
    componentProps: {
      listHeight: 200,
    },
    colProps: { span: 5 },
  },
];
/**
 * 新增或编辑导则库表单配置
 */
export const addOrUpdateTemplateHazardSchema: FormSchema[] = [
  {
    field: 'hdbcd',
    label: '导则库代码',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'hdbnm',
    label: '导则库名称',
    component: 'Input',
    componentProps: {},
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'isconstruction',
    label: '是否为在建阶段',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '是',
          value: true
        },
        {
          label: '否',
          value: false
        }
      ]
    },
    colProps: { span: 22 },
    required: true,
    defaultValue: true,
  },
  {
    field: 'fileid',
    label: '附件',
    required: false,
    slot: 'fileid',
    component: 'Upload',
  },
];



/**
 * 危险源清单表配置
 */
export const hazardListTableColumns: BasicColumn[] = [
  {
    title: '代码',
    dataIndex: 'code',
    width: 200,
    ifShow: false,
  },
  {
    title: '名称',
    dataIndex: 'name',
    align: "left",
    width: 150,
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 80,
  },
  {
    title: '标签',
    dataIndex: 'tags',
    width: 150,
    slots: { customRender: 'tags' },
  },
  {
    title: '是否为重大危险源',
    dataIndex: 'ismajor',
    width: 100,
    slots: { customRender: 'ismajor' },
  },
  {
    title: 'L范围',
    width: 120,
    dataIndex: 'lmin',
    slots: { customRender: 'LData' },
  },
  {
    title: 'E范围',
    width: 120,
    dataIndex: 'emin',
    slots: { customRender: 'EData' },
  },
  {
    title: 'C范围',
    width: 120,
    dataIndex: 'cmin',
    slots: { customRender: 'CData' },
  },
  {
    title: '事故诱因',
    dataIndex: 'cause',
    width: 200,
    customRender: ({ text }) => {
      if (text == null) return '-'
      else return text;
    }
  },
  {
    title: '可能导致的结果',
    dataIndex: 'consequence',
    width: 200,
    customRender: ({ text }) => {
      if (text == null) return '-'
      else return text;
    }
  },
  {
    title: '防控措施',
    dataIndex: 'prevent',
    width: 200,
    customRender: ({ text }) => {
      if (text == null) return '-'
      else return text;
    }
  },
  {
    title: '应急处理措施',
    dataIndex: 'emergency',
    width: 200,
    customRender: ({ text }) => {
      if (text == null) return '-'
      else return text;
    }
  },
  {
    title: '描述',
    width: 200,
    dataIndex: 'description',
  },
]

/**
 * 定义危险源清单查询条件表单
 */
export const queryHazardsListConditionFormSchema: FormSchema[] = [
  // {
  //   field: 'period',
  //   label: '阶段标记',
  //   component: 'Select',
  //   colProps: { span: 5 },
  //   componentProps: {
  //     options: [
  //       {
  //         label: '运行阶段',
  //         value: 0
  //       },
  //       {
  //         label: '在建阶段',
  //         value: 1
  //       }
  //     ]
  //   },
  // },
  {
    field: 'keytype',
    label: '关键字类型',
    component: 'Select',
    componentProps: {

    },
    defaultValue: 0,
    required: true,
    colProps: { span: 5 },
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    //场所/单元、设备设施、作业、管理、环境
    field: 'tags',
    label: '标签',
    component: 'Select',
    componentProps: {
      listHeight: 200,
    },
    colProps: { span: 5 },
  },
];

/**
 * 新增或编辑危险源类别表单
 */
export const addCategoryFormSchema: FormSchema[] = [
  {
    field: 'hdbcd',
    label: '所属导则库代码',
    component: 'Input',
    colProps: { span: 22 },
    required: true,
    componentProps: { disabled: true },
    show: false,
  },
  {
    field: 'hcatcd',
    label: '类别代码',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'hcatnm',
    label: '类别名称',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  }, {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    colProps: { span: 22 },
    required: true,
    defaultValue: 9,
  }, {
    //场所/单元、设备设施、作业、管理、环境
    field: 'tags',
    label: '标签',
    component: 'Select',
    required: true,
    componentProps: {
      listHeight: 200,
    },
    colProps: { span: 22 },
  },
];


/**
 * 新增或编辑危险源项目表单
 */
export const addProjectFormSchema: FormSchema[] = [
  {
    field: 'hcatcd',
    label: '所属类别代码',
    component: 'Input',
    componentProps: { disabled: true },
    colProps: { span: 22 },
    required: true,
    show: false,

  },
  {
    field: 'hpjtcd',
    label: '项目代码',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'hpjtnm',
    label: '项目名称',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    colProps: { span: 22 },
    required: true,
    defaultValue: 9,
  },
  {
    //场所/单元、设备设施、作业、管理、环境
    field: 'tags',
    label: '标签',
    component: 'Select',
    componentProps: {
      listHeight: 200,
    },
    required: true,
    colProps: { span: 22 },
  },
];
/**
 * 新增或编辑危险源清单表单
 */
export const addItemFormSchema: FormSchema[] = [
  {
    label: '代码',
    field: 'hicd',
    component: 'Input',
    colProps: { span: 11 },
    required: true
  },
  {
    label: '名称',
    field: 'hinm',
    component: 'Input',
    colProps: { span: 11 },
    required: true
  },
  {
    label: '所属项目代码',
    field: 'hpjtcd',
    component: 'Input',
    colProps: { span: 11 },
    componentProps: { disabled: true },
    required: true,
    show: false,
  },
  {
    label: '是否为重大危险源',
    field: 'ismajor',
    component: 'RadioGroup',
    colProps: { span: 22 },
    labelWidth: 180,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ]
    },
    required: true,
    defaultValue: true,
  },
  {
    label: 'L最小',
    field: 'lmin',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      style: { width: '100%' },
      max: 100,
      min: 0.1
    }
  },
  {
    label: 'L最大',
    field: 'lmax',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      max: 100,
      min: 0.1,
      style: { width: '100%' },
    }
  },
  {
    label: 'E最小',
    field: 'emin',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      style: { width: '100%' },
      max: 100,
      min: 0.1
    }
  },
  {
    label: 'E最大',
    field: 'emax',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      style: { width: '100%' },
      max: 100,
      min: 0.1
    }
  },
  {
    label: 'C最小',
    field: 'cmin',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      style: { width: '100%' },
      max: 100,
      min: 0.1
    }
  },
  {
    label: 'C最大',
    field: 'cmax',
    component: 'InputNumber',
    colProps: { span: 11 },
    componentProps: {
      style: { width: '100%' },
      max: 100,
      min: 0.1
    }
  },
  {
    label: '事故诱因',
    field: 'cause',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    label: '可能导致的结果',
    field: 'consequence',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    label: '防控措施',
    field: 'prevent',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    label: '应急处理措施',
    field: 'emergency',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
  {
    label: '描述说明',
    field: 'description',
    component: 'InputTextArea',
    colProps: { span: 22 },
  },
]
