import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import moment from 'moment';
import { uploadApi } from '/@/api/platform/upload';

export const warehouseIventoryTableColumns: BasicColumn[] = [
  {
    title: '仓库名称',
    dataIndex: 'name',
    width: 200,
  },
  {
    title: '所属部门',
    dataIndex: 'deptid',
    width: 200,
    slots: { customRender: 'deptid' },
  },
  {
    title: '仓库位置',
    dataIndex: 'location',
    width: 200,
  },
  {
    title: '说明文字',
    dataIndex: 'description',
  }
]


export const warehouseInventoryRecordColumns: BasicColumn[] = [
  {
    title: '时间',
    dataIndex: 'tm',
    width: 120,
  },
  {
    title: '操作类型',
    dataIndex: 'optype',
    slots: { customRender: 'optype' },
    width: 100,
  },
  {
    title: '标题',
    dataIndex: 'name',
    width: 200,
  },
  {
    title: '操作人',
    dataIndex: 'opusername',
    width: 120,
  },
  {
    title: '操作时间',
    dataIndex: 'optm',
    width: 120,
  },
  {
    title: '操作说明',
    dataIndex: 'description',
    width: 120,
  },
  {
    title: '复核时间',
    dataIndex: 'checktm',
    customRender: ({ record }) => {
      return record.checktm ? moment(record.checktm).format('YYYY-MM-DD HH:mm:ss') : ''
    },
    width: 120,
  },
  {
    title: '复核人',
    dataIndex: 'checkusername',
    width: 120,
  }
]

export const queryWareHouseInventoryRecordSchema: FormSchema[] = [
  {
    field: 'tm',
    label: '时间',
    component: 'RangePicker',
    setType: 'string',
    colProps: { span: 10 },
    componentProps: {
      picker: 'date',
      valueFormat: 'YYYY-MM-DD',
      allowClear: false
    },
    required: true,
    defaultValue: [moment().startOf('month').format('YYYY-MM-DD'), moment().endOf('month').format('YYYY-MM-DD')],
  },
]


export const addWarehouseInventorySchema: FormSchema[] = [
  {
    field: 'tm',
    label: '时间',
    component: 'DatePicker',
    colProps: { span: 23 },
    required: true,
    componentProps: ({ formModel }) => {
      return {
        style: 'width:100%',
        picker: 'date',
        valueFormat: 'YYYY-MM-DD',
        allowClear: false,
        getPopupContainer: triggerNode => document.body
      }
    },
    // defaultValue: moment().add(1, 'days').format('YYYY-MM-DD')
  },
  {
    field: 'optype',
    label: '操作类型',
    component: 'Select',
    required: true,
    colProps: { span: 23 },
    componentProps: {}
  },
  {
    field: 'name',
    label: '标题',
    required: true,
    component: 'Input',
    colProps: { span: 23 },
  },
  {
    field: 'description',
    label: '操作说明',
    component: 'InputTextArea',
    colProps: { span: 23 },
  },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'ehs', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
]
