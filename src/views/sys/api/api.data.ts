import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getApiGroup } from '/@/api/platform/api';

/**
 * Api表配置
 */
export const apiTableColumns: BasicColumn[] = [
  {
    title: '接口分组',
    dataIndex: 'apigroup',
    width: 250,
    align: 'left'
  },
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
  },
  {
    title: '接口名称',
    dataIndex: 'name',
    width: 150,
  },
  {
    title: '请求方法',
    dataIndex: 'method',
    width: 100,
    slots: { customRender: 'method' }
  },
  {
    title: '接口路径',
    dataIndex: 'url',
    width: 200,
  },
  {
    title: '状态',
    dataIndex: 'enable',
    width: 100,
    slots: { customRender: 'enableFlag' },
  },
  {
    title: '描述',
    dataIndex: 'description',
    align: 'left'
  },
]

/**
 * api查询表单
 */
export const apiConditionFormSchema: FormSchema[] = [
  // {
  //   field: 'group',
  //   label: '接口分组',
  //   component: 'Input', //控件类型
  //   colProps: { span: 6 }, //列属性集合
  //   required: false, //是否必须
  // },
  // {
  //   field: 'method',
  //   label: '请求方法',
  //   component: 'Input',
  //   colProps: { span: 6 },
  //   required: false,
  // },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    required: false, //是否必须
  },
]

/**
 * 新增或编辑Api表单配置
 */
export const addOrUpdateApiSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    colProps: { span: 22 },
    required: true
  },
  {
    field: 'method',
    label: '请求方法',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'url',
    label: '接口路径',
    component: 'Input',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'name',
    label: '接口名称',
    component: 'Input',
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'enable',
    label: '状态',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '启用',
          value: true
        },
        {
          label: '禁用',
          value: false
        }
      ]
    },
    required: true,
    defaultValue: true,
    colProps: { span: 11 },
  },
  {
    field: 'apigroup',
    label: '接口分组',
    component: 'Input',
    required: false,
    colProps: { span: 22 },
  },
  {
    field: 'description',
    label: '描述',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 3 }
    },
    colProps: {
      span: 22,
    },
  }
];

