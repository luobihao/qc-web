import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { DescItem } from '/@/components/Description/index';
import moment from 'moment';
import { h } from 'vue';


export const ipBlackListColumns: BasicColumn[] = [{
  title: 'ID', //显示表头名称
  dataIndex: 'id', //显示数据的字段名或序号
  fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
  ifShow: false, //设置在表格界面中是否显示
  width: 250,
},
{
  title: '记录时间',
  dataIndex: 'tm',
  // fixed: 'left', //表格列固定在左边
  width: 120,
},
{
  title: 'IP地址',
  dataIndex: 'ip',
  width: 100,
},
{
  title: 'url',
  dataIndex: 'url',
  width: 120,
},
{
  title: '是否有效',
  dataIndex: 'isvalid',
  width: 70,
  slots: { customRender: 'isvalid' },
},
{
  title: '过期时间',
  dataIndex: 'expiredtm',
  // slots: { customRender: 'expiredtm' },
  width: 120,
},

{
  title: '原因说明',
  dataIndex: 'reason',
  width: 130,
},
{
  title: '命中次数',
  dataIndex: 'hitcount',
  width: 70,
},
{
  title: '最新命中时间',
  dataIndex: 'hittm',
  width: 120,
},
{
  title: '操作人',
  dataIndex: 'username',
  width: 80,
},
];


export const ipBlackListConditionFormSchema: FormSchema[] = [
  {
    field: 'onlyvalid',
    label: '仅当前有效',
    labelWidth: 120,
    component: 'RadioGroup',
    colProps: { span: 5 },
    componentProps: {
      options: [
        {
          label: '是',
          value: true,
        },
        {
          label: '否',
          value: false,
        },
      ]
    },
    defaultValue: false
  },
  {
    field: 'ip',
    label: 'IP地址',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    helpMessage: '在URL、原因说明和操作人信息中模糊匹配；'
  },

]

export const addIpBlackListFormSchema: FormSchema[] = [
  {
    field: 'ip',
    label: 'IP地址',
    component: 'Input',
    // required: true,
    colProps: { span: 23 },
  },
  {
    field: 'url',
    label: 'URL',
    component: 'Input',
    // required: true,
    colProps: { span: 23 },
  },
  {
    field: 'expiredtm',
    label: '过期时间',
    component: 'DatePicker',
    colProps: { span: 23 },
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      getPopupContainer: triggerNode => document.body,
      disabledDate: (current) => {
        return current && current < Date.now();
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
  },
  {
    field: 'reason',
    label: '原因说明',
    required: true,
    componentProps: {
      autoSize: { minRows: 4 },
    },
    component: 'InputTextArea',
    colProps: { span: 23 },
  }
]
