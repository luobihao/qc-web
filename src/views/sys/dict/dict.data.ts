import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 字典分类表格列配置
 */
export const categoryTableColumns: BasicColumn[] = [
  {
    title: '分类编码',
    dataIndex: 'code',
    fixed: 'left', //表格列固定在左边
    width: 90,
  },
  {
    title: '分类名称',
    dataIndex: 'name',
    width: 90,
  },
  {
    title: '描述',
    dataIndex: 'description',
    align: 'left',
  },
]


/**
 * 新增或编辑字典分类表单配置
 */
export const addOrUpdateCategorySchema: FormSchema[] = [
  {
    field: 'code',
    label: '分类编码',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'name',
    label: '分类名称',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'description',
    label: '描述',
    componentProps: {
      autoSize: { minRows: 4 }
    },
    component: 'InputTextArea',
    required: false,
    colProps: { span: 22 },
  }
];

/**
 * 字典分类查询表单配置
 */
export const categoryConditionFormSchema: FormSchema[] = [
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 10 },
    componentProps: {
      size: 'medium'
    }
  },
];


/**
 * 字典项表格列配置
 */
export const itemTableColumns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    fixed: 'left', //表格列固定在左边
    width: 60,
    ifShow: false,
  },
  // {
  //     title: '分类编码',
  //     dataIndex: 'categorycode',
  //     width: 150,
  //     fixed: 'left'
  // },
  {
    title: '字典值',
    dataIndex: 'k',
    width: 100,
  },
  {
    title: '显示文本',
    dataIndex: 'v',
    width: 100,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    width: 100,
    slots: { customRender: 'flag' },
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 80,

  },
];

/**
 * 字典项查询表单配置
 */
export const categoryItemConditionFormSchema: FormSchema[] = [
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'flag',
    label: '状态标记',
    component: 'Select',
    labelWidth: 100,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    colProps: { span: 8 },
  }
];
/**
 * 新增或编辑字典项表单配置
 */
export const addOrUpdateItemFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: '字典项ID',
    component: 'Input',
    show: false
  },
  {
    field: 'categorycode',
    label: '字典分类',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'k',
    label: '字典值',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'v',
    label: '显示文本',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    colProps: { span: 11 },
    required: true,
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '禁用', value: '5' },
      ],
    },
    defaultValue: '0'
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 100
    },
    required: true,
    colProps: { span: 11 },
    defaultValue: 9
  },
];
