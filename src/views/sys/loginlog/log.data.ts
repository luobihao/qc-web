import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { DescItem } from '/@/components/Description/index';
import moment from 'moment';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

/**
 * 定义api日志表格的表头
 */
export const loginLogTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 250,
  },
  {
    title: '时间',
    dataIndex: 'tm',
    fixed: 'left', //表格列固定在左边
    width: 150,
  },
  {
    title: '项目编码',
    dataIndex: 'project',
    width: 80,
  },
  {
    title: '用户名',
    dataIndex: 'ucode',
    width: 100,
  },
  {
    title: '登录结果状态',
    dataIndex: 'status',
    width: 140,
    slots: { customRender: 'statusFlag' },
  },
  {
    title: '用户ID',
    dataIndex: 'uid',
    width: 80,
  },
  {
    title: '用户名称',
    dataIndex: 'username',
    width: 100,
  },
  {
    title: '客户端类型',
    dataIndex: 'clienttype',
    // fixed: 'left', //表格列固定在左边
    width: 80,
  },
  {
    title: '客户端IP',
    dataIndex: 'ip',
    width: 120,
    slots: { customRender: 'ip' },
  },
  {
    title: '验证方式',
    dataIndex: 'logintype',
    width: 100,
  },
  {
    title: '登录结果信息',
    dataIndex: 'msg',
    width: 150,
  },
  {
    title: 'TOKEN',
    dataIndex: 'token',
    width: 120,
    slots: { customRender: 'token' },
  },
  {
    title: '过期时间',
    dataIndex: 'expiredtm',
    slots: { customRender: 'expiredtm' },
    width: 150,
  },

];

/**
 * 定义查询条件表单
 */
export const loginLogQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker', //控件类型
    colProps: { span: 6 }, //列属性集合
    required: true, //是否必须
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      //设置不可选
      disabledDate: (current) => {
        //不可选择未来时间
        return current && current > Date.now();
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().startOf('date').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    colProps: { span: 6 },
    required: true,
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      //设置不可选
      disabledDate: (current) => {
        //不可选择未来时间
        return current && current > Date.now();
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().endOf('date').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'uid',
    label: '用户ID',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'token',
    label: 'TOKEN',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    helpMessage: '模糊匹配，可以为登录名、客户端信息、登录结果信息；',
  },
  {
    field: 'status',
    label: '登录是否成功',
    component: 'RadioButtonGroup',
    colProps: { span: 8 },
    componentProps: {
      options: [
        { label: '全部', value: '' },
        { label: '登录成功', value: 1 },
        { label: '登录失败', value: 0 },
      ],
    },
    defaultValue: '',
  },
  {
    field: 'projectcode',
    label: '项目编码',
    component: 'Input',
    colProps: { span: 6 },
  }
];

/**
 * 定义Api日志详情回显属性对象
 */
export const loginLogDetailSchema: DescItem[] = [
  // {
  //   label: 'ID', //显示表头名称
  //   field: 'id', //显示数据的字段名或序号
  // },
  {
    label: '时间',
    field: 'tm',
  },
  {
    label: 'TOKEN',
    field: 'token',
  },
  {
    label: '过期时间',
    field: 'expiredtm',
  },
  {
    label: '是否已过期',
    field: 'hasexpired',
    render: (text) => {
      return h(Tag, { color: text === true ? 'green' : 'red' }, () => {
        return text === true ? '是' : '否';
      });
    }
  },
  {
    label: '项目编码',
    field: 'project',
  },
  {
    label: '用户名',
    field: 'ucode',
  },
  {
    label: '用户ID',
    field: 'uid',
  },
  {
    label: '用户昵称',
    field: 'username',
  },
  {
    label: '登录结果状态',
    field: 'status',
  },
  {
    label: '登录是否成功',
    field: 'success',
    render: (text) => {
      return h(Tag, { color: text === true ? 'green' : 'red' }, () => {
        return text === true ? '是' : '否';
      });
    }
  },
  {
    label: '登录验证方式',
    field: 'logintype',
  },
  {
    label: '登录结果信息',
    field: 'msg',
  },
  {
    label: '客户端类型',
    field: 'clienttype',
  },
  {
    label: '客户端IP',
    field: 'ip',
  }, {
    label: '客户端信息',
    field: 'clientinfo',
    span: 2
  },
];
