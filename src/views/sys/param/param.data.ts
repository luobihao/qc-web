import { truncate } from 'fs';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 分组表单列配置
 */
export const groupTableColumns: BasicColumn[] = [
  {
    title: '分组编码',
    dataIndex: 'code',
    fixed: 'left', //表格列固定在左边
    width: 100,
  },
  {
    title: '分组名称',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '数据库表编码',
    dataIndex: 'tablecode',
    width: 120,
  },
  {
    title: '节点名称',
    dataIndex: 'nodes',
    width: 100,
  },
  {
    title: '查询sql语句',
    dataIndex: 'querysql',
    width: 250,
    align: 'left',
  },
  {
    title: '修改sql语句',
    dataIndex: 'updatesql',
    width: 250,
    align: 'left',
  },
  {
    title: '描述',
    dataIndex: 'description',
    align: 'left',
  },
]


/**
 * 新增或编辑分组表单配置
 */
export const addOrUpdateGroupSchema: FormSchema[] = [
  {
    field: 'code',
    label: '分组编码',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'name',
    label: '分组名称',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'tablecode',
    label: '数据库表编码',
    component: 'Select',
    componentProps: {},
    colProps: { span: 11 },
    required: true,
  },
  {
    field: 'nodes',
    label: '节点名称',
    component: 'Input',
    required: false,
    colProps: { span: 11 },
    helpMessage: '可以为多个；多个之间使用逗号分隔，每个中使用大于符号＞指定上下级关系，使用连接符号&连接多个同级节点/属性;'
  },
  {
    field: 'querysql',
    label: '查询sql语句',
    component: 'InputTextArea',
    required: false,
    colProps: { span: 22 },
  },
  {
    field: 'updatesql',
    label: ' 修改sql语句',
    component: 'InputTextArea',
    required: false,
    colProps: { span: 22 },
    helpMessage: '占位符使用问号?'
  },
  {
    field: 'description',
    label: '描述',
    componentProps: {
      autoSize: { minRows: 4 }
    },
    component: 'InputTextArea',
    required: false,
    colProps: { span: 22 },
  }
];

/**
 * 参数项表格列配置
 */
export const itemTableColumns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    fixed: 'left', //表格列固定在左边
    width: 60,
    ifShow: false
  },
  // {
  //     title: '分组编码',
  //     dataIndex: 'groupcode',
  //     fixed: 'left', //表格列固定在左边
  //     width: 120,
  // },
  {
    title: '参数项编码',
    dataIndex: 'itemcode',
    width: 150,
    fixed: 'left'
  },
  {
    title: '参数项名称',
    dataIndex: 'name',
    width: 150,
  },
  {
    title: '数据类型',
    dataIndex: 'datatype',
    width: 80,
    slots: { customRender: 'paramType' },
  },
  {
    title: '非空',
    dataIndex: 'notnull',
    width: 60,
    slots: { customRender: 'nullFlag' },

  },
  {
    title: '默认值',
    dataIndex: 'defaultval',
    width: 60,

  },
  {
    title: '取值范围',
    dataIndex: 'valrange',
    width: 100
  },
  {
    title: '单位',
    dataIndex: 'unit',
    width: 60,

  },
  {
    title: '标记',
    dataIndex: 'flag',
    width: 60,
    slots: { customRender: 'statusFlag' },
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 60,
  },
  {
    title: '描述',
    dataIndex: 'description',
    customHeaderCell: () => ({ style: { textAlign: 'center' } }),
    align: 'left',
  },
];

/**
 * 查询表单配置
 */
export const paramConditionFormSchema: FormSchema[] = [
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 5 },
  },
];


/**
 * 新增或编辑参数项表单配置
 */
export const addOrUpdateItemFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'groupcode',
    label: '分组编码',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: { span: 22 },
  },
  {
    field: 'itemcode',
    label: '参数项编码',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'name',
    label: '参数项名',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'datatype',
    label: '数据类型',
    component: 'Select',
    required: true,
    componentProps: {},
    colProps: { span: 11 },
  },
  {
    field: 'notnull',
    label: '非空',
    component: 'RadioGroup',
    required: true,
    defaultValue: true,
    componentProps: {
      options: [
        {
          label: '是',
          value: true
        }, {
          label: '否',
          value: false
        }
      ]
    },
    colProps: { span: 11 },
  },
  {
    field: 'defaultval',
    label: '默认值',
    component: 'Input',
    required: false,
    colProps: { span: 11 },
  },
  {
    field: 'valrange',
    label: '取值范围',
    component: 'Input',
    required: false,
    colProps: { span: 11 },

  },
  {
    field: 'unit',
    label: '单位',
    component: 'Input',
    required: false,
    colProps: { span: 11 },
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    colProps: { span: 11 },
    required: true,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'description',
    label: '描述',
    component: 'InputTextArea',
    required: false,
    colProps: { span: 22 },
  },
];
