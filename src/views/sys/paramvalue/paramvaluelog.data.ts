import { BasicColumn } from '/@/components/Table';

/**
 * 定义参数项日志信息表头
 */
export const paramValueLogColumns: BasicColumn[] = [
  {
    title: '参数项',
    dataIndex: 'paramname',
    fixed: 'left',
    width: 200,
  },
  {
    title: '对象ID/编码',
    dataIndex: 'objcode',
    width: 100,
    ifShow: false,
  },
  {
    title: '对象名称',
    dataIndex: 'objname',
    ifShow: false,
    width: 100,
  },
  {
    title: '分组编码',
    dataIndex: 'groupcode',
    width: 200,
  },
  {
    title: '参数节点字符串',
    dataIndex: 'nodestr',
    width: 150,
  },
  {
    title: '操作时间',
    dataIndex: 'tm',
    width: 200,
  },
  {
    title: '操作类型',
    dataIndex: 'optype',
    slots: { customRender: 'optypeRender' },
    width: 100,
  },
  {
    title: '操作前值',
    dataIndex: 'beforeval',
    width: 100,
  },
  {
    title: '操作后值',
    dataIndex: 'afterval',
    width: 100,
  },
];
