import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义数据库信息表头
 */
export const dbColumns: BasicColumn[] = [
  {
    title: '数据库编码',
    dataIndex: 'code',
    fixed: 'left',
    width: 50,
  },
  {
    title: '数据库名称',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '数据库连接信息',
    dataIndex: 'con',
    width: 100,
  },
];

/**
 * 定义新增/修改数据库信息表单
 */
export const dbFormSchema: FormSchema[] = [
  {
    field: 'code',
    label: '数据库编码',
    component: 'Input',
    required: true,
  },
  {
    field: 'name',
    label: '数据库名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'con',
    label: '数据库连接信息',
    component: 'CodeMirror',
    componentProps: {
      mode: 'application/json'
    },
    required: true,
  },
];
