import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义接口过滤规则的表头
 */
export const apiFilterTableColumns: BasicColumn[] = [
  {
    title: '规则ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false,
    width: 250,
  },
  {
    title: 'URL',
    dataIndex: 'url',
    fixed: 'left', //表格列固定在左边
    width: 200,
  },
  {
    title: '请求方法',
    dataIndex: 'method',
    width: 100,
  },

  {
    title: '用户ID',
    dataIndex: 'uid',
    width: 120,
    slots: { customRender: 'uidDisplay' },
  },
  {
    title: '耗时',
    dataIndex: 'ts',
    width: 50,
    // slots: { customRender: 'tsFlag' },
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    width: 80,
    slots: { customRender: 'statusFlag' },
  },
  {
    title: '命中次数',
    dataIndex: 'hitcount',
    width: 80,
  },
  {
    title: '最新命中时间',
    dataIndex: 'hittm',
    width: 150,
  },
  {
    title: '更新时间',
    dataIndex: 'upDateTm',
    width: 150,
  },
];

/**
 * 定义查询条件表单
 */
export const conditionFormSchema: FormSchema[] = [
  {
    field: 'flag',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '禁用', value: '5' },
      ],
    },
    colProps: { span: 8 },
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 8 },
    componentProps: {
      placeholder: '匹配url',
    },
  },
];

/**
 * 定义新增/修改过滤规则表单
 */
export const apiFilterFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'url',
    label: 'URL',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'method',
    label: '请求方法',
    component: 'Input',
    required: false,
    colProps: { span: 11 },
  },
  {
    field: 'flag',
    label: '状态',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
    colProps: { span: 11 },
  },

  {
    field: 'uid',
    label: '用户ID',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'ts',
    label: '耗时/毫秒',
    component: 'Input',
    colProps: { span: 11 },
  },
  {
    field: 'hitcount',
    label: '命中次数',
    component: 'Input',
    colProps: { span: 11 },
    defaultValue: 0,
    componentProps: {
      disabled: true,
    },
  },

  {
    field: 'upDateTm',
    label: '规则更新时间',
    component: 'Input',
    colProps: { span: 11 },
    componentProps: {
      disabled: true,
    },
  },
];
