import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { DescItem } from '/@/components/Description/index';
import moment from 'moment';

/**
 * 定义api日志表格的表头
 */
export const apiLogTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 250,
  },
  {
    title: '时间',
    dataIndex: 'tm',
    // fixed: 'left', //表格列固定在左边
    width: 150,
  },
  {
    title: '请求方法',
    dataIndex: 'method',
    // fixed: 'left', //表格列固定在左边
    width: 60,
  },
  {
    title: 'URL',
    dataIndex: 'url',
    width: 200,
  },
  {
    title: '客户端IP',
    dataIndex: 'ip',
    width: 120,
    slots: { customRender: 'ip' },
  },
  {
    title: 'TOKEN',
    dataIndex: 'token',
    slots: { customRender: 'token' },
    width: 220,
  },
  {
    title: '状态码',
    dataIndex: 'status',
    width: 50,
    slots: { customRender: 'statusFlag' },
  },
  {
    title: '耗时',
    dataIndex: 'ts',
    width: 50,
    slots: { customRender: 'tsFlag' },
  },
];

/**
 * 定义查询条件表单
 */
export const apiLogQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker', //控件类型
    colProps: { span: 6 }, //列属性集合
    required: true, //是否必须
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      //设置不可选
      disabledDate: (current) => {
        //不可选择未来时间
        return current && current > Date.now();
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().startOf('date').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    colProps: { span: 6 },
    required: true,
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      //设置不可选
      disabledDate: (current) => {
        //不可选择未来时间
        return current && current > Date.now();
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().endOf('date').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'url',
    label: 'URL',
    component: 'Input',
    colProps: { span: 6 },
    helpMessage: '模糊匹配，支持%和_通配符',
  },
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    helpMessage: '模糊匹配请求参数或请求内容',
  },
  {
    field: 'uid',
    label: '用户ID',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'token',
    label: 'TOKEN',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'ip',
    label: '客户端IP',
    component: 'Input',
    colProps: { span: 6 },
    helpMessage: '模糊匹配，支持%和_通配符',
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '全部', value: '' },
        { label: '失败', value: 'fail' },
        { label: '1**', value: '1' },
        { label: '2**', value: '2' },
        { label: '3**', value: '3' },
        { label: '4**', value: '4' },
        { label: '5**', value: '5' },
      ],
    },
    colProps: { span: 6 },
    defaultValue: '',
  },
  {
    field: 'ts',
    label: '耗时/ms',
    component: 'Input',
    colProps: { span: 6 },
    helpMessage: '输入值时查询耗时时间>=N毫秒'
  },
];

/**
 * 定义Api日志详情回显属性对象
 */
export const apiLogDetailSchema: DescItem[] = [
  {
    field: 'id',
    label: 'ID',
    labelMinWidth: 80,
  },
  {
    field: 'tm',
    label: '时间',
    labelMinWidth: 80,
  },
  {
    field: 'method',
    label: '请求方法',
    labelMinWidth: 80,
  },
  {
    field: 'url',
    label: 'URL',
    labelMinWidth: 80,
  },
  {
    field: 'ip',
    label: '客户端IP',
    labelMinWidth: 80,
  },
  {
    field: 'params',
    label: '请求参数',
    labelMinWidth: 80,
  },
  {
    field: 'body',
    label: '请求内容',
    labelMinWidth: 80,
  },
  {
    field: 'token',
    label: 'TOKEN',
    labelMinWidth: 80,
  },
  {
    field: 'status',
    label: '状态码',
    labelMinWidth: 80,
  },
  {
    field: 'ts',
    label: '耗时/ms',
    labelMinWidth: 80,
  },
  {
    field: 'data',
    label: '响应数据',
    labelMinWidth: 80,
  },
  {
    field: 'result',
    label: '返回结果',
    labelMinWidth: 80,
  },
];
