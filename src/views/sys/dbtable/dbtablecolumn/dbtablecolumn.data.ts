import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义数据库表列信息表头
 */
export const dbTableColumnColumns: BasicColumn[] = [
  // 基本信息
  {
    title: '数据库表编码',
    dataIndex: 'tbcode',
    fixed: 'left',
    width: 120,
    ifShow: false,
  },
  {
    title: '列编码',
    dataIndex: 'code',
    fixed: 'left',
    width: 100,
    sorter: {
      compare: (a, b) => a.code.length - b.code.length,
    },
  },
  {
    title: '列名称',
    dataIndex: 'name',
    fixed: 'left',
    width: 100,
    sorter: {
      compare: (a, b) => a.name.length - b.name.length,
    },
  },
  {
    title: '主键',
    dataIndex: 'isprimary',
    slots: { customRender: 'isPrimary' },
    width: 50,
  },
  {
    title: '数据类型',
    dataIndex: 'datatype',
    slots: { customRender: 'datatypeRender' },
    width: 80,
  },
  {
    title: '列类型',
    dataIndex: 'coltype',
    slots: { customRender: 'coltypeRender' },
    width: 80,
  },
  {
    title: '长度',
    dataIndex: 'length',
    width: 50,
  },
  {
    title: '非空',
    dataIndex: 'notnull',
    slots: { customRender: 'isNotNull' },
    width: 50,
  },
  {
    title: '默认值',
    dataIndex: 'defaultval',
    width: 100,
  },
  {
    title: '取值范围',
    dataIndex: 'valrange',
    width: 100,
  },
  {
    title: '单位',
    dataIndex: 'unit',
    width: 50,
  },
  {
    title: '格式化字符串',
    dataIndex: 'formatstr',
    width: 150,
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 100,
  },

  // 查询结果设置
  {
    title: '查询结果设置',
    dataIndex: 'query-result-set',
    children: [
      {
        title: '表格中显示',
        dataIndex: 'qrshow',
        slots: { customRender: 'isqrshow' },
        width: 100,
      },
      {
        title: '排序',
        dataIndex: 'qrodr',
        width: 80,
        sorter: {
          compare: (a, b) => a.qrodr - b.qrodr,
        },
      },
      {
        title: '列宽',
        dataIndex: 'qrwidth',
        width: 50,
      },
      {
        title: '固定位置',
        dataIndex: 'qrfixed',
        width: 100,
      },
      {
        title: '图表配置',
        dataIndex: 'qrcharts',
        width: 100,
      },
      {
        title: '排序优先级',
        dataIndex: 'qrorderidx',
        width: 120,
        sorter: {
          compare: (a, b) => a.qrorderidx - b.qrorderidx,
        },
      },
      {
        title: '升序',
        dataIndex: 'qrascorder',
        width: 120,
      },
    ],
  },

  // 查询条件设置
  {
    title: '查询条件设置',
    dataIndex: 'query-condition-set',
    children: [
      {
        title: '作为查询条件',
        dataIndex: 'qcflag',
        slots: { customRender: 'isqcflag' },
        width: 120,
      },
      {
        title: '非空',
        dataIndex: 'qcnotnull',
        slots: { customRender: 'isqcnotnull' },
        width: 100,
      },
      {
        title: '比较类型',
        dataIndex: 'qccmptype',
        slots: { customRender: 'qccmptypeRender' },
        width: 100,
      },
    ],
  },

  // 编辑表单设置
  {
    title: '编辑表单设置',
    dataIndex: 'edit-form-set',
    children: [
      {
        title: '是否显示',
        dataIndex: 'efshow',
        slots: { customRender: 'isefshow' },
        width: 100,
      },
      {
        title: '组件宽度',
        dataIndex: 'efcolspan',
        width: 100,
      },
      {
        title: '后台自动重置',
        dataIndex: 'efreset',
        width: 120,
      },
    ],
  },
];

/**
 * 定义新增/修改数据库SQL信息表单
 */

export const dbTableColumnFormSchema: FormSchema[] = [
  // 基本信息
  {
    field: 'basic-info',
    component: 'Divider',
    label: '基本信息',
    componentProps: {
      style: {
        color: 'grey', //文字颜色设置
        fontWeight: 'bold', //文字加粗
        fontSize: '15px', // 文字字号设置
        borderColor: '#D5D2D2', // 设置分隔线颜色
      },
    },
  },
  {
    field: 'tbcode',
    label: '数据库表编码',
    component: 'Input',
    required: true,
    colProps: { span: 8 },
    show: false,
  },
  {
    field: 'code',
    label: '列编码',
    component: 'Input',
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'name',
    label: '列名称',
    component: 'Input',
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'isprimary',
    label: '主键',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: false,
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'datatype',
    label: '数据类型',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: { span: 8 },
    defaultValue: 0,
  },
  {
    field: 'coltype',
    label: '列类型',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: { span: 8 },
    defaultValue: 0,
  },
  {
    field: 'notnull',
    label: '非空',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: false,
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'length',
    label: '长度',
    component: 'Input',
    required: false,
    colProps: { span: 8 },
  },
  {
    field: 'defaultval',
    label: '默认值',
    component: 'Input',
    required: false,
    colProps: { span: 8 },
  },
  {
    field: 'unit',
    label: '单位',
    component: 'Input',
    required: false,
    colProps: { span: 8 },
  },
  {
    field: 'formatstr',
    label: '格式化字符串',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 2, maxRows: 5 },
    },
    required: false,
    helpMessage: [
      '数据类型为数值，可以设置数值格式化保留小数位数，例如保留一位小数：F1',
      '数据类型为时间类型，可以自定义格式化，例如：YYYY-MM-DD HH:mm:ss',
      '数据类型为布尔或者整数，格式化设置如下样式，例如：[{"value":"数据值","label":"文字内容","color":"格式化的颜色"},{"value":"数据值","label":"文字内容","color":"格式化的颜色"}]；',
    ],
    colProps: { span: 24 },
  },
  {
    field: 'valrange',
    label: '取值范围',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 2, maxRows: 5 },
    },
    required: false,
    colProps: { span: 24 },
    helpMessage: [
      '列类型为字典时输入逗号分隔的序列值；可以指定对应的key数值，使用‘=’连接，表示为键值对，如：0=未处理,1=已处理；未指定key默认从0开始按顺序编号。',
      '列类型为枚举字典输入完整的枚举类名。',
      '列类型为数据字典时输入字典编码。',
      '列类型为对象ID或对象名称时输入对象信息表--数据库表编码。',
    ],
  },
  {
    field: 'description',
    label: '描述',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 2, maxRows: 5 },
    },
    colProps: { span: 24 },
    required: false,
  },

  // 查询结果设置
  {
    field: 'query-result-set',
    component: 'Divider',
    label: '查询结果设置 --> 结果显示设置',
    componentProps: {
      style: {
        color: 'grey', //文字颜色设置
        fontWeight: 'bold', //文字加粗
        fontSize: '15px', // 文字字号设置
        borderColor: '#D5D2D2', // 设置分隔线颜色
      },
    },
  },
  {
    field: 'qrshow',
    label: '表格中显示',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: true,
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'qrodr',
    label: '顺序',
    component: 'Input',
    required: false,
    helpMessage: '在表格中显示列的先后顺序。',
    colProps: { span: 8 },
  },
  {
    field: 'qrorderidx',
    label: '排序优先级',
    component: 'Input',
    required: false,
    helpMessage: '查询SQL中的排序字段优先顺序。',
    colProps: { span: 8 },
  },
  {
    field: 'qrwidth',
    label: '表格列宽',
    component: 'InputNumber',
    required: true,
    helpMessage: '可以为px、%或者填数值',
    colProps: { span: 8 },
  },
  {
    field: 'qrfixed',
    label: '表格中固定',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '不固定', value: 'noFixed' },
        { label: '固定左边', value: 'left' },
        { label: '固定右边', value: 'right' },
      ],
    },
    defaultValue: 'noFixed',
    required: false,
    colProps: { span: 8 },
  },

  {
    field: 'qrascorder',
    label: '排序',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '不参与排序', value: '0' },
        { label: '升序', value: 'true' },
        { label: '降序', value: 'false' },
      ],
    },
    defaultValue: '0',
    required: false,
    colProps: { span: 8 },
  },
  {
    field: 'qrcharts',
    label: '图表显示配置',
    component: 'InputTextArea',
    required: false,
    helpMessage: [
      '配置为json格式，可配置节点有：图表类型、最大值、最小值、最小间隔、左右位置、上下反转。{"type":"","min":"","max":"","minInterval":"","position":"","inverse":fase}；',
    ],
    colProps: { span: 24 },
  },

  // 查询条件设置
  {
    field: 'query-condition-set',
    component: 'Divider',
    label: '查询条件设置',
    componentProps: {
      style: {
        color: 'grey', //文字颜色设置
        fontWeight: 'bold', //文字加粗
        fontSize: '15px', // 文字字号设置
        borderColor: '#D5D2D2', // 设置分隔线颜色
      },
    },
  },
  {
    field: 'qcflag',
    label: '作为查询条件',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: false,
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'qcnotnull',
    label: '非空',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: false,
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'qccmptype',
    label: '比较类型',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: { span: 8 },
    defaultValue: 0,
  },

  // 编辑表单设置
  {
    field: 'edit-form-set',
    component: 'Divider',
    label: '编辑表单设置',
    componentProps: {
      style: {
        color: 'grey', //文字颜色设置
        fontWeight: 'bold', //文字加粗
        fontSize: '15px', // 文字字号设置
        borderColor: '#D5D2D2', // 设置分隔线颜色
      },
    },
  },
  {
    field: 'efshow',
    label: '是否显示',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: true,
    required: true,
    colProps: { span: 8 },
  },
  {
    field: 'efcolspan',
    label: '组件宽度',
    component: 'Select',
    componentProps: {
      options: [
        { label: '4', value: 4 },
        { label: '6', value: 6 },
        { label: '8', value: 8 },
        { label: '10', value: 10 },
        { label: '12', value: 12 },
        { label: '16', value: 16 },
        { label: '20', value: 20 },
        { label: '24', value: 24 },
      ],
    },
    required: true,
    colProps: { span: 8 },
    defaultValue: 6,
  },
  {
    field: 'efreset',
    label: '后台自动重置',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '不重置', value: false },
        { label: '重置', value: true },
      ],
    },
    defaultValue: false,
    required: false,
    helpMessage: [
      '对列类型为写入时间、更新时间、状态等需要在新增或修改时进行重置的，由后台自动使用默认值进行重置。',
    ],
    colProps: { span: 8 },
  },
];
