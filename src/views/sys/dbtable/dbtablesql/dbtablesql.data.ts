import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义数据库表SQL信息表头
 */
export const dbTableSQLColumns: BasicColumn[] = [
  {
    title: '数据库表编码',
    dataIndex: 'tbcode',
    fixed: 'left',
    width: 120,
    ifShow: false,
  },
  {
    title: 'SQL语句编码',
    dataIndex: 'code',
    fixed: 'left',
    width: 100,
  },
  {
    title: 'SQL语句名称',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '操作类型',
    dataIndex: 'optype',
    slots: { customRender: 'optypeRender' },
    width: 100,
  },
  {
    title: '标记',
    dataIndex: 'flag',
    slots: { customRender: 'flagRender' },
    width: 100,
  },
  // editflag:查询结果编辑标记，查询的数据是否可以进行录入、修改、删除等操作
  {
    title: '可编辑操作标记',
    dataIndex: 'editflag',
    slots: { customRender: 'editflagRender' },
    width: 150,
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 100,
  },
  {
    title: 'SQL语句',
    dataIndex: 'sqlstr',
    width: 300,
  },
];

/**
 * 定义新增/修改数据库SQL信息表单
 */
export const dbTableSQLFormSchema: FormSchema[] = [
  {
    field: 'tbcode',
    label: '数据库表编码',
    component: 'Input',
    required: true,
    colProps: { span: 12 },
  },
  {
    field: 'code',
    label: 'SQL语句编码',
    component: 'Input',
    required: true,
    colProps: { span: 12 },
  },
  {
    field: 'name',
    label: 'SQL语句名称',
    component: 'Input',
    required: true,
    colProps: { span: 12 },
  },
  {
    field: 'optype',
    label: '操作类型',
    component: 'Select',
    componentProps: {
      options: [
        { label: '查询', value: '0' },
        { label: '新增', value: '1' },
        { label: '修改', value: '2' },
        { label: '删除', value: '3' },
      ],
    },
    defaultValue: '0',
    required: true,
    colProps: { span: 12 },
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '禁用', value: '5' },
      ],
    },
    required: false,
    defaultValue: '0',
    colProps: { span: 12 },
  },
  {
    field: 'editflag',
    label: '可编辑操作标记',
    component: 'CheckboxGroup',
    componentProps: {
      options: [
        { label: '新增', value: 0x1 },
        { label: '修改', value: 0x2 },
        { label: '删除', value: 0x4 },
      ],
    },
    required: false,
    defaultValue: 0x0,
    colProps: { span: 12 },
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'InputNumber',
    defaultValue: 0,
    required: false,
  },
  {
    field: 'sqlstr',
    label: 'SQL语句',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 5, maxRows: 20 },
    },
    required: true,
  },
];
