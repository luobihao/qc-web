import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义数据库表信息表头
 */
export const dbTableColumns: BasicColumn[] = [
  {
    title: '数据库表编码',
    dataIndex: 'code',
    fixed: 'left',
    width: 200,
  },
  {
    title: '数据库表名称',
    dataIndex: 'name',
    width: 200,
  },
  {
    title: '分类编码',
    dataIndex: 'ctgcode',
    width: 150,
  },
  {
    title: '数据库编码',
    dataIndex: 'db',
    width: 100,
  },
  {
    title: '描述信息',
    dataIndex: 'description',
    width: 200,
  },
];

/**
 * 定义查询表单
 */
export const dbTableQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'ctgcode',
    label: '分类编码',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: '',
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 6 },
    defaultValue: '',
  },
];

/**
 * 定义新增/修改数据库信息表单
 */
export const dbTableFormSchema: FormSchema[] = [
  {
    field: 'db',
    label: '数据库',
    component: 'Select',
    componentProps: {},
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'code',
    label: '数据库表编码',
    component: 'Input',
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'name',
    label: '数据库表名称',
    component: 'Input',
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'ctgcode',
    label: '分类编码',
    component: 'Input',
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'description',
    label: '描述信息',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 2, maxRows: 5 },
    },
    required: false,
    colProps: { span: 20 },
  },
];
