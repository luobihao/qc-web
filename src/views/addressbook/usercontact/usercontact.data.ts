import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
// import { useUserStore } from '/@/store/modules/user';
//获取当前登录用户项目编码
// const userStore = useUserStore();
// const currentProjectCode = userStore.getUserCurrentProjectCode;

/**
 * 定义用户联系信息表头
 */
export const userContactTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 80,
    customFilterDropdown: true, //开启自定义筛选项
    onFilter: (value, record) => record.name.toString().toLowerCase().includes(value.toLowerCase()),
  },
  {
    title: '职务',
    dataIndex: 'duty',
    width: 80,
  },
  {
    title: '手机号码',
    dataIndex: 'phone',
    width: 100,
  },
  {
    title: '短号',
    dataIndex: 'cornet',
    width: 100,
  },
  {
    title: '办公室/座机号码',
    dataIndex: 'tel',
    width: 100,
  },
];

/**
 * 定义号码查询条件表单
 */
export const conditionFormSchema: FormSchema[] = [
  {
    field: 'project',
    label: '项目编码',
    component: 'Input',
    colProps: { span: 6 },
    show: false,
    // defaultValue: currentProjectCode, //默认为当前登录用户项目编码
  },
  {
    field: 'dept',
    label: '部门',
    component: 'Select',
    colProps: { span: 6 },
    show: false,
  },
  {
    field: 'words',
    label: '关键字',
    component: 'Input',
    componentProps: {
      placeholder: '匹配姓名或者手机号码',
    },
    colProps: { span: 8 },
  },
];
