import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getAll } from '/@/api/addressbook/official/category';

/**
 * 定义公务号码表头
 */
export const phoneTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '名称',
    dataIndex: 'name',
    width: 80,
  },
  {
    title: '号码',
    dataIndex: 'phone',
    width: 100,
  },
  {
    title: '所属分类ID',
    dataIndex: 'categoryId',
    width: 80,
    ifShow: false,
  },
  {
    title: '状态标记',
    dataIndex: 'flag',
    slots: { customRender: 'statusFlag' },
    width: 70,
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 70,
  },
  {
    title: '备注',
    dataIndex: 'notes',
    width: 200,
  },
];

/**
 * 定义号码查询条件表单
 */
export const phoneQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'categoryId',
    label: '所属分类ID',
    component: 'Input',
    colProps: { span: 6 },
    show: false,
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    componentProps: {
      placeholder: '匹配名称或者号码',
    },
    colProps: { span: 8 },
  },
];

/**
 * 定义新增/修改号码表单
 */
export const phoneFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'phone',
    label: '号码',
    component: 'Input',
    required: true,
  },
  {
    field: 'categoryId',
    label: '所属分类',
    component: 'Select',
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
  },
  {
    field: 'notes',
    label: '备注',
    component: 'Input',
  },
];

/**
 * 定义设置号码分类表单
 */
export const setCategoryFormSchema: FormSchema[] = [
  {
    field: 'ids',
    label: '号码id集合',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'categoryId',
    label: '所属分类',
    component: 'ApiSelect',
    componentProps: {
      api: getAll,
      immediate: true,
      labelField: 'name',
      valueField: 'id',
    },
  },
];

/**
 * 公务号码通讯录表头
 */
export const officialContactTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '名称',
    dataIndex: 'name',
    width: 80,
    customFilterDropdown: true, //开启自定义筛选项
    onFilter: (value, record) => record.name.toString().toLowerCase().includes(value.toLowerCase()),
  },
  {
    title: '号码',
    dataIndex: 'phone',
    width: 100,
    // customFilterDropdown: true, //开启自定义筛选项
    // onFilter: (value, record) => record.phone.toString().toLowerCase().includes(value.toLowerCase()),
  },
];
