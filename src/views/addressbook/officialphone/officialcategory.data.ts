import { FormSchema } from '/@/components/Table';

/**
 * 定义新增/修改项目用户表单
 */
export const officialCategoryFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '分类名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
  },
];
