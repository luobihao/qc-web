import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h, ref, reactive } from 'vue';
import { useMessage } from '/@/hooks/web/useMessage';
import moment from 'moment';

//定义表格的列
export const userTableColumns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    fixed: 'left', //表格列固定在左边
    ifShow: false, //设置在表格界面中是否显示
    width: 250,
  },
  {
    title: '名称',
    dataIndex: 'name',
    fixed: 'left', //表格列固定在左边
    width: 120,
  },
  {
    title: '登录名',
    dataIndex: 'code',
    fixed: 'left', //表格列固定在左边
    width: 80,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 80,
  },
  {
    title: '过期时间',
    dataIndex: 'expiredtm',
    width: 120,
  },
  {
    title: '是否启用',
    dataIndex: 'enable',
    width: 80,
    slots: { customRender: 'userFlag' },
  },
  {
    title: '部门ID',
    dataIndex: 'deptid',
    width: 80,
  },
];

//定义查询表单
export const userQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 3 },
  },
  {
    field: 'status',
    label: '是否启用',
    component: 'Select',
    componentProps: {
      options: [
        { label: '全部', value: '1' },
        { label: '启用', value: '2' },
        { label: '禁用', value: '3' },
      ],
    },
    colProps: { span: 3 },
    defaultValue: '1', //简单类型不使用ref包裹，使用正常
    // defaultValue: ref(''),//简单类型使用ref包裹，使用正常
  },
];

//定义新增/修改表单
export const userFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '用户名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'code',
    label: '登录名',
    component: 'Input',
    required: false,
  },
  {
    field: 'phone',
    label: '手机号',
    component: 'InputNumber',
    componentProps: {
      controls: false,
      style: {
        width: '100%',
      },
    },
    required: false,
  },
  {
    field: 'expiredtm',
    label: '过期时间',
    component: 'DatePicker',
    required: false,
  },
  // {
  //   field: 'enable',
  //   label: '是否启用',
  //   component: 'RadioGroup',
  //   componentProps: {
  //     options: [
  //       { label: '启用', value: true },
  //       { label: '禁用', value: false },
  //     ],
  //   },
  //   defaultValue: true,
  // },
  {
    field: 'enable',
    label: '是否启用',
    component: 'RadioButtonGroup',
    componentProps: {
      options: [
        { label: '启用', value: true },
        { label: '禁用', value: false },
      ],
    },
    defaultValue: true,
  },
  {
    field: 'deptid',
    label: '所在部门',
    component: 'Select',
    componentProps: {},
  },
];
