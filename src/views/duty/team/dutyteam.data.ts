import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义班值管理的表头
 */
export const dutyTeamTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '值班分组ID',
    dataIndex: 'groupid',
    ifShow: false,
    width: 100,
  },
  {
    title: '值班分组名称',
    dataIndex: 'groupname',
    width: 100,
  },
  {
    title: '班值名称',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '班值成员',
    dataIndex: 'membersName',
    width: 100,
  },
];

/**
 * 定义新增/修改班值的表单
 */
export const dutyTeamFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'groupid',
    label: '值班分组',
    component: 'Select',
    componentProps: {},
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'name',
    label: '班值名称',
    component: 'Input',
    colProps: { span: 20 },
    required: true,
  },
];

/**
 * 定义班值成员管理的表头
 */
export const membersTableColumns: BasicColumn[] = [
  {
    title: '班值ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '人员ID',
    dataIndex: 'memberid',
    ifShow: false,
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'memberName',
    width: 50,
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 50,
  },
  {
    title: '是否是值班长',
    dataIndex: 'chief',
    width: 50,
    slots: { customRender: 'chiefFlag' },
  },
];

/**
 * 定义修改班值成员的表单
 */
export const dutyTeamMemberFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: '班值ID',
    component: 'Input',
    required: false,
    show: false,
    componentProps: {
      disabled: true,
    },
  },
  {
    field: 'memberid',
    label: '人员ID',
    component: 'Input',
    required: false,
    show: false,
    componentProps: {
      disabled: true,
    },
  },
  {
    field: 'memberName',
    label: '姓名',
    component: 'Input',
    componentProps: {
      disabled: true,
    },
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
  },
  {
    field: 'chief',
    label: '是否是值班长',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
];

/**
 * 定义班值成员管理新增成员的表头
 */
export const memberListTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '部门ID',
    dataIndex: 'dept',
    ifShow: false,
    width: 100,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 100,
  },
  {
    title: '短号',
    dataIndex: 'cornet',
    width: 100,
  },
];
