import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义值班分组表头
 */
export const dutyGroupTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '所属部门ID',
    dataIndex: 'deptid',
    ifShow: false,
    width: 100,
  },
  {
    title: '所属部门名称',
    dataIndex: 'deptname',
    width: 100,
  },
  {
    title: '值班分组名称',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 50,
  },
  {
    title: '显示列数',
    dataIndex: 'col',
    width: 50,
  },
];

/**
 * 定义新增/修改值班分组表单
 */
export const dutyGroupFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '值班分组名称',
    component: 'Input',
    colProps: { span: 20 },
    required: true,
  },

  {
    field: 'deptid',
    label: '所属部门',
    component: 'Select',
    componentProps: {},
    colProps: { span: 20 },
    required: true,
    // defaultValue: 0,
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
    colProps: { span: 20 },
    required: false,
  },
  {
    field: 'col',
    label: '显示列数',
    component: 'Input',
    colProps: { span: 20 },
    required: false,
  },
];
