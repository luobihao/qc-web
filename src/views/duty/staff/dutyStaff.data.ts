import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义值班人员表头
 */
export const dutyStaffTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '部门ID',
    dataIndex: 'dept',
    ifShow: false,
    width: 100,
  },
  {
    title: '部门',
    dataIndex: 'deptName',
    width: 100,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 100,
  },
  {
    title: '短号',
    dataIndex: 'cornet',
    width: 100,
  },
];

/**
 * 定义新增/修改值班人员表单
 */
export const dutyStaffFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '姓名',
    component: 'Input',
    colProps: { span: 20 },
    required: true,
  },

  {
    field: 'dept',
    label: '所属部门',
    component: 'Select',
    componentProps: {},
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'phone',
    label: '手机号',
    component: 'Input',
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'cornet',
    label: '短号',
    component: 'Input',
    colProps: { span: 20 },
    required: false,
  },
];
