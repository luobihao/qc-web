import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 定义排班管理的表头
 */
export const dutyScheduleTableColumns: BasicColumn[] = [
  {
    title: '排班ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '值班分组ID',
    dataIndex: 'groupid',
    ifShow: false,
    width: 50,
  },
  {
    title: '值班分组名称',
    dataIndex: 'groupname',
    width: 80,
  },
  {
    title: '开始时间',
    dataIndex: 'begintm',
    width: 100,
  },
  {
    title: '结束时间',
    dataIndex: 'endtm',
    width: 100,
  },
  {
    title: '是否为节假日',
    dataIndex: 'holiday',
    slots: { customRender: 'holidayFlag' },
    width: 80,
  },
  {
    title: '描述',
    dataIndex: 'tmtext',
    width: 80,
  },
  {
    title: '成员',
    dataIndex: 'membersName',
    width: 100,
  },
];

/**
 * 定义排班的查询条件表单
 */
export const scheduleQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'groupids',
    label: '值班分组',
    component: 'Select',
    componentProps: {
      mode: 'multiple', //多选
    },
    colProps: { span: 6 },
    required: true,
  },
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker',
    colProps: { span: 6 },
    required: true,
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    colProps: { span: 6 },
    required: true,
  },
];

/**
 * 定义批量新增班次的表单
 */
export const BatchAddShiftFormSchema: FormSchema[] = [
  {
    field: 'groupids',
    label: '值班分组',
    component: 'Select',
    componentProps: {
      mode: 'multiple', //多选
    },
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker',
    required: true,
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    required: true,
  },
  {
    field: 'days',
    label: '值班天数',
    component: 'Input',
    componentProps: {
      placeholder: '一个班次值班的天数',
    },
    required: true,
  },
];

/**
 * 定义批量设置值班成员的表单
 */
export const BatchAddMemberFormSchema: FormSchema[] = [
  {
    field: 'groupid',
    label: '值班分组',
    component: 'Select',
    componentProps: {},
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker',
    required: true,
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    required: true,
  },
  {
    field: 'teamcount',
    label: '班值总数',
    component: 'Input',
    required: true,
  },
  {
    field: 'teamid',
    label: '班值人员',
    component: 'Select',
    componentProps: {},
    colProps: { span: 20 },
    required: true,
  },
];

/**
 * 定义编辑班次的表单
 */
export const SetShiftFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: '排班ID',
    component: 'Input',
    required: true,
    show: false,
  },
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker',
    required: true,
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    required: true,
  },
  {
    field: 'tmtext',
    label: '时间段描述',
    component: 'Input',
  },
  {
    field: 'holiday',
    label: '是否为节假日',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
];

/**
 * 定义值班成员管理的表头
 */
export const membersTableColumns: BasicColumn[] = [
  {
    title: '班次ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '人员ID',
    dataIndex: 'memberid',
    ifShow: false,
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'memberName',
    width: 50,
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 50,
  },
  {
    title: '是否是值班长',
    dataIndex: 'chief',
    width: 50,
    slots: { customRender: 'chiefFlag' },
  },
];

/**
 * 定义值班成员管理新增值班成员的表头
 */
export const memberListTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '部门ID',
    dataIndex: 'dept',
    ifShow: false,
    width: 100,
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    width: 100,
  },
  {
    title: '短号',
    dataIndex: 'cornet',
    width: 100,
  },
];

/**
 * 定义新增值班成员的表单
 */
export const addMemberFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: '排班ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'teamid',
    label: '班值人员',
    component: 'Select',
    required: false,
    componentProps: {},
  },
];

/**
 * 定义修改值班成员的表单
 */
export const dutyScheduleMemberFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: '班次ID',
    component: 'Input',
    required: false,
    show: false,
    componentProps: {
      disabled: true,
    },
  },
  {
    field: 'memberid',
    label: '人员ID',
    component: 'Input',
    required: false,
    show: false,
    componentProps: {
      disabled: true,
    },
  },
  {
    field: 'memberName',
    label: '姓名',
    component: 'Input',
    componentProps: {
      disabled: true,
    },
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
  },
  {
    field: 'chief',
    label: '是否是值班长',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
];
