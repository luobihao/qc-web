import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { DataRecordStatisticsConditionDto } from '/@/api/qms/model/exchangeDataModel';
import moment from 'moment';
/**
 * 定义查询条件表单
 */
export const exchangeDataQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'btm',
    label: '开始时间',
    component: 'DatePicker', //控件类型
    colProps: { span: 6 }, //列属性集合
    required: true, //是否必须
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      //设置不可选
      disabledDate: (current) => {
        //不可选择未来时间
        return current && current > Date.now();
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().startOf('date').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    colProps: { span: 6 },
    required: true,
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      //设置不可选
      disabledDate: (current) => {
        //不可选择未来时间
        return current && current > Date.now();
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().endOf('date').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'uid',
    label: '查询对象',
    component: 'Cascader',
    colProps: { span: 6 },
  },
];
/**
 * 数据记录查询表单结构
 */
export const dataStsConditionSchema: FormSchema[] = [
  {
    field: 'ratetype',
    label: '时间频率类型',
    component: 'Select',
    componentProps: {
    },
    required: true,
    colProps: { span: 19 },

  },
  {
    field: 'ratevalue',
    label: '频率数值',
    component: 'InputNumber',
    componentProps: {
      max: 60
    },
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'ratebefore',
    label: '频率允许提前分钟数',
    component: 'InputNumber',
    helpMessage: '数据时标的提前误差范围',
    required: true,
    colProps: { span: 20 },
    componentProps: {
      max: 60
    },
  },
  {
    field: 'rateafter',
    label: '频率允许延后分钟数',
    component: 'InputNumber',
    helpMessage: '数据时标的延后误差范围',
    required: true,
    colProps: { span: 20 },
    componentProps: {
      max: 60
    },
  },
  {
    field: 'ontime',
    label: '是否统计准点率',
    component: 'RadioGroup',
    componentProps: {
      options: [
        {
          label: '是',
          value: true
        }, {
          label: '否',
          value: false
        }
      ]
    },
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'ontimebefore',
    label: '准点率最大提前分钟数',
    component: 'InputNumber',
    required: true,
    componentProps: {
      max: 60
    },
    colProps: { span: 20 },
  },
  {
    field: 'ontimeafter',
    label: '准点率最大延后分钟数',
    component: 'InputNumber',
    required: true,
    componentProps: {
      max: 60
    },
    colProps: { span: 20 },
  },
]

/**
 * 数据值统计结果
 */
export const dataValueColumns: BasicColumn[] = [
  {
    dataIndex: 'name',
    title: '名称',
    fixed: 'left',
    width: 100,
    slots: { customRender: 'nameAndUnit' },
  },
  // {
  //     dataIndex: 'unit',
  //     title: '数据序列单位',
  //     width: 80,
  // },
  {
    dataIndex: 'avgv',
    title: '平均值',
    width: 80,
  },
  {
    dataIndex: 'sumv',
    title: '合计值',
    width: 80,
  },
  {
    dataIndex: 'diffv',
    title: '差值',
    width: 80,
  },
  {
    dataIndex: 'maxv',
    title: '最大值',
    width: 80,
  },
  {
    dataIndex: 'maxtm',
    title: '最大值时间',
    width: 80,
    format: 'date|YYYY-MM-DD HH:MM:SS'
  },
  {
    dataIndex: 'minv',
    title: '最小值',
    width: 80,
  },
  {
    dataIndex: 'mintm',
    title: '最小值时间',
    width: 80,
    format: 'date|YYYY-MM-DD HH:MM:SS'
  }
]

/**
 * 默认的交换数据统计条件
 */
export const defaultExchangeStatisticsCondition: DataRecordStatisticsConditionDto = {
  ratetype: 1,
  ontimeafter: 0,
  ontimebefore: 0,
  ontime: false,
  ratevalue: 0,
  rateafter: 0,
  ratebefore: 0
}

/**
 * 交换数据日志表列
 */
export const exchangeDataLogColumns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'logid',
    ifShow: false
  },
  {
    title: '起始时间',
    dataIndex: 'begintm',
    width: 100,
  },
  {
    title: '截止时间',
    dataIndex: 'endtm',
    width: 100,
  },
  {
    title: '操作',
    dataIndex: 'optype',
    width: 70,
    customRender: ({ record }) => {
      return record.optype == 1 ? '修改' : record.optype == 2 ? '删除' : '生成写入'
    }
  },
  {
    title: '为人工操作',
    dataIndex: 'ismanual',
    width: 90,
    customRender: ({ record }) => {
      return record.ismanual ? '是' : '否'
    }
  },
  {
    title: '执行结果',
    dataIndex: 'execresult',
    slots: { customRender: 'execresult' },
    width: 80,
  },
  {
    title: 'SQL语句',
    dataIndex: 'sqlstr',
    customRender: ({ record }) => {
      return record.sqlstr ?? '-'
    }
  },
  {
    title: '执行完毕时间',
    dataIndex: 'exectm',
    width: 120,
  },
  {
    title: '操作人名称',
    dataIndex: 'username',
    customRender: ({ record }) => {
      return record.username ?? '-'
    },
    width: 100,
  },
]

/**
 * 交换数据日志表查询条件
 */
export const exchangeDataLogCondition: FormSchema[] = [
  {
    field: 'tm',
    label: '查询时间',
    component: 'RangePicker',
    colProps: { span: 10 },
    labelWidth: 80,
    required: true,
    componentProps: {
      picker: 'date',
      allowClear: false,
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
      showTime: {
        format: 'HH:mm:ss',
      },
    },
    defaultValue: [moment().startOf('date').format('YYYY-MM-DD HH:mm:ss'), moment().endOf('date').format('YYYY-MM-DD HH:mm:ss')],
  },
  {
    field: 'ismanual',
    label: '操作',
    component: 'RadioGroup',
    colProps: { span: 7 },
    labelWidth: 60,
    componentProps: {
      options: [{
        label: '人工',
        value: 1
      }, {
        label: '自动',
        value: 2
      }, {
        label: '全部',
        value: 0
      }]
    },
    defaultValue: 0
  },
  {
    field: 'optype',
    label: '操作类型',
    component: 'Select',
    colProps: { span: 7 },
    componentProps: {
      options: [{
        label: '修改',
        value: 1
      }, {
        label: '删除',
        value: 2
      }, {
        label: '生成写入',
        value: 3
      }]
    },
    labelWidth: 80,
  },
  {
    field: 'execresult',
    label: '执行结果',
    component: 'Select',
    colProps: { span: 8 },
    componentProps: {
      options: [{
        label: '成功',
        value: 1
      }, {
        label: '失败',
        value: 2
      }]
    },
    labelWidth: 80,
  },
  {
    field: 'keywords',
    label: '关键字',
    component: 'Input',
    colProps: { span: 8 },
    labelWidth: 80,
  }
]

/**
 * 新增交换数据日志人工操作表单项
 */
export const manualOperateFormSchema: FormSchema[] = [
  {
    field: 'stcd',
    label: '站点',
    component: 'Select',
    colProps: { span: 22 },
    required: true,
    componentProps: {
      getPopupContainer: triggerNode => document.body
    }
  },
  {
    field: 'begintm',
    label: '时间',
    component: 'RangePicker',
    colProps: { span: 22 },
    required: true,
    componentProps: {
      picker: 'date',
      allowClear: false,
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
      showTime: {
        format: 'HH:mm:ss',
      },
      getPopupContainer: triggerNode => document.body
    },
  },
  {
    field: 'optype',
    label: '操作',
    component: 'RadioGroup',
    required: true,
    componentProps: {
      options: [{
        label: '重新生成',
        value: 3
      }, {
        label: '修改',
        value: 1
      },
      {
        label: '删除',
        value: 2
      }]
    },
    colProps: { span: 22 },
    defaultValue: 3
  },
  {
    field: 'sqlstr',
    label: 'SQL语句',
    component: 'InputTextArea',
    colProps: { span: 22 },
    componentProps: {
      autoSize: { minRows: 5 }
    },
    ifShow: ({ values }) => values.optype !== 3
  }
]
