import { queryAllStation } from '/@/api/qms/station';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 站点表单列配置
 */
export const stationTableColumns: BasicColumn[] = [
  {
    title: '站点编码',
    dataIndex: 'stcd',
    fixed: 'left', //表格列固定在左边
    width: 100,
    ifShow: true,
  },
  {
    title: '站点名称',
    dataIndex: 'stnm',
    width: 150,
  },

  // 向外 = 1、向内 = 2，数值求和
  {
    title: '数据交换方向',
    dataIndex: 'direction',
    slots: { customRender: 'direction' },
    width: 150,
  },
  // {
  //     title: '是否关联',
  //     dataIndex: 'checked',
  //     slots: { customRender: 'checked' },
  //     width: 100,
  //     ifShow: false,
  // },
  // {
  //     title: '参数',
  //     dataIndex: 'params',
  //     align: 'left',
  // },
]


/**
 * 设置关联表格配置
 */
export const stationConnectTableColumns: BasicColumn[] = [
  {
    title: '站点编码',
    dataIndex: 'stcd',
    fixed: 'left', //表格列固定在左边
    width: 150,
  },
  {
    title: '站点名称',
    dataIndex: 'stnm',
    width: 150,
  },
  // {
  //     title: '是否关联',
  //     dataIndex: 'checked',
  //     slots: { customRender: 'checked' },
  //     width: 100,
  //     ifShow: false,
  // },

]


/**
 * 设置站点交换方向表单配置
 */
export const updateStationDirectionsSchema: FormSchema[] = [
  {
    field: 'id',
    label: '分类id',
    component: 'Input',
    colProps: { span: 11 },
    show: false
  },
  {
    field: 'stcds',
    label: '站点编码',
    component: "ApiSelect",
    colProps: { span: 15 },
    componentProps: {
      api: queryAllStation,
      immediate: true, //是否立即请求接口，否则将在第一次点击时候触发请求
      params: {},
      fieldNames: {
        label: "stnm",
        key: "stcd",
        value: "stcd",
        children: 'children'
      },
      multiple: true,
      listHeight: 100,
      disabled: true,
    },
    required: true
  },
  {
    field: 'direction',
    label: '交换方向',
    colProps: { span: 22 },
    component: 'CheckboxGroup',
    componentProps: {
      options: [
        {
          label: '数据入',
          value: 1
        },
        {
          label: '数据出',
          value: 2
        }
      ]
    },
    required: true
  }
];
