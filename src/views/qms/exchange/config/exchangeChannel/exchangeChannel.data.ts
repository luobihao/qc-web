import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 通道表单列配置
 */
export const channelTableColumns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    fixed: 'left', //表格列固定在左边
    width: 50,
  },
  {
    title: '通道名称',
    dataIndex: 'name',
    width: 200,
  },
  // TCP、UDP、使用的协议等，使用枚举类CommunicationChannelTypeEnum
  {
    title: '通信类型',
    dataIndex: 'comtype',
    width: 80,
    slots: { customRender: 'comtype' },
  },
  // 向外 = 1、向内 = 2，数值求和
  {
    title: '数据交换方向',
    dataIndex: 'direction',
    slots: { customRender: 'direction' },
    width: 120,
  }
]


/**
 * 新增或编辑通道表单配置
 */
export const addOrUpdateChannelSchema: FormSchema[] = [
  {
    field: 'id',
    label: '通道ID',
    component: 'Input',
    colProps: { span: 11 },
    show: false
  },
  {
    field: 'name',
    label: '通道名称',
    component: 'Input',
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'comtype',
    label: '通信类型',
    component: 'Select',
    componentProps: {

    },
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'direction',
    label: '数据交换方向',
    component: 'CheckboxGroup',
    required: true,
    colProps: { span: 20 },
    componentProps: {
      options: [
        {
          label: '向内',
          value: 2
        }, {
          label: '向外',
          value: 1
        }
      ]
    },
  },
];
