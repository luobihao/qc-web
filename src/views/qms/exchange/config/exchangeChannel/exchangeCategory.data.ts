import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 数据交换分类表单列配置
 */
export const categoryTableColumns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    fixed: 'left', //表格列固定在左边
    width: 50,
  },
  {
    title: '数据分类名称',
    dataIndex: 'name',
    width: 150,
  },
  {
    title: '使用的通道ID',
    dataIndex: 'cid',
    width: 150,
    slots: { customRender: 'cid' },
    ifShow: false
  },
  {
    title: '存储的数据库表',
    dataIndex: 'tablecode',
    width: 150,
    slots: { customRender: 'tablecode' },
  },
  {
    title: '参数',
    dataIndex: 'params',
    align: 'left'
  }
]


/**
 * 新增或编辑数据分类表单配置
 */
export const addOrUpdateCategorySchema: FormSchema[] = [
  {
    field: 'id',
    label: '数据分类ID',
    component: 'Input',
    colProps: { span: 12 },
    show: false
  },
  {
    field: 'name',
    label: '数据分类名称',
    component: 'Input',
    required: true,
    colProps: { span: 20 },
  },
  {
    field: 'cid',
    label: '使用的通道ID',
    component: 'Select',
    componentProps: {
    },
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'tablecode',
    label: '存储的数据库表代码',
    component: 'Select',
    componentProps: {
    },
    colProps: { span: 20 },
    required: true,
  },
  {
    field: 'params',
    label: '参数',
    align: 'left',
    component: 'CodeMirror',
    componentProps: {
      mode: 'application/json'
    },
    colProps: {
      span: 20,
    },
  }
];
