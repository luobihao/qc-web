import { h } from 'vue';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

/**
 * 数据交换站点表单列配置
 */
export const stationTableColumns: BasicColumn[] = [
  {
    title: '站点编码',
    dataIndex: 'stcd',
    fixed: 'left', //表格列固定在左边
    sorter: (a: TableDataType, b: TableDataType) => a.stcd - b.stcd,
    width: 200,
  },
  {
    title: '站点名称',
    dataIndex: 'stnm',
    defaultSortOrder: 'descend',
    sorter: (a: TableDataType, b: TableDataType) => a.stnm.localeCompare(b.stnm),
    width: 200,
  },
  {
    title: '参数',
    dataIndex: 'params',
    align: 'left',
  },

]


/**
 * 新增或编辑站点表单配置
 */
export const addOrUpdateStationSchema: FormSchema[] = [
  {
    field: 'stcd',
    label: '站点编码',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'stnm',
    label: '站点名称',
    component: 'Input',
    required: true,
    colProps: { span: 11 },
  },
  {
    field: 'params',
    label: '参数',
    component: 'CodeMirror',
    componentProps: {
      mode: 'application/json'
    },
    colProps: {
      span: 22,
    },
  }
];
