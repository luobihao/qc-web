import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { uploadApi } from '/@/api/platform/upload';

/**
 * 定义轮播图列表表头
 */
export const swipperTableColumns: BasicColumn[] = [
  {
    title: '分组',
    dataIndex: 'groupid',
    fixed: 'left',
    width: 60,
    slots: { customRender: 'groupText' },
    // ifShow: false,
  },
  {
    title: 'ID',
    dataIndex: 'id',
    fixed: 'left',
    ifShow: false,
    width: 100,
  },
  {
    title: '栏目ID', //显示表头名称
    dataIndex: 'cid', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 100,
  },
  {
    title: '显示标题文字',
    dataIndex: 'title',
    fixed: 'left',
    width: 100,
    align: 'left',
  },
  {
    title: '文章ID',
    dataIndex: 'articleid',
    fixed: 'left',
    ifShow: true,
    width: 30,
    slots: { customRender: 'articleidDisplay' },
  },
  {
    title: '图片ID',
    dataIndex: 'fid',
    fixed: 'left',
    ifShow: true,
    width: 30,
    slots: { customRender: 'fidDisplay' },
  },
  {
    title: '排序号',
    dataIndex: 'odr',
    width: 30,
    ifShow: true,
  },
  {
    title: '是否跳转至指定栏目',
    dataIndex: 'iscategory',
    width: 80,
    slots: { customRender: 'iscategoryFlag' },
  },
  {
    title: '标记',
    dataIndex: 'flag',
    width: 40,
    slots: { customRender: 'statusFlag' },
  },
];

/**
 * 定义新增/修改轮播图表单
 */
export const swipperFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'groupid',
    label: '分组',
    component: 'Select',
    componentProps: {
      options: [
        { label: '栏目轮播图组', value: 0 },
        { label: '横隔组1', value: 1 },
        { label: '横隔组2', value: 2 },
        { label: '横隔组3', value: 3 },
        { label: '横隔组4', value: 4 },
        { label: '横隔组5', value: 5 },
        { label: '横隔组6', value: 6 },
        { label: '横隔组7', value: 7 },
        { label: '横隔组8', value: 8 },
        { label: '横隔组9', value: 9 },
      ],
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'iscategory',
    label: '是否跳转至指定栏目',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    defaultValue: false,
  },
  {
    field: 'selectArticle',
    label: '素材来源',
    component: 'Input',
    required: false,
    slot: 'selectArticleButton',
  },
  {
    field: 'cid',
    label: '栏目ID',
    component: 'Input',
    required: true,
    show: false,
  },
  {
    field: 'title',
    label: '标题',
    component: 'Input',
    required: false,
    show: true,
    helpMessage: '显示在轮播图图片上方的文字。',
  },
  {
    field: 'articleid',
    label: '文章ID',
    component: 'Input',
    required: false,
    show: true,
    helpMessage: '点击轮播图跳转至的文章。设置为空或小于1时不能点击跳转。',
  },
  {
    field: 'files',
    label: '上传图片',
    component: 'Upload',
    required: false,
    show: true,
    helpMessage: '如不使用从已发布文章中选择的文章封面图片，请点击右侧按钮从本地选择图片进行上传。',
    componentProps: {
      maxSize: 20,
      multiple: false,
      maxNumber: 1,
      api: uploadApi,
      uploadParams: { system: 'cms', module: 'swipper', title: '' },
      // accept: ['image/*'],
      accept: ['jpg', 'jpeg', 'png',],
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
  {
    field: 'fid',
    label: '图片ID',
    component: 'Input',
    required: false,
    show: true,
  },
  {
    field: 'flag',
    label: '状态标记',
    component: 'RadioGroup',
    required: true,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    defaultValue: 0,
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
    required: false,
  },
];
