import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { uploadApi } from '/@/api/platform/upload';

/**
 * 定义栏目表头
 */
export const categoryTableColumns: BasicColumn[] = [
  {
    title: '栏目名称',
    dataIndex: 'name',
    fixed: 'left',
    width: 120,
    align: 'left',
    slots: { customRender: 'name' },
  },
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    // ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '封面图片ID',
    dataIndex: 'cover',
    width: 100,
    // ifShow: false,
  },
  {
    title: '父栏目ID',
    dataIndex: 'pid',
    width: 100,
    ifShow: false,
  },
  {
    title: '栏目内容',
    dataIndex: 'content',
    width: 100,
    align: 'left',
  },
  {
    title: '排序',
    dataIndex: 'odr',
    width: 50,
  },
  {
    title: '标记',
    dataIndex: 'flag',
    width: 80,
    slots: { customRender: 'statusFlag' },
  },
  {
    title: '是否在菜单中显示',
    dataIndex: 'visible',
    width: 80,
    slots: { customRender: 'visibleFlag' },
  },
  {
    title: '审核层级数',
    dataIndex: 'auditcount',
    width: 80,
    slots: { customRender: 'auditFlag' },
  },
  {
    title: '是否控制栏目权限',
    dataIndex: 'deptauth',
    width: 80,
    slots: { customRender: 'deptauthFlag' },
  },
  {
    title: '是否允许发文',
    dataIndex: 'candraft',
    width: 80,
    slots: { customRender: 'candraftFlag' },
  },
];

/**
 * 部门设置的部门表头
 */
export const deptTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '部门名称',
    dataIndex: 'name',
    fixed: 'left',
    width: 200,
    align: 'left',
    slots: { customRender: 'name' },
  },
];

/**
 * 定义查询表单
 */
export const categoryQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'isall',
    label: '包含所有',
    component: 'Select',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    colProps: { span: 4 },
  },
  {
    field: 'isdept',
    label: '根据部门访问',
    component: 'Select',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    colProps: { span: 4 },
  },
];

/**
 * 定义新增/修改栏目表单
 */
export const categoryFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '栏目名称',
    component: 'Input',
    colProps: { span: 11 },
    required: true,
  },
  {
    field: 'pid',
    label: '父栏目',
    component: 'Select',
    componentProps: {},
    colProps: { span: 11 },
    required: false,
    defaultValue: 0,
  },
  {
    field: 'visible',
    label: '是否在导航菜单中显示',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    colProps: { span: 11 },
    defaultValue: true,
  },
  {
    field: 'auditcount',
    label: '审核层级数',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '直接发布', value: 0 },
        { label: '二审发布', value: 1 },
        { label: '三审三校', value: 2 },
      ],
    },
    colProps: { span: 11 },
    defaultValue: 1,
  },
  {
    field: 'candraft',
    label: '是否允许发文',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
    colProps: { span: 11 },
    defaultValue: true,
  },
  {
    field: 'flag',
    label: '标记',
    component: 'RadioGroup',
    required: false,
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '禁用', value: 5 },
      ],
    },
    colProps: { span: 11 },
    defaultValue: 0,
  },
  {
    field: 'cover',
    label: '封面图片文件ID',
    component: 'Input',
    colProps: { span: 11 },
    required: false,
    show: false,
  },
  {
    field: 'files',
    label: '上传封面图片',
    component: 'Upload',
    colProps: { span: 11 },
    required: false,
    // show: false,
    componentProps: {
      maxSize: 20,
      multiple: false,
      maxNumber: 1,
      api: uploadApi,
      uploadParams: { system: 'cms', module: 'category', title: '' },
      accept: ['jpg', 'jpeg', 'png',],
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
  {
    field: 'odr',
    label: '排序号',
    component: 'Input',
    colProps: { span: 11 },
    required: false,
  },
  {
    field: 'content',
    label: '栏目内容',
    component: 'Input',
    colProps: { span: 22 },
    // colProps: { span: 22 },
    required: false,
  },
  {
    field: 'layout',
    label: '版面布局配置',
    component: 'CodeMirror',
    componentProps: {
      mode: 'application/json'
    },
    colProps: { span: 11 },
    required: false,
  },
  {
    field: 'article',
    label: '文章显示配置',
    component: 'CodeMirror',
    componentProps: {
      mode: 'application/json'
    },
    colProps: { span: 11 },
    required: false,
  },
];
