import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { uploadApi } from '/@/api/platform/upload';

/**
 * 定义文章表格的表头
 */
export const articlesTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: true, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '文章栏目',
    dataIndex: 'cid',
    fixed: 'left', //表格列固定在左边
    width: 100,
    slots: { customRender: 'categoryRender' },
  },
  {
    title: '标题',
    dataIndex: 'title',
    fixed: 'left', //表格列固定在左边
    align: 'left',
    width: 120,
    slots: { customRender: 'titleRender' },
  },
  {
    title: '类型',
    dataIndex: 'ctype',
    width: 50,
    ifShow: false,
    slots: { customRender: 'ctypeRender' },
  },
  {
    title: '来源',
    dataIndex: 'source',
    ifShow: false,
    width: 120,
  },
  {
    title: '作者',
    dataIndex: 'author',
    ifShow: false,
    width: 120,
  },
  {
    title: '起草时间',
    dataIndex: 'intm',
    width: 100,
  },
  {
    title: '起草人',
    dataIndex: 'inuser',
    width: 100,
  },
  {
    title: '部门',
    dataIndex: 'indept',
    width: 100,
  },
  {
    title: '更新时间',
    dataIndex: 'uptm',
    width: 120,
    ifShow: false,
  },
  {
    title: '更新人',
    dataIndex: 'upuser',
    width: 120,
    ifShow: false,
  },
  {
    title: '状态',
    dataIndex: 'flag',
    width: 80,
    filters: [
      {
        text: '草稿',
        value: '0',
      },
      {
        text: '待审核',
        value: '1',
      },
      {
        text: '审核通过',
        value: '2',
      },
      {
        text: '审核未通过',
        value: '3',
      },
      {
        text: '二审通过',
        value: '12',
      },
      {
        text: '二审未通过',
        value: '13',
      },
      {
        text: '已发布',
        value: '4',
      },
      {
        text: '发布撤回',
        value: '5',
      },
    ],
    onFilter: (value: string, record: TableDataType) => record.flag == value,
    slots: { customRender: 'flagRender' },
  },
  {
    title: '标签',
    dataIndex: 'tags',
    width: 40,
    slots: { customRender: 'tagsRender' },
  },
  {
    title: '浏览次数',
    dataIndex: 'count',
    width: 50,
  },
];

/**
 * 定义查询条件表单
 */
export const articlesQueryConditionFormSchema: FormSchema[] = [
  {
    field: 'cid',
    label: '栏目',
    component: 'Select',
    componentProps: {},
    colProps: { span: 6 },
    required: false, //是否必须
  },
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker', //控件类型
    colProps: { span: 6 }, //列属性集合
    required: false, //是否必须
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    colProps: { span: 6 },
    required: false,
  },
  {
    field: 'keywords',
    label: '标题/关键字',
    component: 'Input',
    colProps: { span: 6 },
    required: false, //是否必须
  },
  {
    field: 'author',
    label: '来源/作者',
    component: 'Input',
    colProps: { span: 6 },
    required: false, //是否必须
  },
];

/**
 * 定义文章新增/修改表单
 */
export const articleEditFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    colProps: { span: 24 },
    required: false,
    show: false,
  },
  {
    field: 'cid',
    label: '栏目',
    component: 'Select',
    colProps: { span: 8 },
    componentProps: {},
    required: true,
  },
  {
    field: 'title',
    label: '文章标题',
    component: 'Input',
    colProps: { span: 16 },
    required: true,
  },
  {
    field: 'author',
    label: '作者',
    helpMessage: '如文章来源于集团或上级公司等外部，可输入以注明原文作者。',
    component: 'Input',
    colProps: { span: 8 },
    required: false,
  },
  {
    field: 'source',
    label: '来源',
    helpMessage: '如文章来源于集团或上级公司等外部，可输入以注明文章来源。',
    component: 'Input',
    colProps: { span: 8 },
    required: false,
  },
  {
    field: 'keywords',
    label: '关键字',
    helpMessage: '方便文章的搜索，可输入搜索使用的关键字。',
    component: 'Input',
    colProps: { span: 8 },
    required: false,
  },
  {
    field: 'ctype',
    label: '内容类型',
    component: 'RadioButtonGroup',
    colProps: { span: 8 },
    componentProps: {
      options: [
        { label: '富文本', value: '1' },
        // { label: 'MarkDown', value: '2' },
        { label: '外部链接', value: '3' },
      ],
      //文章内容类型选择变化时需要在vue页面中对编辑器使用的组件进行切换；
      //目前只使用富文本和外部链接，均使用tinymce编辑器，不需要进行组件切换
      //使用tinymce编辑器会有p标签，导致外部链接无法正常打开，需要在选择为外部链接时切换为文本框
      //onchange在ts文件中定义事件方法名称但没有方法实现会提示错误，在vue文件中使用updateSchema进行事件添加
      // onchange: handleFormCtypeChange,
    },
    helpMessage: '使用[外部链接]时请将完整的链接地址填写在[内容]中。',
    show: true,
    defaultValue: '1',
  },
  {
    field: 'cover',
    label: '封面图片',
    component: 'Input',
    colProps: { span: 8 },
    required: false,
    show: true,
  },
  {
    field: 'swipperCid',
    label: '首页轮播',
    component: 'RadioButtonGroup',
    colProps: { span: 8 },
    componentProps: {
      options: [
        { label: '不自动发布', value: '0' },
        { label: '发布到首页轮播', value: '1' },
      ],
    },
    required: false,
    helpMessage: '将文章及封面图片自动发布到首页轮播图中显示。如未设置文章封面图片无法自动发布到首页轮播图中。',
    show: true,
    defaultValue: '0',
  },
  {
    field: 'swipperCid',
    label: '轮播图栏目ID',
    component: 'Input',
    colProps: { span: 8 },
    required: false,
    show: false,
  },
  {
    field: 'uploadImages',
    label: '封面图片',
    helpMessage: '从已上传文章内容中的图片选择1张作为文章的封面图片。或点击右侧的按钮上传图片作为文章的封面图片。',
    component: 'Input',
    colProps: { span: 24 },
    required: false,
    defaultValue: '',
    slot: 'articleCoverImageSelect',
  },
  {
    field: 'content',
    label: '文章内容',
    component: 'Input',
    colProps: { span: 24 },
    required: false,
    defaultValue: '',
    slot: 'articleContentEditor',
  },
  // {
  //   field: 'content',
  //   label: '内容',
  //   component: 'Tinymce',
  //   // component: 'MarkDown',
  //   required: false,
  //   defaultValue: '',
  // },
  {
    field: 'attachment',
    label: '附件',
    component: 'Upload',
    required: false,
    show: true,
    componentProps: {
      maxSize: 1024,
      multiple: true,
      maxNumber: 10,
      api: uploadApi,
      uploadParams: { system: 'cms', module: 'attachment', title: '' },
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
];

/**
 * 定义文章基本信息显示的表单，用于在查看文章日志或文章操作时显示文章ID、标题等基本信息
 */
export const articleBasicInfoSchema: DescItem[] = [
  {
    field: 'id',
    label: 'ID',
    show: false,
  },
  {
    field: 'title',
    label: '标题',
    show: true,
  },
];

/**
 * 定义文章日志表格的表头
 */
export const articlesLogsTableColumns: BasicColumn[] = [
  {
    title: '操作时间',
    dataIndex: 'tm',
    width: 150,
  },
  {
    title: '操作人',
    dataIndex: 'user',
    width: 80,
  },
  {
    title: '操作',
    dataIndex: 'op',
    slots: { customRender: 'opRender' },
    width: 80,
  },
  {
    title: '操作信息',
    dataIndex: 'msg',
  },
];

/**
 * 定义文章操作表单，审核不通过、发布撤回操作表单
 */
export const articleOperateFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    show: false,
  },
  {
    field: 'op',
    label: '操作',
    component: 'Input',
    show: false,
  },
  {
    field: 'msg',
    label: '操作说明',
    component: 'InputTextArea',
    required: true,
    componentProps: {
      autoSize: { minRows: 2, maxRows: 6 },
    },
  },
];

export const logsTableTestData: any[] = [
  {
    msg: null,
    op: '0',
    tm: '2023-07-14 15:18:01',
    user: '管理员',
  },
  {
    msg: null,
    op: '0',
    tm: '2023-07-15 15:32:25',
    user: '管理员',
  },
  {
    msg: null,
    op: '1',
    tm: '2023-07-15 15:32:38',
    user: '管理员',
  },
  {
    msg: null,
    op: '0',
    tm: '2023-07-15 15:39:18',
    user: '管理员',
  },
  {
    msg: null,
    op: '0',
    tm: '2023-07-15 15:50:54',
    user: '管理员',
  },
  {
    msg: null,
    op: '1',
    tm: '2023-07-15 22:05:19',
    user: '管理员',
  },
  {
    msg: null,
    op: '2',
    tm: '2023-07-15 22:13:45',
    user: '管理员',
  },
  {
    msg: null,
    op: '0',
    tm: '2023-07-15 22:15:15',
    user: '管理员',
  },
  {
    msg: null,
    op: '1',
    tm: '2023-07-15 22:15:31',
    user: '管理员',
  },
];

/**
 * 移动文章到指定栏目表单
 */
export const moveToCategoryFormSchema: FormSchema[] = [
  {
    field: 'ids',
    label: '文章id集合',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'cid',
    label: '栏目',
    component: 'Select',
    componentProps: {},
  },
];


export const articlesReplaceTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: true, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '文章栏目',
    dataIndex: 'cid',
    fixed: 'left', //表格列固定在左边
    width: 100,
    slots: { customRender: 'categoryRender' },
  },
  {
    title: '标题',
    dataIndex: 'title',
    fixed: 'left', //表格列固定在左边
    align: 'left',
    width: 120,
    slots: { customRender: 'titleRender' },
  },
  {
    title: '敏感词',
    dataIndex: 'sensitivewords',
    align: 'left',
    width: 150,
  },
  {
    title: '类型',
    dataIndex: 'ctype',
    width: 50,
    ifShow: false,
    slots: { customRender: 'ctypeRender' },
  },
  {
    title: '来源',
    dataIndex: 'source',
    ifShow: false,
    width: 120,
  },
  {
    title: '作者',
    dataIndex: 'author',
    ifShow: false,
    width: 120,
  },
  {
    title: '起草时间',
    dataIndex: 'intm',
    width: 100,
  },
  {
    title: '起草人',
    dataIndex: 'inuser',
    width: 100,
  },
  {
    title: '部门',
    dataIndex: 'indept',
    width: 100,
  },
  {
    title: '更新时间',
    dataIndex: 'uptm',
    width: 120,
    ifShow: false,
  },
  {
    title: '更新人',
    dataIndex: 'upuser',
    width: 120,
    ifShow: false,
  },
  {
    title: '状态',
    dataIndex: 'flag',
    width: 80,
    slots: { customRender: 'flagRender' },
  },
  {
    title: '标签',
    dataIndex: 'tags',
    width: 40,
    slots: { customRender: 'tagsRender' },
  },
  {
    title: '浏览次数',
    dataIndex: 'count',
    width: 50,
  },
];



export const articleReplaceWordFormSchema: FormSchema[] = [
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker', //控件类型
    colProps: { span: 6 }, //列属性集合
    required: false, //是否必须
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    colProps: { span: 6 },
    required: false,
  },
  {
    field: 'sensitivewords',
    label: '敏感词',
    component: 'Select',
    slot: 'sensitivewords',
    colProps: { span: 8 },
    required: true, //是否必须
  },
]
export const replaceWordFormSchema: FormSchema[] = [
]
