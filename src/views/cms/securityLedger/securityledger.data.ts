import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { uploadApi } from '/@/api/platform/upload';
import moment from 'moment';

/**
 * 定义安全台账表头
 */
export const securityLedgerTableColumns: BasicColumn[] = [
  {
    title: 'ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: false, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '发生日期',
    dataIndex: 'tm',
    ifShow: true,
    width: 50,
  },
  {
    title: '责任单位',
    dataIndex: 'dept',
    width: 50,
  },
  {
    title: '事故分类',
    dataIndex: 'category',
    width: 50,
  },
  {
    title: '责任分类',
    dataIndex: 'cause',
    width: 50,
  },
  {
    title: '障碍等级',
    dataIndex: 'obstacle',
    width: 50,
  },
  {
    title: '事故详情内容',
    dataIndex: 'content',
    width: 300,
  }
];

//导出Excel的表头数据
// export const exportArrHeader = securityLedgerTableColumns.map((column) => column.title);

/**
 * 定义新增/修改安全台账表单
 */
export const securityLedgerFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'tm',
    label: '事故日期',
    component: 'DatePicker',
    colProps: { span: 24 },
    required: true,
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD',
      },
      //设置不可选择未来时间
      disabledDate: (current) => {
        return current && current > Date.now();
      },
      valueFormat: 'YYYY-MM-DD',
    },
    defaultValue: moment().startOf('date').format('YYYY-MM-DD'),
  },
  {
    field: 'dept',
    label: '责任单位',
    component: 'Input',
    colProps: { span: 12 },
    required: true,
  },
  {
    field: 'category',
    label: '事故分类',
    component: 'Input',
    colProps: { span: 12 },
    required: true,
  },
  {
    field: 'cause',
    label: '责任分类',
    component: 'Input',
    colProps: { span: 12 },
    required: true,
  },
  {
    field: 'obstacle',
    label: '障碍等级',
    component: 'Input',
    colProps: { span: 12 },
    required: false,
  },
  {
    field: 'content',
    label: '事件详情内容',
    component: 'InputTextArea',
    colProps: { span: 24 },
    required: true,
  },
];
