import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { queryTreeTableDetail } from '/@/api/cms/category';
import dayjs from 'dayjs';
import moment from 'moment';

/**
 * 定义`统计查询`的表头
 */
/**
 * 按作者统计表头
 */
export const queryTableAuthorsColumns: BasicColumn[] = [
  {
    title: '发布人',
    dataIndex: 'name',
    slots: { customRender: 'authorName' },
    width: 50,
  },
  {
    title: '发布文章数量',
    dataIndex: 'value',
    width: 50,
  },
];

export const authorArrHeader = queryTableAuthorsColumns.map((column) => column.title);

/**
 * 按部门统计表头
 */
export const queryTableDeptsColumns: BasicColumn[] = [
  {
    title: '部门名称',
    dataIndex: 'name',
    slots: { customRender: 'deptName' },
    width: 50,
  },
  {
    title: '发布文章数量',
    dataIndex: 'value',
    width: 50,
  },
];

export const deptArrHeader = queryTableDeptsColumns.map((column) => column.title);

/**
 * 指定作者/部门查询表头
 */
export const articleTableColumns: BasicColumn[] = [
  {
    title: '标题',
    dataIndex: 'title',
    fixed: 'left', //表格列固定在左边
    align: 'left',
    width: 200,
    slots: { customRender: 'titleRender' },
  },
  {
    title: '作者',
    dataIndex: 'author',
    width: 100,
  },
  {
    title: '部门',
    dataIndex: 'indept',
    width: 100,
  },
  {
    title: '浏览次数',
    dataIndex: 'count',
    width: 100,
  },
];

/**
 * 定义查询表单
 */
export const queryConditionFormSchema: FormSchema[] = [
  {
    field: 'beginEndtm',
    label: '日期范围',
    component: 'RangePicker', //控件类型
    colProps: { span: 7 }, //列属性集合
    componentProps: {
      picker: 'date',
    },
    //默认值设置为当月第一天和最后一天
    defaultValue: [
      dayjs(dayjs().startOf('month').format('YYYY-MM-DD')),
      dayjs(dayjs().endOf('month').format('YYYY-MM-DD')),
    ],
    required: true, //是否必须
  },
  {
    field: 'author',
    label: '作者',
    show: false,
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'dept',
    label: '部门',
    show: false,
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'ids',
    label: '栏目',
    component: 'ApiTreeSelect',
    componentProps: {
      api: queryTreeTableDetail, //数据接口
      params: { isall: false, isdept: false, isvisible: true }, //接口参数
      immediate: true, //是否立即请求接口，否则将在第一次点击时候触发请求
      treeCheckable: true, //	显示checkbox，多选
      showCheckedStrategy: 'SHOW_ALL', //定义选中项回填的方式:默认只显示子节点;SHOW_ALL:显示所有选中节点(包括父节点);SHOW_PARENT:只显示父节点(当父节点下所有子节点都选中时).
    },
    colProps: { span: 7 },
  },
];

// ====================================================以下为新的文章统计文件使用============================================================

/**
 * 按发布人统计文章表头
 */
export const queryTableByInUserColumns: BasicColumn[] = [
  {
    title: '发布人',
    dataIndex: 'inuser',
    width: 80,
    fixed: 'left',
    customCell: ({ rowSpan }) => {
      return {
        rowSpan: rowSpan,
      };
    },
  },
  {
    title: '文章数量',
    dataIndex: 'inUserCount',
    width: 80,
    customCell: ({ rowSpan }) => {
      return {
        rowSpan: rowSpan,
      };
    },
  },
  {
    title: '文章标题',
    dataIndex: 'title',
    align: 'left',
    width: 200,
  },
  {
    title: '发布时间',
    dataIndex: 'intm',
    width: 100,
    customRender: (time) => {
      // console.log('time', time);
      return moment(time.record.intm).format('YYYY-MM-DD');
    },
  },
  {
    title: '发布部门',
    dataIndex: 'indept',
    width: 150,
  },
  {
    title: '作者',
    dataIndex: 'author',
    align: 'left',
    width: 100,
  },
];

//按发布人统计导出表头数组
export const inUserArrHeader = queryTableByInUserColumns.map((column) => column.title);

/**
 * 按发布部门统计文章表头
 */
export const queryTableByInDeptColumns: BasicColumn[] = [
  {
    title: '发布部门',
    dataIndex: 'indept',
    width: 50,
    customCell: ({ rowSpan }) => {
      return {
        rowSpan: rowSpan,
      };
    },
  },
  {
    title: '文章数量',
    dataIndex: 'inDeptCount',
    width: 50,
    customCell: ({ rowSpan }) => {
      return {
        rowSpan: rowSpan,
      };
    },
  },
  {
    title: '发布人',
    dataIndex: 'inuser',
    width: 50,
    customCell: ({ inuserRowSpan }) => {
      return {
        rowSpan: inuserRowSpan,
      };
    },
  },
  {
    title: '文章数量',
    dataIndex: 'inUserCount',
    width: 50,
    customCell: ({ inuserRowSpan }) => {
      return {
        rowSpan: inuserRowSpan,
      };
    },
  },
];
