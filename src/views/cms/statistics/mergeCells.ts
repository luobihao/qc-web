/**
 * 按发布人统计文章合并单元格处理，表格显示
 * @param data 数据源
 */
export function mergeCellsByInUser(data) {
  //声明一个新字段表示当前发布人
  //文章数量因为合并行数与发布人一致，不需要单独处理合并，会出错；直接发布人合并几行文字数量就合并几行
  let currentInUser = '';
  data.map((item, index) => {
    //发布人列处理
    if (currentInUser !== item.inuser) {
      currentInUser = item.inuser;
      let rowSpan = 0;
      for (let i = 0; i < data.length; i++) {
        if (i >= index) {
          if (currentInUser === data[i].inuser) {
            rowSpan += 1;
          } else {
            break;
          }
        }
      }
      item.rowSpan = rowSpan;
    } else {
      item.rowSpan = 0;
      item.inUserCount = 0;
    }
  });
}

/**
 * 按发布部门统计文字合并单元格处理，表格显示
 * @param data 数据源
 */
export function mergeCellsByInDept(data) {
  //声明一个新字段表示当前发布部门、发布人
  //注意：部门的文章数量因为大多数数量都是一样的，不需要单独处理合并，单独处理会出错；直接发布部门合并几行文章数量就合并几行
  let currentInDept = '';
  let currentInUser = '';
  data.map((item, index) => {
    //发布部门列处理
    if (currentInDept !== item.indept) {
      currentInDept = item.indept;
      let rowSpan = 0;
      for (let i = 0; i < data.length; i++) {
        if (i >= index) {
          if (currentInDept === data[i].indept) {
            rowSpan += 1;
          } else {
            break;
          }
        }
      }
      item.rowSpan = rowSpan;
    } else {
      item.rowSpan = 0;
    }

    //发布人列处理
    //注意：发布人的文章数量因为大多数数量都是一样的，不需要单独处理合并，单独处理会出错；直接发布人合并几行文章数量就合并几行
    if (currentInUser !== item.inuser) {
      currentInUser = item.inuser;
      let inuserRowSpan = 0;
      for (let i = 0; i < data.length; i++) {
        if (i >= index) {
          if (currentInUser === data[i].inuser) {
            inuserRowSpan += 1;
          } else {
            break;
          }
        }
      }
      item.inuserRowSpan = inuserRowSpan;
    } else {
      item.inuserRowSpan = 0;
    }
  });
}

/**
 * 处理按发布人统计导出excel需要合并的单元格数据位置
 * @param data 数组数据源
 * @returns 合并单元格位置数组 ['A1:A2','B1:B2',...]
 */
export function computeMergeRow(data) {
  //计算合并行，数据中每条数据已有合并行rowSpan
  const groupData = JSON.parse(JSON.stringify(data));
  console.log('groupData', groupData);
  const mergesArr = [];
  //内容数据合并单元格
  groupData.forEach((item, index) => {
    if (item.rowSpan && item.rowSpan > 0) {
      const indexNew = index + 2;
      //
      const cellArr = ['A', 'B'];
      cellArr.forEach((cellItem) => {
        const cell = cellItem + indexNew + ':' + cellItem + (indexNew + item.rowSpan - 1);
        mergesArr.push(cell);
      });
    }
  });
  // console.log('需要合并的单元格', mergesArr);
  return mergesArr;
}
