import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { uploadApi } from '/@/api/platform/upload';
import moment from 'moment';

/**
 * 定义Led画面表头
 */
export const ledViewTableColumns: BasicColumn[] = [
  {
    title: '画面ID', //显示表头名称
    dataIndex: 'id', //显示数据的字段名或序号
    fixed: 'left', //表格列固定，取值：'left'|'right'|boolean
    ifShow: true, //设置在表格界面中是否显示
    width: 50,
  },
  {
    title: '画面名称',
    dataIndex: 'name',
    ifShow: true,
    width: 200,
  },
  {
    title: '是否固定',
    dataIndex: 'fixed',
    width: 50,
    slots: { customRender: 'fixedFlag' },
  },
  {
    title: '开始时间',
    dataIndex: 'begintm',
    width: 100,
  },
  {
    title: '结束时间',
    dataIndex: 'endtm',
    width: 100,
  }
];

/**
 * 定义新增/修改Led画面表单
 */
export const ledViewFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    required: false,
    show: false,
  },
  {
    field: 'name',
    label: '画面名称',
    component: 'Input',
    colProps: { span: 24 },
    required: true,
  },
  {
    field: 'imageDisplayType',
    label: '显示方式',
    component: 'RadioGroup',
    componentProps: {
      options: [
        { label: '图片作为背景图', value: 1 },
        { label: '左图右文字', value: 2 },
        { label: '右图左文字', value: 3 },
      ],
    },
    required: true,
    defaultValue: 1,
    colProps: { span: 24 },
  },
  {
    field: 'fixed',
    label: '是否固定',
    component: 'RadioGroup',
    required: true,
    componentProps: {
      options: [
        { label: '固定显示', value: true },
        { label: '切换显示', value: false },
      ],
    },
    defaultValue: 1,
    colProps: { span: 24 },
  },
  {
    field: 'begintm',
    label: '开始时间',
    component: 'DatePicker',
    colProps: { span: 12 },
    required: true,
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      //设置不可选择未来时间
      // disabledDate: (current) => {
      //     return current && current > Date.now();
      // },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().startOf('date').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'endtm',
    label: '结束时间',
    component: 'DatePicker',
    colProps: { span: 12 },
    required: true,
    componentProps: {
      //显示时分秒配置，不写不显示选择具体的时分秒
      showTime: {
        format: 'YYYY-MM-DD HH:mm:ss',
      },
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
    defaultValue: moment().startOf('date').add(1, 'days').format('YYYY-MM-DD HH:mm:ss'),
  },
  {
    field: 'title',
    label: '显示标题',
    component: 'Input',
    colProps: { span: 24 },
    required: false,
  },
  {
    field: 'fontSize',
    label: '显示文字字号',
    component: 'Select',
    componentProps: {
      options: [
        { label: '12pt', value: '12pt' },
        { label: '14pt', value: '14pt' },
        { label: '18pt', value: '18pt' },
        { label: '24pt', value: '24pt' },
        { label: '36pt', value: '36pt' },
        { label: '42pt', value: '42pt' },
        { label: '48pt', value: '48pt' },
        { label: '52pt', value: '52pt' },
      ],
    },
    colProps: { span: 12 },
    required: false,
    defaultValue: '12pt',
  },
  {
    field: 'scrollStep',
    label: '滚动速度',
    component: 'InputNumber',
    helpMessage: '文字的滚动速度，数值越大滚动速度越快，可填小数，不为负数',
    colProps: { span: 12 },
    required: false,
    defaultValue: 1,
  },
  {
    field: 'content',
    label: '显示文字内容',
    component: 'InputTextArea',
    componentProps: {
      autoSize: { minRows: 5, maxRows: 10 },
    },
    colProps: { span: 24 },
    required: false,
  },
  {
    field: 'imageId',
    label: '显示图片',
    component: 'Upload',
    required: false,
    show: true,
    helpMessage: '请点击右侧按钮从本地选择图片进行上传。',
    componentProps: {
      maxSize: 20,
      multiple: false,
      maxNumber: 1,
      api: uploadApi,
      uploadParams: { system: 'led', module: 'view', title: '' },
      // accept: ['image/*'],
      accept: ['jpg', 'jpeg', 'png', 'gif'],
      showPreviewNumber: true,
      showUploadList: {
        showPreviewIcon: true,
        showRemoveIcon: true,
        showDownloadIcon: false,
      },
    },
  },
];
