import { DeptDtoList, DeptSimpleDtoList } from './model/deptModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  GetAll = '/dept/all',
  GetProjectValidDept = '/qc/dept/projectvalid',
}

/**
 * @description: 查询前置机数据，使用body传入查询条件
 */
export function getAllDepts(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptDtoList>(
    {
      url: Api.GetAll,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取项目有效部门集合
 */
export function getProjectValidDept(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.GetProjectValidDept,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}
