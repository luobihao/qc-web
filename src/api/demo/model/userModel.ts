/**
 * UserQueryConditionDto
 */
export interface UserQueryConditionDto {
  /**
   * 关键字，关键字可输入用户名称、登录名、手机号中的一种
   */
  keywords?: string;
  /**
   * 用户状态，1获取全部，2启用，3禁用
   */
  status: number;
}
/**
 * UserDto
 */
export interface UserDto {
  /**
   * 登录名
   */
  code?: string;
  /**
   * 部门ID
   */
  deptid: number;
  // deptid: string;
  /**
   * 是否启用
   */
  enable: boolean;
  /**
   * 过期时间
   */
  expiredtm?: string;
  /**
   * 用户ID
   */
  id: number;
  /**
   * 用户名称
   */
  name: string;
  /**
   * 手机号
   */
  phone?: string;
}
export type UserDtoList = UserDto[];
/**
 * UserStatusDto
 */
export interface UserStatusDto {
  key: number;
  value: string;
}

export type UserStatusDtoList = UserStatusDto[];

/**
 * UserAddDto
 */
export interface UserAddDto {
  /**
   * 登录名
   */
  code?: string;
  /**
   * 部门ID
   */
  deptid: number;
  /**
   * 是否启用
   */
  enable: boolean;
  /**
   * 过期时间
   */
  expiredtm?: string;
  /**
   * 用户名称
   */
  name: string;
  /**
   * 手机号
   */
  phone?: string;
}
