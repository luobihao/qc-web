/**
 * DeptDto
 */
export interface DeptDto {
  /**
   * 部门ID
   */
  id: number;
  // id: string;
  /**
   * 部门名称
   */
  name: string;
}

/**
 * 部门DTO集合
 */
export type DeptDtoList = DeptDto[];

/**
 * 部门简要信息DTO
 */
export interface DeptSimpleDto {
  //部门id
  id: string;

  //部门名称
  name: string;

  //父级id
  parentid: string;

  //层级，从0开始
  level: number;
}

/**
 * 部门简要信息DTO集合
 */
export type DeptSimpleDtoList = DeptSimpleDto[];
