import { UserQueryConditionDto, UserDto, UserStatusDto, UserAddDto } from './model/userModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  GetAllStatus = '/user/status',
  Query = '/user/query',
  Get = '/user/get',
  Add = '/user/add',
  Update = '/user/update',
  Delete = '/user/delete',
}

/**
 * @description: 获取所有用户状态列表
 */
export function getAllStatus(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserStatusDtoList>(
    {
      url: Api.GetAllStatus,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询
 */
export function queryUser(params: UserQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<UserDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
      //使用mock或测试地址时，可通过设置apiUrl使用.env中配置的VITE_PROXY不同别名
      // apiUrl: '/demo-api',
      //使用mock或测试接口时，如返回的直接为数据，不包含渠成网关中统一定义的code和msg，可通过设置isQuChengGatewayUnifyFormat为false
      // isQuChengGatewayUnifyFormat: false,
      //可选参数
      // joinParamsToUrl: true,
      // joinTime: false,
    },
  );
}
/**
 * @description: 新增
 */
export function addUser(params: UserAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 修改
 */
export function updateUser(params: UserDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 获取指定
 */
export function getUser(params: { id: number }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserDto>(
    {
      url: Api.Get,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除
 */
export function deleteUser(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      //采用url传参需要指定joinParamsToUrl为true
      url: Api.Delete + '/' + id,
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
