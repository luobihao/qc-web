import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { getMenuListResultModel } from './model/menuModel';

import {
  MenuQueryConditionDto,
  MenuSimpleDtoList,
  MenuTreeSimple,
  MenuDtoList,
  MenuTree,
  MenuTreeTableDetailDto,
  MenuDto,
} from './model/menuModel';
import { TreeDataDto } from '../platform/model/qcPublicModel';


enum Api {
  QueryListSimple = '/qc/menu/listsimple',
  QueryTreeSimple = '/qc/menu/treesimple',
  QueryListDetail = '/qc/menu/listdetail',
  QueryTreeDetail = '/qc/menu/treedetail',
  QueryTreeTable = '/qc/menu/treetable',
  Get = '/qc/menu/get',
  Add = '/qc/menu/add',
  Update = '/qc/menu/update',
  Delete = '/qc/menu/delete',
  GetMenuApiTree = '/qc/menu/getapistree',
  SetMenuApis = '/qc/menu/setapis',
}

/**
 * @description: 查询简要菜单列表
 */
export function getMenuListSimple(params: MenuQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<MenuSimpleDtoList>(
    {
      url: Api.QueryListSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询简要菜单树形
 */
export function getMenuTreeSimple(params: MenuQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<MenuTreeSimple>(
    {
      url: Api.QueryTreeSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询菜单详情列表
 */
export function getMenuListDetail(params: MenuQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<MenuDtoList>(
    {
      url: Api.QueryListDetail,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询菜单详情树形
 */
export function getMenuTreeDetail(params: MenuQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<MenuTree>(
    {
      url: Api.QueryTreeDetail,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询菜单详情树表
 */
export function getMenuTreeTableDetail(
  params: MenuQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<MenuTreeTableDetailDto>(
    {
      url: Api.QueryTreeTable,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定菜单
 */
export function getMenu(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<MenuDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增菜单
 */
export function addMenu(params: MenuDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改菜单
 */
export function updateMenu(params: MenuDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除菜单
 */
export function deleteMenu(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}


/**
 * @description: 获取Api信息tree
 * @param id: 菜单ID
 */
export function getMenuApisTree(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeDataDto>(
    {
      url: Api.GetMenuApiTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置菜单Api关联关系
 */
export function setMenuApis(params: MenuAPISetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetMenuApis,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
