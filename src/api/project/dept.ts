import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  DeptDto,
  DeptDtoList,
  DeptSimpleDtoList,
  DeptTreeTableDto,
  DeptUserSetDto,
  DeptTreeSimpleDto
} from './model/deptModel';
import { TreeNodeList, TreeDataDto } from '../platform/model/qcPublicModel';

enum Api {
  QueryListDetail = '/qc/dept/listdetail',
  QueryListSimple = '/qc/dept/listsimple',
  QueryTreeTable = '/qc/dept/treetable',
  GetProjectDeptsTree = '/qc/dept/gettree',
  Get = '/qc/dept/get',
  Add = '/qc/dept/add',
  Update = '/qc/dept/update',
  Delete = '/qc/dept/delete',
  GetDeptUsersTree = '/qc/dept/getuserstree',
  SetDeptUsers = '/qc/dept/setusers',
  ProjectValidDept = '/qc/dept/projectvalid',
  ProjectValidDeptTree = '/qc/dept/projectvalidtree',
  UserValidDept = '/qc/dept/uservalid',
  Valid = '/qc/dept/valid',
  GetDefaultProjectValidTree = '/qc/dept/validtree',
  GetTreeSimple = '/qc/dept/treesimplebydept',
  GetDeptLeadersTree = '/qc/dept/getleaderstree',
  SetDeptLeaders = '/qc/dept/setleaders',
  GetDeptChildsList = '/qc/dept/deptchildslist',
  GetDeptChildsTree = '/qc/dept/deptchildstree'

}

/**
 * @description: 查询指定项目下所有部门详情列表
 * @param pcode 项目编码
 */
export function queryListDetail(params: { pcode: string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptDtoList>(
    {
      url: Api.QueryListDetail,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定项目下所有部门简要列表
 * @param pcode 项目编码
 */
export function queryListSimple(params: { pcode: string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.QueryListSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定项目下所有有效部门树形集合
 * @param pcode 项目编码
 */
export function queryTreeTable(params: { pcode: string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptTreeTableDto>(
    {
      url: Api.QueryTreeTable,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取项目中的部门tree
 * @param pcode 项目编码
 */
export function getUserDeptsTree(params: { pcode: string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeNodeList>(
    {
      url: Api.GetProjectDeptsTree,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定部门信息
 * @param params
 */
export function getDept(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增部门信息
 */
export function addDept(params: DeptDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改部门信息
 */
export function updateDept(params: DeptDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除部门信息
 * @param id 部门ID
 */
export function deleteDept(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 获取部门人员树形信息
 * @param id: 角色ID
 */
export function getDeptUsersTree(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeDataDto>(
    {
      url: Api.GetDeptUsersTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置部门用户关联关系
 */
export function setDeptUsers(params: DeptUserSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetDeptUsers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定项目下有效部门简要集合
 * @param id 项目编码
 */
export function getProjectValidDept(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.ProjectValidDept,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定项目下有效部门树结构集合
 * @param id 项目编码
 */
export function getProjectValidDeptTree(params: { id: string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeNodeList>(
    {
      url: Api.ProjectValidDeptTree,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定项目下当前用户所在部门简要集合
 * @param id 项目编码
 */
export function getUserValidDept(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.UserValidDept,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取平台中的默认项目中的所有有效部门
 * @param id 项目编码
 */
export function getDefaultProjectValidDepts(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.Valid,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取平台中的默认项目中的所有有效部门树形结构
 * @param id 项目编码
 */
export function getDefaultProjectValidTree(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeNodeList>(
    {
      url: Api.GetDefaultProjectValidTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * 
 * @param id 
 * @param mode 
 * @description: 获取指定部门下有效子部门简要树形集合
 */
export function getTreeSimple(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptTreeSimpleDto>(
    {
      url: Api.GetTreeSimple,
      params: { deptid: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定部门负责人关联
 * @param id 部门id
 * 
 */
export function getDeptLeadersTree(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<QCTreeNode>(
    {
      url: Api.GetDeptLeadersTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置指定部门负责人关联
 * @param id 部门id
 * 
 */
export function setDeptLeaders(params: DeptUserSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetDeptLeaders,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定部门下的有效部门List-getDeptChildsList
 */
export function GetDeptChildsList(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptTreeSimpleDto>(
    {
      url: Api.GetDeptChildsList,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定部门下的有效部门tree-getDeptChildsTree
 */
export function GetDeptChildsTree(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptTreeSimpleDto>(
    {
      url: Api.GetDeptChildsTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}
