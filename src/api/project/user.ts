import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  UserQueryConditionDto,
  UserSimpleInfoDtoList,
  UserInfoDtoList,
  UserAddDto,
  UserEditDto,
  UserInfoDto,
  UserDeleteDto,
  UserImportDto,
  UserDeptsConditionDto,
  UserDeptDtoList,
  UserDeptSetDto,
  UserProjectDtoList,
  UserProjectSetDto,
  UserCustomInfoSetDto
} from './model/userModel';

import { TreeDataDto } from '../platform/model/qcPublicModel';

enum Api {
  QueryPlatformUsers = '/qc/user/queryplatform',
  QueryPlatformUserSimple = '/qc/user/queryplatformsimple',
  QueryProjectUsers = '/qc/user/queryproject',
  QueryProjectUserSimple = '/qc/user/queryprojectsimple',
  AddPlatformUser = '/qc/user/addplatform',
  AddProjectUser = '/qc/user/addproject',
  Get = '/qc/user/get',
  Update = '/qc/user/update',
  DeletePlatformUser = '/qc/user/deleteplatform',
  DeleteProjectUser = '/qc/user/deleteproject',
  Disable = '/qc/user/disable',
  Enable = '/qc/user/enable',
  Import = '/qc/user/import',
  ResetPassword = '/qc/user/resetpwd',
  GetUserDepts = '/qc/user/getdepts',
  GetUserDeptsTree = '/qc/user/getdeptstree',
  SetUserDepts = '/qc/user/setdepts',
  GetUserProjects = '/qc/user/getproject',
  SetUserProjects = '/qc/user/setprojects',
  GetDeptUsers = '/qc/user/deptusers',
  RestorePlatformUser = '/qc/user/restore',//恢复已删除的平台用户-restorePlatformUser
  GetUserCustomInfo = '/qc/usercustominfo/get', //获取用户自定义信息
  SetUserCustomInfo = '/qc/usercustominfo/set', //设置用户自定义信息
}

/**
 * @description: 查询平台用户详情列表
 */
export function queryPlatformUsersDetail(
  params: UserQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<UserInfoDtoList>(
    {
      url: Api.QueryPlatformUsers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询平台用户简要列表
 */
export function queryPlatformUserSimple(
  params: UserQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<UserSimpleInfoDtoList>(
    {
      url: Api.QueryPlatformUserSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定项目中的用户详情列表
 */
export function queryProjectUsersDetail(
  params: UserQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<UserInfoDtoList>(
    {
      url: Api.QueryProjectUsers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定项目中的用户简要列表
 */
export function queryProjectUsersSimple(
  params: UserQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<UserSimpleInfoDtoList>(
    {
      url: Api.QueryProjectUserSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加平台用户
 */
export function addPlatformUser(params: UserAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddPlatformUser,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加项目用户
 */
export function addProjectUser(params: UserAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddProjectUser,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定用户信息
 * @param id: 用户ID
 */
export function getUser(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserInfoDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改用户信息
 */
export function updateUser(params: UserEditDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除平台用户
 * @param id 用户ID
 */
export function deletePlatformUser(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeletePlatformUser,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 删除项目用户
 */
export function deleteProjectUser(params: UserDeleteDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteProjectUser,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 禁用用户
 */
export function disableUser(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Disable,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 启用用户
 */
export function enableUser(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Enable,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 导入用户 //TODO 后台未实现
 */
export function importUser(params: UserImportDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Import,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 重置密码
 */
export function resetPassword(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.ResetPassword,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 获取部门信息列表，返回有用户是否已经关联字段
 * @param id: 角色ID
 */
export function getUserDepts(params: UserDeptsConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<UserDeptDtoList>(
    {
      url: Api.GetUserDepts,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取部门树形信息
 * @param id: 角色ID
 */
export function getUserDeptsTree(params: UserDeptsConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TreeDataDto>(
    {
      url: Api.GetUserDeptsTree,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置用户部门关联关系
 */
export function setUserDepts(params: UserDeptSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetUserDepts,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取项目信息列表，返回有用户是否已经关联字段
 * @param id: 角色ID
 */
export function getUserProjects(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserProjectDtoList>(
    {
      url: Api.GetUserProjects,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 设置用户项目关联关系
 */
export function setUserProjects(params: UserProjectSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetUserProjects,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定部门中的有效用户集合-getDeptUsers
 */
export function getDeptUsers(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserDeptUsersDtoList>(
    {
      url: Api.GetDeptUsers,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 恢复已删除用户
 */
export function restorePlatformUser(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.RestorePlatformUser,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 用户自定义信息获取
 */
export function getUserCustomInfo(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.GetUserCustomInfo,
    },
    {
      errorMessageMode: mode,
    }
  )
}

/**
 * @description: 用户自定义信息设置
 */
export function setUserCustomInfo(params: UserCustomInfoSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetUserCustomInfo,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
