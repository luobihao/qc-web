import { UserSimpleInfoDto } from './userModel';
import { MenuSimpleDto } from './menuModel';
import { DeptSimpleDto } from './deptModel';

/**
 * 角色查询条件DTO
 * 查询平台用户时为空，查询项目用户时必须指定
 */
export interface RoleQueryConditionDto {
  //关键字
  words?: string;
  //项目编码
  project?: string;
}

/**
 * 角色信息DTO
 */
export interface RoleDto {
  //角色ID
  id: string;
  //项目编码
  pcode: string;
  //角色名称
  name: string;
  //状态标记
  flag: number;
  //描述
  description: string;
}

/**
 * 角色信息DTO集合
 */
export type RoleDtoList = RoleDto[];

/**
 * 角色用户关联信息DTO
 */
export interface RoleUserDto {
  //用户简要信息
  user: UserSimpleInfoDto;
  //是否已经关联
  checked: boolean;
}

/**
 * 角色用户关联信息DTO集合
 */
export type RoleUserDtoList = RoleUserDto[];

/**
 * 角色用户关联设置DTO
 */
export interface RoleUserSetDto {
  //角色ID
  roleid: number | string;
  //用户ID集合
  uids?: number[] | string[];
}

/**
 * 角色菜单关联信息DTO
 */
export interface RoleMenuDto {
  //菜单简要信息
  menu: MenuSimpleDto;
  //是否已经关联
  checked: boolean;
}

/**
 * 角色菜单关联信息DTO集合
 */
export type RoleMenuDtoList = RoleMenuDto[];

/**
 * 角色菜单关联设置DTO
 */
export interface RoleMenuSetDto {
  //角色ID
  roleid: number | string;
  //菜单ID集合
  mids: number[] | string[];
}

/**
 * 角色部门关联信息DTO
 */
export interface RoleDeptDto {
  //部门简要信息
  dept: DeptSimpleDto;
  //是否已经关联
  checked: boolean;
}

/**
 * 角色部门关联信息DTO集合
 */
export type RoleDeptDtoList = RoleDeptDto[];

/**
 * 角色部门关联设置DTO
 */
export interface RoleDeptSetDto {
  //角色ID
  roleid: number | string;
  //部门ID集合
  dids: number[] | string[];
}

/**
 * RoleAPISetDto 角色API关联设置DTO
 */
export interface RoleAPISetDto {
  /**
   * api唯一标识集合
   */
  apis?: string[];
  /**
   * 角色ID
   */
  roleid: number;
  [property: string]: any;
}
