import type { RouteMeta } from 'vue-router';
export interface RouteItem {
  path: string;
  component: any;
  meta: RouteMeta;
  name?: string;
  alias?: string | string[];
  redirect?: string;
  caseSensitive?: boolean;
  children?: RouteItem[];
}

/**
 * @description: Get menu return value
 */
export type getMenuListResultModel = RouteItem[];

/**
 * 菜单查询条件DTO
 */
export interface MenuQueryConditionDto {
  //项目编码
  project: string;
  //关键字
  keywords?: string;
}

/**
 * 菜单信息DTO
 */
export interface MenuDto {
  //ID
  id: number;
  //项目编码
  pcode: string;
  //菜单名称
  name: string;
  //图标
  icon: string;
  //路径
  path: string;
  //父级ID
  parentid: number;
  //状态标记
  flag: number;
  //是否显示在菜单中
  visible: boolean;
  //内容/文件路径
  content: string;
  //排序号
  order: string;
  //层级，菜单的层级从1开始
  level: number;
}

/**
 * 菜单信息DTO集合
 */
export type MenuDtoList = MenuDto[];

/**
 * 菜单简要信息DTO
 */
export interface MenuSimpleDto {
  //菜单ID
  id: number;
  //父ID
  parentid: number;
  //名称
  name: string;
  //层级，菜单的层级，从1开始
  level: number;
}

/**
 * 菜单简要信息DTO集合
 */
export type MenuSimpleDtoList = MenuSimpleDto[];

/**
 * 菜单树形简要信息DTO
 */
export interface MenuTreeSimpleDto {
  info: MenuSimpleDto;
  childs: MenuTreeSimpleDto[];
}

/**
 * 菜单树形简要信息DTO集合
 */
export type MenuTreeSimple = MenuTreeSimpleDto[];

/**
 * 菜单树形详细信息DTO
 */
export interface MenuTreeDto {
  info: MenuDto;
  childs: MenuTreeDto[];
}

/**
 * 菜单树形详细信息DTO集合
 */
export type MenuTree = MenuTreeDto[];

/**
 * 菜单TreeTable详细信息DTO
 */
export interface MenuTreeTableDetailDto {
  //ID
  id: number;
  value: number;
  //项目编码
  pcode: string;
  //菜单名称
  name: string;
  label: string;
  //图标
  icon: string;
  //路径
  path: string;
  //父级ID
  parentid: number;
  //状态标记
  flag: number;
  //是否显示在菜单中
  visible: boolean;
  //内容/文件路径
  content: string;
  //排序号
  order: string;
  //子级菜单
  children: MenuTreeTableDetailDto[];
}
/**
 * MenuAPISetDto 菜单关联Api接口DTO
 */
export interface MenuAPISetDto {
  /**
   * api唯一标识集合
   */
  apis?: string[];
  /**
   * 菜单ID
   */
  menuid: number;
  [property: string]: any;
}
