import { DeptSimpleDto } from './deptModel';
import { ProjectDto } from './projectModel';

/**
 * 用户查询条件DTO UserQueryConditionDto
 */
export interface UserQueryConditionDto {
  /**
   * 部门ID，可以查询指定部门下的用户
   */
  dept?: string;
  /**
   * 是否包含已删除，是否查询已删除用户
   */
  includedelete: boolean;
  /**
   * 项目编码，查询平台用户时为空，查询项目用户时必须指定
   */
  project?: string;
  /**
   * 关键字
   */
  words?: string;
  [property: string]: any;
}

/**
 * 用户信息DTO
 */
export interface UserInfoDto {
  //用户ID
  id: number;
  //名称
  name: string;
  //登录用户名
  code: string;
  //手机号
  phone: string;
  //状态标记
  flag: number;
  //允许登录起始时间
  allowtime: string;
  //过期时间
  expiretime: string;
  //微信ID
  weixinid: string;
  //排序号
  order: number;
  //短号
  cornet: string;
  //办公室/座机号码
  tel: string;
  //职务
  duty: string;
}

/**
 * 用户信息DTO集合
 */
export type UserInfoDtoList = UserInfoDto[];

/**
 * 用户简要信息DTO
 */
export interface UserSimpleInfoDto {
  // 用户ID
  id: number;
  // 用户名称
  name: string;
  // 手机号，经过脱敏后的
  phone: string;
}

/**
 * 用户简要信息DTO集合
 */
export type UserSimpleInfoDtoList = UserSimpleInfoDto[];

/**
 * 用户新增DTO
 */
export interface UserAddDto {
  //名称
  name: string;
  //登录用户名
  code: string;
  //手机号
  phone: string;
  //状态标记
  flag: number;
  //允许登录起始时间
  allowtime: string;
  //过期时间
  expiretime: string;
  //微信ID
  weixinid: string;
  //排序号
  order: number;
  //项目编码，可以为空
  project?: string;
  //选中的部门ID集合，在项目中新增用户时可选传入，可新增时设置用户与1个或多个部门关联
  depts?: number[];
  //短号
  cornet: string;
  //办公室/座机号码
  tel: string;
  //职务
  duty: string;
}

/**
 * 用户修改DTO
 */
export interface UserEditDto {
  //用户ID
  id: number;
  //名称
  name: string;
  //登录用户名
  code: string;
  //手机号
  phone: string;
  //状态标记
  flag: number;
  //允许登录起始时间
  allowtime: string;
  //过期时间
  expiretime: string;
  //微信ID
  weixinid: string;
  //排序号
  order: number;
  //选中的部门ID集合，在项目中修改用户时可选传入，可修改时设置用户与1个或多个部门关联
  depts?: number[];
  //短号
  cornet: string;
  //办公室/座机号码
  tel: string;
  //职务
  duty: string;
}

/**
 * 用户导入DTO
 */
export interface UserImportDto {
  //文件ID
  fileid: number;
  //名称列
  name: string;
  //手机号列
  phone: string;
  //项目编码
  project: string;
}

/**
 * 用户部门信息获取条件DTO
 */
export interface UserDeptsConditionDto {
  id: string;
  project: string;
}

/**
 * 用户部门关联信息DTO
 */
export interface UserDeptDto {
  //部门简要信息
  dept: DeptSimpleDto;
  //是否已经关联
  checked: boolean;
}

/**
 * 用户部门关联信息DTO集合
 */
export type UserDeptDtoList = UserDeptDto[];

/**
 * 用户部门关联设置DTO
 */
export interface UserDeptSetDto {
  //用户ID
  userid: number | string;
  //项目编码
  project: string;
  //部门ID集合
  dids?: number[] | string[];
}

/**
 * 用户项目关联信息DTO
 */
export interface UserProjectDto {
  //项目信息
  project: ProjectDto;
  //是否已经关联
  checked: boolean;
}

/**
 * 用户项目关联信息DTO集合
 */
export type UserProjectDtoList = UserProjectDto[];

/**
 * 用户项目关联设置DTO
 */
export interface UserProjectSetDto {
  //用户ID
  userid: number | string;
  //项目编码集合
  pids: number[] | string[];
}

/**
 * 用户删除DTO
 */
export interface UserDeleteDto {
  //用户ID
  id: number;
  //项目编码，删除平台用户时为空，删除项目用户时必须指定
  project?: string;
}


/**
 * UserCustomInfoSetDto 用户自定义信息设置DTO
 */
export interface UserCustomInfoSetDto {
  /**
   * json格式字符串
   */
  info?: string;
  [property: string]: any;
}
