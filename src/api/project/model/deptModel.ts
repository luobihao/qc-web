/**
 * 部门信息简要DTO
 */
export interface DeptSimpleDto {
  //ID
  id: number;
  //名称
  name: string;
  //父级ID
  parentid?: number;
  //层级，从0开始
  level: number;
}

/**
 * 部门信息简要DTO集合
 */
export type DeptSimpleDtoList = DeptSimpleDto[];

/**
 * 部门信息DTO
 */
export interface DeptDto {
  //部门ID
  id: number;
  //项目编码
  pcode: string;
  //部门名称
  name: string;
  //父级ID
  parentid?: number;
  //状态标记
  flag: number;
  //排序号
  order: string;
  //备注
  description: string;
  //层级
  level?: number;
}

/**
 * 部门信息DTO集合
 */
export type DeptDtoList = DeptDto[];

/**
 * 部门TreeTable信息DTO
 */
export interface DeptTreeTableDto {
  //部门ID
  id: number;
  //项目编码
  pcode: string;
  //部门名称
  name: string;
  //父级ID
  parentid?: number;
  //状态标记
  flag: number;
  //排序号
  order: string;
  //备注
  description: string;
  //层级
  level?: number;
  //子级部门
  children: DeptTreeTableDto[];
}

/**
 * 部门用户关联设置DTO
 */
export interface DeptUserSetDto {
  //部门ID
  deptid: number | string;
  //用户id集合
  uids: number[] | string[];
}

/**
 * DeptTreeSimpleDto 部门TreeSimple简要信息DTO
 */
export interface DeptTreeSimpleDto {
  children?: DeptSimpleDto;
  /**
   * ID
   */
  id: number;
  /**
   * 层级，从0开始
   */
  level: number;
  /**
   * 名称
   */
  name: string;
  /**
   * 父ID
   */
  parentid?: number;
  [property: string]: any;
}

/**
 * DeptSimpleDto
 */
export interface DeptSimpleDto {
  /**
   * ID
   */
  id: number;
  /**
   * 层级，从0开始
   */
  level: number;
  /**
   * 名称
   */
  name: string;
  /**
   * 父级ID
   */
  parentid?: number;
  [property: string]: any;
}


/**
 * QCTreeNode
 */
export interface QCTreeNode {
  /**
   * 设置独立节点是否展示 Checkbox
   */
  checkable: boolean;
  /**
   * 子节点集合
   */
  children: ChildElement[];
  /**
   * 禁掉 checkbox
   */
  disableCheckbox: boolean;
  /**
   * 是否禁用
   */
  disabled: boolean;
  /**
   * 图标
   */
  icon?: string;
  /**
   * 是否是叶子节点
   */
  isLeaf: boolean;
  /**
   * 值，id
   */
  key: string;
  /**
   * 是否可选
   */
  selectable: boolean;
  /**
   * 文字，显示名称、标题
   */
  title: string;
  /**
   * 数据类型，用于同时有多种对象在树形结构中时后台标识返回的数据/对象类型
   */
  type?: string;
  [property: string]: any;
}

/**
 * QCTreeNode
 */
export interface ChildElement {
  /**
   * 设置独立节点是否展示 Checkbox
   */
  checkable: boolean;
  /**
   * 子节点集合
   */
  children: ChildElement[];
  /**
   * 禁掉 checkbox
   */
  disableCheckbox: boolean;
  /**
   * 是否禁用
   */
  disabled: boolean;
  /**
   * 图标
   */
  icon?: string;
  /**
   * 是否是叶子节点
   */
  isLeaf: boolean;
  /**
   * 值，id
   */
  key: string;
  /**
   * 是否可选
   */
  selectable: boolean;
  /**
   * 文字，显示名称、标题
   */
  title: string;
  /**
   * 数据类型，用于同时有多种对象在树形结构中时后台标识返回的数据/对象类型
   */
  type?: string;
  [property: string]: any;
}
