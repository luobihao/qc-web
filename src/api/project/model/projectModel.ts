/**
 * 渠成项目DTO
 */
export interface ProjectDto {
  //项目编码
  code: string;
  //项目名称
  name: string;
  //完整标题
  fulltitle: string;
  //简要标题
  title: string;
  //LOGO
  logo: string;
  //版权
  copyright: string;
  //状态标记
  flag: number;
}

/**
 * 渠成项目DTO集合
 */
export type ProjectDtoList = ProjectDto[];

/**
 * 项目用户关联DTO
 */
export interface ProjectUserRelateDto {
  //项目编码
  project: string;
  //用户id集合
  uids: number[] | string[];
}

/**
 * 项目查询条件DTO
 */
export interface ProjectQueryConditionDto {
  //关键字，可以为项目编码、项目名称、项目标题(完整、简要标题)，没有则查询全部
  words?: string;
}

/**
 * ProjectGetDeptAndUserConditionDto 获取项目中的部门和用户条件DTO
 */
export interface ProjectGetDeptAndUserConditionDto {
  /**
   * 不包含的层级，层级分为上级-1、同级-2、下级-4，可以指定不包含的层级；如委派工作只能对下级，不包含的层级为上级+同级=3
   */
  excludelevel: number;
  /**
   * 不包含自己，返回结果中不包含当前用户
   */
  excludeself: boolean;
  /**
   * 项目编码
   */
  project: string;
  [property: string]: any;
}


/**
 * 树节点DTO
 */
export interface TreeData {
  /**
   * 设置独立节点是否展示 Checkbox
   */
  checkable: boolean;
  /**
   * 子节点集合
   */
  children: TreeData[];
  /**
   * 禁掉 checkbox
   */
  disableCheckbox: boolean;
  /**
   * 是否禁用
   */
  disabled: boolean;
  /**
   * 图标
   */
  icon?: string;
  /**
   * 是否是叶子节点
   */
  isLeaf: boolean;
  /**
   * 值，id
   */
  key: string;
  /**
   * 是否可选
   */
  selectable: boolean;
  /**
   * 文字，显示名称、标题
   */
  title: string;
  /**
   * 数据类型，用于同时有多种对象在树形结构中时后台标识返回的数据/对象类型
   */
  type?: string;
  [property: string]: any;
}
