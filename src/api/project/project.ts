import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  ProjectQueryConditionDto,
  ProjectDtoList,
  ProjectDto,
  ProjectUserRelateDto,
  ProjectGetDeptAndUserConditionDto,
  TreeData
} from './model/projectModel';
import { UserSimpleInfoDtoList } from './model/userModel';

enum Api {
  Query = '/qc/project/query',
  Get = '/qc/project/get',
  Add = '/qc/project/add',
  Update = '/qc/project/update',
  Delete = '/qc/project/delete',
  GetProjectUsers = '/qc/project/getusers',
  AddProjectUsers = '/qc/project/addusers',
  DeleteProjectUsers = '/qc/project/deleteusers',
  GetProjectDeptUserTree = '/qc/project/deptusertree',
  SetProjectParams = '/qc/project/params'
}

/**
 * @description 根据关键字查询，关键字可以为项目编码、项目名称、项目标题(完整、简要标题)，没有则查询全部
 */
export function queryProjects(params: ProjectQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ProjectDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定项目信息
 * @param id: 项目编码
 */
export function getProject(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<ProjectDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增项目
 */
export function addProject(params: ProjectDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改项目
 */
export function updateProject(params: ProjectDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除项目
 * @param id 项目编码
 */
export function deleteProject(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description 获取指定项目关联的用户
 * @param id 项目编码
 */
export function getProjectUsers(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserSimpleInfoDtoList>(
    {
      url: Api.GetProjectUsers,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 添加项目用户用户关系
 */
export function addProjectUsers(params: ProjectUserRelateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddProjectUsers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 删除项目用户关联关系
 */
export function deleteProjectUsers(params: ProjectUserRelateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteProjectUsers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * 
 * @param params 
 * @param mode 
 * @description 获取部门用户树
 */
export function getProjectDeptUserTree(params: ProjectGetDeptAndUserConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TreeData>(
    {
      url: Api.GetProjectDeptUserTree,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description 设置项目参数
 */
export function setProjectParams(params: ProjectDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetProjectParams,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
