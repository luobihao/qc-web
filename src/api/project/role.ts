import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  RoleQueryConditionDto,
  RoleDtoList,
  RoleDto,
  RoleUserDtoList,
  RoleUserSetDto,
  RoleMenuDtoList,
  RoleMenuSetDto,
  RoleDeptDtoList,
  RoleDeptSetDto,
  RoleAPISetDto
} from './model/roleModel';
import { TreeDataDto } from '../platform/model/qcPublicModel';

enum Api {
  Query = '/qc/role/query',
  Get = '/qc/role/get',
  Add = '/qc/role/add',
  Update = '/qc/role/update',
  Delete = '/qc/role/delete',
  GetRoleUsers = '/qc/role/getusers',
  GetRoleUsersTree = '/qc/role/getuserstree',
  SetRoleUsers = '/qc/role/setusers',
  GetRoleMenus = '/qc/role/getmenus',
  GetRoleMenusTree = '/qc/role/getmenustree',
  SetRoleMenus = '/qc/role/setmenus',
  GetRoleDepts = '/qc/role/getdepts',
  GetRoleDeptsTree = '/qc/role/getdeptstree',
  SetRoleDepts = '/qc/role/setdepts',
  GetRoleApiTree = '/qc/role/getapistree',
  SetRoleApis = '/qc/role/setapis',
}

/**
 * @description 查询指定项目下角色，返回结果不判断启用状态；
 */
export function queryRoles(params: RoleQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<RoleDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定角色信息
 * @param id: 角色ID
 */
export function getRole(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<RoleDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增角色
 */
export function addRole(params: RoleDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改角色
 */
export function updateRole(params: RoleDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除角色
 * @param id 角色ID
 */
export function deleteRole(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 获取用户信息列表，返回有指定角色是否已经关联字段
 * @param id: 角色ID
 */
export function getRoleUsers(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<RoleUserDtoList>(
    {
      url: Api.GetRoleUsers,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取用户信息tree
 * @param id: 角色ID
 */
export function getRoleUsersTree(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeDataDto>(
    {
      url: Api.GetRoleUsersTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置角色用户关联关系
 */
export function setRoleUsers(params: RoleUserSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetRoleUsers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取菜单信息列表，返回有指定角色是否已经关联字段
 * @param id: 角色ID
 */
export function getRoleMenus(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<RoleMenuDtoList>(
    {
      url: Api.GetRoleMenus,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取菜单信息tree
 * @param id: 角色ID
 */
export function getRoleMenusTree(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeDataDto>(
    {
      url: Api.GetRoleMenusTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置角色菜单关联关系
 */
export function setRoleMenus(params: RoleMenuSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetRoleMenus,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取部门信息列表，返回有指定角色是否已经关联字段
 * @param id: 角色ID
 */
export function getRoleDepts(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<RoleDeptDtoList>(
    {
      url: Api.GetRoleDepts,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取部门信息tree
 * @param id: 角色ID
 */
export function getRoleDeptsTree(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeDataDto>(
    {
      url: Api.GetRoleDeptsTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置角色部门关联关系
 */
export function setRoleDepts(params: RoleDeptSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetRoleDepts,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}



/**
 * @description: 获取Api信息tree
 * @param id: 角色ID
 */
export function getRoleApisTree(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TreeDataDto>(
    {
      url: Api.GetRoleApiTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置角色Api关联关系
 */
export function setRoleApis(params: RoleAPISetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetRoleApis,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
