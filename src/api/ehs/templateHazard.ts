import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { TplHazardDbQueryConditionDto, TplHazardCategoryDto, TplHazardItemDto, TplHazardProjectDto, TplHazardDbDto, TplHazardImportDto, TplHazardDbFullInfoDto, TplHazardTreeResult, TplHazardDbResult } from './model/templateHazardModel';

enum Api {
  // 危险源导则库
  QueryDb = '/ehs/tplhazard/querydb',
  QueryDbDangerList = '/ehs/tplhazard/treetable',
  GetDb = '/ehs/tplhazard/getdb',
  GetDbFull = '/ehs/tplhazard/getdbfull',
  AddDb = '/ehs/tplhazard/adddb',
  UpdateDb = '/ehs/tplhazard/updatedb',
  DeleteDb = '/ehs/tplhazard/deletedb',
  ImportDb = '/ehs/tplhazard/importdb',
  //危险源类别
  GetDbCategory = '/ehs/tplhazard/getcategories',
  GetCategory = '/ehs/tplhazard/getcategory',
  AddCategory = '/ehs/tplhazard/addcategory',
  UpdateCategory = '/ehs/tplhazard/updatecategory',
  DeleteCategory = '/ehs/tplhazard/deletecategory',
  //危险源项目
  GetCategoryProject = '/ehs/tplhazard/getprojects',
  GetProject = '/ehs/tplhazard/getproject',
  AddProject = '/ehs/tplhazard/addproject',
  UpdateProject = '/ehs/tplhazard/updateproject',
  DeleteProject = '/ehs/tplhazard/deleteproject',
  //危险源清单
  GetProjectItems = '/ehs/tplhazard/getitems',
  GetItem = '/ehs/tplhazard/getitem',
  AddItem = '/ehs/tplhazard/additem',
  UpdateItem = '/ehs/tplhazard/updateitem',
  DeleteItem = '/ehs/tplhazard/deleteitem',

}

/**
 * @description 查询危险源导则库
 */
export function queryDangerDb(params: TplHazardDbQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TplHazardDbDto>(
    {
      url: Api.QueryDb,
      params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询导则库危险源清单
 */
export function queryDangerDbTreeList(params: TplHazardDbQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TplHazardDbDto>(
    {
      url: Api.QueryDbDangerList,
      params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定危险源导则库
 */
export function getDangerDb(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardDbDto>(
    {
      url: Api.GetDb,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定危险源导则库完整信息
 */
export function getDangerDbFull(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardDbFullInfoDto>(
    {
      url: Api.GetDbFull,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description 新增危险源导则库
 */
export function addDangerDb(params: TplHazardDbDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddDb,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改危险源导则库
 */
export function updateDangerDb(params: TplHazardDbDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.UpdateDb,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除危险源导则库
 */
export function deleteDangerDb(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteDb,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description 导入危险源导则库
 */
export function importDangerDb(params: TplHazardImportDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.ImportDb,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定危险源导则库类别
 */
export function getDangerDbCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardCategoryDto[]>(
    {
      url: Api.GetDbCategory,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}


//危险源类别

/**
 * @description: 查询指定危险源类别
 */
export function getCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardCategoryDto>(
    {
      url: Api.GetCategory,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description 新增指定危险源类别
 */
export function addCategory(params: TplHazardCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddCategory,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改指定危险源类别
 */
export function updateCategory(params: TplHazardCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.UpdateCategory,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除指定危险源类别
 */
export function deleteCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteCategory,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

//危险源项目

/**
 * @description: 查询指定危险源类别中的项目
 */
export function getCategoryProject(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardProjectDto[]>(
    {
      url: Api.GetCategoryProject,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定危险源项目
 */
export function getProject(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardProjectDto>(
    {
      url: Api.GetProject,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增指定危险源项目
 */
export function addProject(params: TplHazardProjectDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddProject,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改指定危险源项目
 */
export function updateProject(params: TplHazardProjectDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.UpdateProject,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除指定危险源项目
 */
export function deleteProject(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteProject,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

//危险源清单

/**
 * @description: 查询指定危险源项目中的清单
 */
export function getProjectItems(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardItemDto[]>(
    {
      url: Api.GetProjectItems,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定危险源清单
 */
export function getItem(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplHazardItemDto>(
    {
      url: Api.GetItem,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增指定危险源项目
 */
export function addItem(params: TplHazardItemDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddItem,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改指定危险源项目
 */
export function updateItem(params: TplHazardItemDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.UpdateItem,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除指定危险源项目
 */
export function deleteItem(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteItem,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
