import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import {
  UnitResponsibilitySetDto
} from './model/responsibilityModel'

enum Api {
  SetUnitResponsibility = '/ehs/responsibility/setunit',
}

/**
 * @description : 设置单元责任机制
 */
export function setUnitResponsibility(params: UnitResponsibilitySetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.SetUnitResponsibility,
    params
  }, {
    errorMessageMode: mode,
  })
}
