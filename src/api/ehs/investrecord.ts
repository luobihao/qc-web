import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  InvestRecordQueryConditionDto,
  InvestRecordDto
} from './model/investrecordModel'

enum Api {
  Query = '/ehs/investrecord/query',  // 查询
  Get = '/ehs/investrecord/get',  // 获取
  Add = '/ehs/investrecord/add',  // 新增
  Update = '/ehs/investrecord/update',  // 修改
  Delete = '/ehs/investrecord/delete',  // 删除
}

/**
 * 
 * @param param 查询
 * @param mode 
 * @returns 
 */
export function queryInvestRecordList(param: InvestRecordQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<InvestRecordDto[]>(
    {
      url: Api.Query,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description: 获取
 * @param id
 * @param mode
 * @returns
 *  */
export function getInvestRecord(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<InvestRecordDto>(
    {
      url: Api.Get,
      params: { id }
    }, {
    errorMessageMode: mode,
  }
  );
}

/**
 * @description: 新增
 * @param param
 * @param mode
 * @returns
 *  */
export function addInvestRecord(param: InvestRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<InvestRecordDto>(
    {
      url: Api.Add,
      data: param
    }, {
    errorMessageMode: mode,
  }
  );
}

/**
 *  @description: 修改
 * @param param
 * @param mode
 * @returns
 *  */
export function updateInvestRecord(param: InvestRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<InvestRecordDto>(
    {
      url: Api.Update,
      data: param
    }, {
    errorMessageMode: mode,
  }
  );
}
/**
 * @description: 删除
 * @param id
 * @param mode
 * @returns
 *  */
export function deleteInvestRecord(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<InvestRecordDto>(
    {
      url: Api.Delete,
      data: { id }
    }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  }
  );
}

