import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { TaskCategoryDto, TaskCategoryQueryConditionDto } from './model/TaskCategoryModel';

enum Api {
  Query = '/ehs/taskcat/all',
  Get = '/ehs/taskcat/get',
  Add = '/ehs/taskcat/add',
  Update = '/ehs/taskcat/update',
  Delete = '/ehs/taskcat/delete',
  QueryByObjecttype = '/ehs/taskcat/query',
}

/**
 * 
 * @param id 组织代码
 * @param mode 
 * @description : 获取全部分类
 */
export function getAllTaskCategory(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskCategoryDto[]>({
    url: Api.Query,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 获取指定任务分类
 */
export function getTaskCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskCategoryDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addTaskCategory(params: TaskCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateTaskCategory(params: TaskCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteTaskCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 根据对象类型查询任务分类
 *  */
export function queryTaskCategoryByObjecttype(params: TaskCategoryQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskCategoryDto[]>(
    {
      url: Api.QueryByObjecttype,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
