import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { EhsOrgDto } from './model/organizationModel';

enum Api {
  Query = '/ehs/org/all',
  Get = '/ehs/org/get',
  Add = '/ehs/org/add',
  Update = '/ehs/org/update',
  Delete = '/ehs/org/delete',
  GetUser = '/ehs/org/user',
  GetOrgDeptTree = '/ehs/org/depttree',
  GetOrgDeptList = '/ehs/org/deptlist'
}

/**
 * @description 查询全部组织
 */
export function queryOrganization(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsOrgDto[]>(
    {
      url: Api.Query,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定组织
 */
export function getOrganization(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsOrgDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addOrganization(params: EhsOrgDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateOrganization(params: EhsOrgDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteOrganization(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
/**
 * @description 获取用户组织
 */
export function getUserOrganization(projectcode: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsOrgDto>(
    {
      url: Api.GetUser,
      params: { projectcode: projectcode },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取指定组织部门Tree-getOrgDeptTree
 * */
export function getOrgDeptTree(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsOrgDto>(
    {
      url: Api.GetOrgDeptTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取指定组织部门List-getOrgDeptList
 * */
export function getOrgDeptList(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsOrgDto>(
    {
      url: Api.GetOrgDeptList,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    }
  )
}
