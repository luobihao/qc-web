import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import {
  EhsBoardQueryConditionDto,
  EhsBoardOrgLatestStatusDTO,
  EhsBoardOrgQueryResultDto,
  EhsBoardDeptQueryResultDto
} from './model/ehsBoardModel'

enum Api {
  QueryOrgBoard = '/ehs/board/org',  // 查询
  QueryDept = '/ehs/board/childs',
  GetOrgLatestStatus = '/ehs/board/latest'
}


/**
 * 查询组织安全状态-getOrgLatestStatus
 */
export function getOrgLatestStatus(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsBoardOrgLatestStatusDTO>(
    {
      url: Api.GetOrgLatestStatus,
      params: {
        id: id
      }
    }, {
    errorMessageMode: 'modal',
  }
  );
}


/**
 * 查询组织的看板数据-queryOrg
 */
export function queryOrgEhsBoard(param: EhsBoardQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<EhsBoardOrgQueryResultDto>(
    {
      url: Api.QueryOrgBoard,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * 查询指定部门的下级部门的看板数据-queryChildDepts
 */
export function queryChildDeptsEhsBoard(param: EhsBoardQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<EhsBoardDeptQueryResultDto[]>(
    {
      url: Api.QueryDept,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}
