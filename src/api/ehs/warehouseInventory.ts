import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { WareHouseInventoryDto, WareHouseInventoryQueryConditionDto, WareHouseInventoryAddDto } from './model/wareHouseInventoryModel';


enum Api {
  GetAll = '/ehs/warehourinventory/query',
  Get = '/ehs/warehourinventory/get',
  Add = '/ehs/warehourinventory/add',
  Check = '/ehs/warehourinventory/check',
  Delete = '/ehs/warehourinventory/delete',
}

/**
 * @description: 获取所有仓库出入库记录
 */
export function queryAllWareHouseInventory(params: WareHouseInventoryQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<WareHouseInventoryDto[]>(
    {
      url: Api.GetAll,
      params: params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定仓库出入库记录
 */
export function getWareHouseInventory(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<WareHouseInventoryDto>(
    {
      url: Api.Get,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
    }
  )
}

/**
 * @description: 添加仓库出入库记录
 */
export function addWareHouseInventory(params: WareHouseInventoryAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params: params
    },
    {
      errorMessageMode: mode,
    }
  )
}

/**
 * @description: 复核出入库记录
 */
export function checkWareHouseInventory(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Check,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true
    }
  )
}

/**
 * @description: 删除仓库出入库记录
 */
export function deleteWareHouseInventory(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true
    }
  )
}
