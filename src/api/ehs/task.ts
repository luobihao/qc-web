import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  TaskInfoDto,
  TaskAddOrEditDto,
  TaskSetGroupObjectsDto,
  TaskGroupObjectsDto,
  TaskScheduleDto,
  TaskScheduleGroupDto,
  TaskGroupInfoDto,
  TaskScheduleGroupItemDto,
  TaskScheduleGroupSummaryDto,
  TaskDispatchDto,
  TaskConsolePreviewResultDto
} from './model/taskModel'

enum Api {
  GetTaskInfo = '/ehs/task/info', // 获取任务信息
  AddTask = '/ehs/task/add', // 添加任务
  UpdateTask = '/ehs/task/update', // 更新任务
  GetTaskGroupObjects = '/ehs/task/groupobjects', // 获取安全任务分组关联的对象-getTaskGroupObjects
  SetTaskGroups = '/ehs/task/groups', //设置任务分组
  DeleteTask = '/ehs/task/delete', // 删除任务
  DispatchTask = '/ehs/task/dispatch', // 任务派单
  FinishTask = '/ehs/task/finish', // 任务完成
  EndTask = '/ehs/task/end', // 任务终止
  ContinueTask = '/ehs/task/continue', // 任务继续
  Preview = '/ehs/task/preview', //任务预览-preview
}

/**
 * @description: 获取任务信息
 */
export function getTaskInfo(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskInfoDto>({
    url: Api.GetTaskInfo,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 添加任务
 */
export function addTask(params: TaskAddOrEditDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddTask,
    params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 更新任务
 *  */
export function updateTask(params: TaskAddOrEditDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>({
    url: Api.UpdateTask,
    params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 获取安全任务分组关联的对象
 * */
export function getTaskGroupObjects(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskGroupObjectsDto>({
    url: Api.GetTaskGroupObjects,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 设置任务分组
 * */
export function setTaskGroups(params: TaskSetGroupObjectsDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.SetTaskGroups,
    params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 删除任务
 * */
export function deleteTask(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteTask,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 任务派单
 * */
export function dispatchTask(params: TaskDispatchDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.DispatchTask,
    params: params
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 任务完成
 * */
export function finishTask(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.FinishTask,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 任务终止
 * */
export function endTask(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.EndTask,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 任务继续
 * */
export function continueTask(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.ContinueTask,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 任务预览
 */
export function previewTask(params: TaskAddOrEditDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TaskConsolePreviewResultDto>({
    url: Api.Preview,
    params: params
  }, {
    errorMessageMode: mode,
  })
}
