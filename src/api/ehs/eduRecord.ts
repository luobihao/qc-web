import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import {
  EduRecordQueryConditionDto,
  EduRecordDto,
  EduRecordUserDto,
  EduRecordUserSetDto

} from './model/eduRecordModel'

enum Api {
  Query = '/ehs/edurecord/query',  // 查询
  Get = '/ehs/edurecord/get',
  Add = '/ehs/edurecord/add',
  Update = '/ehs/edurecord/update',
  Delete = '/ehs/edurecord/delete',
  GetUsers = '/ehs/edurecord/getusers',
  SetUsers = '/ehs/edurecord/setusers'
}

/**
 * 查询
 */
export function queryEduRecord(param: EduRecordQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<EduRecordDto[]>(
    {
      url: Api.Query,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description: 获取指定
 */
export function getEduRecord(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EduRecordDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增
 */
export function addEduRecord(data: EduRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      data: data,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改
 */
export function updateEduRecord(data: EduRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      data: data,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteEduRecord(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 获取记录人员关联信息
 */
export function getEduRecordUsers(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EduRecordUserDto[]>(
    {
      url: Api.GetUsers,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 设置记录人员关联信息
 */
export function setEduRecordUsers(data: EduRecordUserSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetUsers,
      data: data,
    },
    {
      errorMessageMode: mode,
    },
  );
}
