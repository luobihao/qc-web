import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { EhsUnitDto, EhsUnitQueryConditionDto, EhsUnitTreeTableItemDto, EhsUnitStaticInfoDto, EhsUnitCategoryQueryConditionDto } from './model/unitDivisionModel';

enum Api {
  QueryUnit = '/ehs/unit/treetable',
  GetUnit = '/ehs/unit/get',
  AddUnit = '/ehs/unit/add',
  UpdateUnit = '/ehs/unit/update',
  DeleteUnit = '/ehs/unit/delete',
  GetCategoryUnitSimple = '/ehs/unit/categorysimple',// 获取指定单元分类的单元简要信息-getCategoryUnitSimple

  QueryUnitStaticInfo = '/ehs/unitstaticinfo/getbyunitid',
  GetUnitStaticInfo = '/ehs/unitstaticinfo/get',
  AddUnitStaticInfo = '/ehs/unitstaticinfo/add',
  UpdateUnitStaticInfo = '/ehs/unitstaticinfo/update',
  DeleteUnitStaticInfo = '/ehs/unitstaticinfo/delete',
}

/**
 * @description 查询单元
 */
export function queryUnitDivision(params: EhsUnitQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<EhsUnitTreeTableItemDto[]>(
    {
      url: Api.QueryUnit,
      params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定单元
 */
export function getUnitDivision(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsUnitDto>(
    {
      url: Api.GetUnit,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addUnitDivision(params: EhsUnitDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddUnit,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateUnitDivision(params: EhsUnitDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.UpdateUnit,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteUnitDivision(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteUnit,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}


/**
 * @description 获取指定单元分类的单元信息
 */
export function getCategoryUnitSimple(params: EhsUnitCategoryQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<EhsUnitDto>(
    {
      url: Api.GetCategoryUnitSimple,
      params: params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询单元静态信息
 */
export function queryUnitStaticInfo(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsUnitStaticInfoDto[]>(
    {
      url: Api.QueryUnitStaticInfo,
      params: { unitid: id }
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定单元静态信息
 */
export function getUnitStaticInfo(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsUnitStaticInfoDto>(
    {
      url: Api.GetUnitStaticInfo,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增指定单元静态信息
 */
export function addUnitStaticInfo(params: EhsUnitStaticInfoDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddUnitStaticInfo,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改指定单元静态信息
 */
export function updateUnitStaticInfo(params: EhsUnitStaticInfoDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.UpdateUnitStaticInfo,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除指定单元静态信息
 */
export function deleteUnitStaticInfo(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteUnitStaticInfo,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
