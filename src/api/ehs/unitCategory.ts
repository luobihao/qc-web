import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { unitCategoryTreeDataResult, TplUnitCategoryDto, TplUnitCategoryTreeTableItemDto, UnitCategoryHazardItemsSetDto } from './model/unitCategoryModel';

enum Api {
  Query = '/ehs/unitcategory/treetable',
  Get = '/ehs/unitcategory/get',
  Add = '/ehs/unitcategory/add',
  Update = '/ehs/unitcategory/update',
  Delete = '/ehs/unitcategory/delete',
  SetRelation = '/ehs/unitcategory/sethazarditems',
  GetRelation = '/ehs/unitcategory/gethazarditemids',
}

/**
 * @description 查询全部单元分类
 */
export function queryUnitCategory(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<unitCategoryTreeDataResult>(
    {
      url: Api.Query,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定单元分类
 */
export function getUnitCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TplUnitCategoryTreeTableItemDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addUnitCategory(params: TplUnitCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateUnitCategory(params: TplUnitCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteUnitCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}


/**
 * @description 设置单元分类与危险源清单关联
 */
export function setHazarditemsRelation(params: UnitCategoryHazardItemsSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetRelation,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description: 获取指定单元分类关联危险源清单ID集合
 */
export function getHazardItemsRelation(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<Array>(
    {
      url: Api.GetRelation,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}
