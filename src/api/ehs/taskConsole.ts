import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  TaskConsoleQueryConditionDto,
  TaskConsoleQueryResultDto,
  TaskConsoleQueryUnitDoneConditionDto,
  TaskScheduleDto,
  TaskConsolePreviewConditionDto,
  TaskExecuteInfoDto,
  TaskGroupItemsDto,
  TaskConsoleUnitTaskItemQueryConditionDto,
  TaskConsoleUnitTaskItemPaginationQueryConditionDto
} from './model/taskConsoleModel'
import { HazardIdentifyInfoDto } from './model/hazardModel'

enum Api {
  Query = '/ehs/taskconsole/query',   // 查询,分页查询，表格显示任务信息表；
  QueryUnitTaskItem = '/ehs/taskconsole/unititems',  // 查询指定单元的任务项(不分页)-queryUnitTaskItem
  QueryPaginationUnitTaskItem = '/ehs/taskconsole/unititempage',  // 查询指定单元的任务项(分页)-queryPaginationUnitTaskItem
  QueryFacilityTodoTask = '/ehs/taskconsole/facilitytodo',  // 查询指定设备设施的待办任务-queryFacilityTodoTask
  QueryHazardTodoTask = '/ehs/taskconsole/hazardtodo',  // 查询指定危险源的待办任务-queryHazardTodoTask
  GetTaskGroupAndItem = '/ehs/taskconsole/groupitems', // 查询指定任务的分组和任务项-getTaskGroupAndItems
  GetTaskExecutes = '/ehs/taskconsole/taskexecutes', // 查询指定任务的所有执行记录-getTaskExecutes
  GetTaskSchedule = '/ehs/taskconsole/schedule',   //查询指定任务的执行情况一览表-getTaskSchedule
  GetTaskIdentifyRecord = '/ehs/taskconsole/identify', //查询指定任务的辨识记录
}

/**
 * @description: 查询
 */
export function queryTaskConsole(params: TaskConsoleQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TaskConsoleQueryResultDto>({
    url: Api.Query,
    params: params
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 查询指定单元的任务项(不分页)
 * */
export function queryUnitTaskItem(params: TaskConsoleUnitTaskItemQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TaskConsoleQueryResultDto>({
    url: Api.QueryUnitTaskItem,
    params: params
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 查询指定单元的任务项(分页)
 * */
export function queryPaginationUnitTaskItem(params: TaskConsoleUnitTaskItemPaginationQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TaskConsoleQueryResultDto>({
    url: Api.QueryPaginationUnitTaskItem,
    params: params
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 查询指定设备设施的待办任务
 * */
export function queryFacilityTodoTask(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskConsoleQueryResultDto>({
    url: Api.QueryFacilityTodoTask,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 查询指定危险源的待办任务
 * */
export function queryHazardTodoTask(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskConsoleQueryResultDto>({
    url: Api.QueryHazardTodoTask,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 查询指定任务的分组和任务项
 * */
export function getTaskGroupAndItem(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskGroupItemsDto>({
    url: Api.GetTaskGroupAndItem,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}
/**
 * @description: 查询指定任务的所有执行记录
 * */
export function getTaskExecutes(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskExecuteInfoDto[]>({
    url: Api.GetTaskExecutes,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/** 
 * @description: 查询指定任务的执行情况一览表
 * */
export function getTaskSchedule(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskScheduleDto>({
    url: Api.GetTaskSchedule,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 查询指定任务的辨识记录
 */
export function getTaskIdentifyRecord(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<HazardIdentifyInfoDto>({
    url: Api.GetTaskIdentifyRecord,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}
