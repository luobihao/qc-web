import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  HazardLedgerQueryConditionDto,
  HazardLedgerQueryResultDto,
  QueryHazardByUnitConditionDto,
  QueryHazardByUnitResultDto,
  QueryOneUnitHazardConditionDto,
  QueryOneUnitHazardResultDto,
  EhsHazardInfoDto,
  EhsHazardFullInfoDto,
  HazardIdentifyQueryConditionDto,
  HazardIdentifyQueryResultDto,
  HazardIdentifyAddDto,
  HazardIdentifyConfirmDto,
  HazardIdentifyConfirmQueryConditionDto,
  HazardIdentifyInfoDto,
  HazardIdentifyOperateResultDto,
  HazardIdentifyConfirmResultDto,
  EhsHazardUnitQueryConditionDto
} from './model/hazardModel'

enum Api {
  // 危险源台账
  QueryHazardLedger = "/ehs/hazardledger/query",  // 危险源台账
  QueryHazardLedgerByUnit = "/ehs/hazard/byunit", // 按单元查询危险源
  QueryHazardByOneUnit = '/ehs/hazardledger/oneunit', // 指定单元查询危险源
  GetUnitHazards = '/ehs/hazardledger/unithazards', // 获取指定单元第一类危险源
  GetUnitHiddenTrouble = '/ehs/hazardledger/unithiddentrouble',   // 获取指定单元隐患
  // 危险源
  GetOneHazardInfo = '/ehs/hazard/get',   // 获取指定危险源信息
  GetOneHazardFullInfo = '/ehs/hazard/full', // 获取指定危险源完整信息
  QueryHazardListByUnit = '/ehs/hazard/unit',  //查询指定单元的危险源集合-queryUnitHazards
  //危险源辨识
  QueryHazardIdentifyRecord = '/ehs/hazardidentify/query', // 危险源辨识记录查询
  GetHazardIdentifyRecordByUnit = '/ehs/hazardidentify/unit', // 获取指定单元的危险源辨识记录
  IdentifyAllByUnit = '/ehs/hazardidentify/overall', //指定单元全面辨识危险源
  GetHazardIdentifyRecordById = '/ehs/hazardidentify/get', // 获取指定危险源辨识记录
  AddIdentifyRecord = '/ehs/hazardidentify/add', // 添加危险源辨识记录
  UpdateIdentifyRecord = '/ehs/hazardidentify/update', // 修改危险源辨识记录
  DeleteIdentifyRecord = '/ehs/hazardidentify/delete', // 删除危险源辨识记录
  IdentifyNoChange = '/ehs/hazardidentify/nochange', //辨识已存在的危险源无变化
  ConfirmIdentifyRecord = '/ehs/hazardidentify/confirm', //确认辨识的危险源记录
  QueryIdentifyConfirm = '/ehs/hazardidentify/queryconfirm', //查询危险源辨识记录确认-queryConfirm
  // 危险源基本信息
  EvaluationHazard = '/ehs/riskevl/evaluation',
  GetEvaluateMethod = '/ehs/riskevl/methods',
  GetEvaluaterLatestRecord = '/ehs/riskevl/latest', // 获取指定危险源最新的评估记录
}

/**
 * @description 获取危险源台账
 */
export function queryHazardLedger(params: HazardLedgerQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardLedgerQueryResultDto>(
    {
      url: Api.QueryHazardLedger,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  });
}

/**
 * @description 按单元查询危险源
 */
export function queryHazardLedgerByUnit(params: QueryHazardByUnitConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<QueryHazardByUnitResultDto>(
    {
      url: Api.QueryHazardLedgerByUnit,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  });
}

/**
 * @description 获取指定单元的危险源
 */
export function getHazardByOneUnit(params: QueryOneUnitHazardConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<QueryOneUnitHazardResultDto[]>(
    {
      url: Api.QueryHazardByOneUnit,
      params: {
        id: params.unitid,
      }
    }, {
    errorMessageMode: 'modal',
  }
  );
}


/**
 * @description 获取指定单元的第一类危险源
 */
export function getUnitHazards(params: QueryOneUnitHazardConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsHazardInfoDto[]>(
    {
      url: Api.GetUnitHazards,
      params: {
        id: params.unitid,
      }
    }, {
    errorMessageMode: 'modal',
  });
}


/**
 * @description 获取指定单元的第二类危险源
 */
export function getUnitHiddenTrouble(params: QueryOneUnitHazardConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsHazardInfoDto[]>(
    {
      url: Api.GetUnitHiddenTrouble,
      params: {
        id: params.unitid,
      }
    }, {
    errorMessageMode: 'modal',
  });
}

/**
 * @description 获取指定危险源信息
 * */
export function getOneHazardInfo(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsHazardInfoDto>(
    {
      url: Api.GetOneHazardInfo,
      params: {
        id: id,
      }
    }, {
    errorMessageMode: 'modal',
  });
}

/**
 * @description 获取指定危险源完整信息
 * */
export function getOneHazardFullInfo(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsHazardFullInfoDto>(
    {
      url: Api.GetOneHazardFullInfo,
      params: {
        id: id,
      }
    }, {
    errorMessageMode: 'modal',
  });
}

/**
 * @description 指定单元查询危险源列表集合
 */
export function queryHazardListByUnit(params: EhsHazardUnitQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<QueryHazardByUnitResultDto[]>(
    {
      url: Api.QueryHazardListByUnit,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  });
}

/// 辨识

/**
 * @description 危险源辨识记录查询
 * 
 * */
export function queryIdentifyRecord(params: HazardIdentifyQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardIdentifyQueryResultDto>(
    {
      url: Api.QueryHazardIdentifyRecord,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  }
  )
}

/**
 * @description 获取指定单元的危险源辨识记录
 * 
 * */
export function queryIdentifyRecordByUnit(params: HazardIdentifyQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardIdentifyQueryResultDto>(
    {
      url: Api.GetHazardIdentifyRecordByUnit,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  }
  )
}

/**
 * @description 指定单元全面辨识危险源
 * */
export function identifyAllHazard(unitid: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardIdentifyOperateResultDto>(
    {
      url: Api.IdentifyAllByUnit,
      params: {
        unitid: unitid,
      }
    }, {
    errorMessageMode: 'modal',
    joinParamsToUrl: true,
  }
  )
}

/**
 * @description 获取指定id的辨识记录
 */
export function getOneIdentifyRecord(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<HazardIdentifyAddDto>(
    {
      url: Api.GetHazardIdentifyRecordById,
      params: {
        id: id,
      }
    }, {
    errorMessageMode: 'modal',
    joinParamsToUrl: true,
  });
}

/**
 * @description 添加辨识记录
 */
export function addIdentifyRecord(params: HazardIdentifyAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardIdentifyOperateResultDto>(
    {
      url: Api.AddIdentifyRecord,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  }
  )
}

/**
 * @description 修改辨识记录
 *  */
export function updateIdentifyRecord(params: HazardIdentifyAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<HazardIdentifyOperateResultDto>(
    {
      url: Api.UpdateIdentifyRecord,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  }
  )
}

/**
 * @description 删除辨识记录
 * */
export function deleteIdentifyRecord(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<HazardIdentifyOperateResultDto>(
    {
      url: Api.DeleteIdentifyRecord,
      params: {
        id: id,
      }
    }, {
    errorMessageMode: 'modal',
    joinParamsToUrl: true,
  }
  )
}

/**
 * @description 辨识已存在的危险源无变化
 * */
export function identifyNoChange(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardIdentifyOperateResultDto>(
    {
      url: Api.IdentifyNoChange,
      params: {
        id: id,
      }
    }, {
    errorMessageMode: 'modal',
    joinParamsToUrl: true,
  }
  )
}

/**
 * @description 确认辨识危险源记录
 * */
export function confirmIdentifyRecord(params: HazardIdentifyConfirmDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardIdentifyConfirmResultDto>(
    {
      url: Api.ConfirmIdentifyRecord,
      params: params
    }, {
    errorMessageMode: 'modal',
  }
  )
}

/**
 * @description 查询危险源确认辨识记录
 * */
export function queryConfirmIdentifyRecord(params: HazardIdentifyConfirmQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<HazardIdentifyInfoDto[]>(
    {
      url: Api.QueryIdentifyConfirm,
      params: {
        ...params,
      }
    }, {
    errorMessageMode: 'modal',
  }
  )
}



/**
 * @description 评价危险源
 */
export function evaluationHazard(params: HazardRiskEvaluationDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.EvaluationHazard,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * 
 * @description 获取风险评价方法
 */
export function getIdentifyMethod(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<HazardRiskEvaluationResult>(
    {
      url: Api.GetEvaluateMethod,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取危险源最新的评价记录
 */
export function getLatestEvaluationRecord(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<HazardRiskEvaluationDto>(
    {
      url: Api.GetEvaluaterLatestRecord,
      params: {
        id: id
      },
    }, {
    errorMessageMode: 'modal',
  }
  )
}
