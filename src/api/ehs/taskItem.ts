import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  TaskItemTransmitDto,
  TaskItemCheckTableConditionDto,
  TaskItemCheckTableResultDto
} from './model/taskItemModel'

enum Api {
  FinishItem = '/ehs/taskitem/finish', // 完成任务项-finishItem
  EndItem = '/ehs/taskitem/end', // 终止任务项-enditem
  ContinueItem = '/ehs/taskitem/continue', // 继续任务项-continueItem
  Transmit = '/ehs/taskitem/transmit', // 转交任务项-transmit
  GetTaskItemCheckTable = '/ehs/taskitem/checktable', // 获取指定任务项的检查表-getTaskItemCheckTable
  CancelTransmit = '/ehs/taskitem/cancel', // 取消转交任务项-cancelTransmit
  FulfillTransmit = '/ehs/taskitem/fulfill', // 完成转交任务项-fulfillTransmit
  RejectTransmit = '/ehs/taskitem/reject', // 拒绝转交任务项-rejectTransmit
  ReturnTransmit = '/ehs/taskitem/return', // 退回转交任务项-returnTransmit
  ReportEhsTask = '/ehs/taskitem/generaterpt', // 生成任务项报告-reportEhsTask
}

/**
 * @description: 完成任务项
 */
export function finishItem(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.FinishItem,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 终止任务项
 */
export function endItem(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.EndItem,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  }
  )
}

/**
 * @description: 继续任务项
 */
export function continueItem(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.ContinueItem,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  }
  )
}

/**
 * @description: 转交任务项
 *  */
export function transmitItem(params: TaskItemTransmitDto | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.Transmit,
    params: params
  }, {
    errorMessageMode: mode,
  }
  )
}

/**
 * @description: 获取指定任务项的检查表
 */
export function getTaskItemCheckTable(params: TaskItemCheckTableConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TaskItemCheckTableResultDto>({
    url: Api.GetTaskItemCheckTable,
    params: params
  }, {
    errorMessageMode: mode,
  }
  )
}

/**
 * @description: 取消转交任务项
 */
export function cancelTransmit(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.CancelTransmit,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  }
  )
}

/**
 * @description: 接收转交任务项
 */
export function fulfillTransmit(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.FulfillTransmit,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  }
  )
}
/**
 * @description: 拒收转交任务项
 */
export function rejectTransmit(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.RejectTransmit,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  }
  )
}

/**
 * @description: 退回任务项
 */
export function returnTransmit(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.ReturnTransmit,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}


/**
 * @description 生成报告
 */
export function reportEhsTask(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.ReportEhsTask,
      params: {
        id: id
      }
    }, {
    errorMessageMode: 'modal',
  }
  );
}
