import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import {
  EhsReportQueryConditionDto,
  EhsReportFileUpdateDto
} from './model/ehsReporyModel'

enum Api {
  QueryInspection = '/ehs/report/inspectionquery',
  GenerateInspectionReport = '/ehs/report/inspectionreport', //安全生产检查工作报告生成,
  QueryTroubleCorrect = '/ehs/report/troublequery',
  GenerateTroubleCorrectReport = '/ehs/report/troublereport'
}

//安全生产检查工作查询-queryInspection
export function queryInspection(params: EhsReportQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<[]>({
    url: Api.QueryInspection,
    params: params,
  }, {
    errorMessageMode: mode,
  })
}

//安全生产检查工作报告生成-generateInspectionReport
export function generateInspectionReport(params: EhsReportQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.GenerateInspectionReport,
    params: params,
  }, {
    errorMessageMode: mode,
  })
}
//隐患治理工作查询-queryTroubleCorrect
export function queryTroubleCorrect(params: EhsReportQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<[]>({
    url: Api.QueryTroubleCorrect,
    params: params,
  }, {
    errorMessageMode: mode,
  })
}
// 隐患治理工作报告生成-generateTroubleCorrectReport
export function generateTroubleCorrectReport(params: EhsReportQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.GenerateTroubleCorrectReport,
    params: params,
  }, {
    errorMessageMode: mode,
  })
}
