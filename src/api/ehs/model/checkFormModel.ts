/**
 * CheckTableDto 检查表DTO
 */
export interface CheckTableDto {
  /**
   * 分类代码
   */
  catcode: string;
  /**
   * 备注
   */
  description?: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元分类名称
   */
  ucnm?: string;
  /**
   * 单元ID集合，检查表适用的单元集合，为空表示使用单元分类下的所有单元
   */
  unitids?: string[];
  /**
   * 单元名称集合
   */
  unitnames?: string[];
  [property: string]: any;
}
/**
 * CheckGroupDto 检查项分组DTO
 */
export interface CheckGroupDto {
  /**
   * 分组ID
   */
  groupid: string;
  /**
   * 排序号
   */
  odr?: number;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  [property: string]: any;
}



/**
 * CheckTableQueryConditionDto 检查表查询条件DTO
 */
export interface CheckTableQueryConditionDto {
  /**
   * 分类代码
   */
  catcode?: string;
  /**
   * 关键字，可以在标题和备注中匹配
   */
  keywords?: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 状态，可以指定是否包含禁用或删除的，默认为只查询启用状态的
   */
  status: string;
  [property: string]: any;
}

/**
 * CheckTableFullDto 检查表完整信息
 */
export interface CheckTableFullDto {
  /**
   * 分类代码
   */
  catcode: string;
  /**
   * 备注
   */
  description?: string;
  /**
   * 分组集合
   */
  groups: CheckGroupFullDto[];
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  /**
   * 单元分类编码
   */
  uccd: string;
  /**
   * 单元分类名称
   */
  ucnm?: string;
  /**
   * 单元ID集合，检查表适用的单元集合，为空表示使用单元分类下的所有单元
   */
  unitids?: string[];
  /**
   * 单元名称集合，与单元ID集合对应
   */
  unitnames: string[];
  [property: string]: any;
}


/**
 * CheckItemDto
 */
export interface CheckItemDto {
  /**
   * 填写文本类型，填写文本信息的类型
   */
  contenttype?: number;
  /**
   * 说明描述信息
   */
  description?: string;
  /**
   * 分组ID
   */
  groupid: string;
  /**
   * 检查项ID
   */
  itemid: string;
  /**
   * 最少图片数量
   */
  minimgcount?: number;
  /**
   * 是否非空
   */
  notnull: boolean;
  /**
   * 排序号
   */
  odr?: number;
  /**
   * 待选项名称，自定义待选项集合
   */
  options?: string;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  [property: string]: any;
}


/**
 * CheckGroupFullDto 检查项分组完整DTO
 */
export interface CheckGroupFullDto {
  /**
   * 分组ID
   */
  groupid: string;
  items: CheckItemDto[];
  /**
   * 排序号
   */
  odr?: number;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  [property: string]: any;
}


/**
 * CheckItemBatchAddDto 批量新增检查项
 */
export interface CheckItemBatchAddDto {
  /**
   * 填写文本类型，填写文本信息的类型
   */
  contenttype?: number;
  /**
   * 分组ID
   */
  groupid: string;
  /**
   * 最少图片数量
   */
  minimgcount?: number;
  /**
   * 是否非空
   */
  notnull: boolean;
  /**
   * 检查项待选项集合，顺序与检查项标题顺序保持一致
   */
  options: string[];
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 检查项标题集合
   */
  title: string[];
  [property: string]: any;
}


/**
 * CheckRecordDto 检查记录DTO
 */
export interface CheckRecordDto {
  /**
   * 记录内容
   */
  content?: string;
  /**
   * 数据ID
   */
  dataid: string;
  /**
   * 创建时间
   */
  intm: string;
  /**
   * 检查记录ID
   */
  recordid: string;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 更新时间
   */
  updatetm: string;
  /**
   * 用户ID
   */
  userid: string;
  [property: string]: any;
}

/**
 * CheckRecordSaveDto 检查记录保存DTO
 */
export interface CheckRecordSaveDto {
  /**
   * 记录内容
   */
  content?: string;
  /**
   * 数据ID
   */
  dataid: string;
  /**
   * 数据类型，数据ID对应的类型，task表示是任务；
   */
  datatype: string;
  /**
   * 创建时间
   */
  intm: string;
  /**
   * 检查记录ID
   */
  recordid: string;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 更新时间
   */
  updatetm: string;
  /**
   * 用户ID
   */
  userid: string;
  [property: string]: any;
}


/**
 * CheckRecordSaveResultDto 检查记录保存结果DTO
 */
export interface CheckRecordSaveResultDto {
  /**
   * 信息，错误提示信息
   */
  msg?: string;
  /**
   * 检查记录ID
   */
  recordid?: string;
  /**
   * 是否保存成功
   */
  success: boolean;
  [property: string]: any;
}


