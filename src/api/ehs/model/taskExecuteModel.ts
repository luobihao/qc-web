/**
 * TaskExecuteInfoDto 任务执行记录信息
 */
export interface TaskExecuteInfoDto {
  /**
   * 执行起始时间
   */
  begintm: string;
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 检查表记录ID
   */
  checkrecordid?: string;
  /**
   * 确认附件信息
   */
  confirmattachment?: string;
  /**
   * 确认描述信息
   */
  confirmdescription?: string;
  /**
   * 确认人签字
   */
  confirmsign?: string;
  /**
   * 确认时间
   */
  confirmtm?: string;
  /**
   * 确认人ID
   */
  confirmuserid?: number;
  /**
   * 确认人名称
   */
  confirmusername?: string;
  /**
   * 执行截止时间
   */
  endtm: string;
  /**
   * 执行附件信息
   */
  execattachment?: string;
  /**
   * 执行内容描述
   */
  execdescription?: string;
  /**
   * 执行记录ID
   */
  execid: string;
  /**
   * 执行负责人签字
   */
  execsign?: string;
  /**
   * 执行时间，界面中填写，文字信息
   */
  executetmstr: string;
  /**
   * 执行人员，界面中填写
   */
  executor: string;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否确认通过，如果不需要确认，在填写执行记录时如执行完成为true自动设置为已确认
   */
  hasconfirm: boolean;
  /**
   * 是否执行完成，表示当前执行记录信息是否完整，如执行完成本次填写的执行记录即为完整信息；
   */
  hasexec: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源称
   */
  hazardname?: string;
  /**
   * 记录填写时间
   */
  intm: string;
  /**
   * 记录填写人ID
   */
  inuserid: number;
  /**
   * 记录填写人名称
   */
  inusername: string;
  /**
   * 执行是否正常，本次执行是否有发现异常
   */
  isnormal: boolean;
  /**
   * 任务项信息，在显示时从任务项表中查询并赋值
   */
  item?: TaskItemInfoDto;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}

/**
 * 任务项信息，在显示时从任务项表中查询并赋值
 *
 * TaskItemInfoDto
 */
export interface TaskItemInfoDto {
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 实际完成时间
   */
  execendtm?: string;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 任务项名称，任务标题+时间周期文字
   */
  itemname: string;
  /**
   * 最新执行开始时间
   */
  latestexecbegintm?: string;
  /**
   * 最新执行截止时间
   */
  latestexecendtm?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 周期内计划执行次数
   */
  planfrequencycount: string;
  /**
   * 周期类型
   */
  planfrequencytype: string;
  /**
   * 计划时间周期文字，时间周期文字
   */
  plantmstr: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 当前责任部门ID，≤0表示为空
   */
  transdeptid?: number;
  /**
   * 当前责任部门名称
   */
  transdeptname?: string;
  /**
   * 当前负责人ID，≤0表示为空
   */
  transuserid?: number;
  /**
   * 当前负责人名称
   */
  transusername?: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}
/**
 * TaskExecuteAddDto 任务执行记录新增
 */
export interface TaskExecuteAddDto {
  /**
   * 执行起始时间
   */
  begintm: string;
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 检查表内容
   */
  checkcontent?: string;
  /**
   * 检查表记录ID
   */
  checkrecordid?: string;
  /**
   * 执行截止时间
   */
  endtm: string;
  /**
   * 执行附件信息
   */
  execattachment?: string;
  /**
   * 执行内容描述
   */
  execdescription?: string;
  /**
   * 执行负责人签字
   */
  execsign?: string;
  /**
   * 执行时间，界面中填写，文字信息
   */
  executetmstr: string;
  /**
   * 执行人员，界面中填写
   */
  executor: string;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否执行完成，表示当前执行记录信息是否完整，如执行完成本次填写的执行记录即为完整信息；
   * 表示任务项是否完成，在任务的指定周期内的任务全部完成；
   */
  hasexec: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 执行是否正常，本次执行是否有发现异常
   */
  isnormal: boolean;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}

/**
 * TaskExecuteUpdateDto 任务执行信息修改
 */
export interface TaskExecuteUpdateDto {
  /**
   * 执行起始时间
   */
  begintm: string;
  /**
   * 检查表内容
   */
  checkcontent?: string;
  /**
   * 检查表记录ID
   */
  checkrecordid?: string;
  /**
   * 执行截止时间
   */
  endtm: string;
  /**
   * 执行附件信息
   */
  execattachment?: string;
  /**
   * 执行内容描述
   */
  execdescription?: string;
  /**
   * 执行记录ID
   */
  execid: string;
  /**
   * 执行负责人签字
   */
  execsign?: string;
  /**
   * 执行时间，界面中填写，文字信息
   */
  executetmstr: string;
  /**
   * 执行人员，界面中填写
   */
  executor: string;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 是否执行完成，表示当前执行记录信息是否完整，如执行完成本次填写的执行记录即为完整信息；
   */
  hasexec: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 执行是否正常，本次执行是否有发现异常
   */
  isnormal: boolean;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}

/**
 * TaskExecuteConfirmDto 任务执行信息确认
 */
export interface TaskExecuteConfirmDto {
  /**
   * 确认附件信息
   */
  confirmattachment?: string;
  /**
   * 确认内容描述
   */
  confirmdescription?: string;
  /**
   * 确认人签字
   */
  confirmsign?: string;
  /**
   * 执行记录ID
   */
  execid: string;
  [property: string]: any;
}
