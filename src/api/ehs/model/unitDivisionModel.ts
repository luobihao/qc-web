/** 
 * EhsUnitDto 单元信息
 */

export interface EhsUnitDto {
  /**
   * 底图
   */
  bgimg?: string;
  /**
   * 所属部门ID
   */
  deptid?: number;
  /**
   * 状态标记，使用枚举类ResourceStatusFlagEnum
   */
  flag: number;
  /**
   * 介绍信息
   */
  introduction?: string;
  /**
   * 是否为在建工程，与所属单元分类中一致，前端不显示
   */
  isconstruction: boolean;
  /**
   * 纬度
   */
  latitude?: number;
  /**
   * 具体位置/部位
   */
  location?: string;
  /**
   * 经度
   */
  longitude?: number;
  /**
   * 排序
   */
  odr?: number;
  /**
   * 组织编码
   */
  orgcode: string;
  /**
   * 上级单元ID
   */
  parentid?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元ID，UUID自动生成
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}


/**
 * EhsUnitTreeTableItemDto 单元Table DTO
 */
export interface EhsUnitTreeTableItemDto {
  /**
   * 底图
   */
  bgimg?: string;
  children: ChildElement[];
  /**
   * 所属部门ID
   */
  deptid: number;
  /**
   * 状态标记
   */
  flag: number;
  /**
   * 介绍信息
   */
  introduction?: string;
  /**
   * 是否为在建工程
   */
  isconstruction: boolean;
  /**
   * 纬度
   */
  latitude?: number;
  /**
   * 具体位置/部位
   */
  location?: string;
  /**
   * 经度
   */
  longitude?: number;
  /**
   * 排序
   */
  odr?: number;
  /**
   * 组织编码
   */
  orgcode: string;
  /**
   * 上级单元ID
   */
  parentid?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}

/**
 * EhsUnitTreeTableItemDto
 */
export interface ChildElement {
  /**
   * 底图
   */
  bgimg?: string;
  children: ChildElement[];
  /**
   * 所属部门ID
   */
  deptid: number;
  /**
   * 状态标记
   */
  flag: number;
  /**
   * 介绍信息
   */
  introduction?: string;
  /**
   * 是否为在建工程
   */
  isconstruction: boolean;
  /**
   * 纬度
   */
  latitude?: number;
  /**
   * 具体位置/部位
   */
  location?: string;
  /**
   * 经度
   */
  longitude?: number;
  /**
   * 排序
   */
  odr?: number;
  /**
   * 组织编码
   */
  orgcode: string;
  /**
   * 上级单元ID
   */
  parentid?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}
/**
 * EhsUnitQueryConditionDto 单元查询条件
 */
export interface EhsUnitQueryConditionDto {
  /**
   * 是否只返回状态正常记录，默认为true只查询状态正常的，单元管理界面查询返回所有状态记录
   */
  flagisvalid: boolean;
  /**
   * 是否包含子级，是否包含下级和子级，为true时表示包含所有下级和子级单元
   */
  includechilds: boolean;
  /**
   * 是否为在建工程，为0表示false，为1表示true，为空或其他时表示不管该标记状态
   */
  isconstruction?: string;
  /**
   * 是否根据用户部门进行过滤，为true表示根据当前用户所关联部门对单元进行筛选过滤，为false表示不过滤
   */
  isdept: boolean;
  /**
   * 关键字
   */
  keywords?: string;
  /**
   * 组织代码，必须要有，只能在用户当前所在的组织中进行查询；是否由后台根据当前登录用户token获取
   */
  orgcode: string;
  /**
   * 上级单元ID
   */
  parentid?: string;
  [property: string]: any;
}
/**
 * EhsUnitSimpleDto 单元简要信息
 */
export interface EhsUnitTreeSimpleDto {
  /**
   * 下级单元集合
   */
  children: EhsUnitSimpleDto[];
  /**
   * 当前单元信息
   */
  info?: EhsUnitSimpleDto;
  [property: string]: any;
}

/**
 * EhsUnitSimpleDto
 *
 * 当前单元信息
 */
export interface EhsUnitSimpleDto {
  /**
   * 所属部门ID
   */
  deptid?: number;
  /**
   * 状态标记
   */
  flag: number;
  /**
   * 是否为在建阶段
   */
  isconstruction: string;
  /**
   * 排序
   */
  odr?: number;
  /**
   * 上级单元ID
   */
  parentid?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}


/**
 * EhsUnitStaticInfoDto 单元静态信息Dto
 */
export interface EhsUnitStaticInfoDto {
  /**
   * 信息内容，存储信息文字、图片路径或其他信息内容
   */
  content: string;
  /**
   * 单元静态信息ID
   */
  id: string;
  /**
   * 信息类型，使用枚举EhsUnitStaticinfoTypeEnum，用于标识信息是文字、图片、视频、全景图
   */
  infotype: number;
  /**
   * 排序号
   */
  odr?: number;
  /**
   * 信息标题
   */
  title: string;
  /**
   * 单元ID
   */
  unitid: string;
  [property: string]: any;
}


/**
 * EhsUnitCategoryQueryConditionDto 单元指定分类查询条件DTO
 */
export interface EhsUnitCategoryQueryConditionDto {
  /**
   * 组织代码，必须传入，由调用方传入；
   */
  orgcode: string;
  /**
   * 分类代码，必须传入
   */
  uccd: string;
  [property: string]: any;
}
