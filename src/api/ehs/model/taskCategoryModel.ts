/**
 * TaskCategoryDto 任务分类
 */
export interface TaskCategoryDto {
  /**
   * 分类代码
   */
  catcode: string;
  /**
   * 分类名称
   */
  catname: string;
  /**
   * 图标
   */
  icon?: string;
  /**
   * 是否需要填写检查表
   */
  needchecktable: boolean;
  /**
   * 执行结果是否需要确认
   */
  needconfirm: boolean;
  /**
   * 是否必须现场签到，如果设置该任务分类的现场签到为true，在移动端执行任务前需要进行定位验证是否在现场范围内；
   */
  needsignin: boolean;
  /**
   * 是否必须关联对象
   */
  notnull: boolean;
  /**
   * 关联的对象类型，使用枚举类EhsTaskRelateObjectTypeEnum
   */
  objecttype: number;
  /**
   * 所属组织代码
   */
  orgcode: string;
  /**
   * 任务大类，枚举定义的任务大类，分为自查、设备设施维保、隐患治理、检查/督查、作业活动；使用枚举EhsTaskCategoryTypeEnum
   */
  tasktype: number;
  [property: string]: any;
}

/**
 * TaskCategoryQueryConditionDto 任务分类查询条件
 */
export interface TaskCategoryQueryConditionDto {
  /**
   * 关联的对象类型，使用枚举类EhsTaskRelateObjectTypeEnum
   */
  objecttype: number;
  /**
   * 所属组织代码
   */
  orgcode: string;
  [property: string]: any;
}
