
/**
 * TaskConsoleQueryResultDto 任务控制台查询结果DTO
 */
export interface TaskConsoleQueryResultDto {
  items: TaskInfoDto[];
  /**
   * 总页数
   */
  pages: number;
  /**
   * 总数
   */
  total: number;
  [property: string]: any;
}

/**
 * TaskInfoDto
 */
export interface TaskInfoDto {
  /**
   * 任务分类，任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数，所有目标对象实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 是否已派单
   */
  hasdispatch: boolean;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 创建时间
   */
  intm: string;
  /**
   * 创建人ID
   */
  inuserid: number;
  /**
   * 创建人名称
   */
  inusername: string;
  /**
   * 是否需填写检查表
   */
  needchecktable: boolean;
  /**
   * 执行结果是否需确认
   */
  needconfirm: boolean;
  /**
   * 是否必须现场签到
   */
  needsignin: boolean;
  /**
   * 任务目标对象是否非空
   */
  notnull: boolean;
  /**
   * 任务目标对象类型
   */
  objtype: number;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 每个周期内计划执行次数
   */
  planfrequencycount: number;
  /**
   * 计划执行周期类型
   */
  planfrequencytype: number;
  /**
   * 计划执行总次数，所有目标对象的计划执行次数之和
   */
  plantotalcount: number;
  /**
   * 任务附件
   */
  taskattachment: string;
  /**
   * 任务内容
   */
  taskcontent: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务标题
   */
  tasktitle: string;
  /**
   * 任务大类
   */
  tasktype: number;
  [property: string]: any;
}


/**
 * TaskConsoleQueryUnitDoneConditionDto 任务控制台查询指定单元的已办任务条件DTO
 */
export interface TaskConsoleQueryUnitDoneConditionDto {
  /**
   * 任务计划的起始时间
   */
  begintm: string;
  /**
   * 任务计划的截止时间
   */
  endtm: string;
  /**
   * 每页条数，默认为10
   */
  page: number;
  /**
   * 页数，默认为1
   */
  pageSize: number;
  /**
   * 单元ID
   */
  unitid: string;
  [property: string]: any;
}

/**
 * TaskScheduleDto 任务执行情况一览表DTO
 */
export interface TaskScheduleDto {
  groups: TaskScheduleGroupDto[];
  /**
   * 执行时间周期集合，时间周期文字信息
   */
  tms: TaskScheduleTmItemDto[];
  [property: string]: any;
}


/**
 * TaskScheduleGroupDto 任务执行情况一览表分组DTO
 */
export interface TaskScheduleGroupDto {
  /**
   * 分组信息
   */
  group: TaskGroupInfoDto;
  /**
   * 分组执行周期完成情况集合，与执行时间周期集合的顺序保持一致
   */
  items: TaskScheduleGroupItemDto[];
  /**
   * 分组执行情况汇总，分组在所有执行周期中的汇总
   */
  summary: TaskScheduleGroupSummaryDto;
  [property: string]: any;
}

/**
 * 分组信息
 *
 * TaskGroupInfoDto
 */
export interface TaskGroupInfoDto {
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 任务分组名称
   */
  groupname: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 最新执行完成时间
   */
  latestexecendtm?: string;
  /**
   * 最新执行周期，文字
   */
  latestexectm?: string;
  /**
   * 分组排序号
   */
  odr: number;
  /**
   * 责任部门ID
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID
   */
  resuserid1?: number;
  /**
   * 参与组员2ID
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}

/**
 * TaskScheduleGroupItemDto
 */
export interface TaskScheduleGroupItemDto {
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 是否超时完成，在执行完成时如完成时间＞计划截止时间判定为超时完成；
   */
  istimeout: boolean;
  [property: string]: any;
}

/**
 * 分组执行情况汇总，分组在所有执行周期中的汇总
 *
 * TaskScheduleGroupSummaryDto
 */
export interface TaskScheduleGroupSummaryDto {
  /**
   * 实际执行总次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 计划执行总次数
   */
  plancount: number;
  /**
   * 超时完成总次数
   */
  timeoutcount: number;
  [property: string]: any;
}




/**
 * TaskExecuteInfoDto 任务执行情况信息表DTO
 */
export interface TaskExecuteInfoDto {
  /**
   * 执行起始时间
   */
  begintm: string;
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 检查表记录ID
   */
  checkrecordid?: string;
  /**
   * 确认附件信息
   */
  confirmattachment?: string;
  /**
   * 确认描述信息
   */
  confirmdescription?: string;
  /**
   * 确认人签字
   */
  confirmsign?: string;
  /**
   * 确认时间
   */
  confirmtm?: string;
  /**
   * 确认人ID
   */
  confirmuserid?: number;
  /**
   * 确认人名称
   */
  confirmusername?: string;
  /**
   * 执行截止时间
   */
  endtm: string;
  /**
   * 执行附件信息
   */
  execattachment?: string;
  /**
   * 执行内容描述
   */
  execdescription?: string;
  /**
   * 执行记录ID
   */
  execid: string;
  /**
   * 执行负责人签字
   */
  execsign?: string;
  /**
   * 执行时间，界面中填写，文字信息
   */
  executetmstr: string;
  /**
   * 执行人员，界面中填写
   */
  executor: string;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否确认通过，如果不需要确认，在填写执行记录时如执行完成为true自动设置为已确认
   */
  hasconfirm: boolean;
  /**
   * 是否执行完成，表示当前执行记录信息是否完整，如执行完成本次填写的执行记录即为完整信息；
   */
  hasexec: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源称
   */
  hazardname?: string;
  /**
   * 记录填写时间
   */
  intm: string;
  /**
   * 记录填写人ID
   */
  inuserid: number;
  /**
   * 记录填写人名称
   */
  inusername: string;
  /**
   * 执行是否正常，本次执行是否有发现异常
   */
  isnormal: boolean;
  /**
   * 任务项信息，在显示时从任务项表中查询并赋值
   */
  item?: TaskItemInfoDto;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}

/**
 * 任务项信息，在显示时从任务项表中查询并赋值
 *
 * TaskItemInfoDto
 */
export interface TaskItemInfoDto {
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 实际完成时间
   */
  execendtm?: string;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 任务项名称，任务标题+时间周期文字
   */
  itemname: string;
  /**
   * 最新执行开始时间
   */
  latestexecbegintm?: string;
  /**
   * 最新执行截止时间
   */
  latestexecendtm?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 周期内计划执行次数
   */
  planfrequencycount: string;
  /**
   * 周期类型
   */
  planfrequencytype: string;
  /**
   * 计划时间周期文字，时间周期文字
   */
  plantmstr: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 当前责任部门ID，≤0表示为空
   */
  transdeptid?: number;
  /**
   * 当前责任部门名称
   */
  transdeptname?: string;
  /**
   * 当前负责人ID，≤0表示为空
   */
  transuserid?: number;
  /**
   * 当前负责人名称
   */
  transusername?: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}




/**
 * TaskGroupItemsDto 任务分组和任务项集合DTO
 */
export interface TaskGroupItemsDto {
  /**
   * 任务分组信息
   */
  group: TaskGroupInfoDto;
  /**
   * 分组下的任务项集合
   */
  items: TaskItemInfoDto[];
  [property: string]: any;
}

/**
 * 任务分组信息
 *
 * TaskGroupInfoDto
 */
export interface TaskGroupInfoDto {
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 任务分组名称
   */
  groupname: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 最新执行完成时间
   */
  latestexecendtm?: string;
  /**
   * 最新执行周期，文字
   */
  latestexectm?: string;
  /**
   * 分组排序号
   */
  odr: number;
  /**
   * 责任部门ID
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID
   */
  resuserid1?: number;
  /**
   * 参与组员2ID
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}

/**
 * TaskItemInfoDto
 */
export interface TaskItemInfoDto {
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 实际完成时间
   */
  execendtm?: string;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 任务项名称，任务标题+时间周期文字
   */
  itemname: string;
  /**
   * 最新执行开始时间
   */
  latestexecbegintm?: string;
  /**
   * 最新执行截止时间
   */
  latestexecendtm?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 周期内计划执行次数
   */
  planfrequencycount: string;
  /**
   * 周期类型
   */
  planfrequencytype: string;
  /**
   * 计划时间周期文字，时间周期文字
   */
  plantmstr: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 当前责任部门ID，≤0表示为空
   */
  transdeptid?: number;
  /**
   * 当前责任部门名称
   */
  transdeptname?: string;
  /**
   * 当前负责人ID，≤0表示为空
   */
  transuserid?: number;
  /**
   * 当前负责人名称
   */
  transusername?: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}


/**
 * TaskConsoleUnitTaskItemQueryConditionDto 任务控制台-指定单元任务项查询条件DTO
 */
export interface TaskConsoleUnitTaskItemQueryConditionDto {
  /**
   * 任务计划的起始时间，为空表示不根据时间过滤
   */
  begintm?: string;
  /**
   * 任务计划的截止时间，为空表示不根据时间过滤
   */
  endtm?: string;
  /**
   * 任务项的状态，0-待办，1-已办，其他-全部
   */
  status: number;
  /**
   * 单元ID
   */
  unitid: string;
  [property: string]: any;
}

/**
 * TaskConsoleUnitTaskItemPaginationQueryConditionDto 任务控制台-指定单元任务项分页查询条件DTO
 */
export interface TaskConsoleUnitTaskItemPaginationQueryConditionDto {
  /**
   * 任务计划的起始时间
   */
  begintm: string;
  /**
   * 任务计划的截止时间
   */
  endtm: string;
  /**
   * 页数，默认为1
   */
  page: number;
  /**
   * 每页条数，默认为10
   */
  pageSize: number;
  /**
   * 任务的状态，0-待办，1-已办，其他-全部
   */
  status: number;
  /**
   * 单元ID
   */
  unitid: string;
  [property: string]: any;
}
