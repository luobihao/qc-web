/**
 * TaskBenchQueryConditionDto 任务工作台查询条件DTO
 */
export interface TaskBenchQueryConditionDto {
  /**
   * 起始时间
   */
  begintm: string;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 任务状态标记，0表示待办、1表示已办、其他表示全部；
   */
  flag: number;
  /**
   * 关键字，可以匹配单元名称、设备设施名称、危险源名称、责任部门名称、参与组员名称
   */
  keywords?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 每页条数，默认为10
   */
  page: number;
  /**
   * 页数，默认为1
   */
  pageSize: number;
  [property: string]: any;
}

/**
 * TaskBenchQueryResultDto 任务工作台查询结果
 */
export interface TaskBenchQueryResultDto {
  items: TaskItemInfoDto[];
  /**
   * 总页数
   */
  pages: number;
  /**
   * 总数
   */
  total: number;
  [property: string]: any;
}

/**
 * TaskItemInfoDto
 */
export interface TaskItemInfoDto {
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 实际完成时间
   */
  execendtm?: string;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 是否为转交接收人，根据当前用户和负责部门判断当前用户是否为转交接收人
   */
  istransreceiver: boolean;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 任务项名称，任务标题+时间周期文字
   */
  itemname: string;
  /**
   * 最新执行开始时间
   */
  latestexecbegintm?: string;
  /**
   * 最新执行截止时间
   */
  latestexecendtm?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 周期内计划执行次数
   */
  planfrequencycount: string;
  /**
   * 周期类型
   */
  planfrequencytype: string;
  /**
   * 计划时间周期文字，时间周期文字
   */
  plantmstr: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 当前责任部门ID，≤0表示为空
   */
  transdeptid?: number;
  /**
   * 当前责任部门名称
   */
  transdeptname?: string;
  /**
   * 转交是否接收，如果转交没有被接收不作为当前责任部门/人的任务
   */
  transhasreceive: boolean;
  /**
   * 接收确认时间，接收确认或不接收时更新该时间
   */
  transreceivetm?: string;
  /**
   * 当前负责人ID，≤0表示为空
   */
  transuserid?: number;
  /**
   * 当前负责人名称
   */
  transusername?: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  /**
   * 报告id
   */
  rptid?: string;
  [property: string]: any;
}
