/**
 * EhsTroubleCorrectInfoDto 隐患治理信息DTO
 */
export interface EhsTroubleCorrectInfoDto {
  /**
   * 附件
   */
  attachment?: string;
  /**
   * 所属危险源类别代码
   */
  categorycode: string;
  /**
   * 危险源类别名称
   */
  categoryname: string;
  /**
   * 验收附件
   */
  checkattachment?: string;
  /**
   * 验收意见
   */
  checkdescription?: string;
  /**
   * 验收负责人签字
   */
  checksign?: string;
  /**
   * 验收时间
   */
  checktm?: string;
  /**
   * 验收人员
   */
  checkusername?: string;
  /**
   * 实际整改附件
   */
  correctattachment?: string;
  /**
   * 实际整改情况描述
   */
  correctdescription?: string;
  /**
   * 整改完成时间
   */
  correctendtm?: string;
  /**
   * 隐患治理ID
   */
  correctid: string;
  /**
   * 整改负责人签字
   */
  correctsign?: string;
  /**
   * 实际整改时间
   */
  correcttm?: string;
  /**
   * 实际整改人员
   */
  correctusername?: string;
  /**
   * 事故诱因
   */
  couse: string;
  /**
   * 问题描述文字
   */
  description: string;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 是否验收通过
   */
  hascheck: boolean;
  /**
   * 是否整改完成
   */
  hascorrect: boolean;
  /**
   * 是否已派单整改
   */
  hasdispatch: boolean;
  /**
   * 危险源ID
   */
  hazardid: string;
  /**
   * 写入时间
   */
  intm: string;
  /**
   * 是否为重大危险源
   */
  ismajor: boolean;
  /**
   * 是否上报
   */
  isreport: boolean;
  /**
   * 所属危险源清单代码
   */
  itemcode: string;
  /**
   * 危险源清单名称
   */
  itemname: string;
  /**
   * 可能导致的伤害
   */
  maycouseharm: string;
  /**
   * 所属组织代码
   */
  orgcode: string;
  /**
   * 具体部位
   */
  position: string;
  /**
   * 所属危险源项目代码
   */
  projectcode: string;
  /**
   * 危险源项目名称
   */
  projectname: string;
  /**
   * 整改要求描述
   */
  reqdescription?: string;
  /**
   * 整改要求完成时间
   */
  reqendtm?: string;
  /**
   * 整改责任部门ID
   */
  resdeptid?: number;
  /**
   * 整改责任部门名称
   */
  resdeptname?: string;
  /**
   * 整改负责人ID
   */
  resuserid?: number;
  /**
   * 整改负责人名称
   */
  resusername?: string;
  /**
   * 整改负责人手机号
   */
  resuserphone?: string;
  /**
   * 风险等级；4个风险等级，使用EhsRiskLevelEnum
   */
  risklevel?: number;
  /**
   * 报告文件ID，≤0表示为空
   */
  rptid?: number;
  /**
   * 标题
   */
  title: string;
  /**
   * 发现时间，发现/上报隐患时间
   */
  tm: string;
  /**
   * 单元名称
   */
  unit: string;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 不安全因素
   */
  unsafeactor: number;
  [property: string]: any;
}


/**
 * EhsTroubleCorrectQueryConditionDto 隐患治理查询条件
 */
export interface EhsTroubleCorrectQueryConditionDto {
  /**
   * 起始时间，按照发现/上报隐患的时间
   */
  begintm: string;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 关键字，可以匹配危险源类别、项目、名称、部位、事故诱因、可能导致的危害、标题、问题描述文字
   */
  keywords?: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 状态，全部、已验收通过、已整改完成
   * 已派单、未派单
   */
  status: string;
  /**
   * 单元ID
   */
  unitid?: string;
  [property: string]: any;
}


/**
 * EhsTroubleCorrectBatchDispatchDto 隐患治理批量派单
 */
export interface EhsTroubleCorrectBatchDispatchDto {
  /**
   * 整改时限
   */
  endtm: string;
  /**
   * 隐患治理ID集合
   */
  ids: string[];
  [property: string]: any;
}

/**
 * EhsTroubleCorrectDispatchDto 隐患治理派单DTO
 */
export interface EhsTroubleCorrectDispatchDto {
  /**
   * 整改责任部门ID
   */
  deptid: number;
  /**
   * 整改责任部门名称
   */
  deptname?: string;
  /**
   * 整改要求描述
   */
  description?: string;
  /**
   * 整改时限
   */
  endtm: string;
  /**
   * 隐患治理ID
   */
  id: string;
  /**
   * 整改负责人ID
   */
  userid: number;
  /**
   * 整改负责人名称
   */
  username?: string;
  /**
   * 整改负责人手机号
   */
  userphone?: string;
  [property: string]: any;
}



/**
 * EhsTroubleCorrectCompleteDto 隐患治理整改完成填写DTO
 */
export interface EhsTroubleCorrectCompleteDto {
  /**
   * 实际整改附件
   */
  attachment?: string;
  /**
   * 实际整改描述信息
   */
  description?: string;
  /**
   * 整改完成时间，完成的时间点/日期
   */
  endtm: string;
  /**
   * 隐患治理ID
   */
  id: string;
  /**
   * 整改负责人签字
   */
  sign?: string;
  /**
   * 实际整改时间，文字信息
   */
  tm: string;
  /**
   * 实际整改人员
   */
  username: string;
  [property: string]: any;
}


/**
 * EhsTroubleCorrectCheckDto 隐患治理验收DTO
 */
export interface EhsTroubleCorrectCheckDto {
  /**
   * 验收附件
   */
  attachment?: string;
  /**
   * 验收描述信息
   */
  description?: string;
  /**
   * 验收时间，验收的时间点/日期
   */
  endtm: string;
  /**
   * 验收是否通过
   */
  hascheck: boolean;
  /**
   * 隐患治理ID
   */
  id: string;
  /**
   * 验收负责人签字
   */
  sign?: string;
  /**
   * 验收人员
   */
  username: string;
  [property: string]: any;
}


/**
 * EhsReportFileUpdateDto
 */
export interface EhsReportFileUpdateDto {
  /**
   * 上传的文件ID
   */
  fileid: number;
  /**
   * 隐患治理或任务项ID
   */
  id: string;
  [property: string]: any;
}
