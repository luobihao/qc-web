/**
 * TaskGroupInfoDto 安全任务分组信息DTO
 */
export interface TaskGroupInfoDto {
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 任务分组名称
   */
  groupname: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 最新执行完成时间
   */
  latestexecendtm?: string;
  /**
   * 最新执行周期，文字
   */
  latestexectm?: string;
  /**
   * 责任部门ID
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID
   */
  resuserid1?: number;
  /**
   * 参与组员2ID
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}


/**
 * TaskGroupItemsDto 任务分组和任务项集合DTO
 */
export interface TaskGroupItemsDto {
  /**
   * 任务分组信息
   */
  group: TaskGroupInfoDto;
  /**
   * 分组下的任务项集合
   */
  items: TaskItemInfoDto[];
  [property: string]: any;
}

/**
 * TaskItemInfoDto
 */
export interface TaskItemInfoDto {
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 最新执行开始时间
   */
  latestexecbegintm?: string;
  /**
   * 最新执行截止时间
   */
  latestexecendtm?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 周期内计划执行次数
   */
  planfrequencycount: string;
  /**
   * 周期类型
   */
  planfrequencytype: string;
  /**
   * 计划时间周期文字
   */
  plantmstr: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 当前责任部门ID，≤0表示为空
   */
  transdeptid?: number;
  /**
   * 当前责任部门名称
   */
  transdeptname?: string;
  /**
   * 当前负责人ID，≤0表示为空
   */
  transuserid?: number;
  /**
   * 当前负责人名称
   */
  transusername?: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}


/**
 * TaskGroupAddDto  新增安全任务分组DTO
 */
export interface TaskGroupAddDto {
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组名称
   */
  groupname: string;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}


/**
 * TaskGroupUpdateDto 更新安全任务分组DTO
 */
export interface TaskGroupUpdateDto {
  /**
   * 分组ID
   */
  groupid: string;
  /**
   * 任务分组名称
   */
  groupname: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  [property: string]: any;
}
