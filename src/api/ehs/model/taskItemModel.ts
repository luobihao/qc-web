/**
 * TaskItemInfoDto 安全任务项信息DTO
 */
export interface TaskItemInfoDto {
  /**
   * 任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 实际完成时间
   */
  execendtm?: string;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 最新执行开始时间
   */
  latestexecbegintm?: string;
  /**
   * 最新执行截止时间
   */
  latestexecendtm?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 周期内计划执行次数
   */
  planfrequencycount: string;
  /**
   * 周期类型
   */
  planfrequencytype: string;
  /**
   * 计划时间周期文字
   */
  plantmstr: string;
  /**
   * 责任部门ID，≤0表示为空
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID，≤0表示为空
   */
  resuserid1?: number;
  /**
   * 参与组员2ID，≤0表示为空
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务大类，枚举
   */
  tasktype: number;
  /**
   * 当前责任部门ID，≤0表示为空
   */
  transdeptid?: number;
  /**
   * 当前责任部门名称
   */
  transdeptname?: string;
  /**
   * 当前负责人ID，≤0表示为空
   */
  transuserid?: number;
  /**
   * 当前负责人名称
   */
  transusername?: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}

/**
 * TaskItemTransmitDto 安全任务项转交DTO
 */
export interface TaskItemTransmitDto {
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 转交部门ID，≤0表示无效
   */
  transdeptid?: number;
  /**
   * 转交部门名称
   */
  transdeptname?: string;
  /**
   * 转交人员ID，≤0表示无效
   */
  transuserid?: number;
  /**
   * 转交人员名称
   */
  transusername?: string;
  [property: string]: any;
}


/**
 * TaskItemCheckTableConditionDto 任务项检查表获取条件DTO
 */
export interface TaskItemCheckTableConditionDto {
  /**
   * 执行记录ID
   */
  execid?: string;
  /**
   * 任务项ID
   */
  itemid: string;
  /**
   * 单元ID
   */
  unitid: string;
  [property: string]: any;
}



/** 
 * TaskItemCheckTableResultDto 任务项检查表获取结果DTO
 */
export interface TaskItemCheckTableResultDto {
  /**
   * 提示信息，用于提示获取结果处理相关的信息
   */
  msg?: string;
  /**
   * 检查表记录，可以为空
   */
  record?: CheckRecordDto;
  /**
   * 检查表信息
   */
  table: CheckTableFullDto;
  [property: string]: any;
}

/**
 * 检查表记录，可以为空
 *
 * CheckRecordDto
 */
export interface CheckRecordDto {
  /**
   * 记录内容
   */
  content?: string;
  /**
   * 数据ID
   */
  dataid: string;
  /**
   * 创建时间
   */
  intm: string;
  /**
   * 检查记录ID
   */
  recordid: string;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 更新时间
   */
  updatetm: string;
  /**
   * 用户ID
   */
  userid: string;
  [property: string]: any;
}

/**
 * 检查表信息
 *
 * CheckTableFullDto
 */
export interface CheckTableFullDto {
  /**
   * 分类代码
   */
  catcode: string;
  /**
   * 备注
   */
  description?: string;
  /**
   * 分组集合
   */
  groups: CheckGroupFullDto[];
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  /**
   * 单元分类编码
   */
  uccd: string;
  /**
   * 单元分类名称
   */
  ucnm?: string;
  /**
   * 单元ID集合，检查表适用的单元集合，为空表示使用单元分类下的所有单元
   */
  unitids?: string[];
  [property: string]: any;
}

/**
 * CheckGroupFullDto
 */
export interface CheckGroupFullDto {
  /**
   * 分组ID
   */
  groupid: string;
  items: CheckItemDto[];
  /**
   * 排序号
   */
  odr?: number;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  [property: string]: any;
}

/**
 * CheckItemDto
 */
export interface CheckItemDto {
  /**
   * 填写文本类型，填写文本信息的类型
   */
  contenttype?: number;
  /**
   * 说明描述信息
   */
  description?: string;
  /**
   * 分组ID
   */
  groupid: string;
  /**
   * 检查项ID
   */
  itemid: string;
  /**
   * 最少图片数量
   */
  minimgcount?: number;
  /**
   * 是否非空
   */
  notnull: boolean;
  /**
   * 排序号
   */
  odr?: number;
  /**
   * 待选项名称，自定义待选项集合
   */
  options?: string;
  /**
   * 状态
   */
  status: number;
  /**
   * 检查表ID
   */
  tableid: string;
  /**
   * 标题
   */
  title: string;
  [property: string]: any;
}
