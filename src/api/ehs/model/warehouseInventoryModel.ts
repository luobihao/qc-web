/**
 * WareHouseInventoryDto 仓库出入库记录DTO
 */
export interface WareHouseInventoryDto {
  /**
   * 附件
   */
  attachment?: string;
  /**
   * 复核时间
   */
  checktm?: string;
  /**
   * 复核人ID
   */
  checkuserid?: number;
  /**
   * 复核人名称
   */
  checkusername?: string;
  /**
   * 操作说明文字
   */
  description?: string;
  /**
   * 出入库记录ID
   */
  id: string;
  /**
   * 操作标题
   */
  name: string;
  /**
   * 操作时间
   */
  optm: string;
  /**
   * 操作类型
   */
  optype: number;
  /**
   * 操作人ID
   */
  opuserid: number;
  /**
   * 操作人名称
   */
  opusername: string;
  /**
   * 时间
   */
  tm: string;
  /**
   * 仓库ID
   */
  warehouseId: string;
  [property: string]: any;
}


/**
 * WareHouseInventoryQueryConditionDto 仓库出入库记录查询条件DTO
 */
export interface WareHouseInventoryQueryConditionDto {
  /**
   * 起始时间
   */
  begintm: string;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 仓库ID
   */
  warehouseid: string;
  [property: string]: any;
}


/**
 * WareHouseInventoryAddDto 仓库出入库记录新增DTO
 */
export interface WareHouseInventoryAddDto {
  /**
   * 附件
   */
  attachment?: string;
  /**
   * 操作说明文字
   */
  description?: string;
  /**
   * 操作标题
   */
  name: string;
  /**
   * 操作类型，出库、入库、报损、废液处理；使用枚举WarehouseInventoryOperateTypeEnum
   */
  optype: number;
  /**
   * 时间
   */
  tm: string;
  /**
   * 仓库ID
   */
  warehouseId: string;
  [property: string]: any;
}
