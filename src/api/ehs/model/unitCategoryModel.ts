/**
 * TplUnitCategoryDto 单元分类DTO
 */
export interface TplUnitCategoryDto {
  /**
   * 导则库代码
   */
  hdbcd: string;
  /**
   * 是否为在建工程
   */
  isconstruction: boolean;
  /**
   * 上级单元分类代码
   */
  puccd?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元分类名称
   */
  ucnm: string;
  /**
   * 单元类型标记，区分为主单元、分/子单元、岗位/设备/作业单元
   */
  uctype: string;
  [property: string]: any;
}

/**
 * TplUnitCategoryTreeTableItemDto 单元分类tableTree DTO
 */
export interface TplUnitCategoryTreeTableItemDto {
  children: ChildElement[];
  /**
   * 导则库代码
   */
  hdbcd: string;
  /**
   * 是否为在建工程
   */
  isconstruction: boolean;
  /**
   * 上级单元分类代码
   */
  puccd?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元分类名称
   */
  ucnm: string;
  /**
   * 单元类型标记，区分为主单元、分/子单元、岗位/设备/作业单元
   */
  uctype: string;
  [property: string]: any;
}

/**
 * TplUnitCategoryTreeTableItemDto
 */
export interface ChildElement {
  children: ChildElement[];
  /**
   * 导则库代码
   */
  hdbcd: string;
  /**
   * 是否为在建工程
   */
  isconstruction: boolean;
  /**
   * 上级单元分类代码
   */
  puccd?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元分类名称
   */
  ucnm: string;
  /**
   * 单元类型标记，区分为主单元、分/子单元、岗位/设备/作业单元
   */
  uctype: string;
  [property: string]: any;
}

/**
 * UnitCategoryHazardItemsSetDto 设置分类与危险源清单关联DTO
 */
export interface UnitCategoryHazardItemsSetDto {
  /**
   * 危险源清单ID集合
   */
  hicds?: string[];
  /**
   * 单元分类代码
   */
  uccd: string;
  [property: string]: any;
}

export type unitCategoryTreeDataResult = TplUnitCategoryTreeTableItemDto[]
