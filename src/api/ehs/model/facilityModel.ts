/**
 * FacilityDto
 */
export interface FacilityDto {
  /**
   * 起始日期，购买或投运日期
   */
  begintm?: string;
  /**
   * 大类，资产国标大类
   */
  cat1: string;
  /**
   * 分类，资产分类
   */
  cat2: string;
  /**
   * 编号，在资产系统或其他系统中的编号
   */
  code: string;
  /**
   * 销售商，供货或采购单位
   */
  dealer?: string;
  /**
   * 截止日期，停用或报废日期
   */
  endtm?: string;
  /**
   * ID
   */
  id: string;
  /**
   * 生产厂家，生产厂家或品牌
   */
  manufactor?: string;
  /**
   * 名称
   */
  name: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 原始价值，单位：元
   */
  ov?: string;
  /**
   * 序列号，设备原厂序列号或SN
   */
  sn?: string;
  /**
   * 规格型号
   */
  specification?: string;
  /**
   * 使用状况，在用、停用
   */
  status: number;
  /**
   * 所属单元ID
   */
  unitid: string;
  [property: string]: any;
}


/**
 * FacilityQueryConditionDto
 */
export interface FacilityQueryConditionDto {
  /**
   * 关键字，可以匹配名称、SN、制造商等
   */
  keywords?: string;
  /**
   * 组织代码，必须有，只能查询指定组织中的设备设施
   */
  orgcode: string;
  /**
   * 使用状况，在用、停用
   */
  status: number;
  /**
   * 所属单元ID
   */
  unitid?: string;
  [property: string]: any;
}
