/**
 * HazardLedgerQueryConditionDto 危险源台账查询条件
 */
export interface HazardLedgerQueryConditionDto {
  /**
   * 是否包含子级，是否包含下级和子级，为true时表示包含所有下级和子级单元
   */
  includechilds: boolean;
  /**
   * 是否为在建工程，为0表示false，为1表示true，为空或其他时表示不管该标记状态
   */
  isconstruction: string;
  /**
   * 是否根据用户部门进行过滤，为true表示根据当前用户所关联部门对单元进行筛选过滤，为false表示不过滤；台账查看时设置为false，辨识评价模式时设置为true
   */
  isdept: boolean;
  /**
   * 危险源类别，为0表示false(第一类危险源)，为1表示true(第二类危险源)，为空或其他时表示不管该标记状态;
   */
  ishidden?: string;
  /**
   * 级别，为0表示false(一般)，为1表示true(重大)，为空或其他时表示不管该标记状态
   */
  ismajor?: string;
  /**
   * 关键字，可以是危险源类别、项目、危险源名称
   */
  keywords?: string;
  /**
   * 组织代码，必须传入，由调用方传入；
   */
  orgcode: string;
  /**
   * 风险等级
   */
  risklevel?: string;
  /**
   * 单元ID，可以指定父级单元编码，没有指定时为全部
   */
  unitid?: string;
  [property: string]: any;
}

/**
 * HazardLedgerQueryResultDto 危险源台账查询结果DTO
 */
export interface HazardLedgerQueryResultDto {
  /**
   * 一般危险源数量，查询结果合计值
   */
  general1: number;
  /**
   * 一般隐患数量，查询结果合计值
   */
  general2: number;
  items: Items;
  /**
   * 低风险危险源数量，查询结果合计值
   */
  level11: number;
  /**
   * 一般风险危险源数量，查询结果合计值
   */
  level12: number;
  /**
   * 较大风险危险源数量，查询结果合计值
   */
  level13: number;
  /**
   * 重大风险危险源数量，查询结果合计值
   */
  level14: number;
  /**
   * 低风险隐患数量，查询结果合计值
   */
  level21: number;
  /**
   * 一般风险隐患数量，查询结果合计值
   */
  level22: number;
  /**
   * 较大风险隐患数量，查询结果合计值
   */
  level23: number;
  /**
   * 重大风险隐患数量，查询结果合计值
   */
  level24: number;
  /**
   * 最大危险源数量，查询结果合计值
   */
  major1: number;
  /**
   * 重大隐患数量，查询结果合计值
   */
  major2: number;
  [property: string]: any;
}

export interface Items {
  /**
   * 危险源类别Tree信息列表
   */
  categorys: The115906801[];
  /**
   * 危险源信息列表
   */
  hazards: EhsHazardInfoDto[];
  /**
   * 单元简要Tree信息列表
   */
  units: EhsUnitTreeSimpleDto[];
  [property: string]: any;
}

/**
 * 危险源类别树型结构DTO
 */
export interface The115906801 {
  children: The115906801;
  /**
   * 危险源类别代码、项目代码或者危险源ID
   */
  code: string;
  /**
   * 层级：1-类别，2-项目，3-危险源
   */
  level: string;
  /**
   * 危险源类别名称、项目名称或者危险源名称
   */
  name: string;
  /**
   * 父代码
   */
  parentcode: string;
  [property: string]: any;
}


/**
 * EhsUnitTreeSimpleDto 
 */
export interface EhsUnitTreeSimpleDto {
  /**
   * 下级单元集合
   */
  children: EhsUnitTreeSimpleDto[];
  /**
   * 当前单元信息
   */
  info?: EhsUnitSimpleDto;
  [property: string]: any;
}

/**
 * 当前单元信息
 *
 * EhsUnitSimpleDto
 */
export interface EhsUnitSimpleDto {
  /**
   * 状态标记
   */
  flag: number;
  /**
   * 是否为在建阶段
   */
  isconstruction: string;
  /**
   * 排序
   */
  odr?: number;
  /**
   * 上级单元ID
   */
  parentid?: string;
  /**
   * 责任部门ID
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 责任人ID
   */
  resuserid?: number;
  /**
   * 责任人名称
   */
  resusername?: string;
  /**
   * 责任人手机号
   */
  resuserphone?: string;
  /**
   * 安全员ID
   */
  safeuserid?: number;
  /**
   * 安全员名称
   */
  safeusername?: string;
  /**
   * 安全员手机号
   */
  safeuserphone?: string;
  /**
   * 单元分类代码
   */
  uccd: string;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}


/**
 * HazardLedgerQueryResultItemDto 危险源台账查询结果项DTO
 */
export interface HazardLedgerQueryResultItemDto {
  children: ChildElement[];
  /**
   * 责任部门ID，数据类型不是单元信息时为空
   */
  deptid?: number;
  /**
   * 危险源信息，如果type为危险源时有数据，为其他时没有数据
   */
  hazard: EhsHazardInfoDto;
  /**
   * ID，单元编码、类别编码或危险源ID
   */
  id: string;
  /**
   * 是否为在建阶段，是否为在建工程单元；数据类型不是单元信息时为空
   */
  isconstruction?: string;
  /**
   * 名称，单元名称、类别名称或危险源名称
   */
  name: string;
  /**
   * 类型，数据的类型，标识该条数据时单元信息、危险源类别、危险源信息
   */
  type: number;
  [property: string]: any;
}

export interface ChildElement {
  children: ChildElement[];
  /**
   * 责任部门ID，数据类型不是单元信息时为空
   */
  deptid?: number;
  /**
   * 危险源信息，如果type为危险源时有数据，为其他时没有数据
   */
  hazard: EhsHazardInfoDto;
  /**
   * ID，单元编码、类别编码或危险源ID
   */
  id: string;
  /**
   * 是否为在建阶段，是否为在建工程单元；数据类型不是单元信息时为空
   */
  isconstruction?: string;
  /**
   * 名称，单元名称、类别名称或危险源名称
   */
  name: string;
  /**
   * 类型，数据的类型，标识该条数据时单元信息、危险源类别、危险源信息
   */
  type: number;
  [property: string]: any;
}

/**
 * 危险源信息，如果type为危险源时有数据，为其他时没有数据
 *
 * EhsHazardInfoDto
 */
export interface EhsHazardInfoDto {
  /**
   * 附件，json格式，可以包含多个附件
   */
  attachment?: string;
  /**
   * 所属危险源类别代码
   */
  categorycode?: string;
  /**
   * 危险源类别名称
   */
  categoryname?: string;
  /**
   * 防控措施，管控措施
   */
  conmeasures?: string;
  /**
   * 事故诱因
   */
  couse?: string;
  /**
   * 危险源描述
   */
  description?: string;
  /**
   * 风险评价时间
   */
  evltm?: string;
  /**
   * 设备设施ID，危险源可能不针对设备，可能针对环境、管理或作业活动
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 是否进行风险评价，在重新辨识后是否进行风险评价
   */
  hasevl?: boolean;
  /**
   * 危险源ID
   */
  hazardid: string;
  /**
   * 危险源名称
   */
  hazardname: string;
  /**
   * 危险源辨识结果；不适用，不存在，第1类危险源，第2类危险源，使用EhsHazardIndentificationResultTypeEnum
   */
  identificationresult: number;
  /**
   * 是否为重大危险源
   */
  ismajor?: boolean;
  /**
   * 是否上报
   */
  isreport: boolean;
  /**
   * 所属危险源清单代码
   */
  itemcode?: string;
  /**
   * 危险源清单名称
   */
  itemname?: string;
  /**
   * 可能导致的伤害
   */
  maycouseharm?: string;
  /**
   * 监测监控措施
   */
  monitoringmeasures?: string;
  /**
   * 所属组织代码
   */
  orgcode: string;
  /**
   * 具体部位
   */
  position?: string;
  /**
   * 所属危险源项目代码
   */
  projectcode?: string;
  /**
   * 危险源项目名称
   */
  projectname?: string;
  /**
   * 最近辨识时间
   */
  recentidentificationtm?: string;
  /**
   * 责任部门ID
   */
  resdeptid?: string;
  /**
   * 责任部门/机构名称
   */
  resdeptname?: string;
  /**
   * 责任人ID
   */
  resuserid?: string;
  /**
   * 责任人名称
   */
  resusername?: string;
  /**
   * 责任人电话
   */
  resuserphone?: string;
  /**
   * 风险等级；4个风险等级，使用EhsRiskLevelEnum
   */
  risklevel?: number;
  /**
   * 标题，隐患/问题的标题
   */
  title: string;
  /**
   * 所属单元代码
   */
  unitid: string;
  /**
   * 所属单元名称
   */
  unitname?: string;
  /**
   * 不安全因素，防控措施失效的原因：人的不安全行为、物的不安全状态、管理缺陷；不安全因素的分类组合值
   */
  unsafefactor?: number;
  [property: string]: any;
}


/**
 * QueryHazardByUnitConditionDto 按单元查询危险源条件DTO
 */
export interface QueryHazardByUnitConditionDto {
  /**
   * 是否为在建工程，为0表示false，为1表示true，为空或其他时表示不管该标记状态
   */
  isconstruction?: string;
  /**
   * 是否根据用户部门进行过滤，为true表示根据当前用户所关联部门对单元进行筛选过滤，为false表示不过滤；台账查看时设置为false，辨识评价模式时设置为true
   */
  isdept: boolean;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 单元ID，过滤指定单元（返回子级）
   */
  unitid?: string;
  [property: string]: any;


  /**
 * QueryHazardByUnitResultDto 按单元查询危险源结果DTO
 */
export interface QueryHazardByUnitResultDto {
  /**
   * 一般危险源数量，查询结果合计值，查询结果根节点单元的合计值
   */
  general1: number;
  /**
   * 一般隐患数量，查询结果合计值，查询结果根节点单元的合计值
   */
  general2: number;
  items: QueryHazardByUnitResultItemDto[];
  /**
   * 低风险危险源数量，查询结果合计值
   */
  level11: number;
  /**
   * 一般风险危险源数量，查询结果合计值
   */
  level12: number;
  /**
   * 较大风险危险源数量，查询结果合计值
   */
  level13: number;
  /**
   * 重大风险危险源数量，查询结果合计值
   */
  level14: number;
  /**
   * 低风险隐患数量，查询结果合计值
   */
  level21: number;
  /**
   * 一般风险隐患数量，查询结果合计值
   */
  level22: number;
  /**
   * 较大风险隐患数量，查询结果合计值
   */
  level23: number;
  /**
   * 重大风险隐患数量，查询结果合计值
   */
  level24: number;
  /**
   * 最大危险源数量，查询结果合计值
   */
  major1: number;
  /**
   * 重大隐患数量，查询结果合计值
   */
  major2: number;
  [property: string]: any;
}

/**
 * QueryHazardByUnitResultItemDto 按单元查询危险源结果项
 */
export interface QueryHazardByUnitResultItemDto {
  children: QueryHazardByUnitResultItemDto[];
  /**
   * 责任部门ID
   */
  deptid: string;
  /**
   * 一般危险源数量
   */
  general1: number;
  /**
   * 一般隐患数量
   */
  general2: number;
  /**
   * 危险源辨识时间，进行危险源辨识的最新时间
   */
  idtm: string;
  /**
   * 是否为在建工程
   */
  isconstruction: boolean;
  /**
   * 低风险危险源数量
   */
  level11: number;
  /**
   * 一般风险危险源数量
   */
  level12: number;
  /**
   * 较大风险危险源数量
   */
  level13: number;
  /**
   * 重大风险危险源数量
   */
  level14: number;
  /**
   * 低风险隐患数量
   */
  level21: number;
  /**
   * 一般风险隐患数量
   */
  level22: number;
  /**
   * 较大风险隐患数量
   */
  level23: number;
  /**
   * 重大风险隐患数量
   */
  level24: number;
  /**
   * 最大危险源数量
   */
  major1: number;
  /**
   * 重大隐患数量
   */
  major2: number;
  /**
   * 上级单元ID
   */
  parentid: string;
  /**
   * 责任部门
   */
  responsibledept: string;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}
}

/**
 * QueryOneUnitHazardConditionDto 指定单元查询危险源条件
 */
export interface QueryOneUnitHazardConditionDto {
  /**
   * 单元ID，可以指定父级单元ID，没有指定时为全部
   */
  unitid: string;
  [property: string]: any;
}



/** 
 * QueryOneUnitHazardResultDto 指定单元查询危险源结果
 */
export interface QueryOneUnitHazardResultDto {
  /**
   * 一般危险源数量
   */
  general1: number;
  /**
   * 一般隐患数量
   */
  general2: number;
  /**
   * 危险源清单
   */
  hazards: EhsHazardInfoDto[];
  /**
   * 危险源辨识时间
   */
  idtm: string;
  /**
   * 是否为在建阶段
   */
  isconstruction: boolean;
  /**
   * 低风险危险源数量
   */
  level11: number;
  /**
   * 一般风险危险源数量
   */
  level12: number;
  /**
   * 较大风险危险源数量
   */
  level13: number;
  /**
   * 重大风险危险源数量
   */
  level14: number;
  /**
   * 低风险隐患数量
   */
  level21: number;
  /**
   * 一般风险隐患数量
   */
  level22: number;
  /**
   * 较大风险隐患数量
   */
  level23: number;
  /**
   * 重大风险隐患数量
   */
  level24: number;
  /**
   * 最大危险源数量
   */
  major1: number;
  /**
   * 重大隐患数量
   */
  major2: number;
  /**
   * 所属单元分类
   */
  uccd: string;
  /**
   * 单元ID
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  [property: string]: any;
}




// 辨识

/**
 * HazardIdentifyQueryConditionDto 危险源辨识查询条件
 */
export interface HazardIdentifyQueryConditionDto {
  /**
   * 查询起始时间，危险源辨识记录中的辨识时间在查询的时间内的记录
   */
  begintm: string;
  /**
   * 查询截止时间，危险源辨识记录中的辨识时间在查询的时间内的记录
   */
  endtm: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 单元ID，过滤指定单元（返回子级）
   */
  unitid?: string;
  [property: string]: any;
}

/**
 * HazardIdentifyQueryResultDto 危险源辨识查询结果DTO
 *
 * HazardIdentifyQueryResultItemDto 
 */
export interface HazardIdentifyQueryResultDto {
  /**
   * 子级单元危险源辨识查询结果项
   */
  childs: HazardIdentifyQueryResultDto[];
  /**
   * 危险源辨识信息集合
   */
  identifys: HazardIdentifyInfoDto[];
  /**
   * 单元简要信息
   */
  unit: EhsUnitSimpleDto;
  [property: string]: any;
}

/**
 * HazardIdentifyInfoDto 危险源辨识信息
 */
export interface HazardIdentifyInfoDto {
  /**
   * 附件，上传成功的附件信息集合，json格式
   */
  attachment?: string;
  /**
   * 危险源类别代码
   */
  categorycode?: string;
  /**
   * 危险源类别名称
   */
  categoryname?: string;
  /**
   * 确认结果描述，文字描述信息
   */
  confirmdescription?: string;
  /**
   * 确认的辨识结果，不存在，第1类危险源；第2类危险源；使用EhsHazardIndentificationResultTypeEnum
   */
  confirmresult?: number;
  /**
   * 确认时间
   */
  confirmtm?: string;
  /**
   * 确认人ID
   */
  confirmuserid?: number;
  /**
   * 确认人名称
   */
  confirmusername?: string;
  /**
   * 事故诱因
   */
  couse?: string;
  /**
   * 危险源描述，文字描述
   */
  description?: string;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 是否已确认
   */
  hasconfirm: boolean;
  /**
   * 危险源ID，确认为存在的危险源ID
   */
  hazardid?: string;
  /**
   * 危险源辨识结果，不存在，第1类危险源；第2类危险源；使用EhsHazardIndentificationResultTypeEnum
   */
  identificationresult: number;
  /**
   * 辨识ID
   */
  identifyid: string;
  /**
   * 记录时间
   */
  intm: string;
  /**
   * 记录人ID，写入信息的用户ID
   */
  inuserid: number;
  /**
   * 记录人名称，写入信息的用户名称
   */
  inusername: string;
  /**
   * 是否为重大危险源
   */
  ismajor?: boolean;
  /**
   * 危险源清单代码
   */
  itemcode?: string;
  /**
   * 危险源清单名称
   */
  itemname?: string;
  /**
   * 可能导致的伤害
   */
  maycouseharm?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 具体部位，文字描述
   */
  position?: string;
  /**
   * 危险源项目代码
   */
  projectcode?: string;
  /**
   * 危险源项目名称
   */
  projectname?: string;
  /**
   * 来源任务项ID
   */
  taskitemid?: string;
  /**
   * 来源任务项名称
   */
  taskitemname?: string;
  /**
   * 标题，危险源标题或隐患标题
   */
  title: string;
  /**
   * 辨识时间
   */
  tm: string;
  /**
   * 单元ID，具体单元
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  /**
   * 不安全因素，不安全因素的分类组合值
   */
  unsafefactor?: number;
  /**
   * 辨识人员
   */
  username?: string;
  [property: string]: any;
}

/**
 * HazardIdentifyQueryResultItemDto 危险源辨识查询结果项DTO
 */
export interface HazardIdentifyQueryResultItemDto {
  /**
   * 子级单元危险源辨识查询结果项
   */
  childs: ChildElement[];
  /**
   * 危险源辨识信息集合
   */
  identifys: HazardIdentifyInfoDto[];
  /**
   * 单元简要信息
   */
  unit: EhsUnitSimpleDto;
  [property: string]: any;
}

/**
 * HazardIdentifyQueryResultItemDto
 */
export interface ChildElement {
  /**
   * 子级单元危险源辨识查询结果项
   */
  childs: ChildElement[];
  /**
   * 危险源辨识信息集合
   */
  identifys: HazardIdentifyInfoDto[];
  /**
   * 单元简要信息
   */
  unit: EhsUnitSimpleDto;
  [property: string]: any;
}


/**
 * HazardIdentifyAddDto 新增危险源辨识记录
 */
export interface HazardIdentifyAddDto {
  /**
   * 附件，上传成功的附件信息集合，json格式
   */
  attachment?: string;
  /**
   * 危险源类别代码
   */
  categorycode?: string;
  /**
   * 危险源类别名称
   */
  categoryname?: string;
  /**
   * 事故诱因
   */
  couse: string;
  /**
   * 危险源描述，文字描述
   */
  description: string;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 危险源辨识结果，不存在，第1类危险源；第2类危险源；使用EhsHazardIndentificationResultTypeEnum
   */
  identificationresult: number;
  /**
   * 是否为重大危险源
   */
  ismajor: boolean;
  /**
   * 危险源清单代码
   */
  itemcode?: string;
  /**
   * 危险源清单名称
   */
  itemname?: string;
  /**
   * 可能导致的伤害
   */
  maycouseharm: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 具体部位，文字描述
   */
  position: string;
  /**
   * 危险源项目代码
   */
  projectcode?: string;
  /**
   * 危险源项目名称
   */
  projectname?: string;
  /**
   * 来源任务ID
   */
  taskid?: string;
  /**
   * 来源任务标题
   */
  taskname?: string;
  /**
   * 标题
   */
  title: string;
  /**
   * 辨识时间
   */
  tm: string;
  /**
   * 单元ID，具体单元
   */
  unitid: string;
  /**
   * 单元名称
   */
  unitname: string;
  /**
   * 不安全因素，不安全因素的分类组合值
   */
  unsafefactor: number;
  /**
   * 辨识人员，界面中填写输入，进行辨识或发现隐患的人员名单
   */
  username: string;
  [property: string]: any;
}


/**
 * HazardOverAllIdentificationDto 使用指定导则库开展全面辨识
 */
export interface HazardOverAllIdentificationDto {
  /**
   * 辨识开始时间
   */
  begintm: string;
  /**
   * 辨识截止时间
   */
  endtm: string;
  /**
   * 使用的导则库编码
   */
  hdbcd: string;
  /**
   * 传入的危险源清单ID集合
   */
  hicds: string[];
  /**
   * 辨识周期文字
   */
  tmstr: string;
  /**
   * 时间范围类型，按月、按季度
   */
  tmtype: number;
  /**
   * 单元编码
   */
  unitcode: string;
  [property: string]: any;
}


/**
 * HazardIdentifyConfirmDto 危险源辨识记录确认
 */
export interface HazardIdentifyConfirmDto {
  /**
   * 确认结果描述，文字描述信息
   */
  confirmdescription?: string;
  /**
   * 确认的辨识结果，不存在，第1类危险源；第2类危险源；使用EhsHazardIndentificationResultTypeEnum
   */
  confirmresult: number;
  /**
   * 危险源ID，界面中选择已有的危险源，如果未选择后台先新增一个危险源再关联到该辨识记录中
   */
  hazardid?: string;
  /**
   * 辨识记录ID
   */
  identifyid: string;
  [property: string]: any;
}


/**
 * HazardIdentifyConfirmQueryConditionDto 危险源辨识记录确认查询条件DTO
 */
export interface HazardIdentifyConfirmQueryConditionDto {
  /**
   * 辨识起始时间
   */
  begintm: string;
  /**
   * 确认状态，全部、未确认、已确认
   */
  confirmstatus: number;
  /**
   * 辨识截止时间
   */
  endtm: string;
  /**
   * 危险源类别，为0表示false(第一类危险源)，为1表示true(第二类危险源)，为空或其他时表示不管该标记状态;
   */
  ishidden: string;
  /**
   * 危险源级别，为0表示false(一般)，为1表示true(重大)，为空或其他时表示不管该标记状态
   */
  ismajor: string;
  /**
   * 关键字，可以是危险源类别、项目、危险源名称
   */
  keywords?: string;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 单元ID
   */
  unitid?: string;
  [property: string]: any;
}


/**
 * EhsHazardFullInfoDto 危险源完整信息
 */
export interface EhsHazardFullInfoDto {
  /**
   * 隐患治理集合
   */
  corrects: string[];
  /**
   * 评价信息集合
   */
  evaluations: HazardRiskEvaluationDto[];
  /**
   * 辨识信息集合
   */
  identifications: HazardIdentifyInfoDto[];
  /**
   * 危险源信息
   */
  info: EhsHazardInfoDto;
  [property: string]: any;
}

/**
 * HazardRiskEvaluationDto
 */
export interface HazardRiskEvaluationDto {
  /**
   * 附件，上传时的附件信息集合，json格式
   */
  attachment?: string;
  /**
   * 评价人/群体，辨识的人员/群体名称
   */
  byuser?: string;
  /**
   * 评价内容，填写风险评价内容，或者用于记录使用的评价方法及对应的取值json
   */
  content?: string;
  /**
   * 评价记录ID
   */
  evlid: string;
  /**
   * 危险源ID
   */
  hazardid: string;
  /**
   * 记录时间
   */
  intm?: string;
  /**
   * 记录人，写入信息的用户名称
   */
  inuser?: string;
  /**
   * 评价方法，直接法、LS法、LEC法
   */
  method?: number;
  /**
   * 风险等级，EhsRiskLevelEnum
   */
  risklevel?: number;
  /**
   * 评价时间
   */
  tm?: string;
  [property: string]: any;
}


/**
 * 危险源信息
 *
 * EhsHazardInfoDto
 */
export interface EhsHazardInfoDto {
  /**
   * 附件，json格式，可以包含多个附件
   */
  attachment?: string;
  /**
   * 所属危险源类别代码
   */
  categorycode?: string;
  /**
   * 危险源类别名称
   */
  categoryname?: string;
  /**
   * 防控措施，管控措施
   */
  conmeasures?: string;
  /**
   * 事故诱因
   */
  couse?: string;
  /**
   * 危险源描述
   */
  description?: string;
  /**
   * 风险评价时间
   */
  evltm?: string;
  /**
   * 设备设施ID，危险源可能不针对设备，可能针对环境、管理或作业活动
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 是否进行风险评价，在重新辨识后是否进行风险评价
   */
  hasevl?: boolean;
  /**
   * 危险源ID
   */
  hazardid: string;
  /**
   * 危险源名称
   */
  hazardname: string;
  /**
   * 危险源辨识结果；不适用，不存在，第1类危险源，第2类危险源，使用EhsHazardIndentificationResultTypeEnum
   */
  identificationresult: number;
  /**
   * 是否为重大危险源
   */
  ismajor?: boolean;
  /**
   * 是否上报
   */
  isreport: boolean;
  /**
   * 所属危险源清单代码
   */
  itemcode?: string;
  /**
   * 危险源清单名称
   */
  itemname?: string;
  /**
   * 可能导致的伤害
   */
  maycouseharm?: string;
  /**
   * 监测监控措施
   */
  monitoringmeasures?: string;
  /**
   * 所属组织代码
   */
  orgcode: string;
  /**
   * 具体部位
   */
  position?: string;
  /**
   * 所属危险源项目代码
   */
  projectcode?: string;
  /**
   * 危险源项目名称
   */
  projectname?: string;
  /**
   * 最近辨识时间
   */
  recentidentificationtm?: string;
  /**
   * 责任部门ID
   */
  resdeptid?: string;
  /**
   * 责任部门/机构名称
   */
  resdeptname?: string;
  /**
   * 责任人ID
   */
  resuserid?: string;
  /**
   * 责任人名称
   */
  resusername?: string;
  /**
   * 责任人电话
   */
  resuserphone?: string;
  /**
   * 风险等级；4个风险等级，使用EhsRiskLevelEnum
   */
  risklevel?: number;
  /**
   * 标题，隐患/问题的标题
   */
  title: string;
  /**
   * 所属单元代码
   */
  unitid: string;
  /**
   * 所属单元名称
   */
  unitname?: string;
  /**
   * 不安全因素，防控措施失效的原因：人的不安全行为、物的不安全状态、管理缺陷；不安全因素的分类组合值
   */
  unsafefactor?: number;
  [property: string]: any;
}



/**
 * HazardCategoryTreeDto 危险源类别树型结构DTO
 */
export interface HazardCategoryTreeDto {
  children: ChildrenObject;
  /**
   * 危险源类别代码、项目代码或者危险源ID
   */
  code: string;
  /**
   * 层级：1-类别，2-项目，3-危险源
   */
  level: string;
  /**
   * 危险源类别名称、项目名称或者危险源名称
   */
  name: string;
  /**
   * 父代码
   */
  parentcode: string;
  [property: string]: any;
}

/**
 * HazardCategoryTreeDto
 */
export interface ChildrenObject {
  children: ChildrenObject;
  /**
   * 危险源类别代码、项目代码或者危险源ID
   */
  code: string;
  /**
   * 层级：1-类别，2-项目，3-危险源
   */
  level: string;
  /**
   * 危险源类别名称、项目名称或者危险源名称
   */
  name: string;
  /**
   * 父代码
   */
  parentcode: string;
  [property: string]: any;
}



/**
 * HazardAccountResultDto  危险源台账结果DTO--新的DTO
 */
export interface HazardAccountResultDto {
  /**
   * 危险源类别Tree信息列表
   */
  categorys: HazardCategoryTreeDto[];
  /**
   * 危险源信息列表
   */
  hazards: EhsHazardInfoDto[];
  /**
   * 单元简要Tree信息列表
   */
  units: EhsUnitTreeSimpleDto[];
  [property: string]: any;
}

/**
 * EhsHazardUnitQueryConditionDto 指定单元危险源查询条件DTO
 */
export interface EhsHazardUnitQueryConditionDto {
  /**
   * 所属危险源类别代码
   */
  categorycode?: string;
  /**
   * 所属危险源清单代码
   */
  itemcode?: string;
  /**
   * 所属危险源项目代码
   */
  projectcode?: string;
  /**
   * 单元ID
   */
  unitid: string;
  [property: string]: any;
}


/**
 * HazardRiskEvaluationDto 风险等级评价信息DTO
 */
export interface HazardRiskEvaluationDto {
  /**
   * 附件，上传时的附件信息集合，json格式
   */
  attachment?: string;
  /**
   * 评价人/群体，辨识的人员/群体名称
   */
  byuser?: string;
  /**
   * 评价内容，填写风险评价内容，或者用于记录使用的评价方法及对应的取值json
   */
  content?: string;
  /**
   * 评价记录ID
   */
  evlid: string;
  /**
   * 危险源ID
   */
  hazardid: string;
  /**
   * 记录时间
   */
  intm?: string;
  /**
   * 记录人，写入信息的用户名称
   */
  inuser?: string;
  /**
   * 评价方法，直接法、LS法、LEC法
   */
  method?: number;
  /**
   * 风险等级，EhsRiskLevelEnum
   */
  risklevel?: number;
  /**
   * 评价时间
   */
  tm?: string;
  [property: string]: any;
}

/**
 * HazardRiskEvaluationMethodDto 危险源风险评价方法DTO
 */
export interface HazardRiskEvaluationMethodDto {
  /**
   * 低风险值，计算结果小于该值为第风险
   */
  level1: number;
  /**
   * 一般风险值
   */
  level2: number;
  /**
   * 较大风险值
   */
  level3: number;
  /**
   * 重大风险值，计算结果大于等于该值为重大风险
   */
  level4: number;
  /**
   * 风险评价方法，1-直接法、2-LS法、3-LEC法
   */
  method: number;
  /**
   * 风险评价方法名称
   */
  name: string;
  options: HazardRiskEvaluationOptionsItemDto[];
  [property: string]: any;
}

/**
 * HazardRiskEvaluationOptionsItemDto
 */
export interface HazardRiskEvaluationOptionsItemDto {
  /**
   * 参数编码
   */
  code: string;
  /**
   * 参数名称
   */
  name: string;
  /**
   * 供选择列表
   */
  options: KeyValuePairDto[];
  /**
   * 提示信息
   */
  prompt?: string;
  [property: string]: any;
}

/**
 * KeyValuePairDto
 */
export interface KeyValuePairDto {
  /**
   * 属性，可以为空；用于将需要的信息传递值前端或后台
   */
  attribute?: string;
  /**
   * 键
   */
  key: string;
  /**
   * 值
   */
  value?: string;
  [property: string]: any;
}


/**
 * HazardIdentifyConfirmResultDto 危险源辨识记录确认结果DTO
 */
export interface HazardIdentifyConfirmResultDto {
  /**
   * 辨识记录ID
   */
  identifyid: string;
  /**
   * 提示信息，操作成功的提示信息，可以对是否更新危险源信息进行提示
   */
  msg?: string;
  /**
   * 是否确认操作，确认操作是否成功，操作成功不代表需要对危险源信息进行新增或修改操作
   */
  success: boolean;
  [property: string]: any;
}

/**
 * HazardIdentifyOperateResultDto 危险源辨识记录操作结果DTO
 */
export interface HazardIdentifyOperateResultDto {
  /**
   * 结果提示信息
   */
  msg?: string;
  /**
   * 操作是否成功
   */
  success: boolean;
  [property: string]: any;
}
