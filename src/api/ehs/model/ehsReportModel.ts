/**
 * EhsReportQueryConditionDto EHS报告查询/统计条件DTO
 */
export interface EhsReportQueryConditionDto {
  /**
   * 起始时间
   */
  begintm: string;
  /**
   * 任务大类，可以为空或指定1个或多个，在查询隐患治理时不使用传入值；
   */
  category: number;
  /**
   * 责任部门ID，可以为空，为0表示无效；可以指定查询1个；
   */
  deptid: number;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 标题文字，模糊匹配，隐患治理的在标题中匹配，检查工作的在任务项名称中匹配；
   */
  title?: string;
  /**
   * 指定单元ID，可以为空，或者指定查询1个单元
   */
  unitid?: string;
  /**
   * 负责人ID，可以为空，为0表示无效；可以指定查询1个；
   */
  userid: number;
  [property: string]: any;
}


/**
 * EhsReportFileUpdateDto EHS报告文件更新DTO
 */
export interface EhsReportFileUpdateDto {
  /**
   * 上传的文件ID
   */
  fileid: number;
  /**
   * 隐患治理或任务项ID
   */
  id: string;
  [property: string]: any;
}
