/**
 * TaskInfoDto 任务信息
 */
export interface TaskInfoDto {
  /**
   * 检查表ID集合
   */
  checktableids: string[];
  /**
   * 任务分类，任务分类名称
   */
  catname: string;
  /**
   * 实际执行次数，所有目标对象实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 是否已派单
   */
  hasdispatch: boolean;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 创建时间
   */
  intm: string;
  /**
   * 创建人ID
   */
  inuserid: number;
  /**
   * 创建人名称
   */
  inusername: string;
  /**
   * 是否需填写检查表
   */
  needchecktable: boolean;
  /**
   * 执行结果是否需确认
   */
  needconfirm: boolean;
  /**
   * 是否必须现场签到
   */
  needsignin: boolean;
  /**
   * 任务目标对象是否非空
   */
  notnull: boolean;
  /**
   * 任务目标对象类型
   */
  objtype: number;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 每个周期内计划执行次数
   */
  planfrequencycount: number;
  /**
   * 计划执行周期类型
   */
  planfrequencytype: number;
  /**
   * 计划执行总次数，所有目标对象的计划执行次数之和
   */
  plantotalcount: number;
  /**
   * 任务附件
   */
  taskattachment: string;
  /**
   * 任务内容
   */
  taskcontent: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 任务标题
   */
  tasktitle: string;
  /**
   * 任务大类
   */
  tasktype: number;
  [property: string]: any;
}

/**
 * TaskAddOrEditDto 任务新增或修改DTO
 */
export interface TaskAddOrEditDto {
  /**
   * 任务分类名称，安全检查、设备保养、隐患治理、作业活动
   */
  catname: string;
  /**
   * 检查表ID集合
   */
  checktableids?: string[];
  /**
   * 任务分组对象ID
   */
  groupobjids?: string[];
  /**
   * 任务分组指定的对象类型，单元、危险源、设备设施
   */
  groupobjtype: number;
  /**
   * 是否需填写检查表
   */
  needchecktable: boolean;
  /**
   * 执行结果是否需确认
   */
  needconfirm: boolean;
  /**
   * 是否必须现场签到
   */
  needsignin: boolean;
  /**
   * 任务目标对象是否非空
   */
  notnull: boolean;
  /**
   * 任务目标对象类型，枚举EhsTaskRelateObjectTypeEnum
   */
  objtype: number;
  /**
   * 组织机构代码
   */
  orgcode: string;
  /**
   * 计划开始时间
   */
  planbegintm: string;
  /**
   * 计划截止时间
   */
  planendtm: string;
  /**
   * 每个周期内计划执行次数
   */
  planfrequencycount: number;
  /**
   * 计划执行周期类型，枚举TimeRangeLengthEnum
   */
  planfrequencytype: number;
  /**
   * 计划执行总次数，所有目标对象的计划执行次数之和
   */
  plantotalcount: number;
  /**
   * 任务附件
   */
  taskattachment: string;
  /**
   * 任务内容
   */
  taskcontent: string;
  /**
   * 任务ID，新增时保持为空，修改时必须传入
   */
  taskid?: string;
  /**
   * 任务标题
   */
  tasktitle: string;
  /**
   * 任务分类
   */
  tasktype: number;
  [property: string]: any;
}

/**
 * TaskSetGroupObjectsDto 设置任务分组对象
 */
export interface TaskSetGroupObjectsDto {
  /**
   * 任务分组对象ID
   */
  groupobjids?: string[];
  /**
   * 任务分组指定的对象类型，单元、危险源、设备设施
   */
  groupobjtype: number;
  /**
   * 任务ID
   */
  taskid: string;
  [property: string]: any;
}

/**
 * TaskGroupObjectsDto 任务分组关联的对象信息
 */
export interface TaskGroupObjectsDto {
  /**
   * 对象ID集合
   */
  objectids: string[];
  /**
   * 任务关联的对象类型，单元、设备设施、危险源、第一类危险源、第二类危险源
   */
  objecttype: number;
  [property: string]: any;
}


/**
 * TaskScheduleDto 任务执行情况一览表DTO
 */
export interface TaskScheduleDto {
  groups: TaskScheduleGroupDto[];
  /**
   * 执行时间周期集合，时间周期文字信息
   */
  tms: string[];
  [property: string]: any;
}

/**
 * TaskScheduleGroupDto 任务执行情况一览表分组DTO
 */
export interface TaskScheduleGroupDto {
  /**
   * 分组信息
   */
  group: TaskGroupInfoDto;
  /**
   * 分组执行周期完成情况集合，与执行时间周期集合的顺序保持一致
   */
  items: TaskScheduleGroupItemDto[];
  /**
   * 分组执行情况汇总，分组在所有执行周期中的汇总
   */
  summary: TaskScheduleGroupSummaryDto;
  [property: string]: any;
}

/**
 * 分组信息
 *
 * TaskGroupInfoDto
 */
export interface TaskGroupInfoDto {
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 设备设施ID
   */
  facilityid?: string;
  /**
   * 设备设施名称
   */
  facilityname?: string;
  /**
   * 任务分组ID
   */
  groupid: string;
  /**
   * 任务分组名称
   */
  groupname: string;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 危险源ID
   */
  hazardid?: string;
  /**
   * 危险源名称
   */
  hazardname?: string;
  /**
   * 最新执行完成时间
   */
  latestexecendtm?: string;
  /**
   * 最新执行周期，文字
   */
  latestexectm?: string;
  /**
   * 责任部门ID
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 参与组员1ID
   */
  resuserid1?: number;
  /**
   * 参与组员2ID
   */
  resuserid2?: number;
  /**
   * 参与组员1名称
   */
  resusername1?: string;
  /**
   * 参与组员2名称
   */
  resusername2?: string;
  /**
   * 任务ID
   */
  taskid: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}

/**
 * TaskScheduleGroupItemDto 任务执行情况一览表分组任务项DTO
 */
export interface TaskScheduleGroupItemDto {
  /**
   * 实际执行次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 是否终止/取消
   */
  hasend: boolean;
  /**
   * 是否执行完成
   */
  hasfinish: boolean;
  /**
   * 是否超时完成，在执行完成时如完成时间＞计划截止时间判定为超时完成；
   */
  istimeout: boolean;
  [property: string]: any;
}

/**
 * 分组执行情况汇总，分组在所有执行周期中的汇总
 *
 * TaskScheduleGroupSummaryDto 任务执行情况一览表分组执行汇总统计DTO
 */
export interface TaskScheduleGroupSummaryDto {
  /**
   * 实际执行总次数
   */
  execcount: number;
  /**
   * 辨识危险源次数
   */
  execidentifycount: number;
  /**
   * 上报隐患次数
   */
  exectroublecount: number;
  /**
   * 计划执行总次数
   */
  plancount: number;
  /**
   * 超时完成总次数
   */
  timeoutcount: number;
  [property: string]: any;
}
/**
 * TaskDispatchDto
 */
export interface TaskDispatchDto {
  /**
   * 起始时间，派单后任务计划执行的起始时间
   */
  begintm: string;
  /**
   * 截止时间，派单后任务计划执行的截止时间
   */
  endtm: string;
  /**
   * 任务ID
   */
  id: string;
  [property: string]: any;
}


/**
 * TaskConsolePreviewResultDto 任务控制台预览结果DTO
 */
export interface TaskConsolePreviewResultDto {
  /**
   * 检查表集合
   */
  checktables: string[];
  /**
   * 分组集合
   */
  groups?: string[];
  /**
   * 任务分组对象类型名称
   */
  grouptypename: string;
  /**
   * 任务分组对象类型
   */
  grouptypevalue: number;
  /**
   * 是否正常，输入的数据检查和校验是否通过
   */
  isok: boolean;
  /**
   * 提示信息，提示不通过的原因文字说明
   */
  msg?: string;
  /**
   * 单元对应的检查表
   */
  unitchecktables?: string[];
  /**
   * 单元名称集合
   */
  units?: string[];
  [property: string]: any;
}
