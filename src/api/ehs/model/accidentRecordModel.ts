/**
 * AccidentRecordQueryConditionDto 事故记录查询条件
 */
export interface AccidentRecordQueryConditionDto {
  /**
   * 发生开始时间
   */
  begintm: string;
  /**
   * 事故原因类型，使用枚举类CauseTypeEnum
   */
  causetype?: number;
  /**
   * 发生结束时间
   */
  endtm: string;
  /**
   * 关键字
   */
  keywords?: string;
  /**
   * 事故等级，使用枚举CauseLevelEnum
   */
  lvl?: number;
  /**
   * 所属单位代码
   */
  orgcode: string;
  /**
   * 单元ID
   */
  unitid?: string;
  [property: string]: any;
}


/**
 * AccidentRecordDto 事故记录DTO
 */
export interface AccidentRecordDto {
  /**
   * 附件
   */
  attechment?: string;
  /**
   * 事故原因类型
   */
  causetype: number;
  /**
   * 事故经过
   */
  content?: string;
  /**
   * 死亡人数
   */
  deathnum: number;
  /**
   * 事故直接原因
   */
  directcause: string;
  /**
   * 直接经济损失，单位：万元
   */
  ecoloss: number;
  /**
   * 是否上报
   */
  hasreport: boolean;
  /**
   * id
   */
  id: string;
  /**
   * 事故波及范围
   */
  incidence?: string;
  /**
   * 事故间接原因
   */
  indirectcause: string;
  /**
   * 重伤人数
   */
  injurednum: number;
  /**
   * 是否为责任事故
   */
  isliability: boolean;
  /**
   * 事故教训
   */
  lesson?: string;
  /**
   * 事故等级
   */
  lvl: number;
  /**
   * 事故整改措施
   */
  measure?: string;
  /**
   * 所属单位代码
   */
  orgcode: string;
  /**
   * 上报方式
   */
  reporttype?: string;
  /**
   * 事故责任
   */
  responsibility?: string;
  /**
   * 事故处理建议
   */
  suggestion?: string;
  /**
   * 事故标题
   */
  title: string;
  /**
   * 发生时间
   */
  tm: string;
  /**
   * 单元ID
   */
  unitid?: string;
  /**
   * 单元名称
   */
  unitname?: string;
  [property: string]: any;
}
