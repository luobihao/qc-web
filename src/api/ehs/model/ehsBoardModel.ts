/**
 * EhsBoardOrgLatestStatusDTO 安全生产看板组织最新安全状态DTO
 */
export interface EhsBoardOrgLatestStatusDTO {
  /**
   * 分析评估说明，风险评估值的说明信息
   */
  description?: string;
  /**
   * 危险源信息，存在的第一类危险源的信息
   */
  hazard: EhsBoardHazardStsInfoDto;
  /** 
   * 风险评估值，0-100表示风险值大小
   */
  risk: number;
  /**
   * 隐患信息，存在的第二类危险源的信息
   */
  trouble: EhsBoardHazardStsInfoDto;
  [property: string]: any;
}

/**
 * 危险源信息，存在的第一类危险源的信息
 *
 * EhsBoardHazardStsInfoDto
 *
 * 隐患信息，存在的第二类危险源的信息
 */
export interface EhsBoardHazardStsInfoDto {
  /**
   * 蓝色等级数量
   */
  bluecount: number;
  /**
   * 总数，当前存在的危险源总数
   */
  count: number;
  /**
   * 重大危险源总数
   */
  majorcount: number;
  /**
   * 橙色等级数量
   */
  orangecount: number;
  /**
   * 红色等级数量
   */
  redcount: number;
  /**
   * 黄色等级数量
   */
  yellowcount: number;
  [property: string]: any;
}


/**
 * EhsBoardQueryConditionDto 安全生产看板查询条件DTO
 */
export interface EhsBoardQueryConditionDto {
  /**
   * 开始时间，查询任务的计划开始时间
   */
  begintm: string;
  /**
   * 部门ID，小于1表示无效
   */
  deptid: number;
  /**
   * 截止时间，查询任务的计划截止时间
   */
  endtm: string;
  /**
   * 组织代码
   */
  orgcode: string;
  [property: string]: any;
}

/**
 * EhsBoardOrgQueryResultDto 安全生产看板组织查询结果DTO
 */
export interface EhsBoardOrgQueryResultDto {
  accident: EhsBoardAccidentStsInfoDto;
  /**
   * 部门ID
   */
  deptid: number;
  edu: EhsBoardEduStsInfoDto;
  identify: EhsBoardHazardIdentifyStsInfoDto;
  invest: EhsBoardInvestStsInfoDto;
  /**
   * 名称，组织名称或部门名称
   */
  name: string;
  riskevl: EhsBoardRiskEvaluationStsInfoDto;
  /**
   * 任务信息，查询时间段内的所有类型任务统计信息
   */
  task: EhsBoardTaskStsInfoDto;
  /**
   * 自查类任务信息
   */
  taskcheck: EhsBoardTaskStsInfoDto;
  /**
   * 设备设施类任务信息
   */
  taskfacility: EhsBoardTaskStsInfoDto;
  /**
   * 督查类任务信息
   */
  taskinspect: EhsBoardTaskStsInfoDto;
  /**
   * 作业活动类任务信息
   */
  taskjob: EhsBoardTaskStsInfoDto;
  /**
   * 隐患治理类任务信息
   */
  tasktrouble: EhsBoardTaskStsInfoDto;
  /**
   * 查询时间段内的隐患治理统计信息
   */
  troublecorrect: EhsBoardTroubleCorrectStsInfoDto;
  [property: string]: any;
}

/**
 * EhsBoardAccidentStsInfoDto
 */
export interface EhsBoardAccidentStsInfoDto {
  /**
   * 次数
   */
  count: number;
  /**
   * 死亡人数
   */
  deathnum: number;
  /**
   * 直接经济损失
   */
  ecoloss: number;
  /**
   * 重伤人数
   */
  injurednum: number;
  /**
   * 重大事故次数，重大及以上
   */
  majorcount: number;
  [property: string]: any;
}

/**
 * EhsBoardEduStsInfoDto
 */
export interface EhsBoardEduStsInfoDto {
  /**
   * 总次数
   */
  count: number;
  /**
   * 发放资料数量
   */
  doccount: number;
  /**
   * 时长
   */
  hour: number;
  /**
   * 人次，总人数
   */
  personcount: number;
  [property: string]: any;
}

/**
 * EhsBoardHazardIdentifyStsInfoDto
 */
export interface EhsBoardHazardIdentifyStsInfoDto {
  /**
   * 辨识危险源次数
   */
  hazardcount: number;
  /**
   * 上报隐患次数
   */
  troublecount: number;
  [property: string]: any;
}

/**
 * EhsBoardInvestStsInfoDto
 */
export interface EhsBoardInvestStsInfoDto {
  /**
   * 总金额，单位：元
   */
  invest: number;
  [property: string]: any;
}

/**
 * EhsBoardRiskEvaluationStsInfoDto
 */
export interface EhsBoardRiskEvaluationStsInfoDto {
  /**
   * 蓝色等级数量
   */
  bluecount: number;
  /**
   * 总次数
   */
  count: number;
  /**
   * 橙色等级数量
   */
  orangecount: number;
  /**
   * 红色等级数量
   */
  redcount: number;
  /**
   * 黄色等级数量
   */
  yellowcount: number;
  [property: string]: any;
}

/**
 * 任务信息，查询时间段内的所有类型任务统计信息
 *
 * EhsBoardTaskStsInfoDto
 *
 * 自查类任务信息
 *
 * 设备设施类任务信息
 *
 * 督查类任务信息
 *
 * 作业活动类任务信息
 *
 * 隐患治理类任务信息
 */
export interface EhsBoardTaskStsInfoDto {
  /**
   * 总数，查询时间段内组织/部门的任务总数
   */
  count: number;
  /**
   * 已完成数量
   */
  finishcount: number;
  /**
   * 超时完成数量
   */
  timeoutfinishcount: number;
  [property: string]: any;
}

/**
 * 查询时间段内的隐患治理统计信息
 *
 * EhsBoardTroubleCorrectStsInfoDto
 */
export interface EhsBoardTroubleCorrectStsInfoDto {
  /**
   * 验收通过数量
   */
  checkcount: number;
  /**
   * 已整改数量
   */
  correctcount: number;
  /**
   * 总数，当前存在的危险源总数
   */
  count: number;
  /**
   * 已派单数量
   */
  dispatchcount: number;
  /**
   * 重大危险源总数
   */
  majorcount: number;
  [property: string]: any;
}


/**
 * EhsBoardDeptQueryResultDto 安全生产看板按部门统计结果DTO
 *
 * EhsBoardDeptResultItemDto
 */
export interface EhsBoardDeptQueryResultDto {
  /**
   * 部门ID
   */
  deptid: number;
  /**
   * 危险源信息，存在的第一类危险源的信息
   */
  hazard: EhsBoardHazardStsInfoDto;
  /**
   * 名称，组织名称或部门名称
   */
  name: string;
  /**
   * 任务信息，查询时间段内的所有类型任务统计信息
   */
  task: EhsBoardTaskStsInfoDto;
  /**
   * 自查类任务信息
   */
  taskcheck: EhsBoardTaskStsInfoDto;
  /**
   * 设备设施类任务信息
   */
  taskfacility: EhsBoardTaskStsInfoDto;
  /**
   * 督查类任务信息
   */
  taskinspect: EhsBoardTaskStsInfoDto;
  /**
   * 作业活动类任务信息
   */
  taskjob: EhsBoardTaskStsInfoDto;
  /**
   * 隐患治理类任务信息
   */
  tasktrouble: EhsBoardTaskStsInfoDto;
  /**
   * 隐患信息，存在的第二类危险源的信息
   */
  trouble: EhsBoardHazardStsInfoDto;
  /**
   * 查询时间段内的隐患治理统计信息
   */
  troublecorrect: EhsBoardTroubleCorrectStsInfoDto;
  [property: string]: any;
}

/**
 * 危险源信息，存在的第一类危险源的信息
 *
 * EhsBoardHazardStsInfoDto
 *
 * 隐患信息，存在的第二类危险源的信息
 */
export interface EhsBoardHazardStsInfoDto {
  /**
   * 蓝色等级数量
   */
  bluecount: number;
  /**
   * 总数，当前存在的危险源总数
   */
  count: number;
  /**
   * 重大危险源总数
   */
  majorcount: number;
  /**
   * 橙色等级数量
   */
  orangecount: number;
  /**
   * 红色等级数量
   */
  redcount: number;
  /**
   * 黄色等级数量
   */
  yellowcount: number;
  [property: string]: any;
}

/**
 * 任务信息，查询时间段内的所有类型任务统计信息
 *
 * EhsBoardTaskStsInfoDto
 *
 * 自查类任务信息
 *
 * 设备设施类任务信息
 *
 * 督查类任务信息
 *
 * 作业活动类任务信息
 *
 * 隐患治理类任务信息
 */
export interface EhsBoardTaskStsInfoDto {
  /**
   * 总数，查询时间段内组织/部门的任务总数
   */
  count: number;
  /**
   * 已完成数量
   */
  finishcount: number;
  /**
   * 超时完成数量
   */
  timeoutfinishcount: number;
  [property: string]: any;
}

/**
 * 查询时间段内的隐患治理统计信息
 *
 * EhsBoardTroubleCorrectStsInfoDto
 */
export interface EhsBoardTroubleCorrectStsInfoDto {
  /**
   * 验收通过数量
   */
  checkcount: number;
  /**
   * 已整改数量
   */
  correctcount: number;
  /**
   * 总数，当前存在的危险源总数
   */
  count: number;
  /**
   * 已派单数量
   */
  dispatchcount: number;
  /**
   * 重大危险源总数
   */
  majorcount: number;
  [property: string]: any;
}
