/**
 * InvestRecordQueryConditionDto 资金投入记录查询条件DTO
 */
export interface InvestRecordQueryConditionDto {
  /**
   * 实际投入起始时间
   */
  begintm: string;
  /**
   * 实际投入截止时间
   */
  endtm: string;
  /**
   * 资金投入类型，任务分类名称、培训、演练、安全工器具等
   */
  investtype?: string;
  /**
   * 关键字，匹配投入项目名称、说明等
   */
  keywords?: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 资金投入记录ID，关联任务ID、培训ID、演练ID、事故处理ID等
   */
  recordid?: string;
  [property: string]: any;
}


/**
 * InvestRecordDto 安全生产资金投入记录DTO
 */
export interface InvestRecordDto {
  /**
   * 附件，json格式，可以有多个附件
   */
  attachment?: string;
  /**
   * 实际投入起始时间，资金使用的时间
   */
  begintm: string;
  /**
   * 资金投入说明
   */
  description?: string;
  /**
   * 实际投入截止时间，资金使用的时间
   */
  endtm: string;
  /**
   * ID
   */
  ID: string;
  /**
   * 记录填报时间
   */
  intm: string;
  /**
   * 资金投入类型，任务分类名称、培训、演练、安全工器具等
   */
  inversttype: string;
  /**
   * 资金投入数值，单位：元
   */
  invest: number;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 资金投入记录ID，关联任务ID、培训ID、演练ID、事故处理ID等
   */
  recordid?: string;
  /**
   * 资金投入项目名称
   */
  title: string;
  /**
   * 记录填报人ID
   */
  userid: number;
  /**
   * 记录填报人名称
   */
  username: string;
  [property: string]: any;
}
