/**
 * EhsOrgDto
 */
export interface EhsOrgDto {
  /**
   * 行政主管组织代码
   */
  admincode?: string;
  /**
   * 在建项目导则库
   */
  contpl?: string;
  /**
   * 部门ID，对应平台中的部门唯一ID
   */
  deptid: string;
  /**
   * 组织代码
   */
  orgcode: string;
  /**
   * 组织名称
   */
  orgname: string;
  /**
   * 组织类型，生产经营单位、行政主管部门、流域管理机构
   */
  orgtype: string;
  /**
   * 安全生产配置参数，json格式，可以扩展，用于配置标准化等级、使用的评审规程等
   */
  params?: string;
  /**
   * 运行导则库
   */
  runtpl?: string;
  [property: string]: any;
}
