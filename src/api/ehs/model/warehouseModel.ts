/**
 * WareHouseDto
 */
export interface WareHouseDto {
  /**
   * 所属部门ID，仓库的出入库权限控制根据所属部门进行控制
   */
  deptid: number;
  /**
   * 说明文字
   */
  description?: string;
  /**
   * 仓库ID
   */
  id: string;
  /**
   * 仓库位置
   */
  location: string;
  /**
   * 仓库名称
   */
  name: string;
  /**
   * 所属组织代码，不能为空，新增或修改时必须有调用方传入；
   */
  orgcode: string;
  [property: string]: any;
}
