/**
 * TplHazardDbDto 危险源导则库DTO
 */
export interface TplHazardDbDto {
  /**
   * 文件ID，对应导则库文件
   */
  fileid: number;
  /**
   * 导则库代码
   */
  hdbcd: string;
  /**
   * 导则库名称
   */
  hdbnm: string;
  /**
   * 是否为在建阶段
   */
  isconstruction: boolean;
  [property: string]: any;
}
export type TplHazardDbResult = TplHazardDbDto[]
/**
 * TplHazardCategoryDto 危险源类别DTO
 */
export interface TplHazardCategoryDto {
  /**
   * 类别代码
   */
  hcatcd: string;
  /**
   * 类别名称
   */
  hcatnm: string;
  /**
   * 危险源导则库代码
   */
  hdbcd: string;
  /**
   * 排序号
   */
  odr: number;
  /**
   * 标签，标识是场所/单元、设备设施、作业、管理、环境等
   */
  tags: number;
  [property: string]: any;
}

/**
 * TplHazardProjectDto 危险源项目DTO
 */
export interface TplHazardProjectDto {
  /**
   * 所属危险源类别
   */
  hdbcd: string;
  /**
   * 危险源项目代码
   */
  hpjtcd: string;
  /**
   * 危险源项目名称
   */
  hpjtnm: string;
  /**
   * 排序号
   */
  odr: number;
  /**
   * 标签，标识是场所/单元、设备设施、作业、管理、环境等
   */
  tags: number;
  [property: string]: any;
}


/**
 * TplHazardItemDto 危险源清单DTO
 */
export interface TplHazardItemDto {
  /**
   * 事故诱因
   */
  cause?: string;
  /**
   * C最大值
   */
  cmax?: number;
  /**
   * C最小值
   */
  cmin?: number;
  /**
   * 可能导致的后果
   */
  consequence?: string;
  /**
   * 描述说明
   */
  description?: string;
  /**
   * E最大值
   */
  emax?: number;
  /**
   * 应急处置措施
   */
  emergency?: string;
  /**
   * E最小值
   */
  emin?: number;
  /**
   * 危险源清单代码
   */
  hicd: string;
  /**
   * 危险源清单名称
   */
  hinm: string;
  /**
   * 危险源项目代码
   */
  hpjtcd: string;
  /**
   * 是否为重大危险源
   */
  ismajor: boolean;
  /**
   * L最大值
   */
  lmax?: number;
  /**
   * L最小值
   */
  lmin?: number;
  /**
   * 防控措施
   */
  prevent?: string;
  [property: string]: any;
}
/**
 * TplHazardDbQueryConditionDto 危险源导则库查询条件DTO
 */
export interface TplHazardDbQueryConditionDto {
  /**
   * 导则库代码
   */
  hdbcd?: string;
  /**
   * 查询的关键字类型，查询的关键是类别、项目还是危险源清单
   */
  keytype: number;
  /**
   * 关键字，代码和名称模糊匹配
   */
  keywords?: string;
  /**
   * 阶段标记，为空或错误表示不管阶段，为1表示在建阶段，为0表时运行阶段
   */
  period: string;
  /**
   * 标签，与类别、项目中的tags一致
   */
  tags: number;
  [property: string]: any;
}

/**
 * TplHazardDbFullInfoDto 危险源导则库完整信息DTO
 */
export interface TplHazardDbFullInfoDto {
  db: TplHazardDbDto;
  /**
   * 危险源集合，根据前端需要确定返回时树形结构还是list
   */
  items: TplHazardTreeTableItemDto[];
  [property: string]: any;
}


/**
 * TplHazardTreeTableItemDto 危险源清单树表
 */
export interface TplHazardTreeTableItemDto {
  /**
   * 事故诱因
   */
  cause?: string;
  children: TplHazardTreeTableItemDto[];
  /**
   * C最大值
   */
  cmax?: number;
  /**
   * C最小值
   */
  cmin?: number;
  /**
   * 代码，类别、项目、清单
   */
  code: string;
  /**
   * 可能导致的后果
   */
  consequence?: string;
  /**
   * D最大值
   */
  dmax?: number;
  /**
   * D最小值
   */
  dmin?: number;
  /**
   * E最大值
   */
  emax?: number;
  /**
   * 应急处置措施
   */
  emergency?: string;
  /**
   * E最小值
   */
  emin?: number;
  /**
   * 是否为重大危险源
   */
  ismajor: boolean;
  /**
   * L最大值
   */
  lmax?: number;
  /**
   * L最小值
   */
  lmin?: number;
  /**
   * 名称，类别、项目、清单
   */
  name: string;
  /**
   * 排序号
   */
  odr: number;
  /**
   * 防控措施
   */
  prevent?: string;
  /**
   * 标签
   */
  tags: number;
  /**
   * 层级1=类别、2=项目、3=清单
   */
  level: number;
  [property: string]: any;
}
export type TplHazardTreeResult = TplHazardTreeTableItemDto[]
/**
 * TplHazardImportDto 危险源导则库导入DTO
 */
export interface TplHazardImportDto {
  /**
   * 导入的文件ID
   */
  fileid: string;
  /**
   * 危险源导则库代码
   */
  hdbcd: string;
  /**
   * 覆盖模式导入，覆盖还是追加
   */
  iscover: boolean;
  [property: string]: any;
}


/**
 * TplHazardTreeTableItemDto 危险源TreeTable清单项DTO
 */
export interface TplHazardTreeTableItemDto {
  /**
   * 事故诱因
   */
  cause?: string;
  children: ChildElement[];
  /**
   * C最大值
   */
  cmax?: number;
  /**
   * C最小值
   */
  cmin?: number;
  /**
   * 代码，类别、项目、清单
   */
  code: string;
  /**
   * 可能导致的后果
   */
  consequence?: string;
  /**
   * 描述说明
   */
  description?: string;
  /**
   * E最大值
   */
  emax?: number;
  /**
   * 应急处置措施
   */
  emergency?: string;
  /**
   * E最小值
   */
  emin?: number;
  /**
   * 是否为重大危险源
   */
  ismajor: boolean;
  /**
   * 分类层级，1=类别、2=项目、3=清单
   */
  level: number;
  /**
   * L最大值
   */
  lmax?: number;
  /**
   * L最小值
   */
  lmin?: number;
  /**
   * 名称，类别、项目、清单
   */
  name: string;
  /**
   * 排序号
   */
  odr: number;
  /**
   * 防控措施
   */
  prevent?: string;
  /**
   * 标签
   */
  tags: number;
  [property: string]: any;
}

/**
 * TplHazardTreeTableItemDto
 */
export interface ChildElement {
  /**
   * 事故诱因
   */
  cause?: string;
  children: ChildElement[];
  /**
   * C最大值
   */
  cmax?: number;
  /**
   * C最小值
   */
  cmin?: number;
  /**
   * 代码，类别、项目、清单
   */
  code: string;
  /**
   * 可能导致的后果
   */
  consequence?: string;
  /**
   * 描述说明
   */
  description?: string;
  /**
   * E最大值
   */
  emax?: number;
  /**
   * 应急处置措施
   */
  emergency?: string;
  /**
   * E最小值
   */
  emin?: number;
  /**
   * 是否为重大危险源
   */
  ismajor: boolean;
  /**
   * 分类层级，1=类别、2=项目、3=清单
   */
  level: number;
  /**
   * L最大值
   */
  lmax?: number;
  /**
   * L最小值
   */
  lmin?: number;
  /**
   * 名称，类别、项目、清单
   */
  name: string;
  /**
   * 排序号
   */
  odr: number;
  /**
   * 防控措施
   */
  prevent?: string;
  /**
   * 标签
   */
  tags: number;
  [property: string]: any;
}
