/**
 * EduRecordQueryConditionDto 培训记录查询条件
 */
export interface EduRecordQueryConditionDto {
  /**
   * 培训起始时间
   */
  begintm: string;
  /**
   * 培训形式，开展培训的方式、形式；使用枚举类EduModeEnum
   */
  edumode?: number;
  /**
   * 培训类型，使用枚举类EduTypeEnum
   */
  edutype?: number;
  /**
   * 培训结束时间
   */
  endtm: string;
  /**
   * 关键字，匹配培训主题、培训内容、培训地点等
   */
  keywords?: string;
  /**
   * 所属单位代码
   */
  orgcode: string;
  [property: string]: any;
}


/**
 * EduRecordDto 安全培训记录DTO
 */
export interface EduRecordDto {
  /**
   * 附件
   */
  attachment?: string;
  /**
   * 培训开始时间
   */
  begintm: string;
  /**
   * 内容描述
   */
  content?: string;
  /**
   * 发放资料数量，宣传时发放的资料数量
   */
  doccount: number;
  /**
   * 开展形式，开展培训的方式、形式；集中授课、在线培训、混合式培训、案例分析、模拟演练、角色扮演、安全知识竞赛、安全文化活动、外部培训、师带徒、安全例会、安全检查
   */
  edumode: string;
  /**
   * 类型，入职安全培训、定期安全培训、改变工艺或设备安全培训、专项安全培训、应急预案培训、安全管理培训、职业健康培训
   */
  edutype: string;
  /**
   * 培训结束时间
   */
  endtm: string;
  /**
   * 时长，小时
   */
  hour: number;
  /**
   * 培训记录ID
   */
  id: string;
  /**
   * 地点
   */
  location?: string;
  /**
   * 所属单位代码
   */
  orgcode: string;
  /**
   * 参与人员，界面中填写人员名单或概括信息
   */
  participants?: string;
  /**
   * 参加人数，参加培训的人数或宣传对象人数
   */
  personcount: number;
  /**
   * 培训主题
   */
  title: string;
  [property: string]: any;
}


/**
 * EduRecordUsersDto 培训记录人员关联信息
 */
export interface EduRecordUsersDto {
  /**
   * 是否关联
   */
  checked: boolean;
  /**
   * 用户ID
   */
  id: string;
  /**
   * 用户名称
   */
  name: string;
  /**
   * 手机号
   */
  phone?: string;
  [property: string]: any;
}

/**
 * EduRecordSetUsersDto 培训记录人员关联设置
 */
export interface EduRecordSetUsersDto {
  /**
   * 培训记录ID
   */
  eduid: string;
  /**
   * 人员ID集合
   */
  uids?: number[];
  [property: string]: any;
}
