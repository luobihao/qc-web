/**
 * UnitResponsibilitySetDto 设置单元责任机制
 */
export interface UnitResponsibilitySetDto {
  /**
   * 责任部门ID
   */
  resdeptid?: number;
  /**
   * 责任部门名称
   */
  resdeptname?: string;
  /**
   * 责任人ID
   */
  resuserid?: number;
  /**
   * 责任人名称
   */
  resusername?: string;
  /**
   * 责任人手机号
   */
  resuserphone?: string;
  /**
   * 安全员ID
   */
  safeuserid?: number;
  /**
   * 安全员名称
   */
  safeusername?: string;
  /**
   * 安全员手机号
   */
  safeuserphone?: string;
  /**
   * 单元ID集合
   */
  unitids: string[];
  [property: string]: any;
}
