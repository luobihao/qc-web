import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  TaskBenchQueryConditionDto,
  TaskBenchQueryResultDto
} from './model/taskBenchModel'
import { HazardIdentifyInfoDto } from './model/hazardModel'

enum Api {
  QueryTodoTask = '/ehs/taskbench/todo',   // 我的待办-todo
  QueryTasks = '/ehs/taskbench/query',  // 提供给个人工作台查询待办、已办任务；
}

/**
 * @description: 查询个人待办
 */
export function queryTodoTask(params: TaskBenchQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TaskBenchQueryResultDto[]>({
    url: Api.QueryTodoTask,
    params: params
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 查询全部
 */
export function queryTasks(params: TaskBenchQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<TaskBenchQueryResultDto[]>({
    url: Api.QueryTasks,
    params: params
  }, {
    errorMessageMode: mode,
  })
}
