import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  AccidentRecordQueryConditionDto,
  AccidentRecordDto
} from './model/accidentRecordModel'

enum Api {
  Query = '/ehs/accidentrecord/query',  // 查询
  Get = '/ehs/accidentrecord/get',  // 获取
  Add = '/ehs/accidentrecord/add',  // 新增
  Update = '/ehs/accidentrecord/update',  // 修改
  Delete = '/ehs/accidentrecord/delete',  // 删除
}


/**
 * 
 * @param param 查询
 * @param mode 
 * @returns 
 */
export function queryAccidentRecord(param: AccidentRecordQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<AccidentRecordDto[]>(
    {
      url: Api.Query,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description: 获取
 * @param id
 * @param mode
 * @returns
 *  */
export function getAccidentRecord(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<AccidentRecordDto>(
    {
      url: Api.Get,
      params: { id }
    }, {
    errorMessageMode: mode,
  }
  );
}

/**
 * @description: 新增
 * @param param
 * @param mode
 * @returns
 *  */
export function addAccidentRecord(param: AccidentRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      data: param
    }, {
    errorMessageMode: mode,
  }
  );
}

/**
 *  @description: 修改
 * @param param
 * @param mode
 * @returns
 *  */
export function updateAccidentRecord(param: AccidentRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      data: param
    }, {
    errorMessageMode: mode,
  }
  );
}
/**
 * @description: 删除
 * @param id
 * @param mode
 * @returns
 *  */
export function deleteAccidentRecord(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      data: { id }
    }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  }
  );
}

