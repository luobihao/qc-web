import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  TaskExecuteInfoDto,
  TaskExecuteAddDto,
  TaskExecuteUpdateDto,
  TaskExecuteConfirmDto
} from './model/taskExecuteModel'

enum Api {
  GetTaskItemExecutes = '/ehs/taskexec/item', // 获取指定任务项的所有执行记录-getTaskItemExecutes
  GetTaskGroupExecutes = '/ehs/taskexec/group',  // 获取指定任务分组的所有执行记录-getTaskGroupExecutes
  GetOneExecuteRecord = '/ehs/taskexec/get', // 获取指定执行记录-get
  AddExecuteRecord = '/ehs/taskexec/add', // 新增执行记录-add
  UpdateExecuteRecord = '/ehs/taskexec/update', // 修改执行记录-update
  ConfirmExecuteRecord = '/ehs/taskexec/confirm', // 确认执行记录-confirm
  DeleteExecuteRecord = '/ehs/taskexec/delete' // 删除执行记录-delete
}

/**
 * @description: 获取指定任务项的所有执行记录
 * @param {number} id 任务项id
 * @returns {Promise<TaskExecuteInfoDto[]>}
 * */
export function getTaskItemExecutes(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskExecuteInfoDto[]>({
    url: Api.GetTaskItemExecutes,
    params: {
      id
    }
  }, {
    errorMessageMode: mode,
  });
}

/**
 * @description: 获取指定任务分组的所有执行记录
 * @param {number} id 任务分组id
 * @returns {Promise<TaskExecuteInfoDto[]>}
 * */
export function getTaskGroupExecutes(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskExecuteInfoDto[]>({
    url: Api.GetTaskGroupExecutes,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  });
}

/**
 * @description: 获取指定执行记录
 * @param {number} id 执行记录id
 * @returns {Promise<TaskExecuteInfoDto>}
 * */
export function getOneExecuteRecord(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskExecuteInfoDto>({
    url: Api.GetOneExecuteRecord,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  });
}

/**
 * @description: 新增执行记录
 * @param {TaskExecuteAddDto} params 执行记录数据
 * */
export function addExecuteRecord(params: TaskExecuteAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddExecuteRecord,
    params: params
  }, {
    errorMessageMode: mode,
  });
}

/**
 * @description: 修改执行记录
 * @param {TaskExecuteUpdateDto} params 执行记录数据
 * */
export function updateExecuteRecord(params: TaskExecuteUpdateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.UpdateExecuteRecord,
    params: params
  }, {
    errorMessageMode: mode,
  });
}

/**
 * @description: 确认执行记录
 * @param {number} id 执行记录id
 * */
export function confirmExecuteRecord(params: TaskExecuteConfirmDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.ConfirmExecuteRecord,
    params: params
  }, {
    errorMessageMode: mode,
  });
}

/**
 * @description: 删除执行记录
 * @param {number} id 执行记录id
 * */
export function deleteExecuteRecord(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteExecuteRecord,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  });
}
