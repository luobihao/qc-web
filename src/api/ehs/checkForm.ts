import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  CheckTableQueryConditionDto,
  CheckTableDto,
  CheckTableFullDto,
  CheckGroupDto,
  CheckItemDto,
  CheckGroupFullDto,
  CheckRecordDto,
  CheckRecordSaveDto,
  CheckRecordSaveResultDto,
  CheckItemBatchAddDto
} from './model/checkFormModel'

enum Api {
  QueryCheckForm = '/ehs/check/querytable', // 查询检查表-queryTable
  GetCheckFormFull = '/ehs/check/gettablefull', // 获取指定检查表完整信息-getTableFull
  GetCheckForm = '/ehs/check/gettable', // 获取指定检查表-getTable
  AddCheckForm = '/ehs/check/addtable', // 新增检查表-addTable
  UpdateCheckForm = '/ehs/check/updatetable', // 修改检查表-updateTable
  DeleteCheckForm = '/ehs/check/deletetable', // 删除检查表-deleteTable
  GetOneCheckFormGroup = '/ehs/check/tablegroups', //获取指定表的 分组-getTableGroups
  GetCheckFormGroup = '/ehs/check/getgroup', // 获取指定 检查表分组-getGroup
  AddCheckGroup = '/ehs/check/addgroup', //新增检查分组-addGroup
  UpdateCheckGroup = '/ehs/check/updategroup', //修改分组-updateGroup
  DeleteCheckGroup = '/ehs/check/deletegroup', //删除检查分组-deleteGroup
  GetCheckGroupItem = '/ehs/check/groupitems', // 获取指定分组的项-getGroupItems
  GetCheckItem = '/ehs/check/getitem', // 获取指定检查项-getItem
  AddCheckItem = '/ehs/check/additem', //新增检查项-addItem
  UpdateCheckItem = '/ehs/check/updateitem', //修改检查项-updateItem
  DeleteCheckItem = '/ehs/check/deleteitem', //删除检查项-deleteItem
  BatchAddCheckItem = '/ehs/check/batchadditem', // 批量新增检查项-batchAddItem
  // 检查记录
  GetCheckRecord = '/ehs/checkrecord/get',  // 查询指定检查记录(用检查记录id查)
  GetCheckRecordByDataId = '/ehs/checkrecord/getbydata',  // 查询指定检查记录(用节点id或其他数据id查)
  AddCheckRecord = '/ehs/checkrecord/add',
  UpdateCheckRecord = '/ehs/checkrecord/update',
  DeleteCheckRecord = '/ehs/checkrecord/delete',
  SaveCheckRecord = '/ehs/checkrecord/save'
}


/**
 * 
 * @description 查询检查表-queryTable
 */
export function queryCheckForm(params: CheckTableQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<CheckTableDto[]>({
    url: Api.QueryCheckForm,
    params: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 获取指定检查表完整信息-getTableFull
 */
export function getCheckFormFull(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckTableFullDto>({
    url: Api.GetCheckFormFull,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 获取指定检查表-getTable
 */
export function getCheckForm(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckTableDto>({
    url: Api.GetCheckForm,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 新增检查表-addTable
 */
export function addCheckForm(params: CheckTableDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddCheckForm,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 修改检查表-updateTable
 *  
 * */
export function updateCheckForm(params: CheckTableDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>({
    url: Api.UpdateCheckForm,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 删除检查表-deleteTable
 *  
 * */
export function deleteCheckForm(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteCheckForm,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description 获取指定表的分组-getGroup
 *  */
export function getOneCheckFormGroup(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckGroupDto>({
    url: Api.GetOneCheckFormGroup,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 获取指定检查表分组-getAllTable
 *  */
export function getCheckFormGroup(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckGroupDto[]>({
    url: Api.GetCheckFormGroup,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 新增检查分组-addGroup
 *  */
export function addCheckGroup(params: CheckGroupDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddCheckGroup,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 修改检查表分组-updateGroup
 * */
export function updateCheckGroup(params: CheckGroupDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>({
    url: Api.UpdateCheckGroup,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 删除检查表分组-deleteGroup
 * */
export function deleteCheckGroup(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteCheckGroup,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description 获取指定分组的项-GetCheckGroupItem
 *  */
export function getCheckGroupItem(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckItemDto[]>({
    url: Api.GetCheckGroupItem,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 获取指定检查项-getItem
 *  */
export function getCheckItem(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckItemDto>({
    url: Api.GetCheckItem,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 新增检查项-AddCheckItem
 *  */
export function addCheckItem(params: CheckItemDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddCheckItem,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 修改检查项-UpdateCheckItem
 * */
export function updateCheckItem(params: CheckItemDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>({
    url: Api.UpdateCheckItem,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 删除检查项-DeleteCheckItem
 * */
export function deleteCheckItem(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteCheckItem,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description 批量新增检查项-BatchAddCheckItem
 */
export function batchAddCheckItem(params: CheckItemBatchAddDto[], mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.BatchAddCheckItem,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

//  检查记录相关api
/**
 * @description 获取检查记录-getCheckRecord(单个通过检查记录id查)
 */
export function getCheckRecord(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckRecordDto>({
    url: Api.GetCheckRecord,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}
/**
 * @description 获取检查记录-getCheckRecord(单个通过节点id或者其他记录数据id获取)
 */
export function getCheckRecordByDataId(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CheckRecordDto>({
    url: Api.GetCheckRecordByDataId,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 新增检查记录-AddCheckRecord
 */
export function addCheckRecord(params: CheckRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddCheckRecord,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 修改检查记录-UpdateCheckRecord
 */
export function updateCheckRecord(params: CheckRecordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>({
    url: Api.UpdateCheckRecord,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description 删除检查记录-DeleteCheckRecord
 * */
export function deleteCheckRecord(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteCheckRecord,
    params: {
      id: id
    },
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}



/**
 * @description 保存检查记录-saveCheckRecord
 */
export function saveCheckRecord(params: CheckRecordSaveDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<CheckRecordSaveResultDto>({
    url: Api.SaveCheckRecord,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}
