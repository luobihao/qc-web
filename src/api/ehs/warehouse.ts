import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { WareHouseDto } from './model/warehouseModel';


enum Api {
  GetAll = '/ehs/warehouse/all',
  Get = '/ehs/warehouse/get',
  Add = '/ehs/warehouse/add',
  Update = '/ehs/warehouse/update',
  Delete = '/ehs/warehouse/delete',
  GetUserValid = '/ehs/warehouse/uservalid',
}

/**
 * @description 查询仓库
 */
export function queryAllWareHouse(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<WareHouseDto[]>(
    {
      url: Api.GetAll,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询单个仓库
 *  */
export function getWareHouse(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<WareHouseDto>(
    {
      url: Api.Get,
      params: { id: id }
    },
  );
}


/**
 * @description 添加仓库
 * */
export function addWareHouse(params: WareHouseDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params: params
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description 更新仓库
 * */
export function updateWareHouse(params: WareHouseDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Update,
      params: params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 删除仓库
 * */
export function deleteWareHouse(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id }
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}


/**
 * @description 查询用户可用的仓库
 */
export function queryUserValidWareHouse(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<WareHouseDto[]>(
    {
      url: Api.GetUserValid,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
    }
  )
}
