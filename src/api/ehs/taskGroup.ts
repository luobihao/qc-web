import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  TaskGroupInfoDto,
  TaskGroupItemsDto,
  TaskGroupAddDto,
  TaskGroupUpdateDto
} from './model/taskGroupModel'

enum Api {
  GetTaskGroups = '/ehs/taskgroup/task',  // 获取指定任务的所有分组信息-getTaskGroups
  GetGroup = '/ehs/taskgroup/group', // 获取指定任务分组信息-getGroup
  AddGroup = '/ehs/taskgroup/add', // 新增任务分组-addGroup
  UpdateGroup = '/ehs/taskgroup/update', // 修改任务分组-updateGroup
  DeleteGroup = '/ehs/taskgroup/delete', // 删除任务分组-deleteGroup
  FinishGroup = '/ehs/taskgroup/finish', // 完成任务分组-finishGroup
  EndGroup = '/ehs/taskgroup/end', // 终止任务分组-endGroup
  ContinueGroup = '/ehs/taskgroup/continue', // 继续任务分组-continueGroup
}

/**
 * @description: 获取指定任务的所有分组信息
 */
export function getTaskGroups(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskGroupInfoDto[]>({
    url: Api.GetTaskGroups,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 获取指定任务分组信息
 *  */
export function getGroup(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<TaskGroupInfoDto>({
    url: Api.GetGroup,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 新增任务分组
 *  */
export function addGroup(params: TaskGroupAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddGroup,
    params
  }, {
    errorMessageMode: mode,
  })
}

/** 
 *  @description: 修改任务分组
 *  */
export function updateGroup(params: TaskGroupUpdateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>({
    url: Api.UpdateGroup,
    params
  }, {
    errorMessageMode: mode,
  })
}

/**
 * @description: 删除任务分组
 *  */
export function deleteGroup(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteGroup,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 完成任务分组
 *  */
export function finishGroup(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.FinishGroup,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 结束任务分组
 *  */
export function endGroup(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.EndGroup,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * @description: 继续任务分组
 *  */
export function continueGroup(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.ContinueGroup,
    params: {
      id: id
    }
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}
