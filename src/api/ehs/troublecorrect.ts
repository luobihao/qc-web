import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  EhsTroubleCorrectInfoDto,
  EhsTroubleCorrectQueryConditionDto,
  EhsTroubleCorrectBatchDispatchDto,
  EhsTroubleCorrectDispatchDto,
  EhsTroubleCorrectCompleteDto,
  EhsTroubleCorrectCheckDto,
  EhsReportFileUpdateDto
} from './model/troublecorrectModel'

enum Api {
  Query = '/ehs/troublecorrect/query',  // 查询
  Get = '/ehs/troublecorrect/get',  // 获取隐患治理记录-get
  GetHazard = '/ehs/troublecorrect/hazard',  // 获取指定危险源的隐患治理记录-getByHazard
  BatchDispatch = '/ehs/troublecorrect/batchdispatch',  // 批量派单
  Dispatch = '/ehs/troublecorrect/dispatch',  // 派单
  Correct = '/ehs/troublecorrect/correct',  // 整改
  Check = '/ehs/troublecorrect/check', // 验收
  Report = '/ehs/troublecorrect/generaterpt', // 生成报告
  UploadReport = '/ehs/troublecorrect/uploadreport', //上传报告
}

/**
 * 
 * @param param 查询
 * @param mode 
 * @returns 
 */
export function queryEhsTroubleCorrectList(param: EhsTroubleCorrectQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<EhsTroubleCorrectInfoDto[]>(
    {
      url: Api.Query,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 获取指定危险源的隐患治理记录-getByHazard
 */
export function getEhsTroubleCorrect(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsTroubleCorrectInfoDto>(
    {
      url: Api.GetHazard,
      params: {
        id: id
      }
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 获取隐患治理记录-get
 */
export function getOneEhsTroubleCorrectInfo(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EhsTroubleCorrectInfoDto>(
    {
      url: Api.Get,
      params: {
        id: id
      }
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 批量派单
 */
export function batchDispatchEhsTroubleCorrect(param: EhsTroubleCorrectBatchDispatchDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.BatchDispatch,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 派单
 */
export function dispatchEhsTroubleCorrect(param: EhsTroubleCorrectDispatchDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Dispatch,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 整改  
 */

export function correctEhsTroubleCorrect(param: EhsTroubleCorrectCompleteDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Correct,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 验收
 */
export function checkEhsTroubleCorrect(param: EhsTroubleCorrectCheckDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Check,
      params: param
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 生成报告
 */
export function reportEhsTroubleCorrect(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.Report,
      params: {
        id: id
      }
    }, {
    errorMessageMode: 'modal',
  }
  );
}

/**
 * @description 上传报告
 */
export function uploadEhsTroubleCorrectReport(params: EhsReportFileUpdateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.UploadReport,
      params: params
    }, {
    errorMessageMode: 'modal',
  }
  );
}
