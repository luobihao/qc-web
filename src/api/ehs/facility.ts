import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  FacilityDto,
  FacilityQueryConditionDto
} from './model/facilityModel'

enum Api {
  GetFacilityList = '/ehs/facility/query',
  GetFacility = '/ehs/facility/get',
  AddFacility = '/ehs/facility/add',
  UpdateFacility = '/ehs/facility/update',
  DeleteFacility = '/ehs/facility/delete',
  ImportFacility = '/ehs/facility/import',
}


export function getFacilityList(params: FacilityQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<FacilityDto[]>({
    url: Api.GetFacilityList,
    params: params,
  }, {
    errorMessageMode: mode,
  })
}

export function getFacility(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<FacilityDto>({
    url: Api.GetFacility,
    params: { id },
  }, {
    errorMessageMode: mode,
  })
}

export function addFacility(params: FacilityDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.AddFacility,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

export function updateFacility(params: FacilityDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>({
    url: Api.UpdateFacility,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}

/**
 * 
 * @describe 删除设备
 */
export function deleteFacility(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>({
    url: Api.DeleteFacility,
    params: { id },
  }, {
    errorMessageMode: mode,
    joinParamsToUrl: true,
  })
}

/**
 * 
 * @describe 导入设备
 */
export function importFacility(params: FacilityDto[], mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>({
    url: Api.ImportFacility,
    data: params,
  }, {
    errorMessageMode: mode,
  })
}
