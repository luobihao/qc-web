/**
 * 值班分组DTO
 */
export interface DutyGroupDto {
  //值班分组id
  id: number;
  //值班分组名称
  name: string;
  //所属部门id
  deptid: number;
  //所属部门名称
  deptname: string;
  //排序号
  odr: number;
  //显示列数：默认为1，最小为1
  col: number;
}
