import { DutyStaffDto } from './dutyStaffModel';

/**
 * 班值添加成员DTO
 */
export interface DutyTeamAddMembersDto {
  //班值ID
  id: number | string;
  //人员ID集合
  memberids: number[] | string[];
}

/**
 * 班值成员DTO
 */
export interface DutyTeamMemberDto {
  //班值ID
  id: number | string;
  //值班人员ID
  memberid: number;
  //值班人员信息
  member: DutyStaffDto;
  //排序号
  odr: number;
  //是否为值班长
  chief: boolean;
}

/**
 * 班值成员列表DTO
 */
export interface DutyTeamMemberListDto {
  items: DutyTeamMemberDto[];
}

/**
 * 班值DTO
 */
export interface DutyTeamDto {
  //班值ID
  id: number;
  //班值名称
  name: string;
  //所属值班分组ID
  groupid: number;
  //所属值班分组名称
  groupname: string;
  //分组所属部门
  deptid: number;
  //班值成员
  members: DutyTeamMemberDto[];
}

/**
 * 班值列表DTO
 */
export interface DutyTeamListDto {
  items: DutyTeamDto[];
}
