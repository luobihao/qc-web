/**
 * 值班人员信息DTO
 */
export interface DutyStaffDto {
  //值班人员ID
  id: number;
  //姓名
  name: string;
  //部门ID
  dept: number;
  //手机号
  phone: string;
  //短号
  cornet: string;
}

/**
 * 值班人员列表
 */
export interface DutyStaffListDto {
  items: DutyStaffDto[];
}
