import { DutyGroupDto } from './dutyGroupModel'
import { DutyStaffDto } from './dutyStaffModel'
/**
 * DutyDisplayQueryConditionDto
 */
export interface DutyDisplayQueryConditionDto {
  /**
   * 日期，对应选择的起始日期
   */
  date: string;
  /**
   * 部门ID集合
   */
  deptids: number[];
  /**
   * 选择时间的时段类型，TimeRangeLengthEnum，月-6，季度-7，年-8
   */
  tmtype: number;
}

/**
 * DutyResultDto
 */
export interface DutyResultDto {
  /**
   * 值班分组
   */
  group: DutyGroupDto;
  /**
   * 值班成员
   */
  members: DutyStaffDto[];
  /**
   * 值班时间
   */
  tm: string;
}