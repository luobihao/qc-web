import { DutyStaffDto } from './dutyStaffModel';

/**
 * 排班查询条件DTO
 */
export interface DutyScheduleQueryConditionDto {
  //值班分组ID集合
  groupids: number[];
  //值班开始时间
  begintm: string;
  //值班结束时间
  endtm: string;
}

/**
 * 值班排班信息DTO
 */
export interface DutyScheduleInfoDto {
  //排班ID
  id: number;
  //值班分组ID
  groupid: number;
  //值班分组名称
  groupname: string;
  //值班开始时间
  begintm: string;
  //值班结束时间
  endtm: string;
  //时间段描述
  tmtext: string;
  //是否为节假日
  holiday: boolean;
  //值班班组成员
  members: DutyScheduleShiftMemberDto[];
  //写入/更新时间
  intm: string;
}

/**
 * 值班排班信息DTO集合
 */
export interface DutyScheduleInfoDtoList {
  items: DutyScheduleInfoDto[];
}

/**
 * 排班班次批量设置DTO
 */
export interface DutyScheduleBatchSetDto {
  //值班分组ID集合
  groupids: number[];
  //值班开始时间
  begintm: string;
  //值班结束时间
  endtm: string;
  //间隔天数：一个班次---值班的天数
  days: number;
  //时间段描述
  tmtext: string;
  //是否为节假日
  holiday: boolean;
}

/**
 * 排班班次设置信息DTO
 */
export interface DutyScheduleShiftSetDto {
  //排班ID
  id: number;
  //值班开始时间
  begintm: string;
  //值班结束时间
  endtm: string;
  //时间段描述
  tmtext: string;
  //是否为节假日
  holiday: boolean;
}

/**
 * 设置班次人员DTO
 */
export interface DutyScheduleSetMembersDto {
  //排班ID
  id: number | string;
  //班值ID：如果设置了班值ID获取该班值中的所有成员
  teamid: number;
  //人员ID集合：如果没有选择班值，就选择人员设置
  staffids: number[] | string[];
}

/**
 * 删除班次人员DTO
 */
export interface DutyScheduleDeleteMemberDto {
  //排班ID
  id: number;
  //成员ID
  memberid: number;
}

/**
 * 班次成员DTO
 */
export interface DutyScheduleShiftMemberDto {
  //班次ID
  id: number;
  //人员ID
  memberid: number;
  //人员信息
  member: DutyStaffDto;
  //排序号
  odr: number;
  //是否为值班长
  chief: boolean;
}

/**
 * 班次成员列表DTO
 */
export type DutyScheduleShiftMemberListDto = DutyScheduleShiftMemberDto[];

/**
 * 批量设置班次人员DTO
 */
export interface DutyScheduleBatchSetMembersDto {
  groupid: number;
  //值班开始时间
  begintm: string;
  //值班结束时间
  endtm: string;
  //班值总数：以班值总数计算值班间隔频次，如班值总数为4，即设置第1、5、9...个班次的值班人员为传入的人员；
  teamcount: number;
  //班值ID
  teamid: number;
  //人员ID集合
  staffids: number[] | string[];
}
