import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { DutyGroupDto } from './model/dutyGroupModel';

enum Api {
  GetAll = '/duty/group/all',
  GetByUserDept = '/duty/group/dept',
  Get = '/duty/group/get',
  Add = '/duty/group/add',
  Update = '/duty/group/update',
  Delete = '/duty/group/delete',
}

/**
 * @description 获取所有值班分组
 */
export function getAllDutyGroup(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyGroupDto>(
    {
      url: Api.GetAll,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 根据当前用户部门获取值班分组
 */
export function getByUserDept(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyGroupDto>(
    {
      url: Api.GetByUserDept,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定值班分组
 */
export function getDutyGroup(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyGroupDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增值班分组
 */
export function addDutyGroup(params: DutyGroupDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改值班分组
 */
export function updateDutyGroup(params: DutyGroupDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除值班分组
 */
export function deleteDutyGroup(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
