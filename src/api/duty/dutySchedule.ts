import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  DutyScheduleQueryConditionDto,
  DutyScheduleInfoDtoList,
  DutyScheduleShiftSetDto,
  DutyScheduleBatchSetDto,
  DutyScheduleShiftMemberListDto,
  DutyScheduleSetMembersDto,
  DutyScheduleShiftMemberDto,
  DutyScheduleDeleteMemberDto,
  DutyScheduleBatchSetMembersDto,
} from './model/dutyScheduleModel';

enum Api {
  Query = '/duty/schedule/query',
  SetShift = '/duty/schedule/shift',
  BatchSetShift = '/duty/schedule/batchshift',
  DeleteShift = '/duty/schedule/shift',
  GetShiftMembers = '/duty/schedule/members',
  SetShiftMembers = '/duty/schedule/members',
  SetShiftMember = '/duty/schedule/member',
  DeleteShiftMember = '/duty/schedule/member',
  BatchSetShiftMembers = '/duty/schedule/batchmembers',
}

/**
 * @description 查询
 */
export function query(params: DutyScheduleQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<DutyScheduleInfoDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 设置班次
 */
export function setShift(params: DutyScheduleShiftSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetShift,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 批量设置班次
 */
export function batchSetShift(params: DutyScheduleBatchSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.BatchSetShift,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 删除班次
 * @param id 排班ID
 */
export function deleteShift(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteShift,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description 获取指定班次的值班成员
 * @param id 班次ID
 */
export function getShiftMembers(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyScheduleShiftMemberListDto>(
    {
      url: Api.GetShiftMembers,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 设置指定班次的值班成员
 */
export function setShiftMembers(
  params: DutyScheduleSetMembersDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<string>(
    {
      url: Api.SetShiftMembers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改班次值班成员信息
 */
export function setShiftMember(
  params: DutyScheduleShiftMemberDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<string>(
    {
      url: Api.SetShiftMember,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 删除班次值班成员信息
 */
export function deleteShiftMember(
  params: DutyScheduleDeleteMemberDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.delete<string>(
    {
      url: Api.DeleteShiftMember,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 批量设置班次成员
 */
export function batchSetShiftMembers(
  params: DutyScheduleBatchSetMembersDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<string>(
    {
      url: Api.BatchSetShiftMembers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
