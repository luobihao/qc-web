import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  DutyTeamAddMembersDto,
  DutyTeamListDto,
  DutyTeamDto,
  DutyTeamMemberListDto,
  DutyTeamMemberDto,
} from './model/dutyTeamModel';

enum Api {
  GetAll = '/duty/team/all',
  GetByUserDept = '/duty/team/dept',
  Get = '/duty/team/get',
  Add = '/duty/team/add',
  Update = '/duty/team/update',
  Delete = '/duty/team/delete',
  GetTeamMembers = '/duty/team/members',
  AddTeamMembers = '/duty/team/members',
  SetTeamMember = '/duty/team/member',
  DeleteTeamMember = '/duty/team/member',
}

/**
 * @description 获取所有班值
 */
export function getAllDutyTeam(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyTeamListDto>(
    {
      url: Api.GetAll,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 根据当前用户部门获取班值
 */
export function getTeamByUserDept(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyTeamListDto>(
    {
      url: Api.GetByUserDept,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定班值
 */
export function getDutyTeam(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyTeamDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增班值
 */
export function addDutyTeam(params: DutyTeamDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改班值
 */
export function updateDutyTeam(params: DutyTeamDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除班值
 */
export function deleteDutyTeam(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 获取指定班值的成员列表
 * @param id 班值ID
 */
export function getTeamMembers(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyTeamMemberListDto>(
    {
      url: Api.GetTeamMembers,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 添加班值成员
 */
export function addTeamMembers(params: DutyTeamAddMembersDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.AddTeamMembers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 设置班值成员
 */
export function setTeamMember(params: DutyTeamMemberDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetTeamMember,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除班值成员
 */
export function deleteTeamMember(params: DutyTeamMemberDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.DeleteTeamMember,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
