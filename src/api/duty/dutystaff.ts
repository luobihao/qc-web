import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { DutyStaffListDto, DutyStaffDto } from './model/dutyStaffModel';

enum Api {
  GetAll = '/duty/staff/all',
  GetDeptStaffs = '/duty/staff/dept',
  Add = '/duty/staff/add',
  Update = '/duty/staff/update',
  Delete = '/duty/staff/delete',
}

/**
 * @description 获取所有值班人员信息
 */
export function getAllDutyStaff(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyStaffListDto>(
    {
      url: Api.GetAll,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 根据当前用户部门获取值班人员信息
 */
export function getDeptStaffs(id?: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyStaffListDto>(
    {
      url: Api.GetDeptStaffs,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增值班人员
 */
export function addDutyStaff(params: DutyStaffDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改值班人员
 */
export function updateDutyStaff(params: DutyStaffDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除值班人员
 */
export function deleteDutyStaff(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
