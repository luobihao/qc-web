import { DutyDisplayQueryConditionDto, DutyResultDto } from './model/displayModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  DisplayCalendar = '/duty/display/calendar',
  DisplayTable = '/duty/display/table',
  GroupMembers = '/duty/display/groupmembers',
}

/**
 * @description: 前端展示，以日历形式展示值班表
 */
export function getDisplayCalendar(
  params: DutyDisplayQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<DutyDisplayQueryConditionDto>(
    {
      url: Api.DisplayCalendar,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 前端展示，以表格形式展示值班表
 */
export function getDisplayTable(
  params: DutyDisplayQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<DutyDisplayQueryConditionDto>(
    {
      url: Api.DisplayTable,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定值班分组的当前值班成员信息
 */
export function getGroupMembers(id: String, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DutyResultDto>(
    {
      url: Api.GroupMembers,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
