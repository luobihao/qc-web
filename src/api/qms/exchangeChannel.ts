import { ExchangeCategoryConditionDto, ExchangeCategoryDto, ExchangeCategoryResult, ExchangeCategoryStationResult, ExchangeCategoryStationSetDto, ExchangeChannelDto, ExchangeChannelResult } from './model/exchangeChannelModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
    QueryChannel = '/qms/exchchannel/all',
    GetChannel = '/qms/exchchannel/get',
    AddChannel = '/qms/exchchannel/add',
    UpdateChannel = '/qms/exchchannel/update',
    DeleteChannel = '/qms/exchchannel/delete',
    GetChannelcategory = '/qms/exchcategory/channel',
    QueryCategory = '/qms/exchcategory/all',
    GetCategory = '/qms/exchcategory/get',
    AddCategory = '/qms/exchcategory/add',
    UpdateCategory = '/qms/exchcategory/update',
    DeleteCategory = '/qms/exchcategory/delete',
    getStations = '/qms/exchcategory/station',
    setStations = '/qms/exchcategory/setstation',
    setStationDirection = '/qms/exchcategory/setdirection'
}

/**
 * @description 查询所有通道
 * @param mode 
 * @returns 
 */
export function queryAllChannel(mode: ErrorMessageMode = 'modal') {
    return defHttp.get<ExchangeChannelResult>(
        {
            url: Api.QueryChannel,
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 查询指定id通道
 * @param id 
 * @param mode 
 * @returns 
 */
export function getChannel(id: Number, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<ExchangeChannelDto>(
        {
            url: Api.GetChannel,
            params: {
                id: id
            }
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 新增通道
 * @param params 
 * @param mode 
 * @returns 
 */
export function addChannel(params: ExchangeChannelDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.AddChannel,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 修改通道
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateChannel(params: ExchangeChannelDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.put<string>(
        {
            url: Api.UpdateChannel,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteChannel(id: Number, mode: ErrorMessageMode = 'modal') {
    return defHttp.delete<string>(
        {
            url: Api.DeleteChannel,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
            joinParamsToUrl: true,
        },
    );
}

/**
 * @description 查询所有分类
 * @param mode 
 * @returns 
 */
export function queryAllCategory(mode: ErrorMessageMode = 'modal') {
    return defHttp.get<ExchangeCategoryResult>(
        {
            url: Api.QueryCategory,
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 查询指定通道分类
 * @param params
 * @param mode 
 * @returns 
 */
export function getChannelCategory(params: ExchangeCategoryConditionDto, mode: ErrorMessageMode = 'modal') {
    if (!params.id) return;
    return defHttp.get<ExchangeCategoryResult>(
        {
            url: Api.GetChannelcategory,
            params: params
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 查询指定id的分类
 * @param id 
 * @param mode 
 * @returns 
 */
export function getCategory(id: Number, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<ExchangeCategoryDto>(
        {
            url: Api.GetCategory,
            params: {
                id: id
            }
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 新增分类
 * @param params 
 * @param mode 
 * @returns 
 */
export function addCategory(params: ExchangeCategoryDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.AddCategory,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 修改分类
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateCategory(params: ExchangeCategoryDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.put<string>(
        {
            url: Api.UpdateCategory,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteCategory(id: Number, mode: ErrorMessageMode = 'modal') {
    return defHttp.delete<string>(
        {
            url: Api.DeleteCategory,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
            joinParamsToUrl: true,
        },
    );
}

/**
 * @description 查询指定分类的站点关联
 * @param id 
 * @param mode 
 * @returns 
 */
export function getCategoryStations(id: Number, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<ExchangeCategoryStationResult>(
        {
            url: Api.getStations,
            params: {
                id: id
            }
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 设置指定分类与站点关联-setStations
 * @param params 
 * @param mode 
 * @returns 
 */
export function setCategoryStations(params: ExchangeCategoryStationSetDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.setStations,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}



/**
 * @description 修改分类站点的交换方向-setStationDirection
 * @param params 
 * @param mode 
 * @returns 
 */
export function setStationDirection(params: ExchangeCategoryStationSetDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.setStationDirection,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}
