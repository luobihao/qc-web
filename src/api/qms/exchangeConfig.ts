import { ExchangeConfigQueryConditionDto } from './model/exchangeConfigModel';
import { ExchangeDataQueryConditionDto, ExchangeDataQueryResultDto } from './model/exchangeDataModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
    Condition = '/qms/exchdata/condition',
    Query = '/qms/exchdata/query',
}

/**
 * @description: 获取查询条件数据
 */
export function getQueryConditionData(
    params: ExchangeConfigQueryConditionDto,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.post<DutyDisplayQueryConditionDto>(
        {
            url: Api.Condition,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}