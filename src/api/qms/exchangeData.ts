import { ExchangeConfigQueryConditionDto } from './model/exchangeConfigModel';
import {
  ExchangeDataQueryConditionDto,
  ExchangeDataRegularByTimeQueryConditionDto,
  ExchangeDataQueryResultDto,
  ExchangeDataQueryRawDataResultDto,
  ExchangeDataSaveDto,
  EnumDto,
  DataExchangeLogDto,
  DataExchangeLogQueryConditionDto,
  ExchangeManualOperateDto
} from './model/exchangeDataModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  Condition = '/qms/exchdata/condition',
  Query = '/qms/exchdata/query',
  QueryRaw = '/qms/exchdata/queryraw',
  Save = '/qms/exchdata/save',
  QueryEnum = '/qc/enum/getenumoptions',
  QueryLogs = '/qms/exchdata/logs',
  ManualOperate = '/qms/exchdata/manual',
  Cancel = '/qms/exchdata/cancel',
  regular = '/qms/exchdata/regular',
}

/**
 * @description: 获取保存交换数据接口的url地址
 */
export function getSaveExchangeDataApiUrl() {
  return Api.Save;
}

/**
 * @description: 获取查询条件数据
 */
export function getQueryConditionData(
  params: ExchangeConfigQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<DutyDisplayQueryConditionDto>(
    {
      url: Api.Condition,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description: 根据指定的条件查询交换数据
 * @param params 
 * @param mode 
 * @returns 
 */
export function queryExchangeData(
  params: ExchangeDataQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ExchangeDataQueryResultDto>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 根据指定的条件查询交换数据--返回原始查询结果
 * @param params 
 * @param mode 
 * @returns 
 */
export function queryExchangeDataAsRawData(
  params: ExchangeDataQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ExchangeDataQueryRawDataResultDto>(
    {
      url: Api.QueryRaw,
      params,
      timeout: 120 * 1000,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 按时间规整数据-regularByTime
 */
export function regularByTime(params: ExchangeDataRegularByTimeQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ExchangeDataQueryResultDto>(
    {
      url: Api.regular,
      params,
      timeout: 120 * 1000,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 交换数据保存
 * @param params 
 * @param mode 
 * @returns 
 */
export function saveExchangeData(params: ExchangeDataSaveDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<String>(
    {
      url: Api.Save,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询枚举类
 * @param id 
 * @param mode 
 * @returns 
 */
export function queryEnumOption(id: String, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EnumDto>(
    {
      url: Api.QueryEnum,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description: 查询交换数据日志
 * @param params
 *  
 * @param mode
 * @returns
 * */
export function queryExchangeDataLogs(params: DataExchangeLogQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<DataExchangeLogDto[]>(
    {
      url: Api.QueryLogs,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}



/**
 * @description: 交换数据人工操作
 * @param params
 *  
 * @param mode
 * @returns
 * */
export function exchangeDataManualOperation(params: ExchangeManualOperateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<String>(
    {
      url: Api.ManualOperate,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}



/**
 * @description: 取消交换数据操作
 * @param params
 *  
 * @param mode
 * @returns
 * */
export function cancelExchangeDataOperation(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<String>(
    {
      url: Api.Cancel,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );

}
