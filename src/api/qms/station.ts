import { StationDeptCondition, StationDeptDto, StationDeptSetDto, StationDto, StationResult } from './model/stationModel';
import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  Query = '/qms/station/all',
  Get = '/qms/station/get',
  Add = '/qms/station/add',
  Update = '/qms/station/update',
  Delete = '/qms/station/delete',
  GetDeptConnect = '/qms/station/getdepts',
  SetDeptConnect = '/qms/station/depts'
}

/**
 * @description: 获取所有站点数据
 */
export function queryAllStation(
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.get<StationResult>(
    {
      url: Api.Query,
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description: 根据指定的id查询交换数据站点
 * @param id
 * @param mode 
 * @returns 
 */
export function getStation(id: String, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<StationDto>(
    {
      url: Api.Get,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 交换数据站点新增
 * @param params 
 * @param mode 
 * @returns 
 */
export function addStation(params: StationDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<String>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description 修改交换数据站点
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateStation(params: StationDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteStation(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}


/**
 * @description 获取栏目部门配置
 */
export function getDepts(params: StationDeptCondition, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<StationDeptDto>(
    {
      url: Api.GetDeptConnect,
      params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 设置栏目部门关联
 */
export function setDepts(params: StationDeptSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetDeptConnect,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
