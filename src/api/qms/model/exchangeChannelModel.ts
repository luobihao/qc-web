/**
 * 交换数据通道模型
 */
export interface ExchangeChannelDto {
    /**
     * 通信类型，TCP、UDP、使用的协议等，使用枚举类CommunicationChannelTypeEnum
     */
    comtype: number;
    /**
     * 数据交换方向，向外=1、向内=2，数值求和
     */
    direction: number;
    /**
     * 通道ID
     */
    id: number;
    /**
     * 通道名称
     */
    name: string;
    [property: string]: any;
}
export type ExchangeChannelResult = ExchangeChannelDto[];
/**
 * 交换数据分类模型
 */
export interface ExchangeCategoryDto {
    /**
     * 使用的通道ID
     */
    cid: number;
    /**
     * 数据分类ID
     */
    id: number;
    /**
     * 数据分类名称
     */
    name: string;
    /**
     * 参数，json格式
     */
    params?: string;
    /**
     * 存储的数据库表代码
     */
    tablecode: string;
    [property: string]: any;
}
export type ExchangeCategoryResult = ExchangeCategoryDto[];

/**
 * 数据交换分类站点
 */
export interface ExchangeCategoryStationDto {
    /**
     * 是否关联
     */
    checked: boolean;
    /**
     * 交换方向，数据出、数据入
     */
    direction: number;
    station: ExchangeStationDto;
    [property: string]: any;
}
export type ExchangeCategoryStationResult = ExchangeCategoryStationDto[];

/**
 * 数据交换站点
 */
export interface ExchangeStationDto {
    /**
     * 参数，json格式
     */
    params?: string;
    /**
     * 站点编码
     */
    stcd: string;
    /**
     * 站点名称
     */
    stnm: string;
    [property: string]: any;
}

/**
 * 数据分类关联站点设置DTO
 */
export interface ExchangeCategoryStationSetDto {
    /**
     * 交换方向
     */
    direction: number;
    /**
     * 分类ID
     */
    id: number;
    /**
     * 站点编码集合，集合为空时清空已有的数据分类与站点关联关系
     */
    stcds: string[];
    [property: string]: any;
}
/**
 * 指定通道id查询分类条件
 */
export interface ExchangeCategoryConditionDto {
    id: number;
}