/**
 * 数据交换配置信息查询条件DTO
 */
export interface ExchangeConfigQueryConditionDto {
    /**
     * 数据分类ID，可以为空，可以为多个，多个使用逗号分隔
     */
    category?: string;
    /**
     * 通道ID，可以为空，可以为多个，多个使用逗号分隔
     */
    channel?: string;
    /**
     * 站点编码，可以为空，可以为多个，多个使用逗号分隔
     */
    stcd?: string;
}

/**
 * ExchangeConfigQueryResultDto
 *
 * ExchangeChannelItemDto
 */
export interface ExchangeConfigQueryResultDto {
    /**
     * 交换数据分类集合
     */
    categories: ExchangeCategoryItemDto[];
    /**
     * 通道信息
     */
    channel: ExchangeChannelDto;
}

/**
 * ExchangeCategoryItemDto
 */
export interface ExchangeCategoryItemDto {
    category: ExchangeCategoryDto;
    /**
     * 站点集合
     */
    stations: ExchangeStationDto[];
}

/**
 * ExchangeCategoryDto
 */
export interface ExchangeCategoryDto {
    /**
     * 使用的通道ID
     */
    cid: number;
    /**
     * 数据分类ID
     */
    id: number;
    /**
     * 数据分类名称
     */
    name: string;
    /**
     * 存储的数据库表代码
     */
    tablecode: string;
}

/**
 * ExchangeStationDto
 */
export interface ExchangeStationDto {
    /**
     * 站点编码
     */
    stcd: string;
    /**
     * 站点名称
     */
    stnm: string;
    /**
     * 参数，json格式
     */
    params?: string;
}

/**
 * 通道信息
 *
 * ExchangeChannelDto
 */
export interface ExchangeChannelDto {
    /**
     * 通信类型，TCP、UDP、使用的协议等
     */
    comtype: number;
    /**
     * 数据交换方向，向外、向内
     */
    direction: number;
    /**
     * 通道ID
     */
    id: number;
    /**
     * 通道名称
     */
    name: string;
}
