import { QCTable } from './../../model/qctableModel';
import { KeyValuePairDto } from './../../model/kvpairModel';
import { ExchangeStationDto } from './exchangeConfigModel';
import { DBTableColumnDto } from '/@/api/platform/model/dbtableColumnModel';

/**
 * ExchangeDataQueryConditionDto
 */
export interface ExchangeDataQueryConditionDto {
  /**
   * 按时间排序方法，true表示按时间升序，false表示按时间降序
   */
  asc?: boolean;
  /**
   * 查询起始时间，包含该时间点
   */
  btm: string;
  /**
   * 交换数据分类ID，categoryID
   */
  cid: number;
  /**
   * 查询截止时间，包含该时间点
   */
  etm: string;
  /**
   * 查询语句代码，sql语句对应的代码，非最终的SQL语句
   */
  sql: string;
  /**
   * 站点编码，多个之间使用逗号分隔
   */
  stcd: string;
  /**
   * 统计标记，后继扩展保留，可以由前端传入参数按指定间隔对数据进行统计分析
   */
  sts?: DataRecordStatisticsConditionDto;
  /**
   * 是否只进行统计
   */
  isOnlyStatics: false
}
/**
 * 交换数据查询结果--返回QCTable结构
 */
export interface ExchangeDataQueryResultDto {
  /**
   * 数据，分表头、数据行
   */
  data: QCTable;
  /**
   * 站点，站点编码和名称对应信息，用于前端显示
   */
  stations: ExchangeStationDto[];
  /**
   * 统计结果，根据输入确定，可以返回统计数据条数信息和数据值特征值信息
   */
  sts: string;
}

/**
 * 交换数据查询结果--返回原始数据结构
 */
export interface ExchangeDataQueryRawDataResultDto {
  /**
   * 数据列
   */
  columns: DBTableColumnDto[];
  /**
   * 数据
   */
  data: any;
  /**
   * 站点，站点编码和名称对应信息，用于前端显示
   */
  stations: ExchangeStationDto[];
  /**
   * 统计结果，根据输入确定，可以返回统计数据条数信息和数据值特征值信息
   */
  sts: ExchangeDataStatisticsResultDto;
}

/**
 * 交换数据保存对象DTO
 */
export interface ExchangeDataSaveDto {
  // 交换数据分类ID
  cid: Number;
  // 保存的对象
  data: any;
  // 对象ID/编码
  objid?: String;
  // 对象名称
  objname?: String;
  // 对象参数
  objparam?: String;
}

/**
 * 数据记录统计条件DTO
 */
export interface DataRecordStatisticsConditionDto {
  /**
   * 是否统计准点率，默认为false，表示不统计准点率，准点率需要根据写入时间判断
   */
  ontime: boolean;
  /**
   * 最大延后分钟数，准点率的最大延后分钟数，可以为0，也可以为负值，后台需要做取绝对值处理
   */
  ontimeafter: number;
  /**
   * 最大提前分钟数，准点率的最大提前分钟数，可以为0，也可以为负值，后台需要做取绝对值处理
   */
  ontimebefore: number;
  /**
   * 频率允许延后分钟数，数据时标的误差范围，可以为0，也可以为负值，后台需要做取绝对值处理
   */
  rateafter: number;
  /**
   * 频率允许提前分钟数，数据时标的误差范围，可以为0，也可以为负值，后台需要做取绝对值处理
   */
  ratebefore: number;
  /**
   * 时间频率类型，按天、小时、分钟、秒等频率，使用枚举类型TimeRangeLengthEnum
   */
  ratetype: number;
  /**
   * 频率数值，小于等于0无效，无效时表示不统计
   */
  ratevalue: number;
}

/**
 * 交换数据统计结果DTO
 */
export interface ExchangeDataStatisticsResultDto {
  /**
   * 数据记录统计结果
   */
  recordsts: DataRecordStatisticsResultDto;
  /**
   * 状态统计，根据数据表中的状态列进行统计的结果，以状态名称和数据条数键值对返回
   */
  statusts: KeyValuePairDto[];
  /**
   * 数据值统计，可以为空，可以有1个或多个
   */
  valuests: DataValueStatisticsResultDto[];
}

/**
 * 数据记录统计结果
 *
 * DataRecordStatisticsResultDto
 */
export interface DataRecordStatisticsResultDto {
  /**
   * 应有数据条数
   */
  duecount: number;
  /**
   * 缺失数据条数
   */
  lostcount: number;
  /**
   * 缺失数据时间集合
   */
  losttms: string[];
  /**
   * 误点到报数据条数
   */
  mistimecount: number;
  /**
   * 误点数据时间集合
   */
  mistms: string[];
  /**
   * 准点到报数据条数
   */
  ontimecount: number;
  /**
   * 准点率统计，是否进行准点率统计
   */
  ontimests: boolean;
  /**
   * 到报率统计，是否进行到报率统计
   */
  ratests: boolean;
  /**
   * 实有数据条数
   */
  realcount: number;
}

/**
 * 数据值统计结果DTO
 */
export interface DataValueStatisticsResultDto {
  /**
   * 平均值
   */
  avgv: number;
  /**
   * 差值
   */
  diffv: number;
  /**
   * 最大值时间
   */
  maxtm: string;
  /**
   * 最大值
   */
  maxv: number;
  /**
   * 最小值时间
   */
  mintm: string;
  /**
   * 最小值
   */
  minv: number;
  /**
   * 数据序列名称
   */
  name: string;
  /**
   * 合计值
   */
  sumv: number;
  /**
   * 数据序列单位
   */
  unit?: string;
}


/**
 * DataExchangeLogDto 日志DTO
 */
export interface DataExchangeLogDto {
  /**
   * 起始时间
   */
  begintm: string;
  /**
   * 数据分类ID
   */
  cid: number;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 执行信息
   */
  execinfo?: string;
  /**
   * 执行结果，表示是否执行和执行是否成功，低1位表示是否执行，高1为表示是否成功
   */
  execresult: number;
  /**
   * 执行完毕时间
   */
  exectm?: string;
  /**
   * 写入时间
   */
  intm: string;
  /**
   * 是否为人工操作
   */
  ismanual: boolean;
  /**
   * 日志ID，在新增日志时可以传入，方便调用端后继进行执行结果修改
   */
  logid: string;
  /**
   * 操作/动作类型
   */
  optype: number;
  /**
   * SQL语句
   */
  sql?: string;
  /**
   * 站点编码
   */
  stcd: string;
  /**
   * 操作人名称
   */
  username?: string;
  [property: string]: any;
}

/**
 * DataExchangeLogQueryConditionDto 交换日志查询条件
 */
export interface DataExchangeLogQueryConditionDto {
  /**
   * 起始时间
   */
  begintm: string;
  /**
   * 数据分类ID
   */
  cid: number;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 执行结果，1为成功，2为失败，其他为全部
   */
  execresult?: number;
  /**
   * 是否为人工操作，1为人工，2为自动，其他为全部
   */
  ismanual?: number;
  /**
   * 关键字，可以匹配sql或执行信息
   */
  keywords?: string;
  /**
   * 操作/动作类型，1为修改，2为删除，3为生成写入，其他为全部
   */
  optype?: number;
  /**
   * 站点编码
   */
  stcd: string;
  [property: string]: any;
}


/**
 * 数据交换日志DTO
 */
export interface ExchangeManualOperateDto {
  /**
   * 起始时间
   */
  begintm: string;
  /**
   * 数据分类ID
   */
  cid: number;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 执行信息
   */
  execinfo?: string;
  /**
   * 执行结果，表示是否执行和执行是否成功，低1位表示是否执行，高1为表示是否成功
   */
  execresult: number;
  /**
   * 执行完毕时间
   */
  exectm?: string;
  /**
   * 写入时间
   */
  intm: string;
  /**
   * 是否为人工操作
   */
  ismanual: boolean;
  /**
   * 日志ID，在新增日志时可以传入，方便调用端后继进行执行结果修改
   */
  logid: string;
  /**
   * 操作/动作类型
   */
  optype: number;
  /**
   * SQL语句
   */
  sql?: string;
  /**
   * 站点编码
   */
  stcd: string;
  /**
   * 操作人名称
   */
  username?: string;
  [property: string]: any;
}

/**
 * 枚举类options
 */
export interface EnumOptionsDto {
  key: String | Number,
  value?: String,
  attribute?: String
}

/**
 * 枚举类
 */
export interface EnumDto {
  enumName: String,
  options: EnumOptionsDto[]
}
export type EnumList = EnumDto[];


/**
 * ExchangeDataRegularByTimeQueryConditionDto 交换数据按时间规整查询条件DTO
 */
export interface ExchangeDataRegularByTimeQueryConditionDto {
  /**
   * 查询起始时间，包含该时间点
   */
  btm: string;
  /**
   * 是否计算差值，对数据值列中的数值是保留原值还是计算前后2个数据的差值
   */
  calcdif?: boolean;
  /**
   * 交换数据分类ID，categoryID
   */
  cid: number;
  /**
   * 查询截止时间，包含该时间点
   */
  etm: string;
  /**
   * 段次类型值，对应枚举类TimeIntervalTypeEnum
   */
  intervaltype: number;
  /**
   * 段次长度，单位与段次类型保持一致，设置为0时默认为1
   */
  intlength: number;
  /**
   * 查询语句代码，sql语句对应的代码，非最终的SQL语句
   */
  sql: string;
  /**
   * 站点编码，多个之间使用逗号分隔
   */
  stcd: string;
  [property: string]: any;
}
