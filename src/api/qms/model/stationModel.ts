/**
 * 数据交换站点Dto
 */
export interface StationDto {
  /**
   * 参数，json格式
   */
  params?: string;
  /**
   * 站点编码
   */
  stcd: string;
  /**
   * 站点名称
   */
  stnm: string;
}

/**
 * 数据交换站点查询结果
 */
export type StationResult = StationDto[];

/**
 * StationDeptDto 获取部门站点关联
 */
export interface StationDeptDto {
  /**
   * 是否关联该部门
   */
  checked: boolean;
  dept: DeptSimpleDto;
  [property: string]: any;
}

/**
 * DeptSimpleDto
 */
export interface DeptSimpleDto {
  id: number;
  /**
   * 部门层级，从1开始
   */
  level: number;
  name: string;
  [property: string]: any;
}


/**
 * StationDeptSetDto 设置站点部门关联
 */
export interface StationDeptSetDto {
  /**
   * 部门ID集合
   */
  dids: number[];
  /**
   * 站点编码集合
   */
  stcds: string[];
  [property: string]: any;
}

export interface StationDeptCondition {
  stcds: string[],
  projectCode: string
}
