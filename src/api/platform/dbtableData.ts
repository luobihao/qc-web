import { DBTableDataGetQueryConditionFormDto, DBTableDataQueryConditionDto, DBTableDataQueryResultDto, DBTableDataEditFormDto, DBTableDataOperateDto } from './model/dbtableDataModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
    Insert = '/qc/dbtabledata/insert',
    Save = '/qc/dbtabledata/save',
    Update = '/qc/dbtabledata/update',
    Delete = '/qc/dbtabledata/delete',
    Condition = '/qc/dbtabledata/condition',
    Query = '/qc/dbtabledata/query',
    Schema = '/qc/dbtabledata/schema',
}

/**
 * @description: 查询指定数据库表的查询条件数据
 */
export function getQueryCondition(
    tableCode: String,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.get<DBTableDataGetQueryConditionFormDto>(
        {
            url: Api.Condition,
            params: { id: tableCode },
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 根据指定条件进行数据查询
 */
export function queryData(
    params: DBTableDataQueryConditionDto,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.post<DBTableDataQueryResultDto>(
        {
            url: Api.Query,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 获取数据编辑表单结构
 */
export function getFormSchema(
    tableCode: String,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.get<DBTableDataEditFormDto>(
        {
            url: Api.Schema,
            params: { id: tableCode },
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 新增数据
 */
export function insertData(
    params: DBTableDataOperateDto,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.post<String>(
        {
            url: Api.Insert,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 保存数据，根据传入的数据组件自动判断是新增还是修改
 */
export function saveData(
    params: DBTableDataOperateDto,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.post<String>(
        {
            url: Api.Save,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 修改数据
 */
export function updateData(
    params: DBTableDataOperateDto,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.put<String>(
        {
            url: Api.Update,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 删除数据
 */
export function deleteData(
    params: DBTableDataOperateDto,
    mode: ErrorMessageMode = 'modal',
) {
    return defHttp.delete<String>(
        {
            url: Api.Delete,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}