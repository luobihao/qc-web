import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { DBTableSqlDto, DBTableSqlDtoList, DBTableSqlSimpleDtoList } from './model/dbtableSqlModel';

enum Api {
  GetTableAll = '/qc/dbtablesql/table',
  GetTableAllSimple = '/qc/dbtablesql/tablesimple',
  Get = '/qc/dbtablesql/get',
  Add = '/qc/dbtablesql/add',
  Update = '/qc/dbtablesql/update',
  Delete = '/qc/dbtablesql/delete',
}

/**
 * @description 获取指定表的所有SQL语句集合
 * @param id 数据库表编码
 */
export function queryDBTableSQL(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBTableSqlDtoList>(
    {
      url: Api.GetTableAll,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取指定表的所有SQL语句简要信息集合
 * @param id 数据库表编码
 */
export function queryDBTableSQLSimple(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBTableSqlSimpleDtoList>(
    {
      url: Api.GetTableAllSimple,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定数据库表SQL信息
 * @param tbcode 数据库表编码
 * @param code SQL语句编码
 */
export function getDBTableSQL(tbcode: string, code: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBTableSqlDto>(
    {
      url: Api.Get,
      params: { tbcode: tbcode, code: code },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addDBTableSQL(params: DBTableSqlDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateDBTableSQL(params: DBTableSqlDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 * @param tbcode 数据库表编码
 * @param code SQL语句编码
 */
export function deleteDBTableSQL(tbcode: string, code: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { tbcode: tbcode, code: code },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
