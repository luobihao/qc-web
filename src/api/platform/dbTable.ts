import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { DBTableQueryConditionDto, DBTableDtoList, DBTableDto } from './model/dbTableModel';

enum Api {
  Query = '/qc/dbtable/query',
  Get = '/qc/dbtable/get',
  Add = '/qc/dbtable/add',
  Update = '/qc/dbtable/update',
  Delete = '/qc/dbtable/delete',
}

/**
 * @description 查询数据库表信息集合
 */
export function queryDBTable(params: DBTableQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<DBTableDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定数据库表信息
 */
export function getDBTable(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBTableDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addDBTable(params: DBTableDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateDBTable(params: DBTableDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteDBTable(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
