import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { DBDtoList, DBDto } from './model/dbModel';

enum Api {
  All = '/qc/db/all',
  Get = '/qc/db/get',
  Add = '/qc/db/add',
  Update = '/qc/db/update',
  Delete = '/qc/db/delete',
}

/**
 * @description 获取所有
 */
export function getAllDB(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBDtoList>(
    {
      url: Api.All,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定
 */
export function getDB(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addDB(params: DBDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateDB(params: DBDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteDB(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
