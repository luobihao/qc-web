import { defHttp } from '/@/utils/http/axios';
import { LoginDto, LoginResultDto, UacUserInfoDto, UserModifyPasswordDto } from './model/userModel';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  // Login = '/login',
  // Login = '/qc-user/login',
  Login = '/qc/uac/login',
  // Logout = '/logout',
  Logout = '/qc/uac/logout',
  // GetUserInfo = '/getUserInfo',
  // GetUserInfo = '/qc-user/userInfo',
  GetUserInfo = '/qc/uac/userinfo',
  GetPermCode = '/getPermCode',
  ModifyPassword = '/qc/uac/modifypwd',
  GetUacMenuList = '/qc/uac/menuList',
}

/**
 * @description: user login api
 */
export function login(params: LoginDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultDto>(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
      //使用mock或测试地址时，可通过设置apiUrl使用.env中配置的VITE_PROXY不同别名
      // apiUrl: '/mock-api',
      //使用mock或测试接口时，如返回的直接为数据，不包含渠成网关中统一定义的code和msg，可通过设置isQuChengGatewayUnifyFormat为false
      // isQuChengGatewayUnifyFormat: false,
      //可选参数
      // joinParamsToUrl: true,
      // joinTime: false,
    },
  );
}

/**
 * @description: getUserInfo
 */
export function getUserInfo() {
  return defHttp.get<UacUserInfoDto>(
    { url: Api.GetUserInfo },
    {
      errorMessageMode: 'none',
      // apiUrl: '/mock-api',
    },
  );
}

/**
 * @description: 根据token获取授权代码，2023年10月已停止使用
 */
export function getPermCode() {
  return defHttp.get<string[]>({ url: Api.GetPermCode });
}

/**
 * @description: 退出登录
 */
export function doLogout() {
  return defHttp.get<string>(
    { url: Api.Logout },
    {
      // apiUrl: '/mock-api',
    },
  );
}

/**
 * @description: 修改密码
 */
export function modifyPassword(params: UserModifyPasswordDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.ModifyPassword,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取登录用户项目的功能菜单
 */
export function getUacMenuList(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get(
    {
      url: Api.GetUacMenuList,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}
