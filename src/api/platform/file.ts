import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

import { FileQueryConditionDto, fileInfoList } from './model/fileModel';

enum Api {
  Upload = '/qc/file/upload',
  GetImageFile = '/qc/file/image/',
  GetPdfFile = '/qc/file/pdf/',
  GetFile = '/qc/file/view/',
  Query = '/qc/file/query',
}
//2023-12-28，对文章内容中的图片/视频（使用文件上传功能）的地址前缀进行替换
//在获取文章内容信息时再对地址前缀进行还原，解决部署服务器器ip地址变更后造成上传文件无法访问的问题
//实际地址---替换地址
//http://192.168.1.100:8088/qc/file/image/{id}---$QcUploadFilePrefix$/qc/file/image/{id}
export const uploadFileViewUrlPrefix = '$QcUploadFilePrefix$';
export const uploadImageFileViewUrlPrefix = '$QcUploadImageFilePrefix$';
export const uploadVideoFileViewUrlPrefix = '$QcUploadVideoFilePrefix$';

export const uploadFileApi = Api.Upload;
export const getImageFileApi = Api.GetImageFile;
export const getPdfFileApi = Api.GetPdfFile;
export const getFileApi = Api.GetFile;

export interface FileInfo {
  id?: number | string; //平台中存储在数据库中的文件ID
  url?: string;
  name: string; //文件名称，用于显示使用，与实际文件名不对应
  type: string; //文件的扩展名
}

/**
 * @description 查询文件
 */
export function queryFiles(params: FileQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<fileInfoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
