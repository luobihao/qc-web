import { ParamValuesQueryResultDto, ParamValuesSaveDto, ParamValuesGetConditionDto, ParamValueLogQueryConditionDto, ParamValueLogQueryResultDto } from './model/paramValueModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  Query = '/qc/paramvalue/query',
  Save = '/qc/paramvalue/save',
  Get = '/qc/paramvalue/get',
  QueryLogs = '/qc/paramvalue/log',
}

/**
 * @description: 查询指定参数分组的参数值信息
 */
export function queryParamGroupValues(groupCode: String, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<ParamValuesQueryResultDto>(
    {
      url: Api.Query,
      params: { id: groupCode },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询指定参数分组的参数值信息
 */
export function saveParamGroupValues(params: ParamValuesSaveDto, mode: ErrorMessageMode = 'modal',) {
  return defHttp.post<String>(
    {
      url: Api.Save,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定参数值信息
 */
export function getParamValues(
  params: ParamValuesGetConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ParamValuesQueryResultDto>(
    {
      url: Api.Get,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 查询日志
 */
export function queryParamValueLogs(
  params: ParamValueLogQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ParamValueLogQueryResultDto>(
    {
      url: Api.QueryLogs,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
