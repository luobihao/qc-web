import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { DBTableColumnDtoList, DBTableColumnDto } from './model/dbtableColumnModel';

enum Api {
  GetTableAll = '/qc/dbtablecolumn/table',
  GetTableAllShow = '/qc/dbtablecolumn/tableshow',
  Get = '/qc/dbtablecolumn/get',
  Add = '/qc/dbtablecolumn/add',
  Update = '/qc/dbtablecolumn/update',
  Delete = '/qc/dbtablecolumn/delete',
  GetAddColumnsInfo = '/qc/dbtablecolumn/getaddcolumnsinfo',
}

/**
 * @description 获取指定表的所有列集合
 * @param id 数据库表编码
 */
export function queryDBTableColumn(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBTableColumnDtoList>(
    {
      url: Api.GetTableAll,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取指定表的所有显示列（只查询在前端表格/图表中显示的列）集合
 * @param id 数据库表编码
 */
export function queryDBTableColumnShow(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBTableColumnDtoList>(
    {
      url: Api.GetTableAllShow,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定数据库表列信息
 * @param tbcode 数据库表编码
 * @param code 列编码
 */
export function getDBTableColumn(tbcode: string, code: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DBTableColumnDto>(
    {
      url: Api.Get,
      params: { tbcode: tbcode, code: code },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addDBTableColumn(params: DBTableColumnDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 修改
 */
export function updateDBTableColumn(params: DBTableColumnDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 * @param tbcode 数据库表编码
 * @param code 列编码
 */
export function deleteDBTableColumn(
  tbcode: string,
  code: string,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { tbcode: tbcode, code: code },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * 获取并添加指定数据库表列结构信息
 * @param id 数据库表编码
 */
export function getAddColumnsInfo(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.GetAddColumnsInfo,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}
