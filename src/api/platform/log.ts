import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { ApiLogQueryConditionDto, ApiLogQueryResultDto, ApiLogDetailDto, UserLoginLogDto, UserLoginLogQueryConditionDto } from './model/logModel';

enum Api {
  QueryApiLog = '/qc/log/queryapi',
  GetApiLog = '/qc/log/getapi',
  QueryLoginLog = '/qc/log/querylogin',
  GetLoginLog = '/qc/log/getlogin',
  SetTokenExpired = '/qc/log/tokenexpired'
}

/**
 * @description 查询api访问日志
 */
export function queryApiLog(params: ApiLogQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ApiLogQueryResultDto>(
    {
      url: Api.QueryApiLog,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取指定api日志详情
 */
export function getApiLog(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<ApiLogDetailDto>(
    {
      url: Api.GetApiLog,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询登录日志
 */
export function queryLoginLog(params: UserLoginLogQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<UserLoginLogDto[]>(
    {
      url: Api.QueryLoginLog,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取指定登录日志
 */
export function getLoginLog(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserLoginLogDto>(
    {
      url: Api.GetLoginLog,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 设置token过期
 */
export function setTokenExpired(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.SetTokenExpired,
      params: {
        id: id,
      },
    },
    {
      errorMessageMode: mode,
    },
  );
}
