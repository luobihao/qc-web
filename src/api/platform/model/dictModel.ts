/**
 * DictCategoryDto 数据字典分类DTO
 */
export interface DictCategoryDto {
    /**
     * 分类编码
     */
    code: string;
    /**
     * 描述
     */
    description?: string;
    /**
     * 分类名称
     */
    name: string;
    [property: string]: any;
}
export type DictCategoryResult = DictCategoryDto[];
/**
 * DictCategoryQueryConditionDto 数据字典分类查询条件DTO
 */
export interface DictCategoryQueryConditionDto {
    /**
     * 关键字
     */
    keywords?: string;
    [property: string]: any;
}

/**
 * DictItemDto 数据字典项DTO
 */
export interface DictItemDto {
    /**
     * 分类编码
     */
    categorycode: string;
    /**
     * 状态标记
     */
    flag: string;
    /**
     * 字典项ID
     */
    id: string;
    /**
     * 字典值
     */
    k: string;
    /**
     * 排序号
     */
    odr: number;
    /**
     * 显示文本
     */
    v: string;
    [property: string]: any;
}
export type DictItemResult = DictItemDto[];
/**
 * DictItemQueryConditionDto 数据字典项查询条件DTO
 */
export interface DictItemQueryConditionDto {
    /**
     * 分类编码
     */
    ctgcode: string;
    /**
     * 状态，可以筛选状态正常的或全部状态
     */
    flag?: string;
    /**
     * 关键字
     */
    keywords?: string;
    [property: string]: any;
}