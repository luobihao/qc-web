/**
 * 参数分组DTO
 *
 */
export interface ParamGroupDto {
    /**
     * 分组编码
     */
    code: string;
    /**
     * 描述
     */
    description?: string;
    /**
     * 分组名称
     */
    name: string;
    /**
     * 节点名称，返回参数集合的节点名称集合
     */
    nodes?: string;
    /**
     * 查询语句，查询对象id、名称和参数的sql语句
     */
    sqlstr?: string;
    /**
     * 数据库表编码
     */
    tablecode: string;
}

/**
 * 参数分组查询条件DTO
 */
export interface ParamGroupQueryConditionDto {
    /**
     * 关键字，匹配分组编码、分组名称或数据库表编码
     */
    keywords?: string;
}

/**
 * 参数项DTO
 */
export interface ParamItemDto {
    /**
     * 数据类型
     */
    datatype: number;
    /**
     * 默认值
     */
    defaultval?: string;
    /**
     * 描述
     */
    description: string;
    /**
     * 状态标记
     */
    flag: number;
    /**
     * 分组编码
     */
    groupcode: string;
    /**
     * ID
     */
    id: number;
    /**
     * 参数项编码
     */
    itemcode: string;
    /**
     * 参数项名称
     */
    name: string;
    /**
     * 不为空
     */
    notnull: boolean;
    /**
     * 排序号
     */
    odr: number;
    /**
     * 单位
     */
    unit?: string;
    /**
     * 取值范围
     */
    valrange?: string;
}


/**
 * 参数组查询条件DTO
 */
export interface ParamGroupQueryConditionDto {
    /**
     * 关键字
     */
    keywords?: string;
}

/**
 * 参数项查询条件DTO
 */
export interface ParamItemQueryConditionDto {
    /**
     * 分组编码
     */
    groupcode: string;
    /**
     * 关键字
     */
    keywords?: string;
}


export interface KeyValueTypeSelect {
    /**
     * 数据类型选择项
     */
    datatypeOptions: KeyValuePairDto[];
    [property: string]: any;
}

/**
 * 键值对DTO
 */
export interface KeyValuePairDto {
    /**
     * 属性，可以为空；用于将需要的信息传递值前端或后台
     */
    attribute?: string;
    /**
     * 键
     */
    key: string;
    /**
     * 值
     */
    value?: string;
    [property: string]: any;
}
export type ParamGroupDtoResult = ParamGroupDto[];
export type ParamItemDtoResult = ParamItemDto[];
export type KeyValueTypeSelectList = KeyValueTypeSelect[];