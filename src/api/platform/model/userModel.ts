/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  username: string;
  password: string;
}

/**
 * @description: 渠成用户登录输入参数，原定义为LoginParams
 */
export interface LoginDto {
  /**
   * 客户端信息
   */
  cinfo?: string;
  /**
   * 用户名
   */
  code: string;
  /**
   * 客户端类型
   */
  ctype?: number;
  /**
   * 项目编码，所登录的项目编码
   */
  project?: string;
  /**
   * 密码，加密后的密码
   */
  pwd: string;
  /**
   * 验证方式，账号密码、手机号或其他
   */
  vtype: number;
}

export interface RoleInfo {
  roleName: string;
  value: string;
}

/**
 * @description: Login interface return value
 */
export interface LoginResultModel {
  userId: string | number;
  token: string;
  role: RoleInfo;
}

/**
 * @description: LoginResultDto渠成用户登录结果，原定义为LoginResultModel
 */
export interface LoginResultDto {
  /**
   * 用户名，用于前端显示
   */
  name?: string;
  /**
   * 默认项目编码
   */
  projectcode?: string;
  /**
   * 项目列表，如未指定项目返回有权限的项目列表以供选择
   */
  projects?: ObjectParamValuesDto[];
  /**
   * 提示信息，如需要修改密码等
   */
  prompt?: string;
  /**
   * 是否需要刷新网关，登录接口中如果新增了黑名单记录，需要由前端刷新网关中的有效黑名单；当该值为true时刷新网关中的黑名单，其他值时不需要处理；
   */
  refreshgateway: boolean;
  /**
   * 是否成功
   */
  success: boolean;
  /**
   * TOKEN
   */
  token?: string;
  [property: string]: any;
}

/**
 * ObjectParamValuesDto
 */
export interface ObjectParamValuesDto {
  /**
   * 参数值json字符串，参数节点名称和节点json字符串键值对
   */
  jsonstrs?: { [key: string]: any };
  /**
   * 对象ID/编码
   */
  objcode: string;
  /**
   * 对象名称
   */
  objname: string;
  /**
   * 参数值，参数名称和值键值对
   */
  values?: { [key: string]: any };
  [property: string]: any;
}



/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel {
  roles: RoleInfo[];
  // 用户id
  userId: string | number;
  // 用户名
  username: string;
  // 真实名字
  realName: string;
  // 头像
  avatar: string;
  // 介绍
  desc?: string;
}

/**
 * @description: 渠成用户信息
 */
export interface UacUserInfoDto {
  /**
   * 登录用户名
   */
  code: string;
  /**
   * 部门ID，如果有多个返回第1个
   */
  deptId?: number;
  /**
   * 部门名称，如果有多个返回第1个
   */
  deptName?: string;
  /**
   * 用户ID：为保证用户ID不被泄露，前端不记录用户ID，后台接口也不会返回用户ID
   */
  // id: number;
  /**
   * 名称
   */
  name: string;
  /**
   * 手机号，需做脱敏处理
   */
  phone: string;
  /**
   * 项目编码
   */
  project?: string;
}

/**
 * 修改用户密码DTO
 */
export interface UserModifyPasswordDto {
  /**
   * 确认新密码，加密后的密码
   */
  confirmpwd: string;
  /**
   * 新密码，加密后的密码
   */
  newpwd: string;
  /**
   * 旧密码，加密后的密码
   */
  oldpwd: string;
}
