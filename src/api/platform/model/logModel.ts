/**
 * ApiLog查询条件DTO
 */
export interface ApiLogQueryConditionDto {
  //开始时间
  begintm: string;

  //结束时间
  endtm: string;

  //用户id
  uid: string;

  //token
  token: string;

  //url
  url: string;

  //ip
  ip: string;

  //关键字：匹配请求参数、请求内容
  words?: string;

  //状态：可以匹配全部（不传默认查全部）、失败（fail）、指定状态码
  status: string;

  //耗时：耗时时间 >= N毫秒
  ts: string;
}

/**
 * Api日志基本DTO
 */
export interface ApiLogInfoDto {
  //日志id
  id: string;

  //时间
  tm: string;

  //请求方法
  method: string;

  //请求URL
  url: string;

  //请求ip
  ip: string;

  //请求token
  token: string;

  //状态码
  status: string;

  //耗时/ms
  ts: string;
}

/**
 * Api日志记录列表
 */
export type apiLogListItems = ApiLogInfoDto[];

/**
 * Api日志查询结果DTO
 */
export interface ApiLogQueryResultDto {
  //总页数
  pages: number;

  //总记录数
  total: number;

  //Api日志记录列表
  items: apiLogListItems;
}

/**
 * Api日志详情DTO
 */
export interface ApiLogDetailDto {
  //日志id
  id: string;

  //时间
  tm: string;

  //请求方法
  method: string;

  //url
  url: string;

  //ip
  ip: string;

  //请求参数
  params: string;

  //请求内容
  body: string;

  //token
  token: string;

  //状态码
  status: string;

  //响应数据
  data: string;

  //耗时/ms
  ts: string;

  //返回结果
  result: string;
}


/**
 * UserLoginLogDto 用户登录日志DTO
 */

export interface UserLoginLogDto {
  /**
   * 客户端信息，从request中获取的userAgent
   */
  clientinfo?: string;
  /**
   * 客户端类型
   */
  clienttype?: string;
  /**
   * 过期时间
   */
  expiredtm?: string;
  /**
   * 是否已过期
   */
  hasexpired: boolean;
  /**
   * 记录ID
   */
  id: string;
  /**
   * 客户端IP，从request中获取的RemoteAddress
   */
  ip?: string;
  /**
   * 登录验证方式
   */
  logintype: string;
  /**
   * 登录结果信息
   */
  msg?: string;
  /**
   * 项目编码
   */
  project?: string;
  /**
   * 密码
   */
  pwd?: string;
  /**
   * 登录结果状态，登录结果状态文字信息
   */
  status?: string;
  /**
   * 登录是否成功
   */
  success: boolean;
  /**
   * 登录时间
   */
  tm: string;
  /**
   * TOKEN
   */
  token?: string;
  /**
   * 登录名
   */
  ucode: string;
  /**
   * 登录用户ID
   */
  uid?: string;
  [property: string]: any;
}


/**
 * UserLoginLogQueryConditionDto 用户登录日志查询条件DTO
 */
export interface UserLoginLogQueryConditionDto {
  /**
   * 起始时间
   */
  begintm: string;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * token，精确匹配
   */
  token?: string;
  /**
   * 用户ID，精确匹配
   */
  uid?: string;
  /**
   * 关键字，模糊匹配；可为登录名、客户端信息或登录结果信息
   */
  words?: string;
  [property: string]: any;
}
