/**
 * 接口日志过滤规则DTO
 */
export interface ApiFilterDto {
  // 规则ID
  id: string;
  // 请求方法，为空表示所有
  method?: string;
  // url
  url: string;
  // 用户ID
  uid?: number;
  // 耗时毫秒，大于0时有效，有效时记录耗时≥该值的请求
  ts?: number;
  // 命中次数，新增或修改时自动清零
  hitcount?: number;
  // 状态标记，0-正常，5-禁用
  flag?: number;
  // 规则更新时间，新增或修改时的系统时间
  updatetm?: string;
}

/**
 * 接口日志过滤规则DTO集合
 */
export type ApiFilterDtoList = ApiFilterDto[];

/**
 * 接口日志过滤规则查询条件DTO
 */
export interface ApiFilterQueryConditionDto {
  // 状态标记，0为正常，5为禁用；对应枚举表示指定状态，不对应枚举表示所有状态
  flag: string;
  // 关键字，匹配url
  keywords?: string;
}
