/**
 * BlackListQueryConditionDto 黑名单查询条件DTO
 */
export interface BlackListQueryConditionDto {
  /**
   * IP，在IP地址中模糊匹配
   */
  ip?: string;
  /**
   * 关键字，在URL、原因说明和操作人信息中模糊匹配
   */
  keywords?: string;
  /**
   * 仅当前有效，是否仅查询当前时间有效
   */
  onlyvalid: boolean;
  [property: string]: any;
}


/**
 * BlackListDto 黑名单信息DTO
 */
export interface BlackListDto {
  /**
   * 过期时间
   */
  expiredtm?: string;
  /**
   * 命中次数
   */
  hitcount: number;
  /**
   * 最新命中时间
   */
  hittm?: string;
  /**
   * 记录ID
   */
  id: number;
  /**
   * IP地址，IP地址和URL至少有1个
   */
  ip?: string;
  /**
   * 当前是否有效，在当前时间该黑名单是否有效
   */
  isvalid: boolean;
  /**
   * 原因说明
   */
  reason: string;
  /**
   * 记录时间，开始日期
   */
  tm: string;
  /**
   * URL
   */
  url?: string;
  /**
   * 操作人，为空表示系统自动添加，不为空时存储操作用户名称信息
   */
  username?: string;
  [property: string]: any;
}


/**
 * BlackListAddDto 黑名单新增DTO
 */
export interface BlackListAddDto {
  /**
   * 过期时间
   */
  expiredtm?: string;
  /**
   * IP地址，IP地址和URL至少有1个不为空
   */
  ip?: string;
  /**
   * 原因说明
   */
  reason: string;
  /**
   * URL，IP地址和URL至少有1个不为空
   */
  url?: string;
  [property: string]: any;
}
