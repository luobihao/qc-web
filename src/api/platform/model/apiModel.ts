/**
 * APIQueryConditionDto API接口查询条件
 */
export interface APIQueryConditionDto {
  /**
   * 接口分组
   */
  // group?: string;
  /**
   * 关键字，关键字匹配接口路径、接口名称等
   */
  keywords?: string;
  /**
   * 请求方法
   */
  // method?: string;
  [property: string]: any;
}
/**
 * APIDto API接口DTO
 */
export interface APIDto {
  /**
   * 唯一标识，接口的唯一标识，可以使用Method+Url或者只使用Url
   */
  api: string;
  /**
   * 接口分组，方便前端显示时按分组进行排列显示
   */
  apigroup?: string;
  /**
   * 描述
   */
  description?: string;
  /**
   * 可用状态，是否可用
   */
  enable: boolean;
  /**
   * 请求方法，使用枚举类RequestMethodEnum
   */
  method: number;
  /**
   * 接口名称
   */
  name: string;
  /**
   * 接口路径，请求路径，与接口设计中的一致；统一以斜线开头
   */
  url: string;
  [property: string]: any;
}
/**
 * 查询回来的Api结果数组
 */
export type ApiResult = APIDto[];


/**
 * APITreeDto api树形结构
 */
export interface APITreeDto {
  childs?: APIDto[];
  info: APIDto;
  [property: string]: any;
}
export type ApiTreeResult = APITreeDto[];
