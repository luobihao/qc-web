import { KeyValuePairDto } from './qcPublicModel';
import { DBTableDto } from './dbtableModel';
import { DBTableColumnDto } from './dbtableColumnModel';

/**
 * 获取数据查询条件表单DTO
 */
export interface DBTableDataGetQueryConditionFormDto {
  /**
   * 表单项集合
   */
  items?: DBTableDataQueryConditionFormItemDto[];
  /**
   * 数据库表编码
   */
  tablecode: string;
}

/**
 * 数据查询条件表单项DTO
 */
export interface DBTableDataQueryConditionFormItemDto {
  /**
   * 比较类型，字段值的比较方式，等于、like、大于、小于、范围等
   */
  cmptype: number;
  /**
   * 编码，字段编码
   */
  code: string;
  /**
   * 渠成列类型，定义列中存储的对象类型标识，id、名称、写入时间、更新时间、参数值等
   */
  coltype: number;
  /**
   * 数据类型，字符串、日期时间、整数、小数、布尔、枚举、BLOB
   */
  datatype: number;
  /**
   * 默认值
   */
  defaultval?: string;
  /**
   * 名称，显示的名称，label
   */
  name: string;
  /**
   * 是否可为空，是否为必须输入条件
   */
  notnull: boolean;
  /**
   * 供选择的数据，列为枚举、数据字典、对象等时后台返回的供选择的数据集合；
   */
  options?: KeyValuePairDto[];
  /**
   * 格式化字符串，前端显示的格式化字符串
   */
  formatstr?: string;
}

/**
 * 数据库表数据查询条件DTO
 */
export interface DBTableDataQueryConditionDto {
  /**
   * 查询条件项集合
   */
  items?: DBTableDataQueryConditionItemDto[];
  /**
   * 数据库表编码
   */
  tablecode: string;
}

/**
 * 数据库表数据查询条件项DTO
 */
export interface DBTableDataQueryConditionItemDto {
  /**
   * 编码，字段编码
   */
  code: string;
  /**
   * 输入值1，如果为范围选择时为起始值
   */
  val1?: string;
  /**
   * 输入值2，如果为范围选择时为截止值
   */
  val2?: string;
}

/**
 * 数据库表数据查询结果DTO
 */
export interface DBTableDataQueryResultDto {
  /**
   * 列集合
   */
  columns?: DBTableColumnDto[];
  /**
   * 数据，数据行集合，List<Map<String, Object>>
   */
  data?: { [key: string]: any }[];
  /**
   * 数据库表
   */
  table: DBTableDto;
}

/**
 * 数据库表数据操作DTO
 */
export interface DBTableDataOperateDto {
  /**
   * 数据对象，表单数据，键值对
   */
  data: { [key: string]: any };
  /**
   * 对象ID/编码，如果保存的信息中有对象ID或编码时传入，可以为空
   */
  objid?: string;
  /**
   * 对象名称，如果保存的信息中有对象名称时传入，可以为空
   */
  objname?: string;
  /**
   * 对象参数，json
   */
  objparam?: string;
  /**
   * 数据操作类型，可以强制指定为insert或update，未指定时根据数据自动判断
   */
  operatetype: number;
  /**
   * 数据库表编码
   */
  tablecode: string;
  /**
   * 操作人ID，如果保存的信息中有操作用户ID时传入，可以为空
   */
  userid?: string;
  /**
   * 操作人名称，如果保存的信息中有操作用户名称时传入，可以为空
   */
  username?: string;
}

/**
 * 数据库表数据编辑表单DTO
 */
export interface DBTableDataEditFormDto {
  /**
   * 表单项集合
   */
  items?: DBTableColumnDto[];
  /**
   * 数据库表
   */
  table: DBTableDto;
}
