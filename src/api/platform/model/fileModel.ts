/**
 * 文件查询条件DTO
 */
export interface FileQueryConditionDto {
  //对应的系统
  sys: string;
  //对应的模块
  module: string;
  //文件类型
  type: number;
  //起始时间
  begintm: string;
  //截至时间
  endtm: string;
}

/**
 * 文件查看DTO
 */
export interface FileInfoDto {
  //文件ID
  id: number;
  //显示名称
  title: string;
  //文件名称，上传时的文件名称
  name: string;
  //扩展名
  extension: string;
  //文件类型
  type: number;
  //文件大小，单位：MB
  size: number;
  //上传时间
  tm: string;
  //浏览次数
  vcount: number;
  //下载次数
  dcount: number;
}

/**
 * 文件详情列表
 */
export type fileInfoList = FileInfoDto[];
