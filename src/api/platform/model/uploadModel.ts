// export interface UploadApiResult {
//   message: string;
//   code: number;
//   url: string;
// }
export interface UploadApiResult {
  success: boolean;
  msg: string;
  fileid: number | string;
  filename: string;
}
