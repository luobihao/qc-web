/**
 * 数据库信息DTO
 */
export interface DBDto {
  /**
   * 数据库代码
   */
  code: string;
  /**
   * 连接信息
   */
  con: string;
  /**
   * 数据库名称
   */
  name: string;
}

/**
 * 数据库信息DTO列表
 */
export type DBDtoList = DBDto[];
