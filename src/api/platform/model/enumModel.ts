import { KeyValuePairDto } from './qcPublicModel';

/**
 * 枚举类集合DTO
 */
export interface EnumDto {
  enumName: string;
  options: KeyValuePairDto[];
}
