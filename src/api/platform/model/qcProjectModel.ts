/**
 * description: 渠成平台中项目配置信息
 */
// export interface QcPlatformProjectConfig {
//   code: string;
//   title: string;
//   fulltitle?: string;
//   logo?: string;
//   copyright?: string;
//   //扩展考虑：使用json动态配置扩展；
//   //还需要配置的信息有：登录页（背景图、效果、排版）、首页布局（右上角设置内容）
// }
//项目显示信息对象已经修改为从项目的参数中获取，统一使用ObjectParamValuesDto对象
// import { ObjectParamValuesDto } from './paramValueModel';

/**
 * 项目参数值获取条件DTO
 */
export interface ProjectParamValueConditionDto {
  /**
   * 参数分组编码
   */
  groupcode: string;
  /**
   * 项目编码
   */
  projectcode: string;
}
