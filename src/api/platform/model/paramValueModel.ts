/**
 * 对象参数值DTO
 */
export interface ObjectParamValuesDto {
  /**
   * 参数值json字符串，参数节点名称和节点json字符串键值对
   */
  jsonstrs?: { [key: string]: any };
  /**
   * 对象ID/编码
   */
  objcode: string;
  /**
   * 对象名称
   */
  objname: string;
  /**
   * 参数值，参数名称和值键值对
   */
  values?: { [key: string]: any };
}

/**
 * 参数值查询结果DTO
 */
export interface ParamValuesQueryResultDto {
  /**
   * 参数分组信息
   */
  group: ParamGroupDto;
  /**
   * 参数项集合
   */
  items: ParamItemDto[];
  /**
   * 对象集合，对象编码、名称和参数值
   */
  objs: ObjectParamValuesDto[];
}

/**
 * 参数值保存DTO
 */
export interface ParamValuesSaveDto {
  /**
   * 参数分组编码
   */
  groupcode: string;
  /**
   * 对象集合，对象编码、名称和参数值
   */
  objs: ObjectParamValuesDto[];
}

/**
 * 参数值获取条件DTO
 */
export interface ParamValuesGetConditionDto {
  /**
   * 对象ID/编码，可以为1个或多个，逗号分隔
   */
  objectids: string;
  /**
   * 参数节点名称，可以为空、可以为多个；多个之间使用逗号分隔，每个中使用大于符号＞指定上下级关系；
   */
  paramnodes?: string;
  /**
   * 数据库表编码
   */
  tablecode: string;
}

/**
 * 参数值日志查询条件DTO
 */
export interface ParamValueLogQueryConditionDto {
  /**
   * 分组编码
   */
  groupcode: string;
  /**
   * 参数编码，可以为空或1个，为空时表示指定对象的参数分组编码中的所有参数值的日志，不为空时表示指定对象的指定参数的日志
   */
  itemids?: string;
  /**
   * 对象ID/编码
   */
  objectid: string;
}

/**
 * 参数值日志DTO
 */
export interface ParamValueLogDto {
  /**
   * 操作后值
   */
  afterval?: string;
  /**
   * 操作前值
   */
  beforeval?: string;
  /**
   * 分组编码
   */
  groupcode: string;
  /**
   * 参数节点字符串
   */
  nodestr: string;
  /**
   * 对象ID/编码
   */
  objcode?: string;
  /**
   * 对象名称
   */
  objname?: string;
  /**
   * 操作类型
   */
  optype: string;
  /**
   * 参数名
   */
  paramname: string;
  /**
   * 操作时间
   */
  tm: string;
}

/**
 * 参数值日志查询结果DTO
 */
export interface ParamValueLogQueryResultDto {
  /**
   * 分组编码
   */
  items: ParamItemDto[];
  /**
   * 对象ID/编码
   */
  logs: ParamValueLogDto[];
}
