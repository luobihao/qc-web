/**
 * 数据库表SQL语句DTO
 */
export interface DBTableSqlDto {
  /**
   * SQL语句代码，语句代码
   */
  code: string;
  /**
   * 查询结果编辑标记，查询结果编辑标记，查询的数据是否可以进行录入、修改、删除等操作
   */
  editflag: number;
  /**
   * 状态标记
   */
  flag?: string;
  /**
   * 语句名称，用于UI中展示的名称
   */
  name: string;
  /**
   * 排序号
   */
  odr: number;
  /**
   * 语句操作类型，查询、修改、删除
   */
  optype: number;
  /**
   * SQL语句，对应数据库表中的实际SQL语句
   */
  sqlstr: string;
  /**
   * 表代码，对应数据库中实际的表名
   */
  tbcode: string;
}

/**
 * 数据库表SQL语句DTO集合
 */
export type DBTableSqlDtoList = DBTableSqlDto[];

/**
 * 数据库表SQL语句简要信息DTO
 */
export interface DBTableSqlSimpleDto {
  /**
   * SQL语句代码，语句代码
   */
  code: string;
  /**
   * 查询结果编辑标记，查询结果编辑标记，查询的数据是否可以进行录入、修改、删除等操作
   */
  editflag: number;
  /**
   * 语句名称，用于UI中展示的名称
   */
  name: string;
  /**
   * 语句操作类型，查询、修改、删除
   */
  optype: number;
  /**
   * 表代码，对应数据库中实际的表名
   */
  tbcode: string;
}

/**
 * 数据库表SQL语句简要DTO集合
 */
export type DBTableSqlSimpleDtoList = DBTableSqlSimpleDto[];
