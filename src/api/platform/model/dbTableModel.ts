import { DBDto } from './dbModel';
import { DBTableSqlDto } from './dbtableSqlModel';
import { DBTableColumnDto } from './dbtableColumnModel';

/**
 * 数据库表DTO
 */
export interface DBTableDto {
  /**
   * 表代码，对应数据库中实际的表名
   */
  code: string;
  /**
   * 分类编码，在特定模块中根据分类查询分类中的表，如用户自定义数据维护表、监测系统数据交换表、工作流表单表等
   */
  ctgcode: string;
  /**
   * 数据库编码
   */
  db: string;
  /**
   * 描述
   */
  description?: string;
  /**
   * 表名称，用于UI中展示的名称
   */
  name: string;
}

/**
 * 数据库表信息DTO列表
 */
export type DBTableDtoList = DBTableDto[];

/**
 * 数据库表查询条件DTO
 */
export interface DBTableQueryConditionDto {
  /**
   * 分类编码，为空时表示查询所有分类，不为空时分类编码与指定相同
   */
  ctgcode?: string;
  /**
   * 关键字，模糊匹配表代码、表名称
   */
  keywords?: string;
}

/**
 * 数据库表完整信息DTO
 */
export interface DBTableFullDto {
  columns: DBTableColumnDto[];
  db: DBDto;
  sqls: DBTableSqlDto[];
  table: DBTableDto;
}
