/**
 * 树节点DTO
 */
export interface QCTreeNode {
  //值：id
  key: string;
  //文字：显示名称、标题
  title: string;
  //图标
  icon?: string;
  //是否禁用
  disabled: boolean;
  //是否是叶子节点
  isLeaf: boolean;
  //是否可选
  // selectable: boolean;
  //禁掉checkbox
  // disableCheckbox: boolean;
  //设置独立节点是否展示checkbox
  // checkable: boolean;
  //子节点
  children: QCTreeNode[];
}

/**
 * 树节点集合
 */
export type TreeNodeList = QCTreeNode[];

/**
 * 树结构数据Dto
 */
export interface TreeDataDto {
  //树节点集合
  nodes: TreeNodeList;
  //已经关联的节点key
  checkedKeys: string[];
}

/**
 * 渠成键值对Dto，可以使用attribute携带参数等其他数据
 */
export interface KeyValuePairDto {
  /**
   * 属性，可以为空；用于将需要的信息传递值前端或后台
   */
  attribute?: string;
  /**
   * 键
   */
  key: string;
  /**
   * 值
   */
  value?: string;
}
