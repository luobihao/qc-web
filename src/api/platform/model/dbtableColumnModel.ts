import { KeyValuePairDto } from './qcPublicModel';

/**
 * 数据库表列DTO
 */
export interface DBTableColumnDto {
  /**
   * 列代码，对应数据库表中的实际列名
   */
  code: string;
  /**
   * 渠成列类型，定义列中存储的对象类型标识，id、名称、写入时间、更新时间、参数值等
   */
  coltype: number;
  /**
   * 数据类型，字符串、日期时间、整数、小数、布尔、枚举、BLOB
   */
  datatype: number;
  /**
   * 默认值，可以指定GUID、now、固定的数值或字符串
   */
  defaultval?: string;
  /**
   * 描述信息
   */
  description?: string;
  /**
   * 编辑表单中的宽度，编辑表单中的宽度，取值1-24
   */
  efcolspan: number;
  /**
   * 修改时重置值，修改数据时是否重置该列的值，使用默认值重置
   */
  efreset: boolean;
  /**
   * 是否在表单中显示，是否在编辑表单中显示
   */
  efshow: boolean;
  /**
   * 格式化字符串，前端显示的格式化字符串
   */
  formatstr?: string;
  /**
   * 是否为主键
   */
  isprimary: boolean;
  /**
   * 长度
   */
  length?: number;
  /**
   * 列名称，用于UI中展示的名称
   */
  name: string;
  /**
   * 是否非空
   */
  notnull: boolean;
  /**
   * 排序号，显示时的列顺序
   */
  qrodr?: number;
  /**
   * 供选择的数据，列为枚举、数据字典、对象等时后台返回的供选择的数据集合；
   */
  options?: KeyValuePairDto[];
  /**
   * 查询条件比较类型，查询条件的比较类型，等于、like、范围等
   */
  qccmptype: number;
  /**
   * 是否作为查询条件，是否作为查询条件，在查询条件表单中显示
   */
  qcflag: boolean;
  /**
   * 非空查询条件，是否为必须查询条件
   */
  qcnotnull: boolean;
  /**
   * 查询时使用升序排序，查询时是否使用升序排序，为true表示升序，为false表示降序，null表示该列不作为排序列
   */
  qrascorder?: string;
  /**
   * 图表显示配置，可以使用图表显示的配置，如配置默认使用的图表类型，支持显示的图表类型
   */
  qrcharts?: string;
  /**
   * 固定显示位置，固定显示位置，默认为空，可以取值left或right
   */
  qrfixed?: string;
  /**
   * 查询时排序顺序，查询时的排序顺序号，按顺序号从小到大排序
   */
  qrorderidx: number;
  /**
   * 是否在表格中显示，是否在前端查询结果中显示
   */
  qrshow: boolean;
  /**
   * 查询结果中显示宽度，查询结果中显示宽度
   */
  qrwidth?: number;
  /**
   * 表代码，对应数据库中实际的表名
   */
  tbcode: string;
  /**
   * 单位，显示在表格和表单中时的单位
   */
  unit?: string;
  /**
   * 取值范围，可以使用0-9表示范围， 也可以使用逗号分隔表示序列，枚举或数据字典名称
   */
  valrange?: string;
}

/**
 * 数据库表列DTO集合
 */
export type DBTableColumnDtoList = DBTableColumnDto[];
