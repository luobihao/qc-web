import { ParamGroupQueryConditionDto, KeyValueTypeSelectList, ParamGroupDto, ParamItemDto, ParamItemDtoResult, ParamItemQueryConditionDto, ParamGroupDtoResult } from './model/paramModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
    QueryGroup = '/qc/param/querygroup',
    GetGroup = '/qc/param/getgroup',
    AddGroup = '/qc/param/addgroup',
    UpdateGroup = '/qc/param/updategroup',
    DeleteGroup = '/qc/param/deletegroup',
    GetGroupItems = '/qc/param/getgroupitems',
    GetItem = '/qc/param/getitem',
    AddItem = '/qc/param/additem',
    UpdateItem = '/qc/param/updateitem',
    DeleteItem = '/qc/param/deleteitem',
    getTypeSelect = '/qc/param/getoptions'
}

/**
 * @description 查询所有分组
 * @param params 
 * @param mode 
 * @returns 
 */
export function queryGroup(params: ParamGroupQueryConditionDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<ParamGroupDtoResult>(
        {
            url: Api.QueryGroup,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 查询指定id参数分组-GetGroup
 * @param id 
 * @param mode 
 * @returns 
 */
export function getGroup(id: String, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<ParamGroupDtoResult>(
        {
            url: Api.GetGroup,
            params: {
                id: id
            }
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 新增分组
 * @param params 
 * @param mode 
 * @returns 
 */
export function addGroup(params: ParamGroupDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.AddGroup,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 修改分组
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateGroup(params: ParamGroupDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.put<string>(
        {
            url: Api.UpdateGroup,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteGroup(id: string, mode: ErrorMessageMode = 'modal') {
    return defHttp.delete<string>(
        {
            url: Api.DeleteGroup,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
            joinParamsToUrl: true,
        },
    );
}


/**
 * @description 查询分组参数项
 * @param params 
 * @param mode 
 * @returns 
 */
export function getGroupItems(params: ParamItemQueryConditionDto, mode: ErrorMessageMode = 'modal') {
    if (!params.groupcode) return;
    return defHttp.post<ParamItemDtoResult>(
        {
            url: Api.GetGroupItems,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 查询指定id参数分组的参数项
 * @param id 
 * @param mode 
 * @returns 
 */
export function getItem(id: String, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<ParamItemDtoResult>(
        {
            url: Api.GetItem,
            params: {
                id: id
            }
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 新增参数项
 * @param params 
 * @param mode 
 * @returns 
 */
export function addItem(params: ParamItemDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.AddItem,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 修改参数项
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateItem(params: ParamItemDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.put<string>(
        {
            url: Api.UpdateItem,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteItem(id: string, mode: ErrorMessageMode = 'modal') {
    return defHttp.delete<string>(
        {
            url: Api.DeleteItem,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
            joinParamsToUrl: true,
        },
    );
}

/**
 * @description 获取参数项的数据选择项
 * @param mode 
 * @returns 
 */
export function getAllTypeSelect(mode: ErrorMessageMode = 'modal') {
    return defHttp.get<KeyValueTypeSelectList>(
        {
            url: Api.getTypeSelect,
        },
        {
            errorMessageMode: mode,
        },
    );
}