import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { BlackListQueryConditionDto, BlackListDto, BlackListAddDto } from './model/blackListModel'

enum Api {
  Query = '/qc/blacklist/query', //查询Ip地址黑名单-query
  Add = '/qc/blacklist/add', //新增Ip地址黑名单-add
  SetExpired = '/qc/blacklist/expired', // 设置为过期-setExpired
  refresh = '//qc/gateway/blacklist-refresh', //刷新黑名单
}


/**
 * @description 查询Ip地址黑名单-query
 */
export function queryBlackList(params: BlackListQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<BlackListDto[]>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增Ip地址黑名单-add
 */
export function addBlackList(params: BlackListAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 设置为过期-setExpired
 */
export function setBlackListExpired(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.SetExpired,
      params: {
        id: id
      },
    },
    {
      errorMessageMode: mode,
    }
  )
}

/**
 * @description 刷新黑名单
 */
export function refreshBlackList(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.refresh,
    },
    {
      errorMessageMode: mode,
    }
  )
}
