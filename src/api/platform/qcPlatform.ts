import { defHttp } from '/@/utils/http/axios';
import { DeptSimpleDto } from '/@/api/project/model/deptModel';
import { ProjectParamValueConditionDto } from './model/qcProjectModel';
import { ObjectParamValuesDto, ParamValueLogQueryConditionDto } from './model/paramValueModel';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  // GetProject = '/qc/project/get',
  GetProjectParamValue = '/qc/project/paramvalue',
  GetDefaultProjectValidDepts = '/qc/dept/valid',
}

/**
 * @description: 获取指定项目的显示信息--从项目的参数配置中获取
 */
export function getProjectDisplayParamValues(
  projectCode: string,
  mode: ErrorMessageMode = 'modal',
) {
  let params: ProjectParamValueConditionDto = {
    projectcode: projectCode,
    groupcode: 'qc_project_displayinfo'//分组编码固定为：qc_project_displayinfo
  }
  return defHttp.post<ObjectParamValuesDto>(
    {
      url: Api.GetProjectParamValue,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取平台中的默认（如果有多个项目则为第一个项目）项目中的所有有效部门
 */
export function getDefaultProjectValidDepts(projectCode?: string, mode: ErrorMessageMode = 'none') {
  return defHttp.get<DeptSimpleDto[]>(
    {
      url: Api.GetDefaultProjectValidDepts,
      params: { id: projectCode },
    },
    {
      errorMessageMode: mode,
    },
  );
}
