import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { EnumDto } from './model/enumModel';

enum Api {
  QueryEnum = '/qc/enum/getenumoptions',
}

/**
 * @description: 查询枚举类集合
 * @param id 参数为定义的枚举类名称，多个使用英文逗号分隔
 */
export function queryEnumOptions(id: String, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<EnumDto>(
    {
      url: Api.QueryEnum,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}
