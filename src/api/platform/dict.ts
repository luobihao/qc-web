import { DictCategoryDto, DictCategoryResult, DictCategoryQueryConditionDto, DictItemResult, DictItemDto, DictItemQueryConditionDto } from './model/dictModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
    QueryCategory = '/qc/dict/queryctg',
    GetCategory = '/qc/dict/getctg',
    AddCategory = '/qc/dict/addctg',
    UpdateCategory = '/qc/dict/updatectg',
    DeleteCategory = '/qc/dict/deletectg',
    GetCategoryItems = '/qc/dict/queryitems',
    GetItem = '/qc/dict/getitem',
    AddItem = '/qc/dict/additem',
    UpdateItem = '/qc/dict/updateitem',
    DeleteItem = '/qc/dict/deleteitem',
}

/**
 * @description 查询字典分类
 * @param params 
 * @param mode 
 * @returns 
 */
export function queryCategory(params: DictCategoryQueryConditionDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<DictCategoryResult>(
        {
            url: Api.QueryCategory,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 查询指定id分类-GetCategory
 * @param id 
 * @param mode 
 * @returns 
 */
export function getCategory(id: String, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<DictCategoryDto>(
        {
            url: Api.GetCategory,
            params: {
                id: id
            }
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 新增分类
 * @param params 
 * @param mode 
 * @returns 
 */
export function addCategory(params: DictCategoryDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.AddCategory,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 修改字典分类
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateCategory(params: DictCategoryDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.put<string>(
        {
            url: Api.UpdateCategory,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteCategory(id: string, mode: ErrorMessageMode = 'modal') {
    return defHttp.delete<string>(
        {
            url: Api.DeleteCategory,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
            joinParamsToUrl: true,
        },
    );
}


/**
 * @description 查询字典项
 * @param params 
 * @param mode 
 * @returns 
 */
export function getCategoryItems(params: DictItemQueryConditionDto, mode: ErrorMessageMode = 'modal') {
    if (!params.ctgcode) return;
    return defHttp.post<DictItemResult>(
        {
            url: Api.GetCategoryItems,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 查询指定id字典分类字典项
 * @param id 
 * @param mode 
 * @returns 
 */
export function getItem(id: String, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<DictItemDto>(
        {
            url: Api.GetItem,
            params: {
                id: id
            }
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description 新增字典项
 * @param params 
 * @param mode 
 * @returns 
 */
export function addItem(params: DictItemDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.AddItem,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 修改参数项
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateItem(params: DictItemDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.put<string>(
        {
            url: Api.UpdateItem,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}


/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteItem(id: string, mode: ErrorMessageMode = 'modal') {
    return defHttp.delete<string>(
        {
            url: Api.DeleteItem,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
            joinParamsToUrl: true,
        },
    );
}
