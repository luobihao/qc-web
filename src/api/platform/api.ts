import { APIQueryConditionDto, APIDto, ApiResult } from './model/apiModel';
import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  Query = '/qc/api/query',
  QueryTree = '/qc/api/tree',
  Get = '/qc/api/get',
  Add = '/qc/api/add',
  Update = '/qc/api/update',
  Delete = '/qc/api/delete',
  Import = '/qc/api/import',
  GetGroup = '/qc/api/getgroups'
}

/**
 * @description: 获取所有Api数据
 */
export function queryAllApi(params: APIQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ApiResult>(
    {
      url: Api.Query,
      params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取所有Api数据(树形结构)
 */
export function queryApiTree(params: APIQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ApiTreeResult>(
    {
      url: Api.QueryTree,
      params
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 根据指定的id查询Api
 * @param id
 * @param mode 
 * @returns 
 */
export function getApi(id: String, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<ApiDto>(
    {
      url: Api.Get,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description Api新增
 * @param params 
 * @param mode 
 * @returns 
 */
export function addApi(params: ApiDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<String>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description 修改Api
 * @param params 
 * @param mode 
 * @returns 
 */
export function updateApi(params: ApiDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 * @param id 
 * @param mode 
 * @returns 
 */
export function deleteApi(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**pi
 * @description 导入
 */
export function importApi(params: ApiResult, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Import,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}



/**
 * @description 获取接口分组
 */
export function getApiGroup(mode: ErrorMessageMode = 'modal') {
  return defHttp.get(
    {
      url: Api.GetGroup,
    },
    {
      errorMessageMode: mode,
    },
  );
}
