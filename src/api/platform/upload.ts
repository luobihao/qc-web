import { UploadApiResult } from './model/uploadModel';
import { defHttp } from '/@/utils/http/axios';
import { UploadFileParams } from '/#/axios';
import { useGlobSetting } from '/@/hooks/setting';

const { uploadUrl = '' } = useGlobSetting();

/**
 * @description: Upload interface
 */
export function uploadApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  return defHttp.uploadFile<UploadApiResult>(
    {
      // url: 'http://localhost:8088/qc/file/upload', //uploadUrl,
      url: uploadUrl + '/qc/file/upload',
      timeout: 5 * 60 * 1000,
      onUploadProgress,
    },
    params,
  );
}
