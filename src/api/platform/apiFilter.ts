import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { ApiFilterDto, ApiFilterDtoList, ApiFilterQueryConditionDto } from './model/apiFilterModel';

enum Api {
  Query = '/qc/apifilter/query',
  Get = '/qc/apifilter/get',
  Add = '/qc/apifilter/add',
  Update = '/qc/apifilter/update',
  Delete = '/qc/apifilter/delete',
  Enable = '/qc/apifilter/enable',
  Disable = '/qc/apifilter/disable',
  refresh = '//qc/gateway/apifilter-refresh'  //刷新日志过滤规则
}

/**
 * @description 查询
 */
export function query(params: ApiFilterQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ApiFilterDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 获取指定
 */
export function getApiFilter(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<ApiFilterDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增
 */
export function addApiFilter(params: ApiFilterDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改
 */
export function updateApiFilter(params: ApiFilterDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteApiFilter(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 启用
 */
export function enableApiFilter(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Enable,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 禁用
 */
export function disableApiFilter(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Disable,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description 刷新日志过滤规则
 */
export function refreshApiFilter(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<string>(
    {
      url: Api.refresh,
    },
    {
      errorMessageMode: mode,
    }
  )
}
