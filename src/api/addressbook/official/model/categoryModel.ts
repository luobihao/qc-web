/**
 * 公务号码分类DTO
 */
export interface OfficialCategoryDto {
  //分类ID
  id: number;
  //分类名称
  name: string;
  //状态
  flag: number;
  //排序号
  odr: number;
}

/**
 * 公务号码分类DTO集合
 */
export type OfficialCategoryDtoList = OfficialCategoryDto[];
