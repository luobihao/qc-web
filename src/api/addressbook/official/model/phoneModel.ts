/**
 * 公务号码信息DTO
 */
export interface OfficialPhoneDto {
  //ID
  id: number;
  //所属分类ID
  categoryId: number;
  //分类名称
  name: string;
  //号码
  phone: string;
  //备注
  notes: string;
  //状态
  flag: number;
  //排序号
  odr: number;
}

/**
 * 公务号码信息DTO集合
 */
export type OfficialPhoneDtoList = OfficialPhoneDto[];

/**
 * 设置公务号码所属分类条件DTO
 */
export interface SetCategoryIdConditionDto {
  //公务号码ID集合
  ids: number[] | string[];
  //分类ID
  categoryId: number;
}

/**
 * 公务号码信息查询条件DTO
 */
export interface OfficialPhoneConditionDto {
  //分类ID
  categoryId: string;
  //关键字
  keywords: string;
}
