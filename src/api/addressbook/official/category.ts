import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { OfficialCategoryDto, OfficialCategoryDtoList } from './model/categoryModel';

enum Api {
  GetAll = '/qc/officialcategory/all',
  Get = '/qc/officialcategory/get',
  Add = '/qc/officialcategory/add',
  Update = '/qc/officialcategory/update',
  Delete = '/qc/officialcategory/delete',
}

/**
 * @description 获取所有
 */
export function getAll(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<OfficialCategoryDtoList>(
    {
      url: Api.GetAll,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定
 */
export function getById(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<OfficialCategoryDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function add(params: OfficialCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改
 */
export function update(params: OfficialCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteCategoryById(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
