import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  OfficialPhoneDto,
  OfficialPhoneDtoList,
  OfficialPhoneConditionDto,
  SetCategoryIdConditionDto,
} from './model/phoneModel';

enum Api {
  Query = '/qc/officialphone/query',
  Get = '/qc/officialphone/get',
  Add = '/qc/officialphone/add',
  Update = '/qc/officialphone/update',
  Delete = '/qc/officialphone/delete',
  SetCategory = '/qc/officialphone/setCategory',
}

/**
 * @description 查询
 */
export function query(params: OfficialPhoneConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<OfficialPhoneDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定
 */
export function getById(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<OfficialPhoneDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function add(params: OfficialPhoneDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改
 */
export function update(params: OfficialPhoneDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteById(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description 设置分类
 */
export function setCategory(params: SetCategoryIdConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetCategory,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
