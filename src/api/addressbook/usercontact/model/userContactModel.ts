/**
 * 用户联系信息DTO
 */
export interface UserContactDto {
  //用户ID
  id: number;
  //名称
  name: string;
  //手机号码
  phone: string;
  //短号
  cornet: string;
  //办公室/座机号码
  tel: string;
  //职务
  duty: string;
}

/**
 * 用户联系信息DTO集合
 */
export type UserContactDtoList = UserContactDto[];

/**
 * 用户联系信息查询条件DTO
 */
export interface UserContactQueryConditionDto {
  //项目编码：查询指定项目中的用户
  project: string;
  //部门ID：可以查询指定部门下的用户
  dept?: string;
  //关键字
  words?: string;
}
