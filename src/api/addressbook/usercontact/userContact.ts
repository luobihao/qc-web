import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { UserContactQueryConditionDto, UserContactDtoList } from './model/userContactModel';

enum Api {
  Query = '/qc/usercontact/query',
}

/**
 * @description 查询用户联系信息
 */
export function query(params: UserContactQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<UserContactDtoList>(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
