/**
 * 数据，分表头、数据行
 *
 * QCTable
 */
export interface QCTable {
    /**
     * 表格内容行集合
     */
    bodyRows: QCTableRow[];
    /**
     * 表头行集合
     */
    headerRows: QCTableRow[];
}

/**
 * QCTableRow
 */
export interface QCTableRow {
    /**
     * 单元格集合
     */
    cells: QCTableCell[];
}

/**
 * QCTableCell
 */
export interface QCTableCell {
    /**
     * 合并列数量，默认为1
     */
    colspan: number;
    /**
     * 合并行数量，默认为1
     */
    rowspan: number;
    /**
     * 单元格样式，css样式集合
     */
    style?: string;
    /**
     * 单元格内容
     */
    text?: string;
}