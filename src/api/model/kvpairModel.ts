/**
 * 键值对DTO
 */
export interface KeyValuePairDto {
    /**
     * 属性，可以为空；用于将需要的信息传递值前端或后台
     */
    attribute?: string;
    /**
     * 键
     */
    key: string;
    /**
     * 值
     */
    value?: string;
}