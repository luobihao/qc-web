import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  CategoryQueryConditionDto,
  CategoryListSimple,
  CategoryListDetail,
  CategoryTreeSimple,
  CategoryTreeDetail,
  CategoryAddDto,
  CategoryDto,
  CategoryDeptSetDto,
  CategoryIds,
  CategoryDeptList,
  CategoryTreeTableDto,
} from './model/categoryModel';

/**
 * @description 后台接口Api
 */
enum Api {
  QueryListSimple = '/cms/category/listsimple',
  QueryValidListSimple = '/cms/category/getvalidlistsimple',
  QueryListDetail = '/cms/category/listdetail',
  QueryTreeSimple = '/cms/category/treesimple',
  QueryTreeDetail = '/cms/category/treedetail',
  QueryTreeTableDetail = '/cms/category/treetabledetail',
  GetCategory = '/cms/category/get',
  Add = '/cms/category/add',
  Update = '/cms/category/update',
  Delete = '/cms/category/delete',
  GetDepts = '/cms/category/getdepts',
  SetDepts = '/cms/category/depts',
}

/**
 * @description 查询栏目列表简要信息
 */
export function queryListSimple(
  params: CategoryQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<CategoryListSimple>(
    {
      url: Api.QueryListSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询栏目列表简要信息,返回用户所在部门有权限的栏目、不按部门控制权限的栏目、有子栏目权限但父栏目不在返回结果中的栏目
 */
export function queryValidListSimple(
  params: CategoryQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<CategoryListSimple>(
    {
      url: Api.QueryValidListSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询栏目列表详细信息
 */
export function queryListDetail(
  params: CategoryQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<CategoryListDetail>(
    {
      url: Api.QueryListDetail,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询栏目树形简要信息
 */
export function queryTreeSimple(
  params: CategoryQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<CategoryTreeSimple>(
    {
      url: Api.QueryTreeSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询栏目树形详细信息
 */
export function queryTreeDetail(
  params: CategoryQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<CategoryTreeDetail>(
    {
      url: Api.QueryTreeDetail,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询栏目树表详细信息
 */
export function queryTreeTableDetail(
  params: CategoryQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<CategoryTreeTableDto>(
    {
      url: Api.QueryTreeTableDetail,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定栏目信息
 */
export function getCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<CategoryDto>(
    {
      url: Api.GetCategory,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增栏目信息
 */
export function addCategory(params: CategoryAddDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改栏目信息
 */
export function updateCategory(params: CategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除栏目信息
 */
export function deleteCategory(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description 获取栏目部门配置
 */
export function getDepts(params: CategoryIds, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<CategoryDeptList>(
    {
      url: Api.GetDepts,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 设置栏目部门关联
 */
export function setDepts(params: CategoryDeptSetDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.SetDepts,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
