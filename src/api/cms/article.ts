import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  Query = '/cms/article/query',
  Audit = '/cms/article/queryaudit',
  Get = '/cms/article/get',
  View = '/cms/article/view',
  Save = '/cms/article/save',
  Submit = '/cms/article/audit',
  Pass = '/cms/article/pass',
  NoPass = '/cms/article/nopass',
  Publish = '/cms/article/publish',
  Recall = '/cms/article/recall',
  Delete = '/cms/article/delete',
  AddTag = '/cms/article/addtag',
  RemoveTag = '/cms/article/removetag',
  Logs = '/cms/article/logs',
  MoveToCategory = '/cms/article/movetocategory',
  QueryWordArticle = '/cms/article/querysensitivewords',
  ReplaceArticleWord = '/cms/article/replacesensitivewords'
}

import {
  ArticleQueryConditionDto,
  ArticleSaveDto,
  ArticleOperateDto,
  MoveToCategoryDto,
  SensitiveWordsQueryConditionDto,
  ReplaceSensitiveWordsDto,
} from './model/articleModel';

// 文章查询
export function queryArticles(params: ArticleQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Query,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

// 文章  待审核
export function queryAuditArticles(
  params: ArticleQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post(
    {
      url: Api.Audit,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

// 文章  获取指定;文章浏览次数不加1
export function getArticle(params: { id: number | string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get(
    {
      url: Api.Get,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

// 文章  查看;文章浏览次数加1（在文章状态为已发布情况下）
export function viewArticle(params: { id: number | string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get(
    {
      url: Api.View,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

// 文章  保存
export function saveArticle(data: ArticleSaveDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Save,
      data,
    },
    {
      errorMessageMode: mode,
    },
  );
}

//  文章  提交审核
export function submmitArticle(data: { id: number | string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Submit,
      data,
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

//  文章  审核通过
export function passArticle(data: { id: number | string, isreview: boolean }, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Pass,
      data,
    },
    {
      errorMessageMode: mode,
    },
  );
}
//  文章  审核不通过
export function nopassArticle(data: ArticleOperateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.NoPass,
      data,
    },
    {
      errorMessageMode: mode,
    },
  );
}
//  文章  发布
export function publishArticle(data: { id: number | string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Publish,
      data,
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
//  文章  撤回
export function recallArticle(data: ArticleOperateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Recall,
      data,
    },
    {
      errorMessageMode: mode,
    },
  );
}

//  文章  删除
export function deleteArticle(data: ArticleOperateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Delete,
      data,
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
      joinTime: false,
    },
  );
}

//  添加文章标签
export function addArticleTag(data: ArticleOperateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.AddTag,
      data,
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

//  删除文章标签
export function removeArticleTag(data: ArticleOperateDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.RemoveTag,
      data,
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

// 文章  查看日志
export function getArticleLogs(params: { id: number | string }, mode: ErrorMessageMode = 'modal') {
  return defHttp.get(
    {
      url: Api.Logs,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * 移动文章到指定栏目下
 */
export function moveArticlesToCategory(data: MoveToCategoryDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.MoveToCategory,
      data,
    },
    {
      errorMessageMode: mode,
    },
  );
}

//
export function querySensitiveWords(params: SensitiveWordsQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.QueryWordArticle,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

//   敏感词替换
export function replaceWord(data: ReplaceSensitiveWordsDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.ReplaceArticleWord,
      data,
    },
    {
      errorMessageMode: mode,
    },
  );
}
