import { LinkModel } from './model/linkModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  Query = '/cms/link/query',
  Add = '/cms/link/add',
  Edit = '/cms/link/update',
  Delete = '/cms/link/delete',
  Get = '/cms/link/get',
}

// 链接查询
export function queryLinks(params: { id: number }) {
  return defHttp.get(
    {
      url: Api.Query,
      params,
    },
    {
      joinParamsToUrl: true,
      joinTime: false,
    },
  );
}

// 链接新增
export function addLink(data: LinkModel) {
  return defHttp.post({
    url: Api.Add,
    data,
  });
}

// 链接修改
export function editLink(data: LinkModel) {
  return defHttp.put({
    url: Api.Edit,
    data,
  });
}

// 链接删除
export function deleteLink(params: { id?: string }) {
  return defHttp.delete(
    {
      url: Api.Delete,
      params,
    },
    {
      joinParamsToUrl: true,
    },
  );
}

// 链接获取指定
export function getLink(params: { id: number }) {
  return defHttp.get({
    url: Api.Get,
    params,
  });
}
