import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { SecurityLedgerDto, SecurityLedgerListDto } from './model/securityLedgerModel';

enum Api {
    Query = '/cms/securityledger/query',
    Get = '/cms/securityledger/get',
    Add = '/cms/securityledger/add',
    Update = '/cms/securityledger/update',
    Delete = '/cms/securityledger/delete',
    Date = '/cms/securityledger/date',
}

/**
 * @description 查询
 */
export function getAllSecurityLedger(mode: ErrorMessageMode = 'modal') {
    return defHttp.get<SecurityLedgerListDto>(
        {
            url: Api.Query,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 获取指定日期事故集合
 */
export function getSecurityLedgerByDate(id?: string, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<SecurityLedgerListDto>(
        {
            url: Api.Date,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 获取指定
 */
export function getSecurityLedger(id: number | string, mode: ErrorMessageMode = 'modal') {
    return defHttp.get<SecurityLedgerDto>(
        {
            url: Api.Get,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description 新增
 */
export function addSecurityLedger(params: SecurityLedgerDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.post<string>(
        {
            url: Api.Add,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 修改
 */
export function updateSecurityLedger(params: SecurityLedgerDto, mode: ErrorMessageMode = 'modal') {
    return defHttp.put<string>(
        {
            url: Api.Update,
            params,
        },
        {
            errorMessageMode: mode,
        },
    );
}

/**
 * @description: 删除
 */
export function deleteSecurityLedger(id: number, mode: ErrorMessageMode = 'modal') {
    return defHttp.delete<string>(
        {
            url: Api.Delete,
            params: { id: id },
        },
        {
            errorMessageMode: mode,
            joinParamsToUrl: true,
        },
    );
}
