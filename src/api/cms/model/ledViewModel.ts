/**
 * LedViewDto
 */
export interface LedViewDto {
  /**
   * 开始时间
   */
  begintm?: string;
  /**
   * 内容，前端自动处理，数据库中保存json字符串
   */
  content?: string;
  /**
   * 结束时间
   */
  endtm?: string;
  /**
   * 是否固定，固定显示配置的画面，不在网站和负荷曲线之间轮换
   */
  fixed: boolean;
  /**
   * ID，画面ID，唯一标识
   */
  id: number;
  /**
   * 画面名称
   */
  name: string;
}

/**
 * 列表
 */
export type LedViewListDto = LedViewDto[];
