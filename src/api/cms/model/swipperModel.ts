/**
 * 轮播图查询条件DTO
 */
export interface SwipperQueryConditionDto {
  /**
   * 栏目ID
   */
  cid: number | string;
  /**
   * 数量，按ID值降序获取前N个
   */
  count: number;
}
/**
 * 轮播图信息DTO
 */
export interface SwipperDto {
  //ID
  id: number;

  //栏目ID
  cid: number;

  //分组ID，0表示栏目中的轮播图，大于1的值表示自定义的分组号
  groupid: number;

  //文件ID
  fid: number;

  //显示标题文字
  title: string;

  //排序号
  odr: string;

  //状态标记
  flag: number;

  //文章ID
  articleid: number;

  //是否跳转栏目；用于标识点击轮播图跳转至栏目还是文章，统一使用articleid存储栏目ID或文章ID
  iscategory: boolean;
}

/**
 * 轮播图列表
 */
export type SwipperListDto = SwipperDto[];

export interface SwipperList {
  items: SwipperListDto;
}

/**
 * 启用/禁用轮播图DTO
 */
export interface EditFlagDto {
  //轮播图id
  id: number;
  //状态值，true-启用，false-禁用
  flagStatus: boolean;
}
