/**
 * 链接DTO
 */
export type LinkModel = {
  cid: number;
  flag: number;
  id?: number;
  name: string;
  odr?: number;
  url: string;
};
