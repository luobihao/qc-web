import { ArticleDto } from './articleModel';

/**
 * 文章统计条件DTO
 */
export interface ArticleStatisticsConditionDto {
  //开始时间
  begintm: string;

  //截止时间
  endtm: string;

  //统计的栏目id集合，可以为空，为空表示统计全部栏目
  ids: number[];

  //作者
  author: string;

  //部门
  dept: string;
}

/**
 * 文章按作者统计条目DTO
 */
export interface ArticleStatisticsAuthorItemDto {
  //发布人名称
  name: string;

  //发布文章数量
  count: number;
}

/**
 * 文章按部门统计条目DTO
 */
export interface ArticleStatisticsDeptItemDto {
  //部门名称
  name: string;

  //发布文章数量
  count: number;
}

//按作者统计
export type authors = ArticleStatisticsAuthorItemDto[];
//按部门统计
export type depts = ArticleStatisticsDeptItemDto[];

/**
 * 文章统计结果DTO
 */
export interface ArticleStatisticsResultDto {
  authors: authors;
  depts: depts;
}

// ====================以下为新的统计接口使用DTO====================================

/**
 * 按发布人统计文章结果DTO
 */
export interface ArticleStatisticsByInUserResultDto {
  // 发布人名称
  name: string;
  // 发布文章数量
  count: number;
  // 文章列表
  articles: ArticleDto[];
}

/**
 * 按发布部门统计文章结果DTO
 */
export interface ArticleStatisticsByInDeptResultDto {
  // 发布部门名称
  name: string;
  // 发布文章数量
  count: number;
  // 按发布人统计文章数量列表
  users: ArticleStatisticsByInUserResultDto[];
}

/**
 * 按发布人统计文章图表展示结果DTO
 */
export interface ArticleStatisticsByInUserChartResultDto {
  //发布人名称
  name: string;
  //文章数量
  count: number;
}

/**
 * 文章多种方式统计结果DTO
 */
export interface ArticleMultiStatisticsResultDto {
  // 按发布人统计文章结果
  authors: ArticleStatisticsByInUserResultDto[];
  // 按发布部门统计文章结果
  depts: ArticleStatisticsByInDeptResultDto[];
  // 按发布人统计文章图表显示结果
  topAuthors: ArticleStatisticsByInUserChartResultDto[];
}

/**
 * 按发布人统计文章未合并行处理结果DTO
 */
export interface ArticleStatisticsByInUserNotMergedRowResultDto {
  //发布人发布文章数量
  inUserCount: number;
  //文章ID
  id: number;
  //栏目ID
  cid: number;
  //标题
  title: string;
  //内容类型
  ctype: number;
  //关键字
  keywords?: string;
  //内容
  content?: string;
  //附件，json格式
  attachment: string;
  //封面文件ID
  cover: number;
  //来源
  source?: string;
  //作者
  author?: string;
  //添加时间
  intm: string;
  //添加人
  inuser: string;
  //添加部门名称
  indept: string;
  //更新时间
  uptm?: string;
  //更新人
  upuser?: string;
  //标记
  flag: string;
  //标签 8表示置顶
  tags: number;
  //浏览次数
  count: number;
}

/**
 * 按发布部门统计文章未合并行处理结果DTO
 */
export interface ArticleStatisticsByInDeptNotMergedRowResultDto {
  //部门发布文章数量
  inDeptCount: number;
  //发布人发布文章数量
  inUserCount: number;
  //文章ID
  id: number;
  //栏目ID
  cid: number;
  //标题
  title: string;
  //内容类型
  ctype: number;
  //关键字
  keywords?: string;
  //内容
  content?: string;
  //附件，json格式
  attachment: string;
  //封面文件ID
  cover: number;
  //来源
  source?: string;
  //作者
  author?: string;
  //添加时间
  intm: string;
  //添加人
  inuser: string;
  //添加部门名称
  indept: string;
  //更新时间
  uptm?: string;
  //更新人
  upuser?: string;
  //标记
  flag: string;
  //标签 8表示置顶
  tags: number;
  //浏览次数
  count: number;
}

/**
 * 文章多种方式统计未合并行处理结果DTO
 */
export interface ArticleMultiStatisticsNotMergedRowResultDto {
  // 按发布人统计文章结果
  authors: ArticleStatisticsByInUserNotMergedRowResultDto[];
  // 按发布部门统计文章结果
  depts: ArticleStatisticsByInDeptNotMergedRowResultDto[];
  // 按发布人统计文章图表显示结果
  topAuthors: ArticleStatisticsByInUserChartResultDto[];
}
