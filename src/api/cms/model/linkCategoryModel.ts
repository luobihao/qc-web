/**
 * 链接分类DTO
 */
export type LinkCategoryModel = {
  id: number;
  name: string;
  flag: number;
  odr?: number;
};
