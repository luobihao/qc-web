/**
 * 文章查询条件DTO
 */
export interface ArticleQueryConditionDto {
  /**
   * 审核层级，在查询待审核时有效，为1表示查询待二审的文章列表，为其他表示查询待终审的文章列表
   */
  audit?: number;
  /**
   * 来源/作者
   */
  author?: string;
  /**
   * 起始时间
   */
  begintm?: string;
  /**
   * 栏目ID
   */
  cid?: number;
  /**
   * 截止时间
   */
  endtm?: string;
  /**
   * 是否有封面图片，为true表示只查询有封面图片ID的文章，为false表示不管是否有封面图片ID
   */
  hasCover?: boolean;
  /**
   * 关键字
   */
  keywords?: string;
  /**
   * 页码
   */
  page: number;
  /**
   * 每页数量
   */
  pageSize: number;
  [property: string]: any;
}

/**
 * 文章保存DTO
 */
export interface ArticleSaveDto {
  author?: string;
  cid: string;
  content?: string;
  cover?: string;
  ctype: string;
  id?: string;
  keywords?: string;
  source?: string;
  title?: string;
  attachment: string; //已经通过文件上传保存后返回的文件集合
}
/**
 * 文章操作DTO
 */
export interface ArticleOperateDto {
  id: number;
  msg?: string;
  op: number;
  isreview?: boolean;
}
/**
 * 文章Dto
 */
export interface ArticleDto {
  //文章ID
  id: number;
  //栏目ID
  cid: number;
  //标题
  title: string;
  //内容类型
  ctype: number;
  //关键字
  keywords?: string;
  //内容
  content?: string;
  //附件，json格式
  attachment: string;
  //封面文件ID
  cover: number;
  //来源
  source?: string;
  //作者
  author?: string;
  //添加时间
  intm: string;
  //添加人
  inuser: string;
  //添加部门名称
  indept: string;
  //更新时间
  uptm?: string;
  //更新人
  upuser?: string;
  //发布时间
  pubtm?: string;
  //发布人名称
  pubuser?: string;
  //发布部门名称
  pubdept?: string;
  //标记
  flag: string;
  //标签 8表示置顶
  tags: number;
  //浏览次数
  count: number;
}

/**
 * 文章DTO集合
 */
export type items = ArticleDto[];

export interface ArticleDtoList {
  items: items;
}

/**
 * 文章查询结果DTO
 */
export interface ArticleQueryResultDto {
  //总数
  total: number;
  //总页数
  pages: number;
  //文章DTO集合
  items: items;
}

/**
 * 移动文章到指定栏目DTO
 */
export interface MoveToCategoryDto {
  // 文章ID集合
  ids: number[];
  // 栏目ID
  cid: number;
}
/**
 * SensitiveWordsQueryConditionDto 敏感词文章查询条件
 */
export interface SensitiveWordsQueryConditionDto {
  /**
   * 开始时间
   */
  begintm: string;
  /**
   * 截止时间
   */
  endtm: string;
  /**
   * 敏感词集合
   */
  sensitivewords: string[];
  [property: string]: any;
}

/**
 * ReplaceSensitiveWordsDto 敏感词替换
 */
export interface ReplaceSensitiveWordsDto {
  /**
   * 文章ID集合
   */
  articleids: string[];
  /**
   * 敏感词与替换词对象
   */
  sensitivereplacewords: { [key: string]: any };
  [property: string]: any;
}
