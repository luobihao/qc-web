/**
 * SecurityLedgerDto
 */
export interface SecurityLedgerDto {
  /**
   * ID
   */
  id: number;
  /**
   * 发生日期
   */
  tm: string;
  /**
   * 责任单位/车间/部门
   */
  dept: string;
  /**
   * 事故分类
   */
  category: string;
  /**
   * 责任分类
   */
  cause: string;
  /**
   * 障碍等级
   */
  obstacle?: string;
  /**
   * 事件详情内容
   */
  content: string;
}

/**
 * 列表
 */
export type SecurityLedgerListDto = SecurityLedgerDto[];
