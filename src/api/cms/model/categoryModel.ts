/**
 * 栏目查询条件DTO
 */
export interface CategoryQueryConditionDto {
  //是否包含所有，为false时不查询已删除或者禁用的栏目
  isall: boolean;

  //是否根据部门访问权限，为true时根据用户所在部门进行过滤
  isdept: boolean;

  //是否包含在菜单上不显示的栏目
  isvisible: boolean;
}

/**
 * 栏目简要信息DTO
 */
export interface CategorySimpleDto {
  //栏目id，数字编号
  id: number;

  //父栏目id
  pid: number;

  //栏目名称
  name: string;

  //层级（栏目的层级从1开始）
  level: number;

  //栏目类型
  ctype: number;

  //栏目内容（外部链接直接放链接地址）
  content: string;

  //是否按部门控制权限，默认为true
  deptauth: boolean;

  // 是否有发文权限
  candraft: boolean;
}


/**
 * CategoryDto 栏目详细信息DTO
 */
export interface CategoryDto {
  /**
   * 文章显示配置，json格式
   */
  article?: string;
  /**
   * 文章发布需要审核层级数，取值0、1、2，默认为1
   */
  auditcount: number;
  /**
   * 是否允许在栏目中发文
   */
  candraft: boolean;
  /**
   * 栏目内容，外部链接直接放链接地址
   */
  content: null | string;
  /**
   * 封面图片ID
   */
  cover?: number;
  /**
   * 栏目类型
   */
  ctype: number;
  /**
   * 是否按部门控制权限
   */
  deptauth: boolean;
  /**
   * 标记
   */
  flag: number;
  /**
   * 栏目ID，数字编号
   */
  id: number;
  /**
   * 版面布局配置，json格式
   */
  layout?: string;
  /**
   * 层级，栏目的层级，从1开始
   */
  level: number;
  /**
   * 栏目名称
   */
  name: string;
  /**
   * 排序号
   */
  odr?: null | string;
  /**
   * 父栏目ID，数字编号，可以为空
   */
  pid: number | null;
  /**
   * 是否在导航菜单中显示
   */
  visible: boolean;
  [property: string]: any;
}


/**
 * 栏目列表简要信息
 */
export type CategorySimpleListDto = CategorySimpleDto[];
export interface CategoryListSimple {
  items: CategorySimpleListDto;
}

/**
 * 栏目树形简要信息
 */
export interface CategoryTreeSimpleDto {
  info: CategorySimpleDto;
  childs: CategoryTreeSimpleDto[];
}
export type CategoryTreeSimple = CategoryTreeSimpleDto[];


/**
 * 栏目列表详细信息
 */
export type CategoryDetailListDto = CategoryDto[];
export interface CategoryListDetail {
  items: CategoryDetailListDto;
}

/**
 * 栏目树形详细信息
 */
export interface CategoryTreeDetailDto {
  info: CategoryDto;
  childs: CategoryTreeDetailDto[];
}
export type CategoryTreeDetail = CategoryTreeDetailDto[];

/**
 * 新增栏目DTO
 */
export interface CategoryAddDto {
  /**
   * 文章显示配置
   */
  article?: string;
  /**
   * 文章发布需要审核层级数，取值0、1、2，默认为1
   */
  auditcount: number;
  /**
   * 是否允许在栏目中发文
   */
  candraft: boolean;
  /**
   * 栏目内容，外部链接直接放链接地址
   */
  content: null | string;
  /**
   * 封面图片ID
   */
  cover?: number;
  /**
   * 栏目类型
   */
  ctype: string;
  /**
   * 是否按部门控制权限
   */
  deptauth: boolean;
  /**
   * 标记
   */
  flag: string;
  /**
   * 版面布局配置，json格式
   */
  layout?: string;
  /**
   * 栏目名称
   */
  name: string;
  /**
   * 排序号
   */
  odr?: null | string;
  /**
   * 父栏目ID，数字编号，可以为空
   */
  pid: number | null;
  /**
   * 是否在导航菜单中显示
   */
  visible: boolean;
  [property: string]: any;
}

/**
 * 栏目树表详情信息列表DTO
 */
export interface CategoryTreeTableListDto {
  //栏目id，数字编号
  id: number;
  value: number;

  //栏目名称
  name: string;
  label: string;

  //父栏目id
  pid: number;

  //栏目类型
  ctype: number;

  //栏目内容，外部链接直接放链接地址
  content: string;

  //封面图片ID
  cover: number;

  //排序号
  odr: number;

  //状态标记
  flag: number;

  //层级，从1开始
  level: number;

  //是否在导航菜单中显示
  visible: boolean;

  //栏目文章发布是否需要审核
  audit: boolean;

  //版面布局配置
  layout: string;

  //文章显示配置
  article: string;

  //是否按部门控制权限，默认为true
  deptauth: boolean;

  // 是否有发文权限
  candraft: boolean;

  //子栏目
  children: CategoryTreeTableListDto[];
}

/**
 * CategoryTreeDto 栏目树形详细信息DTO
 */
export interface CategoryTreeDto {
  childs?: CategoryDto[];
  info: CategoryDto;
  [property: string]: any;
}


/**
 * 栏目id集合
 */
export interface CategoryIds {
  ids: number[] | string[];
}

/**
 * 部门/组织简要信息DTO
 */
export interface DeptSimpleDto {
  //部门id
  id: number;

  //部门名称
  name: string;

  //层级，从1开始
  level: number;
}

/**
 * 栏目部门配置信息DTO
 */
export interface CategoryDeptDto {
  //部门简要信息
  dept: DeptSimpleDto;

  //是否关联该部门
  checked: boolean;
}

/**
 * 栏目部门配置信息集合DTO
 */
export type CategoryDeptListDto = CategoryDeptDto[];
export interface CategoryDeptList {
  items: CategoryDeptListDto;
}

/**
 * 设置栏目部门关联DTO
 */
export interface CategoryDeptSetDto {
  //栏目id集合
  cids: number[] | string[];

  //部门id集合
  dids: number[] | string[];
}


/** 
 * CategoryTreeTableDto 栏目TreeTable详细信息DTO
 */
export interface CategoryTreeTableDto {
  /**
   * 文章显示配置，json格式
   */
  article?: string;
  /**
   * 文章发布需要审核层级数，取值0、1、2，默认为1
   */
  auditcount: number;
  /**
   * 是否允许在栏目中发文
   */
  candraft: boolean;
  children?: CategoryDto[];
  /**
   * 栏目内容，外部链接直接放链接地址
   */
  content: null | string;
  /**
   * 栏目类型
   */
  ctype: number;
  /**
   * 是否按部门控制权限
   */
  deptauth: boolean;
  /**
   * 标记
   */
  flag: number;
  /**
   * 栏目ID，数字编号
   */
  id: number;
  /**
   * 版面布局配置，json格式
   */
  layout?: string;
  /**
   * 层级，栏目的层级，从1开始
   */
  level: number;
  /**
   * 栏目名称
   */
  name: string;
  /**
   * 排序号
   */
  odr: null | string;
  /**
   * 父栏目ID，数字编号，可以为空
   */
  pid: number | null;
  /**
   * 是否在导航菜单中显示
   */
  visible: boolean;
  [property: string]: any;
}
