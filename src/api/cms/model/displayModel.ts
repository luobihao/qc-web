// 前端展示查询
export type QueryDetail = {
  isall: boolean; // 是否包含所有
  isdept: boolean; // 是否根据部门访问权限
};
export interface QueryCategoryConditionDto = {
  isall: boolean; // 是否包含所有
  isdept: boolean; // 是否根据部门访问权限
  isvisible: boolean; // 为true表示不管栏目是否在菜单上显示与否，为false表示只包含在菜单上显示的栏目
};

import { ArticleDto } from './articleModel';

export interface DisplayCategoryArticlesDto {
  // 文章列表
  articles: ArticleDto[];
}
