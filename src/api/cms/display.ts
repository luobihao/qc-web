import { ArticleDto, ArticleQueryConditionDto, ArticleQueryResultDto } from './model/articleModel';
import { DisplayCategoryArticlesDto } from './model/displayModel';
import { SwipperQueryConditionDto } from './model/swipperModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  DisplayCommon = '/cms/display/common',
  DisplayCategory = '/cms/display/category',
  DisplayCategorySwippers = '/cms/display/categoryswippers',
  QueryArticles = '/cms/display/queryarticles',
  GetCategoryArticles = '/cms/display/categoryArticles',
}

/**
 * @description: 前端展示，查询指定栏目的信息（包含轮播图、栏目信息、子栏目（含子栏目信息和N条文章信息），传入参数栏目ID和文章数量
 */
export function getDisplayCategory(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get(
    {
      url: Api.DisplayCategory + '/' + id,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 前端展示，查询指定栏目及子栏目下的的N条文章信息
 */
export function getDisplayCategoryArticles(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DisplayCategoryArticlesDto>(
    {
      url: Api.GetCategoryArticles,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 返回CMS的栏目（包含子栏目）列表、友情链接（包含分类和链接集合）
 */
export function getDisplayCommon(mode: ErrorMessageMode = 'modal') {
  return defHttp.get(
    { url: Api.DisplayCommon },
    {
      joinTime: false, //去除时间戳
    },
  );
}

/**
 * @description: 获取指定栏目的轮播图数据，仅为有效状态，不包含栏目中的横隔轮播图数据
 */
export function getDisplayCategorySwippers(params: SwipperQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<SwipperListDto>(
    {
      //采用url传参需要指定joinParamsToUrl为true
      url: Api.DisplayCategorySwippers,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 查询文章
 */
export function queryArticles(params: ArticleQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ArticleQueryResultDto>(
    {
      url: Api.QueryArticles,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
