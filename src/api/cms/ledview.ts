import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { LedViewDto, LedViewListDto } from './model/ledViewModel';

enum Api {
  Query = '/cms/ledview/query',
  Get = '/cms/ledview/get',
  Add = '/cms/ledview/add',
  Update = '/cms/ledview/update',
  Delete = '/cms/ledview/delete',
}

/**
 * @description 查询
 */
export function getAllLedView(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<LedViewListDto>(
    {
      url: Api.Query,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定
 */
export function getLedView(id: number | string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<LedViewDto>(
    {
      url: Api.Get,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 新增
 */
export function addLedView(params: LedViewDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改
 */
export function updateLedView(params: LedViewDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.put<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除
 */
export function deleteLedView(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}
