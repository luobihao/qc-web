import { LinkCategoryModel } from './model/linkCategoryModel';

import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  Query = '/cms/linkcategory/query',
  Add = '/cms/linkcategory/add',
  Edit = '/cms/linkcategory/update',
  Delete = '/cms/linkcategory/delete',
  Get = '/cms/linkcategory/get',
}

// 链接分类查询
export function queryLinkCategory() {
  return defHttp.get(
    {
      url: Api.Query,
    },
    {
      joinParamsToUrl: true,
      joinTime: false,
    },
  );
}

// 链接分类新增
export function addLinkCategory(data: LinkCategoryModel) {
  return defHttp.post({
    url: Api.Add,
    data,
  });
}

// 链接分类修改
export function editLinkCategory(data: LinkCategoryModel) {
  return defHttp.put({
    url: Api.Edit,
    data,
  });
}

// 链接分类删除
export function deleteLinkCategory(params: { id?: string }) {
  return defHttp.delete(
    {
      url: Api.Delete,
      params,
    },
    {
      joinParamsToUrl: true,
    },
  );
}

// 链接分类获取指定
export function getLinkCategory(params: { id: number }) {
  return defHttp.get({
    url: Api.Get,
    params,
  });
}
