import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import {
  ArticleStatisticsConditionDto,
  ArticleStatisticsResultDto,
  ArticleMultiStatisticsResultDto,
  ArticleMultiStatisticsNotMergedRowResultDto,
} from './model/statisticsModel';
import { ArticleDtoList } from './model/articleModel';

/**
 * @description 后台接口Api
 */
enum Api {
  QueryStatistics = '/cms/statistics/query',
  QueryByAuthor = '/cms/statistics/author',
  QueryByDept = '/cms/statistics/dept',
  StatisticsByInUserAndDept = '/cms/statistics/sts',
  StatisticsByInUserAndDeptNotMerged = '/cms/statistics/stsnotmergedrow',
}

/**
 * @description 统计查询
 */
export function queryStatistics(
  params: ArticleStatisticsConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ArticleStatisticsResultDto>(
    {
      url: Api.QueryStatistics,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 指定作者查询
 */
export function queryByAuthor(
  params: ArticleStatisticsConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ArticleDtoList>(
    {
      url: Api.QueryByAuthor,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 指定部门查询
 */
export function queryByDept(
  params: ArticleStatisticsConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ArticleDtoList>(
    {
      url: Api.QueryByDept,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 按发布人和部门统计查询
 */
export function queryStatisticsByInUserAndDept(
  params: ArticleStatisticsConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ArticleMultiStatisticsResultDto>(
    {
      url: Api.StatisticsByInUserAndDept,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description 按发布人和部门统计查询，数据未合并处理
 */
export function queryStatisticsByInUserAndDeptNotMerged(
  params: ArticleStatisticsConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<ArticleMultiStatisticsResultDto>(
    {
      url: Api.StatisticsByInUserAndDeptNotMerged,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
