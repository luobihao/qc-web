import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

import { SwipperList, SwipperDto, EditFlagDto, SwipperQueryConditionDto } from './model/swipperModel';

enum Api {
  GetCategorySwipperList = '/cms/swipper/category',
  Add = '/cms/swipper/add',
  Update = '/cms/swipper/update',
  Delete = '/cms/swipper/delete',
  EditFlag = '/cms/swipper/editflag',
}

/**
 * @description: 获取指定栏目轮播图列表信息
 */
export function getCategorySwipperList(params: SwipperQueryConditionDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<SwipperList>(
    {
      url: Api.GetCategorySwipperList,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 新增栏目轮播图信息
 */
export function addSwipper(params: SwipperDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Add,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 修改栏目轮播图信息
 */
export function updateSwipper(params: SwipperDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.Update,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 删除栏目轮播图信息
 */
export function deleteSwipper(id: number, mode: ErrorMessageMode = 'modal') {
  return defHttp.delete<string>(
    {
      url: Api.Delete,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
      joinParamsToUrl: true,
    },
  );
}

/**
 * @description: 启用/禁用轮播图
 */
export function editFlag(params: EditFlagDto, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<string>(
    {
      url: Api.EditFlag,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
