/**
 * @description: 渠成用户信息
 */
export interface UacUserInfoDto {
  /**
   * 登录用户名
   */
  code: string;
  /**
   * 部门ID，如果有多个返回第1个
   */
  deptId?: number;
  /**
   * 部门名称，如果有多个返回第1个
   */
  deptName?: string;
  /**
   * 用户ID：为保证用户ID不被泄露，前端不记录用户ID，后台接口也不会返回用户ID
   */
  // id: number;
  /**
   * 名称
   */
  name: string;
  /**
   * 手机号，需做脱敏处理
   */
  phone: string;
  /**
   * 项目编码
   */
  project?: string;
}

/**
 * 用户查询条件DTO UserQueryConditionDto
 */
export interface UserQueryConditionDto {
  /**
   * 部门ID，可以查询指定部门下的用户
   */
  dept?: string;
  /**
   * 是否包含已删除，是否查询已删除用户
   */
  includedelete: boolean;
  /**
   * 项目编码，查询平台用户时为空，查询项目用户时必须指定
   */
  project?: string;
  /**
   * 关键字
   */
  words?: string;
  [property: string]: any;
}

/**
 * 用户简要信息DTO
 */
export interface UserSimpleInfoDto {
  // 用户ID
  id: number;
  // 用户名称
  name: string;
  // 手机号，经过脱敏后的
  phone: string;
}

/**
 * 用户简要信息DTO集合
 */
export type UserSimpleInfoDtoList = UserSimpleInfoDto[];


/**
 * 部门信息简要DTO
 */
export interface DeptSimpleDto {
  //ID
  id: number;
  //名称
  name: string;
  //父级ID
  parentid?: number;
  //层级，从0开始
  level: number;
}

/**
 * 部门信息简要DTO集合
 */
export type DeptSimpleDtoList = DeptSimpleDto[];


/**
 * DeptTreeSimpleDto 部门TreeSimple简要信息DTO
 */
export interface DeptTreeSimpleDto {
  children?: DeptSimpleDto;
  /**
   * ID
   */
  id: number;
  /**
   * 层级，从0开始
   */
  level: number;
  /**
   * 名称
   */
  name: string;
  /**
   * 父ID
   */
  parentid?: number;
  [property: string]: any;
}
