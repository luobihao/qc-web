import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import { UacUserInfoDto, UserQueryConditionDto, UserSimpleInfoDtoList, DeptSimpleDtoList, DeptTreeSimpleDto } from './model/publicModel';

enum Api {
  GetUserInfo = '/qc/invoke/userinfo',
  QueryProjectUserSimple = '/qc/invoke/projectusers',
  GetDeptUsers = '/qc/invoke/deptchildusers',
  UserValidDept = '/qc/invoke/userrelatedeptlist',
  GetUserCanAccessDeptList = '/qc/invoke/useraccessdeptlist',
  GetProjectValidRootDepts = '/qc/invoke/projectrootdeptlist',
  GetDeptChildsList = '/qc/invoke/deptchildslist',
  GetDeptChildsTree = '/qc/invoke/deptchildstree',
  GetProjectDeptList = '/qc/invoke/projectdeptlist',
}


/**
 * @description: getUserInfo
 */
export function getUserInfo() {
  return defHttp.get<UacUserInfoDto>(
    { url: Api.GetUserInfo },
    {
      errorMessageMode: 'none',
      // apiUrl: '/mock-api',
    },
  );
}

/**
 * @description: 查询指定项目中的用户简要列表
 */
export function queryProjectUsersSimple(
  params: UserQueryConditionDto,
  mode: ErrorMessageMode = 'modal',
) {
  return defHttp.post<UserSimpleInfoDtoList>(
    {
      url: Api.QueryProjectUserSimple,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description: 获取指定部门中的有效用户集合-getDeptUsers
 */
export function getDeptUsers(id: string | number, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<UserDeptUsersDtoList>(
    {
      url: Api.GetDeptUsers,
      params: {
        id: id
      }
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description: 获取指定项目下当前用户所在部门简要集合
 * @param id 项目编码
 */
export function getUserValidDept(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.UserValidDept,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取当前用户有访问权限的部门List-getUserCanAccessDeptList
 */
export function getUserCanAccessDeptList(mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.GetUserCanAccessDeptList,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定项目中的根部门List-getProjectValidRootDepts
 */
export function getProjectValidRootDepts(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.GetProjectValidRootDepts,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  )
}


/**
 * @description: 获取指定部门下的有效部门List-getDeptChildsList
 */
export function getDeptChildsList(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.GetDeptChildsList,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取指定部门下的有效部门tree-getDeptChildsTree
 */
export function getDeptChildsTree(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptTreeSimpleDto>(
    {
      url: Api.GetDeptChildsTree,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}


/**
 * @description: 获取指定项目下的有效部门List-GetProjectDeptList
 */
export function getProjectDeptList(id: string, mode: ErrorMessageMode = 'modal') {
  return defHttp.get<DeptSimpleDtoList>(
    {
      url: Api.GetProjectDeptList,
      params: { id: id },
    },
    {
      errorMessageMode: mode,
    },
  );
}
