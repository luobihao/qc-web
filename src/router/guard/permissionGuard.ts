import type { Router, RouteRecordRaw } from 'vue-router';

import { usePermissionStoreWithOut } from '/@/store/modules/permission';

import { PageEnum } from '/@/enums/pageEnum';
import { useUserStoreWithOut } from '/@/store/modules/user';

import { PAGE_NOT_FOUND_ROUTE } from '/@/router/routes/basic';

import { RootRoute } from '/@/router/routes';

import { useI18n } from '/@/hooks/web/useI18n';
import { useMessage } from '/@/hooks/web/useMessage';
import { h } from 'vue';

const LOGIN_PATH = PageEnum.BASE_LOGIN;

const ROOT_PATH = RootRoute.path;
const { createMessage } = useMessage();
/**
 * 白名单路径集合，在此集合中的路径不需要登录可以直接访问
 */
const whitePathList: PageEnum[] = [LOGIN_PATH];

export function createPermissionGuard(router: Router) {
  console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard router:', router);
  console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard whitePathList:', whitePathList);
  const userStore = useUserStoreWithOut();
  const permissionStore = usePermissionStoreWithOut();
  router.beforeEach(async (to, from, next) => {
    console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard beforeEach from:', from);
    console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard beforeEach to:', to);
    if (
      from.path === ROOT_PATH &&
      to.path === PageEnum.HOME &&
      userStore.getUserInfo.homePath &&
      userStore.getUserInfo.homePath !== PageEnum.HOME
    ) {
      next(userStore.getUserInfo.homePath);
      return;
    }

    const token = userStore.getToken;
    console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard beforeEach token=', token);
    // Whitelist can be directly entered
    //如果请求的目标在白名单中，可以直接进行路由跳转
    if (whitePathList.includes(to.path as PageEnum)) {
      console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard beforeEach whitePathList.includes');
      //在白名单中的目标路由，不需要判断是否有token信息
      //此处必须要加上token判断，否则此处不会进行路由跳转，导致继续往下执行到判断token为空中陷入死循环
      if (to.path === LOGIN_PATH && token) {
        const isSessionTimeout = userStore.getSessionTimeout;
        try {
          await userStore.afterLoginAction();
          return;
          // if (!isSessionTimeout) {
          //   next((to.query?.redirect as string) || '/');
          //   return;
          // }


        } catch { }
      }
      next();
      return;
    }
    //如果跳转to的meta中不需要身份验证信息，不管是否有token均进行直接跳转
    // You can access without permission. You need to set the routing meta.ignoreAuth to true
    if (to.meta.ignoreAuth) {
      next();
      return;
    }
    // token does not exist
    //如果token为空，不存在token，跳转至登录的路由地址
    if (!token) {
      console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard beforeEach token为空，目标地址需要登录后才能访问');
      //界面中显示提示信息
      const { t } = useI18n();
      createMessage.info({
        content: () => h('span', t('sys.uac.notExistTokenMessage')),
        duration: 5,
      });
      //跳转到登录路由地址，指定登录后的跳转路由为toto.path
      const redirectData: { path: string; replace: boolean; query?: Recordable<string> } = {
        path: LOGIN_PATH,
        replace: true,
      };
      if (to.path) {
        redirectData.query = {
          ...redirectData.query,
          redirect: to.path,
        };
      }
      next(redirectData);
      return;
    }

    // Jump to the 404 page after processing the login
    if (
      from.path === LOGIN_PATH &&
      to.name === PAGE_NOT_FOUND_ROUTE.name &&
      to.fullPath !== (userStore.getUserInfo.homePath || PageEnum.HOME)
    ) {
      next(userStore.getUserInfo.homePath || PageEnum.HOME);
      return;
    }

    // get userinfo while last fetch time is empty
    if (userStore.getLastUpdateTime === 0) {
      try {
        const userInfo = await userStore.getUserInfoAction();
        console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard beforeEach userStore.getUserInfoAction()', userInfo);
        //如果根据token获取到的用户信息为null，界面中显示提示信息
        if (userInfo === undefined || userInfo === null) {
          const { t } = useI18n();
          createMessage.error({
            content: () => h('span', t('sys.uac.tokenInvalidMessage')),
            duration: 5,
          });
          //跳转到登录路由地址，指定登录后的跳转路由为toto.path
          const redirectData: { path: string; replace: boolean; query?: Recordable<string> } = {
            path: LOGIN_PATH,
            replace: true,
          };
          if (to.path) {
            redirectData.query = {
              ...redirectData.query,
              redirect: to.path,
            };
          }
          next(redirectData);
          return;
        }
      } catch (err) {
        console.log('src\\router\\guard\\permissionGuard.ts createPermissionGuard beforeEach userStore.getUserInfoAction() error', err);
        next();
        return;
      }
    }
    if (permissionStore.getIsDynamicAddedRoute) {
      next();
      return;
    }

    const routes = await permissionStore.buildRoutesAction();

    routes.forEach((route) => {
      router.addRoute(route as unknown as RouteRecordRaw);
    });

    router.addRoute(PAGE_NOT_FOUND_ROUTE as unknown as RouteRecordRaw);

    permissionStore.setDynamicAddedRoute(true);

    if (to.name === PAGE_NOT_FOUND_ROUTE.name) {
      // 动态添加路由后，此处应当重定向到fullPath，否则会加载404页面内容
      next({ path: to.fullPath, replace: true, query: to.query });
    } else {
      const redirectPath = (from.query.redirect || to.path) as string;
      const redirect = decodeURIComponent(redirectPath);
      const nextData = to.path === redirect ? { ...to, replace: true } : { path: redirect };
      next(nextData);
    }
  });
}
