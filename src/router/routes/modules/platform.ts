import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const platform: AppRouteModule = {
  path: '/platform',
  name: 'Platform', //name中的值与path相同，提示404，修改为首字母大写就正常
  //大屏页面的首页即为独立页面，不需要有tab页；
  component: LAYOUT,
  redirect: '/platform/log',
  meta: {
    icon: 'ion:grid-outline',
    title: '平台维护',
    orderNo: 99,
  },
  children: [
    // {
    //   path: 'config',
    //   name: 'config',
    //   meta: {
    //     title: '系统配置维护',
    //   },
    // },
    // {
    //   path: 'platformUser',
    //   name: 'PlatformUser',
    //   meta: {
    //     title: '用户管理',
    //   },
    // },

    // {
    //   path: 'ad',
    //   name: 'ad',
    //   meta: {
    //     title: '行政区划维护',
    //   },
    // },
    {
      path: 'apilog',
      name: 'Apilog',
      component: () => import('/@/views/sys/apilog/index.vue'),
      meta: {
        title: '后台API日志',
      },
    },
  ],
};

export default platform;
