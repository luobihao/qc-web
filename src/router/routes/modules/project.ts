import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const Project: AppRouteModule = {
  path: '/project',
  name: 'Project', //name中的值与path相同，提示404，修改为首字母大写就正常
  component: LAYOUT,
  redirect: '/project/user',
  meta: {
    icon: 'ion:grid-outline',
    title: '系统配置',
    orderNo: 90,
  },
  children: [
    {
      path: 'project',
      name: 'Project',
      component: () => import('/@/views/project/project/index.vue'),
      meta: {
        title: '项目管理',
      },
    },
    {
      path: 'role',
      name: 'role',
      component: () => import('/@/views/project/role/index.vue'),
      meta: {
        title: '角色管理',
      },
    },
    {
      path: 'platformUser',
      name: 'PlatformUser',
      component: () => import('/@/views/project/user/platformuser/index.vue'),
      meta: {
        title: '平台用户管理',
      },
    },
    {
      path: 'projectUser',
      name: 'ProjectUser',
      component: () => import('/@/views/project/user/projectUser/index.vue'),
      meta: {
        title: '项目用户管理',
      },
    },
    {
      path: 'menu',
      name: 'Menu',
      component: () => import('/@/views/project/menu/index.vue'),
      meta: {
        title: '菜单管理',
      },
    },
    {
      path: 'dept',
      name: 'Dept',
      component: () => import('/@/views/project/dept/index.vue'),
      meta: {
        title: '部门管理',
      },
    },
    {
      path: 'deptuser',
      name: 'Deptuser',
      component: () => import('/@/views/project/deptuser/index.vue'),
      meta: {
        title: '部门用户管理',
      },
    },
  ],
};

export default Project;
