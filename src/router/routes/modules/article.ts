import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
//导入Cms展示页面的布局模板
// import CmsLayout from '/@/views/cms/CmsLayout.vue';

const CmsArticle: AppRouteModule = {
  path: '/cmsarticle',
  name: 'CmsArticle',
  component: LAYOUT,
  redirect: '/cmsarticle/cmsArticles',
  meta: {
    icon: 'ion:grid-outline',
    title: '文章管理',
    orderNo: 20,
  },
  children: [
    {
      path: 'cmsArticles',
      name: 'CmsArticles',
      component: () => import('/@/views/cms/articles/Index.vue'),
      meta: {
        title: '文章管理',
      },
    },
    {
      path: 'cmsArticlesAudit',
      name: 'CmsArticlesAudit',
      component: () => import('/@/views/cms/articles/AuditOneIndex.vue'),
      meta: {
        title: '文章审核',
      },
    },
  ],
};
export default CmsArticle;
