import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
//导入Cms展示页面的布局模板
// import CmsLayout from '/@/views/cms/CmsLayout.vue';

const CmsManage: AppRouteModule = {
  path: '/cmsmanage',
  name: 'CmsManage',
  component: LAYOUT,
  redirect: '/cmsmanage/cmsCategory',
  meta: {
    icon: 'ion:grid-outline',
    title: 'CMS后台管理',
    orderNo: 20,
  },
  children: [
    {
      path: 'cmsCategory',
      name: 'CmsCategory',
      component: () => import('/@/views/cms/category/index.vue'),
      meta: {
        title: '栏目管理',
      },
    },
    {
      path: 'linkCategory',
      name: 'LinkCategory',
      component: () => import('/@/views/cms/linkCategory/index.vue'),
      meta: {
        title: '链接分类管理',
      },
    },
    {
      path: 'link',
      name: 'Link',
      component: () => import('/@/views/cms/link/index.vue'),
      meta: {
        title: '链接管理',
      },
    },
    {
      path: 'cmsStatistics',
      name: 'CmsStatistics',
      component: () => import('/@/views/cms/statistics/index.vue'),
      meta: {
        title: 'CMS统计',
      },
    },
  ],
};
export default CmsManage;
