import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const DutyManage: AppRouteModule = {
  path: '/duty',
  name: 'Duty',
  component: LAYOUT,
  redirect: '/duty/dutySchedule',
  meta: {
    icon: 'ion:grid-outline',
    title: '排班管理',
    orderNo: 20,
  },
  children: [
    {
      path: 'dutyTable',
      name: 'DutyTable',
      // component: () => import('/@/views/duty/DutyCalendarIndex.vue'),
      component: () => import('/@/views/duty/DutyTableIndex.vue'),
      meta: {
        title: '部门值班表',
      },
    },
    {
      path: 'dutyGroup',
      name: 'DutyGroup',
      component: () => import('/@/views/duty/group/index.vue'),
      meta: {
        title: '值班分组管理',
      },
    },
    {
      path: 'dutyStaff',
      name: 'DutyStaff',
      component: () => import('/@/views/duty/staff/index.vue'),
      meta: {
        title: '值班人员管理',
      },
    },
    {
      path: 'dutyTeam',
      name: 'DutyTeam',
      component: () => import('/@/views/duty/team/index.vue'),
      meta: {
        title: '班值管理',
      },
    },
    {
      path: 'dutySchedule',
      name: 'DutySchedule',
      component: () => import('/@/views/duty/schedule/index.vue'),
      meta: {
        title: '排班管理',
      },
    },
  ],
};
export default DutyManage;
