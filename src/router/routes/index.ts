import type { AppRouteRecordRaw, AppRouteModule } from '/@/router/types';
import { t } from '/@/hooks/web/useI18n';

import { PAGE_NOT_FOUND_ROUTE, REDIRECT_ROUTE } from '/@/router/routes/basic';

import { CMS_LAYOUT } from '/@/router/constant';

import { mainOutRoutes } from './mainOut';
import { PageEnum } from '/@/enums/pageEnum';
import { t } from '/@/hooks/web/useI18n';

const modules = import.meta.globEager('./modules/**/*.ts');

const routeModuleList: AppRouteModule[] = [];

Object.keys(modules).forEach((key) => {
  const mod = modules[key].default || {};
  const modList = Array.isArray(mod) ? [...mod] : [mod];
  routeModuleList.push(...modList);
});

export const asyncRoutes = [PAGE_NOT_FOUND_ROUTE, ...routeModuleList];

export const RootRoute: AppRouteRecordRaw = {
  path: '/',
  name: 'Root',
  redirect: PageEnum.HOME, // cms时跳到cms首页BASE_HOME, 其他项目(qms,ehs)跳到LOGIN BASE_LOGIN
  //测试跳转至单独的首页
  // redirect: '/demo',
  meta: {
    title: 'Root',
  },
};

/*
 * 系统登录路由
 */
export const LoginRoute: AppRouteRecordRaw = {
  path: '/login',
  name: 'Login',
  component: () => import('/@/views/sys/login/Login.vue'),
  meta: {
    title: t('routes.basic.login'),
  },
};

/*
 * 系统关于信息（显示系统中的版本号、本地store信息，用于开发调测使用）路由
 */
export const SysDevRoute: AppRouteRecordRaw = {
  path: '/dev',
  name: 'Dev',
  // component: LAYOUT,
  redirect: '/dev/about',
  meta: {
    icon: 'ion:grid-outline',
    title: '开发调测',
    ignoreAuth: true,
    orderNo: 20,
  },
  children: [
    {
      path: '/dev/about',
      name: 'About',
      component: () => import('/@/views/sys/about/index.vue'),
      meta: {
        title: '关于',
        ignoreAuth: true,
      },
    },
  ],
};

/**
 * CMS展示页面路由
 */
export const CmsDisplayRoute: AppRouteRecordRaw = {
  path: '/cms',
  name: 'Cms',
  component: CMS_LAYOUT,
  redirect: '/cms/index',
  meta: {
    icon: 'ion:grid-outline',
    title: 'CMS展示页',
    ignoreAuth: true,
    orderNo: 20,
  },
  children: [
    {
      path: '/cms/index',
      name: 'Index',
      redirect: '/cms/category/0',
      meta: {
        title: '首页',
        ignoreAuth: true,
      },
    },
    {
      path: '/cms/category/:id',
      // path: '/cms/category/:path(.*)*',
      name: 'Category',
      component: () => import('/@/views/cms/CmsCategory.vue'),
      meta: {
        title: '栏目页',
        ignoreAuth: true,
      },
    },
    {
      path: '/cms/article/:id',
      name: 'Article',
      component: () => import('/@/views/cms/CmsArticle.vue'),
      meta: {
        title: '文章页',
        ignoreAuth: true,
      },
    },
    {
      path: '/cms/displaySearch/:keywords',
      name: 'DisplaySearch',
      component: () => import('/@/views/cms/CmsSearchArticle.vue'),
      meta: {
        title: '搜索页',
        ignoreAuth: true,
      },
    },
    {
      path: '/cms/duty',
      name: 'DisplayDuty',
      component: () => import('/@/views/duty/DutyTableIndex.vue'),
      meta: {
        title: '值班表',
        ignoreAuth: true,
      },
    },
    {
      path: '/addressbook/userContact/:project',
      name: 'UserContact',
      component: () => import('/@/views/addressbook/UserContactIndex.vue'),
      meta: {
        title: '用户通讯录',
        ignoreAuth: true,
      },
    },
    {
      path: '/addressbook/officialContact',
      name: 'OfficialContact',
      component: () => import('/@/views/addressbook/OfficialContactIndex.vue'),
      meta: {
        title: '公务号码通讯录',
        ignoreAuth: true,
      },
    },
  ],
};

/*
 * 不需要权限控制和身份验证的路由集合，在此集合内的可以直接访问，不在集合中的会进入登录界面并带有跳转路由login?redirect=/cms
 */
export const basicRoutes = [
  LoginRoute,
  CmsDisplayRoute, //CMS前端展示页面路由
  RootRoute,
  SysDevRoute,
  ...mainOutRoutes,
  REDIRECT_ROUTE,
  PAGE_NOT_FOUND_ROUTE,
];
