// Number类型工具

//默认位数数字，保留小数位默认为2位小数
const DEFAULT_DIGIT = '2';
//默认格式化字符串，默认保留固定2位小数
const DEFAULT_FORMAT_STR = 'F2';

/**
 * 按照指定的格式化字符串对数值进行格式化，Fn表示固定n位小数，Gn表示n位有效数字
 * 
 * @param num 数值
 * @param format 格式化字符串
 * @returns 格式化后的字符串
 */
export function numberFormatToString(
  num: Number | string,
  format?: string | Number,
): string {
  if (num === null || num === undefined) return '';
  //暂时固定按照n位小数进行处理，后继再进行完善
  let prefix = 'F';//格式化前缀，默认为F表示固定小数位
  let digit = DEFAULT_DIGIT;
  if (format != undefined && format != null) {
    if (typeof format === 'number') {
      digit = format;
    }
    else {
      //如果format不是数值，使用第1个字符作为前缀
      prefix = format.slice(0, 1);
      digit = format.slice(1);
    }
  }

  // console.log('numberFormatToString prefix=' + prefix + ' digit=' + digit);

  //根据前缀判断调用对应的方法
  if (prefix === 'F' || prefix === 'f')
    return numberFormatToFixedString(num, digit);
  else if (prefix === 'G' || prefix === 'g')
    return Number(num).toPrecision(digit);

  //默认返回原始输入内容
  return num;
}

/**
 * 保留固定n位小数格式化输出
 * 
 * @param num 数值
 * @param digit 小数位数，默认为2位
 * @returns 
 */
export function numberFormatToFixedString(
  num: Number | string,
  digit = DEFAULT_DIGIT,
): string {
  //先转换为Number再格式化
  return Number(num).toFixed(digit);
}
