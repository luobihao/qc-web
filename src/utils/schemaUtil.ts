// 提供根据渠成查询条件表单项Dto、数据库列Dto转换为FormSchema对象
//提供根据渠成数据库表列Dto转换为BasicColumn对象方法
import { DBTableDataQueryConditionFormItemDto } from '/@/api/platform/model/dbtableDataModel';
import { DBTableColumnDto } from '/@/api/platform/model/dbtableColumnModel';
import { FormSchema } from '/@/components/Form';
import { BasicColumn } from '/@/components/Table';
import { OperationTypeEnum } from '/@/enums/appEnum';
import { KeyValuePairDto } from '/@/api/platform/model/qcPublicModel';
import { formatToDateTime, formatToDate, dateUtil } from '/@/utils/dateUtil';
import { numberFormatToString } from '/@/utils/numberUtil';
import dayjs from 'dayjs';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

/**
 * 根据查询条件表单项转换为对应的界面显示表单结构
 *
 * @param   DBTableDataQueryConditionFormItemDto  formItem   查询条件表单项
 * @return  FormSchema 表单结构
 */
export function getFormSchemaByConditionFormItem(
  formItem: DBTableDataQueryConditionFormItemDto,
): FormSchema {
  let schema: FormSchema = {};
  if (formItem != undefined && formItem != null) {
    schema = getFormSchema(
      formItem.code,
      formItem.name,
      OperationTypeEnum.QUERY,
      formItem.cmptype,
      formItem.coltype,
      formItem.datatype,
      undefined,
      false,
      formItem.notnull,
      undefined,
      formItem.defaultval,
      undefined,
      formItem.options,
      formItem.formatstr,
      undefined,
      undefined,
    );
  }

  return schema;
}

/**
 * 根据数据库表列信息转换为对应的界面显示表单结构
 *
 * @param   DBTableColumnDto  column   数据库表列信息
 * @return  FormSchema 表单结构
 */
export function getFormSchemaByColumn(
  column: DBTableColumnDto,
  isAdd: boolean = false,
): FormSchema {
  let schema: FormSchema = {};
  if (column != undefined && column != null) {
    let operatetype = isAdd === true ? OperationTypeEnum.ADD : OperationTypeEnum.EDIT;
    schema = getFormSchema(
      column.code,
      column.name,
      operatetype,
      undefined,
      column.coltype,
      column.datatype,
      column.unit,
      column.isprimary,
      column.notnull,
      column.efcolspan,
      column.defaultval,
      column.valrange,
      column.options,
      column.formatstr,
      column.length,
      column.description,
    );
  }

  return schema;
}

/**
 * 根据指定的信息（来源于查询条件表单或数据库列信息）转换为对应的界面显示表单结构
 *
 */
export function getFormSchema(
  code: string,
  name: string,
  operatetype: OperationTypeEnum,
  cmptype: number | undefined,
  coltype: number | string,
  datatype: number | string,
  unit: string | undefined,
  isprimary: boolean = false,
  notnull: boolean = false,
  efcolspan: number | undefined,
  defaultval: any | undefined,
  valrange: string | undefined,
  options: KeyValuePairDto[] | undefined,
  formatstr: string | undefined,
  maxLength: number | undefined,
  description: string | undefined,
): FormSchema {
  let result: FormSchema = {};
  //设置字段名、label文字、默认值、是否必填
  result.field = code;

  //label设置为列的名称，如果有单位加上（单位名称）
  if (unit != undefined && unit != null) result.label = name + '(' + unit + ')';
  else result.label = name;

  //如果有默认值，设置默认值；默认值在后面不同的控件中分别设置
  // if (defaultval != undefined)
  // result.defaultValue = defaultval;

  //如果有描述备注信息，显示在提示信息中
  if (description != undefined && description != null) result.helpMessage = description;
  result.required = notnull;

  // 查询条件类型不为=、like、like_left、like_right时，处理显示条件提示信息
  if (cmptype != 0 && cmptype != 10 && cmptype != 11 && cmptype != 12) {
    if (cmptype === 1) result.label += ': !=';
    else if (cmptype === 2) result.label += ': >';
    else if (cmptype === 3) result.label += ': >=';
    else if (cmptype === 4) result.label += ': <';
    else if (cmptype === 5) result.label += ': <=';
    else if (cmptype === 20) result.label += ': IN';
    else if (cmptype === 21) {
      result.labelWidth = 130;
      result.label += ': NOT_IN';
    } else if (cmptype === 30) {
      result.labelWidth = 140;
      result.label += ': BETWEEN';
    } else if (cmptype === 31) {
      result.labelWidth = 200;
      result.label += ': NOT_BETWEEN';
    }
  }

  //如果是新增或者修改，列时主键列时要求非空
  if (!notnull) {
    if (operatetype == OperationTypeEnum.ADD || operatetype == OperationTypeEnum.EDIT) {
      if (isprimary) {
        result.required = true;
      }
    }
  }

  //根据表单项的数据类型和列类型设置控制类型和控件属性
  let component = 'Input'; //控件类型默认为Input
  const componentProps = {}; //控件属性对象定义，根据不同的控件进行不同的赋值
  //如果是修改数据操作主键列不允许编辑
  if (operatetype == OperationTypeEnum.EDIT) {
    if (isprimary) {
      componentProps.disabled = true;
    }
  }

  //根据数据类型、列类型、对比类型设置不同的控件和控件属性
  //根据列类型判断：使用Select控件的列类型：20-对象ID、21-对象名称、70-字典、71-枚举、72-数据字典
  if (
    coltype === 20 ||
    coltype === '20' ||
    coltype === 21 ||
    coltype === '21' ||
    coltype === 70 ||
    coltype === '70' ||
    coltype === 71 ||
    coltype === '71' ||
    coltype === 72 ||
    coltype === '72'
  ) {
    component = 'Select';
    // 设置默认值
    if (defaultval != undefined && defaultval != null && defaultval !== '')
      result.defaultValue = defaultval;
    //设置选择的下拉数据
    if (options != undefined && options != null && options.length > 0x0) {
      const selectOptions = [];
      options.forEach((opt) => {
        selectOptions.push({ value: opt.key, label: opt.value });
      });
      componentProps.options = selectOptions;
    }
    // qcnotnull为true默认选中第1个并且不可以清空，如qcnotnull为false使用Select（默认为空，可以清空选择）；
    if (notnull) {
      result.defaultValue = options[0];
      componentProps.allowClear = false;
    }
    // 如果是查询并且比较类型是范围，设置多选（默认单选），编辑表单固定为单选
    if (
      operatetype == OperationTypeEnum.QUERY &&
      (cmptype === 30 || cmptype === '30' || cmptype === 31 || cmptype === '31')
    ) {
      componentProps.mode = 'multiple';
    }
  } else {
    //如果不是select控件，再根据数据类型判断所需的控件类型
    if (datatype === 1 || datatype === '1') {
      //判断如果是查询并且条件比较类型是范围，需要生成2个日期时间控件
      if (
        operatetype == OperationTypeEnum.QUERY &&
        (cmptype === 30 || cmptype === '30' || cmptype === 31 || cmptype === '31')
      ) {
        component = 'RangePicker';
        if (formatstr !== undefined && formatstr !== null && formatstr !== '') {
          componentProps.showTime = { format: formatstr };
          componentProps.valueFormat = formatstr;
        }
        // TODO 范围日期设置默认值
      } else {
        //新增或编辑表单时的日期时间控件
        component = 'DatePicker';
        //日期时间设置格式化
        if (formatstr !== undefined && formatstr !== null && formatstr !== '') {
          componentProps.showTime = { format: formatstr };
          componentProps.valueFormat = formatstr;
        }

        //日期时间设置默认值
        if (defaultval !== undefined && defaultval !== null && defaultval !== '')
          result.defaultValue = dayjs(defaultval);
      }
    } else if (datatype === 2 || datatype === '2' || datatype === 3 || datatype === '3') {
      // 2-整数，3-小数
      component = 'InputNumber';
      //默认值
      if (defaultval !== undefined && defaultval !== null) result.defaultValue = defaultval;
      // 如果有取值范围，设置取值的最大值和最小值
      if (valrange !== undefined && valrange !== null && valrange !== '') {
        let rangeArr = valrange.split('~');

        if (rangeArr != undefined && rangeArr != null && rangeArr.length > 0x0) {
          //如果只有1个元素，设置为最小值
          if (rangeArr.length > 0x0) {
            componentProps.min = rangeArr[0x0];
          }
          //如果有至少2个元素，设置第2个元素为最大值
          if (rangeArr.length > 0x1) {
            componentProps.max = rangeArr[0x1];
          }
        }
      }
    } else if (datatype === 4 || datatype === '4') {
      // 布尔
      component = 'Switch';
      //默认值--是否选中
      let defaultChecked = true;
      if (defaultval !== undefined && defaultval !== null && defaultval !== '') {
        if (
          defaultval === 0 ||
          defaultval === '0' ||
          defaultval === false ||
          defaultval === 'false'
        )
          defaultChecked = false;
      }
      componentProps.defaultChecked = defaultChecked;

      if (operatetype == OperationTypeEnum.QUERY) {
        // qcnotnull为true使用Switch，qcnotnull为false使用Select（默认为空，可以清空选择）；
        if (!notnull) {
          component = 'Select';
          // 下拉选项设置,后台接口处理options
          if (options != undefined && options != null && options.length > 0x0) {
            const selectOptions = [];
            options.forEach((opt) => {
              selectOptions.push({ value: opt.key, label: opt.value });
            });
            componentProps.options = selectOptions;
          }
        }
      }
    } else {
      // 字符串
      component = 'Input';
      // 默认值
      if (defaultval !== undefined && defaultval !== null && defaultval !== '')
        result.defaultValue = defaultval;
      // 如果有长度，设置输入长度maxLength
      if (maxLength != undefined && maxLength != null && maxLength > 0x0) {
        // 如果设置的最大长度大于指定值（如：100），需要使用textarea 控件显示
        if (maxLength > 100) {
          component = 'InputTextArea';
        }
        //如果有设置最大长度，设置显示字数和最大长度属性
        componentProps.showCount = true;
        componentProps.maxLength = maxLength;
      }
    }
  }

  // 设置控件的宽度
  if (operatetype == OperationTypeEnum.QUERY) {
    // 判断条件比较类型是否为范围，分别设置宽度
    if (cmptype === 30 || cmptype === 31) {
      result.colProps = { span: 9 };
    } else {
      result.colProps = { span: 6 };
    }
  } else {
    if (!!efcolspan) result.colProps = { span: efcolspan };
    else result.colProps = { span: 20 };
  }

  result.component = component;
  result.componentProps = componentProps;

  console.log('getFormSchema result', result);
  return result;
}

/**
 * 根据数据库表列信息转换为对应的界面显示表单结构
 *
 * @param   DBTableColumnDto  dto   数据库表列信息
 * @return  BasicColumn 表格列信息
 */
export function getBasicColumnByColumnDto(dto: DBTableColumnDto): BasicColumn {
  let column: BasicColumn = {};
  if (dto != undefined && dto != null) {
    column = {
      title: dto.name, //显示表头名称
      dataIndex: dto.code, //显示数据的字段名或序号
    };
    //是否固定
    if (dto.qrfixed !== undefined && dto.qrfixed !== null && dto.qrfixed !== '') {
      //如果有值默认固定在左，如果指定为右时设置在右
      if (dto.qrfixed === 'left' || dto.qrfixed === 'Left')
        column.fixed = 'left';
      else if (dto.qrfixed === 'right' || dto.qrfixed === 'Right')
        column.fixed = 'right';
    }
    //是否显示
    if (dto.qrshow != undefined && dto.qrshow != null && dto.qrshow === false)
      column.ifShow = false;
    //单位；如果有单位修改显示的表头名称中加上单位
    if (dto.unit !== undefined && dto.unit !== null && dto.unit !== '')
      column.title = dto.name + '(' + dto.unit + ')';
    //显示宽度
    if (dto.qrwidth !== undefined && dto.qrwidth !== null && dto.qrwidth !== '' && dto.qrwidth > 0x0)
      column.width = dto.qrwidth;
    //如果有描述备注信息，显示在提示信息中
    if (dto.description !== undefined && dto.description !== null && dto.description !== '')
      column.helpMessage = dto.description;

    /**
     * 根据数据类型进行格式化输出
     * 1=日期类型，使用封装的formatToDateTime进行格式化
     * 2=整数类型，使用封装的numberFormatToString进行格式化
     * 3=小数类型，使用封装的numberFormatToString进行格式化
     */
    // 如果是日期类型列，根据formatstr设置日期列格式化，默认格式化为'YYYY-MM-DD HH:mm:ss'
    if (dto.datatype === 1 || dto.datatype === '1') {
      column.customRender = ({ text }) => {
        if (text !== undefined && text !== null && text !== '') {
          return formatToDateTime(text, dto.formatstr);
        }
      };
    }
    else if (dto.datatype === 2 || dto.datatype === '2') {
      column.customRender = ({ text }) => {
        if (text !== undefined && text !== null && text !== '') {
          //如果有设置格式化字符串使用格式化字符串，如果没有设置默认保留整数
          if (dto.formatstr !== undefined && dto.formatstr !== null && dto.formatstr !== '')
            return numberFormatToString(text, dto.formatstr);
          else
            return numberFormatToString(text, 'F0');
        }
      };
    }
    else if (dto.datatype === 3 || dto.datatype === '3') {
      column.customRender = ({ text }) => {
        //实测发现text为数值0.0时if (text != undefined && text != null && text != '')判断结果为false
        if (text != undefined && text != null && text !== '') {
          return numberFormatToString(text, dto.formatstr);
        }
      };
    }

    /**
     * 在数据类型为：bool=4或int=2时，并且列类型为：70-字典、71-枚举、72-数据字典
     * 根据格式化字符串formatstr和options数据进行显示输出控制
     * 
     * 处理逻辑：
     * 1.先根据格式化字符串formatstr，要求为json数组，
     * 2.再根据options处理
     */
    if (dto.datatype === 2 || dto.datatype === '2' || dto.datatype === 4 || dto.datatype === '4') {
      if (dto.coltype === 70 || dto.coltype === '70' || dto.coltype === 71 || dto.coltype === '71' || dto.coltype === 72 || dto.coltype === '72') {
        // console.log('schemaUtil.getBasicColumnByColumnDto column.customRender 列数据类型和列类型判断匹配 dto.name=' + dto.name + ' dto.datatype=' + dto.datatype + ' dto.coltype=' + dto.coltype + ' dto.formatstr=' + dto.formatstr);
        //至少要格式化字符串formatstr和options必须有1个
        //如果formatstr无法解析为json数组，再根据options是否有值进行处理
        let isFormatStrValid = false;
        if (dto.formatstr !== undefined && dto.formatstr !== null && dto.formatstr !== '') {
          // console.log('schemaUtil.getBasicColumnByColumnDto column.customRender 步骤1-根据formatstr处理 dto.name=' + dto.name + ' dto.datatype=' + dto.datatype + ' dto.coltype=' + dto.coltype + ' dto.formatstr=' + dto.formatstr);
          try {
            let formatStringJsonObj = JSON.parse(dto.formatstr);
            if (formatStringJsonObj != undefined && formatStringJsonObj != null && formatStringJsonObj.length > 0x0) {
              //如果formatstr正确解析为json数组，设置isFormatStrValid为true
              isFormatStrValid = true;
              // console.log('schemaUtil.getBasicColumnByColumnDto column.customRender 步骤1-根据formatstr处理，formatstr为有效json dto.name=' + dto.name + ' dto.datatype=' + dto.datatype + ' dto.coltype=' + dto.coltype + ' dto.formatstr=' + dto.formatstr);
              //格式化字符串格式为：[{"value":false,"label":"未上传","color":"red"},{"value":true,"label":"已上传","color":"green"}]
              //要求必须要有属性：value、label、color
              column.customRender = ({ text }) => {
                //记录找到对应项的索引，如果没有找到显示原始内容
                let idx = -1;
                if (text !== undefined && text !== null && text !== '') {
                  for (let i = 0; i < formatStringJsonObj.length; i++) {
                    //先判断text与value是否绝对相同，如果绝对相等不进行后面的多种兼容判断
                    if (text === formatStringJsonObj[i].value) {
                      idx = i;
                      break;
                    }
                    else {
                      //根据列数据类型分别判断处理，如果为bool类型判定字符串true、1均表示true
                      if (dto.datatype === 4 || dto.datatype === '4') {
                        if (text == true || text == 'true' || text == '1' || text == 1) {
                          //判断内容text为true，此处判断不使用绝对相等，需要自动进行类型转换
                          if (formatStringJsonObj[i].value == true || formatStringJsonObj[i].value == 'true' || formatStringJsonObj[i].value == '1' || formatStringJsonObj[i].value == 1) {
                            idx = i;
                            break;
                          }
                        }
                        else {
                          //判断内容text为false，此处判断不使用绝对相等，需要自动进行类型转换
                          if (formatStringJsonObj[i].value != true && formatStringJsonObj[i].value != 'true' && formatStringJsonObj[i].value != '1' && formatStringJsonObj[i].value != 1) {
                            idx = i;
                            break;
                          }
                        }
                      }
                      else {
                        //数据类型为整数，直接比较2个的值，此处判断不使用绝对相等，需要自动进行类型转换
                        if (formatStringJsonObj[i].value == text) {
                          idx = i;
                          break;
                        }
                      }
                    }
                  }
                }
                //可以选中使用Tag或span
                //return h('span', text);
                if (idx >= 0x0)
                  return h(Tag, { color: formatStringJsonObj[idx].color }, () => formatStringJsonObj[idx].label);
                else
                  return text;
              };
            }
          }
          catch (e) {
            console.log('schemaUtil.getBasicColumnByColumnDto JSON.parse(dto.formatstr) Exception', e);
          }
        }
        //如果没有使用formatstr进行格式化显示输出，再根据options是否有数据进行判断处理
        if (isFormatStrValid === false) {
          if (dto.options != undefined && dto.options != null && dto.options.length > 0x0) {
            // console.log('schemaUtil.getBasicColumnByColumnDto column.customRender 步骤2-根据options处理 dto.name=' + dto.name + ' dto.datatype=' + dto.datatype + ' dto.coltype=' + dto.coltype + ' dto.formatstr=' + dto.formatstr);
            column.customRender = ({ text }) => {
              //记录找到对应项的索引，如果没有找到显示原始内容
              let idx = -1;
              if (text !== undefined && text !== null && text !== '') {
                for (let i = 0; i < dto.options.length; i++) {
                  //options的属性为：key、value、attribute
                  //根据列数据类型分别判断处理，如果为bool类型判定字符串true、1均表示true
                  if (dto.datatype === 4 || dto.datatype === '4') {
                    if (text === true || text === 'true' || text === '1' || text === 1) {
                      //判断内容text为true，此处判断不使用绝对相等，需要自动进行类型转换
                      if (dto.options[i].key === true || dto.options[i].key == 'true' || dto.options[i].key == '1' || dto.options[i].key == 1) {
                        idx = i;
                        break;
                      }
                    }
                    else {
                      //判断内容text为false，此处判断不使用绝对相等，需要自动进行类型转换
                      if (dto.options[i].key != true && dto.options[i].key != 'true' && dto.options[i].key != '1' && dto.options[i].key != 1) {
                        idx = i;
                        break;
                      }
                    }
                  }
                  else {
                    //数据类型为整数，直接比较2个的值，此处判断不使用绝对相等，需要自动进行类型转换
                    if (dto.options[i].key == text) {
                      idx = i;
                      break;
                    }
                  }
                }
              }
              //可以在attribute中指定样式属性
              // return h(Tag, { color: formatstr[i].color }, () => dto.options[i].label);
              if (idx >= 0x0)
                return h(Tag, () => dto.options[idx].value);
              else
                return text.toString();
            }
          }
          else {
            // console.log('schemaUtil.getBasicColumnByColumnDto column.customRender 步骤3-formatstr和options均没有处理 dto.name=' + dto.name + ' dto.datatype=' + dto.datatype + ' dto.coltype=' + dto.coltype + ' dto.formatstr=' + dto.formatstr);
            //formatstr和options 2个都无效，显示为原内容
            column.customRender = ({ text }) => {
              return text.toString();
            };
          }
        }
      }
      else {
        // console.log('schemaUtil.getBasicColumnByColumnDto column.customRender 列类型不需要处理 dto.name=' + dto.name + ' dto.datatype=' + dto.datatype + ' dto.coltype=' + dto.coltype + ' dto.formatstr=' + dto.formatstr);
        //列类型为其他类型时显示原始内容
        column.customRender = ({ text }) => {
          //直接返回text时数据为bool型显示为空，需要使用toString
          return text.toString();
        };
      }
    }

    /**
     * 单独处理列类型为：20-对象ID/编码，根据options中的进行匹配显示，数据为对象ID/编码，显示为对应的对象名称
     * 不管数据类型，数据类型可能为整数、字符串
     */
    if (dto.coltype === 20 || dto.coltype === '20') {
    }

    return column;
  }
}
