import { QcUnifyTransToChildProps } from '/#/QcUnify';
import { OperationTypeEnum } from '/@/enums/appEnum';

/**
 * 根据传值的属性值获取显示的标题文字
 *
 * @param   QcUnifyTransToChildProps  qcUnifyProps   传值的属性值
 * @return  String 显示的标题文字
 */
export function displayTitle(qcUnifyProps: QcUnifyTransToChildProps | undefined) {
  //标题显示格式：moduleName--operateType/operateString（dataTitle）
  let title = '';
  if (qcUnifyProps != undefined) {
    //模块名称
    title += qcUnifyProps.moduleName;
    //操作类型、名称--如果有指定名称字符串优先使用
    if (
      qcUnifyProps.operateString != undefined &&
      qcUnifyProps.operateString != null
    ) {
      title += '--' + qcUnifyProps.operateString;
    } else if (
      qcUnifyProps.operateType != OperationTypeEnum.UN_KNOWN &&
      qcUnifyProps.operateType != OperationTypeEnum.CUSTOME
    ) {
      title += '--' + qcUnifyProps.operateType.toString();
    }
    //操作的数据名称
    if (qcUnifyProps.dataTitle != undefined && qcUnifyProps.dataTitle != null) {
      title += '(当前操作数据：' + qcUnifyProps.dataTitle + ')';
    }
  }
  return title;
}

/**
 * 字符串全部替换方法，因为ES中的replaceAll方法对浏览器版本有要求可能无法正常使用
 * @param str 完整的原始字符串
 * @param replaceOldStr 要替换的旧字符串
 * @param replaceNewStr 要替换为的字符串内容
 * @returns 
 */
export function replaceAll(str: String, replaceOldStr: String, replaceNewStr: String) {
  let result = str;
  while (result.indexOf(replaceOldStr) >= 0) {
    result = result.replace(replaceOldStr, replaceNewStr);
  }
  return result;
}