import type { CmsRouteParam } from '/#/store';
import type { ErrorMessageMode } from '/#/axios';
import { defineStore } from 'pinia';
import { store } from '/@/store';
import { Persistent } from '/@/utils/cache/persistent';
import { MODULE_CMS_ROUTE_PREFIX_KEY } from '/@/enums/cacheEnum';
import { deepMerge } from '/@/utils';

/*
 * store中存储信息定义
 */
interface CmsState {
  cmsRouteParam: CmsRouteParam;
}

export const useCmsStore = defineStore({
  id: 'app-cms',
  state: (): CmsState => ({
    cmsRouteParam: null,
  }),
  getters: {
    getCmsRouteParam(): CmsRouteParam {
      return this.cmsRouteParam || localStorage.getItem(MODULE_CMS_ROUTE_PREFIX_KEY);
    },
  },
  actions: {
    setCmsRouteParam(config: DeepPartial<CmsRouteParam>): void {
      this.cmsRouteParam = deepMerge(this.cmsRouteParam || {}, config);
      Persistent.setLocal(MODULE_CMS_ROUTE_PREFIX_KEY, this.cmsRouteParam);
    },
    resetState() {
      this.cmsRouteParam = null;
    },
  },
});

// Need to be used outside the setup
export function useCmsStoreWithOut() {
  return useCmsStore(store);
}
