import { OperationTypeEnum } from '/@/enums/appEnum';

/**
 * 渠成统一定义--在前端中传递给子级（对应界面中打开编辑或设置界面）的数据
 */
interface QcUnifyTransToChildProps {
  // 模块名称--用于在modal或table等界面中显示
  moduleName: string;
  // 操作类型枚举
  operateType: OperationTypeEnum;
  // 操作类型字符串，用于自定义描述当前操作
  operateString?: string;
  // 操作的数据ID
  dataId?: string | number | any;
  // 操作的数据标题，用于显示
  dataTitle?: string;
  // 操作的模型对象
  model?: Recordable;
}

export interface QcUnifyTransToChildPropsExtend extends QcUnifyTransToChildProps {
  text?: string;
}

export function useQcUnifyProps({
  moduleName,
  operateType,
  operateString,
  dataId,
  dataTitle,
  model,
}: QcUnifyTransToChildProps) {
  const qcUnifyProps: QcUnifyTransToChildProps = {
    moduleName: moduleName,
    operateType: operateType,
    operateString: operateString,
    dataId: dataId,
    dataTitle: dataTitle,
    model: model,
  };
  return qcUnifyProps;
}

/**
 * 根据传值的属性值获取显示的标题文字
 *
 * @param   QcUnifyTransToChildProps  qcUnifyProps   传值的属性值
 * @return  String 显示的标题文字
 */
export function displayTitle(qcUnifyProps: QcUnifyTransToChildProps | undefined) {
  //标题显示格式：moduleName--operateType/operateString（dataTitle）
  let title = '';
  if (qcUnifyProps != undefined) {
    //模块名称
    title += qcUnifyProps.moduleName;
    //操作类型、名称--如果有指定名称字符串优先使用
    if (qcUnifyProps.operateString != undefined && qcUnifyProps.operateString != null) {
      title += '--' + qcUnifyProps.operateString;
    } else if (
      qcUnifyProps.operateType != OperationTypeEnum.UN_KNOWN &&
      qcUnifyProps.operateType != OperationTypeEnum.CUSTOME
    ) {
      title += '--' + qcUnifyProps.operateType.toString();
    }
    //操作的数据名称
    if (qcUnifyProps.dataTitle != undefined && qcUnifyProps.dataTitle != null) {
      title += '(当前操作数据：' + qcUnifyProps.dataTitle + ')';
    }
  }
  return title;
}
