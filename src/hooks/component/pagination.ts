import { reactive } from 'vue';
const pagination = reactive({
  // 表格分页的配置
  current: 1,
  pageSize: 10,
  showSizeChanger: true, // 用于控制展示每页多少条的下拉
  showQuickJumper: true,
  total: 0,
  pageSizeOptions: ['5', '10', '20', '50', '100', '200'],
  showTotal: (total) => `共 ${total} 条`,
  onShowSizeChange: pageSizeChange,
  onChange: pageChange,
});

// 页数改变的方法

function pageSizeChange(val, pageNum) {
  pagination.pageSize = pageNum; // 修改每页显示的条数
  pagination.current = 1;
}
// 点击上一页下一页的方法
function pageChange(page, val) {
  // console.log(page, val);
  pagination.current = page;
}

export default pagination;
