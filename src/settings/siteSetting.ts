// 渠成科技web前端项目代码URL
export const GITEE_WEB_URL = 'https://gitee.com/luobihao/qc-web/';

// 渠成科技开源地址，后继放文档
export const DOC_URL = 'https://gitee.com/luobihao/';

// 渠成科技官网URL
export const SITE_URL = 'https://www.ynqckj.cn/';
