// 与图表公用相关的属性，与具体使用的图标组件、类型(折线图/柱状图)无关

/**
 * 图表Y轴属性
 */
export interface ChartYAxisProps {
    id: String,//Y轴的ID，可以为数据对应的属性名；如果指定id相同表示多个序列共用共一个Y轴坐标系
    field: String,//Y轴对应的字段名（数据库表列名）
    name: String,//Y轴的名称
    unit?: String,//Y轴的单位，用于显示Y轴名称和序列数据值显示时追加在数据值后面
    type: String,//Y轴的类型，数值、类目、时间、对数
    min?: number | String,//最小值
    max?: number | String,//最大值
    minInterval?: number | String,//最小间隔值
    position: String,//左右位置
    inverse: boolean,//上下位置，是否反转
    seriesType: String,//数据序列的类型，
    //是否需要直接指定使用Y轴的数据序列对应的type，确定数据序列是使用折线图还是柱状图显示
    offset?: number,//Y轴位置的偏移量
    format?: String,//数值格式化
    show: boolean,//初始状态下是否显示，默认为true表示显示
    native?: object,//序列显示的原生属性，如使用echarts可以使用series的原生属性
}

/**
 * 与时间序列图表组件相关的属性定义，用于父组件调用时间序列数据图表组件传值使用
 * 时间序列图表可以显示1个或多个对象的1组或多组时序数据，使用同一个X轴显示时间，Y轴根据传入数据确定几个Y轴
 */
export interface TimeSeriesChartProps {
    title?: String,//图表的标题文字
    minTime?: Date,//X轴的最小和最大时间
    maxTime?: Date,
    timeFormat?: String,//时间格式化字符串
    objects: String[],//对象名称数组
    yaxis: ChartYAxisProps[],//Y轴序列，定义每个Y轴的名称、左右位置、是否反向、最小、最大、分割间隔、折线图/柱状图?
    series: TimeSeriesProps[],//数据序列，定义每个数据序列的对应对象名称序号、Y轴序号、时间和值集合、折线图/柱状图?
}

/**
 * 时间序列属性
 */
export interface TimeSeriesProps {
    type: String,//折线图/柱状图
    name?: String,//序列名称，为空时自动根据对象名称和Y轴名称生成
    objectIndex: number,//对应的对象index
    yAxisIndex: number,//使用的 y 轴的 index
    data: [Date, number][],//时间和值集合
    native?: object,//序列显示的原生属性，如使用echarts可以使用series的原生属性
}