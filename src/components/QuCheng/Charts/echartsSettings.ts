import { right } from "inquirer/lib/utils/readline";

// ECharts图表相关的统一设置：如背景色、线条颜色、前景色
const chartBackGroundColor = '#0f375f'; //图表背景色
const textColor = '#ccc'; //标题、轴刻度文字颜色
const lineColor = '#ccc'; //x、y轴分割线颜色

/**
 * 与ECharts图表全局相关的配置项
 */
export interface EChartSetting {
  color: any[]; //数据序列颜色列表
  backgroundColor: String; //背景色
  title: TitleSetting;
  legend: LegendSetting;
  tooltip: TooltipSetting;
  xAxis: XAxisSetting;
  yAxis: YAxisSetting;
}

/**
 * 与图表标题相关的配置项
 */
export interface TitleSetting {
  show: Boolean;
  text: String;
}

/**
 * 与图表Tooltip相关的配置项
 */
export interface TooltipSetting {
  trigger: String;
  axisPointer: any;
}

/**
 * 与图表 Legend相关的配置项
 */
export interface LegendSetting {
  textStyle: any;
}

/**
 * 与图表X轴相关的配置项
 */
export interface XAxisSetting {
  axisLine: any;
}

/**
 * 与图表Y轴相关的配置项
 */
export interface YAxisSetting {
  splitLine: any;
  axisLine: any;
  scale: boolean;
  alignTicks: boolean;
}

export interface DataZoomSetting {
  // dataZoom: any,
  type: string;
}

// 图表配置项
export interface gridSetting {
  // grid 组件离容器左侧、右侧的距离。
  left: string;
  right: string;
}

/**
 * 与图表工具栏相关的配置项
 */
export interface ToolboxSetting {
  show: boolean;
  feature: any;
}

export const title: TitleSetting = {
  show: true,
  textStyle: {
    color: '#fff',
  }
};

export const tooltip: TooltipSetting = {
  trigger: 'axis',
  axisPointer: {
    type: 'cross',
    label: {
      show: false,
      backgroundColor: '#333',
    },
  },
};

export const legend: LegendSetting = {
  top: '7%',
  textStyle: {
    color: textColor,
  },
};

export const xAxis: XAxisSetting = {
  axisLine: {
    lineStyle: {
      color: lineColor,
    },
  },
};

export const yAxis: YAxisSetting = {
  scale: true,
  //在多个 y 轴为数值轴的时候，可以开启该配置项自动对齐刻度。只对'value'和'log'类型的轴有效。
  //建议在有多个Y轴时设置为false，否则会造成要对齐Y轴分隔导致的Y轴最值不是设置的值出现图表数据显示超出Y轴的最值范围
  alignTicks: true,
  // 坐标轴在 grid 区域中的分隔线。
  splitLine: {
    show: false,
    lineStyle: {
      opacity: 0.2,
    },
  },
  // 坐标轴线
  axisLine: {
    // show: true,
    lineStyle: {
      color: lineColor,
    },
    onZero: false,//Y轴相对于默认位置的偏移offset需要将yAxis.axisLine.onZero设为false
  },
};

export const toolbox: ToolboxSetting = {
  show: true,
  orient: 'horizontal',
  feature: {
    // dataZoom: {
    //   // yAxisIndex: 'none',
    //   show: true,
    //   filterMode: 'filter',
    // },
    dataView: {
      readOnly: false,
    },
    // magicType: {
    //     type: ["line", "bar"]
    // },
    restore: {},
    saveAsImage: {},
  },
};

export const dataZoom: DataZoomSetting = {
  type: 'slider',
};

export const grid: gridSetting = {
  left: '5%',
  right: '5%',
  top: '13%'
};

//js的顺序按文件中的定义顺序，必须要放在其他变量初始化赋值之后才能使用
export const options: EChartSetting = {
  backgroundColor: chartBackGroundColor,
  dataZoom: dataZoom,
  grid: grid,
  legend: legend,
  title: title,
  toolbox: toolbox,
  tooltip: tooltip,
  xAxis: xAxis,
  yAxis: yAxis,
};
