export enum ResponseStatusEnum {
  /***
     *成功
     */
  SUCCESS = 0;
  /***
   *警告提示
   */
  WARN = 1;//输入不正确或其他提示信息，预期内的
  /***
   *错误
   */
  ERROR = 2;//错误或异常，非预期的
  /***
   *权限不足
   */
  AUTH_PERMISSION_DENIED = 5001;
  /***
   *无效令牌
   */
  AUTH_INVALID_TOKEN = 5002;
  /***
   *无访问令牌
   */
  AUTH_NO_TOKEN = 5003;
  /***
   *令牌已过期
   */
  AUTH_EXPIRED_TOKEN = 5004;
  /***
   *未授权访问
   */
  AUTH_AUTHENTICATION_FAILED = 5005;
  /***
   *用户名或密码错误
   */
  AUTH_LOGIN_FAIL = 5006;
  /***
   *注销成功
   */
  AUTH_LOGOUT = 5007;
  /***
   *不支持的认证方式
   */
  AUTH_UN_SUPPORT_GRANT_TYPE = 5008;
  /***
   *非法客户端
   */
  AUTH_INVALID_CLIENT = 5009;
};
