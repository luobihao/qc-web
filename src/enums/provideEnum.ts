// 父子组件使用provide和inject进行属性传递是定义的key，在此文件中统一定义，避免出现重复

/**
 * 文件上传的属性KEY
 */
export const FILE_UPLOAD_PROPS_KEY = 'FILE_UPLOAD_PROPS';
/**
 * 文件上传的文件列表KEY
 */
export const FILE_UPLOAD_FILES_KEY = 'FILE_UPLOAD_FILES_KEY';