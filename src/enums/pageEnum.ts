export enum PageEnum {
  // basic login path
  BASE_LOGIN = '/login',
  HOME = '/home/index',
  // basic home path
  // BASE_HOME = '/dashboard',
  //元谋水务设置默认跳转到大屏首页
  // BASE_HOME = '/datav',
  //测试跳转至测试页面
  // BASE_HOME = '/demo',
  BASE_HOME = '/cms',
  // error page path
  ERROR_PAGE = '/exception',
  // error log page path
  ERROR_LOG_PAGE = '/error-log/list',
}
