//***********与整个应用有关的缓存，以APP开头********
//包含语言、主题等
//主题样式，不要修改定义Key值，在index.html中有直接使用值
export const APP_DARK_MODE_KEY_ = '_APP_DARK_MODE_';
//多国语言
export const APP_LOCALE_KEY = '_APP_LOCALE_';
//当前项目的综合配置，包含缓存类型、权限控制模式、主题颜色、头部设置、菜单设置等
export const APP_PROJ_CFG_KEY = '_APP_PROJ_CFG_KEY_';
//多tab页配置
export const APP_MULTIPLE_TABS_KEY = '_APP_MULTIPLE_TABS_KEY_';
// base global local key
export const APP_LOCAL_CACHE_KEY = '_APP_COMMON_LOCAL_KEY_';
// base global session key
export const APP_SESSION_CACHE_KEY = '_APP_COMMON_SESSION_KEY_';
//最后打开菜单的路由，在用户登录后跳转路由为根路由（斜线/）时跳转到最后打开菜单的路由地址
export const APP_MENU_ROUTE_KEY = '_APP_MENU_ROUTE_KEY_';
//***********与整个应用有关的缓存



//***********与用户访问控制有关的缓存，以UAC开头****
//包含token、用户信息、当前项目编码等
// token key
export const UAC_TOKEN_KEY = 'UAC_TOKEN_';
//渠成当前用户可用的项目列表store中的key
export const UAC_USER_VALID_PROJECTS_KEY = 'UAC_USER_VALID_PROJECTS_';
//渠成当前用户当前项目信息store中的key
export const UAC_USER_CURRENT_PROJECT_KEY = 'UAC_USER_CURRENT_PROJECT_';
//渠成当前用户当前项目编码key
export const UAC_USER_CURRENT_PROJECT_CODE_KEY = 'UAC_USER_CURRENT_PROJECT_CODE_';
// user info key
export const UAC_USER_INFO_KEY = 'UAC_USER_INFO_';
// role info key
export const UAC_ROLES_KEY = 'UAC_ROLES_KEY_';
// lock info
export const UAC_LOCK_INFO_KEY = 'UAC_LOCK_INFO_KEY_';
// 用户登录提示信息
export const UAC_LOGIN_PROMPT_KEY = 'UAC_LOGIN_PROMPT_';
//***********与用户访问控制有关的缓存****************



//***********与具体模块有关的缓存，以MODULE开头，由各模块定义和维护
//Cms中用于路由跳转携带信息key，用于携带跳转栏目ID、文章ID
export const MODULE_CMS_ROUTE_PREFIX_KEY = 'MODULE_CMS_ROUTER_PREFIX_';
//***********与具体模块有关的缓存****************



//缓存类型枚举，缓存是使用本地缓存还是sesion缓存
export enum CacheTypeEnum {
  SESSION,
  LOCAL,
}
