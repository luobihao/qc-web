export const SIDE_BAR_MINI_WIDTH = 48;
export const SIDE_BAR_SHOW_TIT_MINI_WIDTH = 80;

export enum ContentEnum {
  // auto width
  FULL = 'full',
  // fixed width
  FIXED = 'fixed',
}

// menu theme enum
export enum ThemeEnum {
  DARK = 'dark',
  LIGHT = 'light',
}

export enum SettingButtonPositionEnum {
  AUTO = 'auto',
  HEADER = 'header',
  FIXED = 'fixed',
}

export enum SessionTimeoutProcessingEnum {
  ROUTE_JUMP,
  PAGE_COVERAGE,
}

/**
 * 权限模式
 */
export enum PermissionModeEnum {
  // role
  ROLE = 'ROLE',
  // black
  BACK = 'BACK',
  // route mapping
  ROUTE_MAPPING = 'ROUTE_MAPPING',
}

//  Route switching animation
export enum RouterTransitionEnum {
  ZOOM_FADE = 'zoom-fade',
  ZOOM_OUT = 'zoom-out',
  FADE_SIDE = 'fade-slide',
  FADE = 'fade',
  FADE_BOTTOM = 'fade-bottom',
  FADE_SCALE = 'fade-scale',
}

/**
 * 操作类型枚举
 */
export enum OperationTypeEnum {
  UN_KNOWN = '未定义',
  QUERY = '查询',
  GET = '获取指定',
  ADD = '新增',
  EDIT = '修改',
  DELETE = '删除',
  GET_RELATION = '获取关联信息',
  SET_RELATION = '设置关联信息',
  DELETE_RELATION = '删除关联信息',
  VIEW = '查看',
  PREVIEW = '预览及操作',
  EXPORT = '导出',
  PRINT = '打印',
  LOG = '操作日志',
  CUSTOME = '自定义',
}

/**
 * 资源状态标记Flag枚举，与后台api中定义保持一致
 */
export enum ResourceStatusFlagEnum {
  NORMAL = 0,//正常状态
  DISABLE = 5,//禁用状态
  DELETED = 9,//已删除状态
}
