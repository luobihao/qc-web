export type ErrorMessageMode = 'none' | 'modal' | 'message' | undefined;

//请求的选项配置
export interface RequestOptions {
  // Splicing request parameters to url
  //是否将参数叠加到url中
  joinParamsToUrl?: boolean;
  // Format request parameter time
  formatDate?: boolean;
  // Whether to process the request result
  //是否对响应结果进行转换，默认为true，只返回响应中的data
  isTransformResponse?: boolean;
  // Whether to return native response headers
  // For example: use this attribute when you need to get the response headers
  //是否直接返回原始响应信息;是否返回原生响应头 比如：需要获取响应头时使用该属性；默认为false
  //原生响应信息中包含：config、data、headers、request、status、statusText
  isReturnNativeResponse?: boolean;
  //是否响应结构为渠成后台网关统一格式，包含code、msg和data；默认为true
  //如果为true，由封装请求中统一处理code和msg后只返回data；如果为false，不进行判断直接返回
  isQuChengGatewayUnifyFormat?: boolean;
  // Whether to join url
  joinPrefix?: boolean;
  // Interface address, use the default apiUrl if you leave it blank
  //api地址的名称，与.env中VITE_PROXY别名配置一致
  apiUrl?: string;
  // URL前缀
  urlPrefix?: string;
  // Error message prompt type
  errorMessageMode?: ErrorMessageMode;
  // Whether to add a timestamp
  joinTime?: boolean;
  ignoreCancelToken?: boolean;
  // Whether to send token in header
  withToken?: boolean;
}

//原框架中定义的HTTP Result
// export interface Result<T = any> {
//   code: number;
//   type: 'success' | 'error' | 'warning';
//   message: string;
//   result: T;
// }
//修改为渠成科技自己定义的Result
export interface Result<T = any> {
  code: number;
  msg: string;
  data: T;
}

// multipart/form-data: upload file
export interface UploadFileParams {
  // Other parameters
  data?: Recordable;
  // File parameter interface field name
  name?: string;
  // file name
  file: File | Blob;
  // file name
  filename?: string;
  [key: string]: any;
}
