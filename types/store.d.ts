import { ErrorTypeEnum } from '/@/enums/exceptionEnum';
import { MenuModeEnum, MenuTypeEnum } from '/@/enums/menuEnum';
import { RoleInfo } from '../src/api/platform/model/userModel';

// Lock screen information
export interface LockInfo {
  // Password required
  pwd?: string | undefined;
  // Is it locked?
  isLock?: boolean;
}

// Error-log information
export interface ErrorLogInfo {
  // Type of error
  type: ErrorTypeEnum;
  // Error file
  file: string;
  // Error name
  name?: string;
  // Error message
  message: string;
  // Error stack
  stack?: string;
  // Error detail
  detail: string;
  // Error url
  url: string;
  // Error time
  time?: string;
}

/*
 * 用户信息，在用户登录成功后存储在本地存储中，方便前端使用
 */
export interface UserInfo {
  //为保证用户ID不被泄露，前端不记录用户ID，后台接口也不会返回用户ID
  // userId: string | number;
  username: string;
  realName: string;
  avatar: string;
  sign: string;
  desc?: string;
  homePath?: string;
  roles: RoleInfo[];
}

export interface BeforeMiniState {
  menuCollapsed?: boolean;
  menuSplit?: boolean;
  menuMode?: MenuModeEnum;
  menuType?: MenuTypeEnum;
}

/*
 * CMS中路由跳转携带参数定义
 */
export interface CmsRouteParam {
  isArticle: boolean = false; //指示跳转目标是栏目还是文章
  id: number; //栏目或文章ID
  data: any; //栏目或文章信息
  keywords: string; //搜索关键字
}
