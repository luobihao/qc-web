import { OperationTypeEnum } from '/@/enums/appEnum';

/**
 * 渠成统一定义--在前端中传递给子级（对应界面中打开编辑或设置界面）的数据
 */
export interface QcUnifyTransToChildProps {
  /**
   * 模块名称--用于在modal或table等界面中显示
   */
  moduleName: string;
  /**
   * 操作类型枚举
   */
  operateType: OperationTypeEnum;
  /**
   * 操作类型字符串，用于自定义描述当前操作
   */
  operateString?: string;
  /**
   * 操作的数据ID
   */
  dataId?: string | number | any;
  /**
   * 操作的数据标题，用于显示
   */
  dataTitle?: string;
  /**
   * 操作的模型对象
   */
  model?: Recordable;
}

export interface QcUnifyTransToChildPropsExtend extends QcUnifyTransToChildProps {
  text?: string;
}
