module.exports = {
  printWidth: 100,
  semi: true, //在结尾添加分号
  vueIndentScriptAndStyle: true,
  singleQuote: true, // 单引号不变双引号
  trailingComma: 'all', //在对象后添加逗号
  proseWrap: 'never',
  htmlWhitespaceSensitivity: 'strict',
  endOfLine: 'auto',
};
